//
//  AppDelegate.swift
//  FansKick
//
//  Created by FansKick Dev on 11/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

// API keys can be generated at <http://developer.indooratlas.com/applications>
let kAPIKey = "263a823a-6a50-41da-a769-7938e5cd74f1"
let kAPISecret = "ymwfFuKHr/WkKfEJwOZiga9K5Js2YmECnCRJmJQMwFbBO2F+Cnq4HblfVxZ1cohjTkjdSZ1fvvz2QILxzQtgD8LmJLxOoaATdXmvhrXYIMwRqe9bvOU4C/bGAfCaTw=="

import UIKit
import UserNotifications
import Google
import GoogleSignIn
import Fabric
import Crashlytics
import IndoorAtlas
import FBSDKLoginKit
import SDWebImage

import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging

let PLAYER_ONLINE_NOTIFICATION = "PLAYER_ONLINE_NOTIFICATION"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate ,FIRMessagingDelegate{

	
    
    var window: UIWindow?
    var navigationController: UINavigationController?
    public var isReachable = false
    var imageView = UIImageView()
    var imageViewBackSide = UIImageView()
    var isPortraitOnly = true
    var application: UIApplication?
   // @objc var currentUnityController: UnityAppController!
    var isUnityRunning = false
    var isUnityInitialized = false
    var isUnityFirstTimeLaunch = true
    
    var cartCount = "0"
    var notificationID = ""
    var timer:Timer? = nil
    var viewForSplash = UIView()
    var images = [UIImage]()
    var launchApp: [UIApplicationLaunchOptionsKey: Any]?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
			
			SDImageCache.shared().clearMemory()

        setupReachability()
        Fabric.with([Crashlytics.self])
        
        let splashGiff = UIImage.gifImageWithName("splash_1")
        imageView = UIImageView(image: splashGiff)
        imageView.frame = UIScreen.main.bounds
        self.window?.rootViewController?.view.addSubview(imageView)
        
        Timer.scheduledTimer(withTimeInterval: TimeInterval(2.0), repeats: false) { (time) in
            
            DispatchQueue.main.async {
                self.imageView.image = UIImage.init(named: "splash28")
            }
            Timer.scheduledTimer(withTimeInterval: TimeInterval(2.0), repeats: false) { (time) in
                
                DispatchQueue.main.async {
                    self.imageView.removeFromSuperview()
                    self.imageView.image = nil
                }
            }
        }
			

			
        var accessToken = ""
        if defaults.value(forKey: kOAuthToken) != nil {
            accessToken = defaults.value(forKey: kOAuthToken) as! String
        } else {
            defaults.setValue("" , forKey: "access_token")
        }
        if accessToken == "" {
            self.perform(#selector(navigateToLogin), with: nil, afterDelay: 4.0)
            
        } else {
            self.perform(#selector(navigateToTabController), with: nil, afterDelay: 4.0)
        }
        
        self.application = application
        self.launchApp = launchOptions
//        unity_init(CommandLine.argc, CommandLine.unsafeArgv)
//        currentUnityController = UnityAppController()
//        currentUnityController.application(application, didFinishLaunchingWithOptions: launchOptions)
        
       // allocateAndInitializeUnityAppController()
        self.changeOrientation(toPortrait:true)
        
        self.perform(#selector(makeRotation), with: nil, afterDelay: 4.5)
        
        authenticateIALocationManager()
//        registerForRemoteNotifications(application)
			

			
//		  	// register for notification
//			  requestNotificationAuthorization(application: application)
//				// Configure firebase.
//				FIRApp.configure()
			
				requestNotificationAuthorization(application: application)

				FIRApp.configure()
			NotificationCenter.default.addObserver(self, selector: #selector(tokenRefreshNotification), name: NSNotification.Name.firInstanceIDTokenRefresh, object: nil)

			
        return true
    }
	

	@objc func tokenRefreshNotification(notification: NSNotification) {
	
		if let token = FIRInstanceID.instanceID().token(){
			defaults.setValue(token, forKey: pDeviceToken)
			webApiMethodToRegisterPushToken()
		}
	
	}
	
//    func allocateAndInitializeUnityAppController() {
//
//        unity_init(CommandLine.argc, CommandLine.unsafeArgv)
//        currentUnityController = UnityAppController()
//        currentUnityController.application(self.application!, didFinishLaunchingWithOptions: self.launchApp)
//    }
	
    @objc func makeRotation() {
        // self.changeOrientation(toPortrait:true)
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    

	
	
	
    func authenticateIALocationManager() {
        
        // Get IALocationManager shared instance
        let manager = IALocationManager.sharedInstance()
        
        // Set IndoorAtlas API key and secret
        manager.setApiKey(kAPIKey, andSecret: kAPISecret)
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url,
                                                 sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                 annotation: options[UIApplicationOpenURLOptionsKey.annotation]) || FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        
    }
    
    // [END disconnect_handler]
    //MARK:- Private functions
    
    fileprivate func setupReachability() {
        // Allocate a reachability object
        let reach = Reachability.forInternetConnection()
        self.isReachable = reach!.isReachable()
        
        // Set the blocks
        reach?.reachableBlock = { (reachability) in
            
            DispatchQueue.main.async(execute: {
                self.isReachable = true
            })
        }
        reach?.unreachableBlock = { (reachability) in
            DispatchQueue.main.async(execute: {
                self.isReachable = false
            })
        }
        reach?.startNotifier()
    }
    
    class func Device() -> Dictionary<String,Any> {
        
        var deviceToken = ""
        if defaults.value(forKey: pDeviceToken) != nil {
            deviceToken = defaults.value(forKey: pDeviceToken) as! String
        } else {
            deviceToken = "fsd79s7f89789v789x787xcv98"
        }
        
        let device  = [pDevice_Type : "iOS",
                       pDeviceToken : deviceToken]
        
        return device
    }
    
    @objc func navigateToLoginafterDelay() {
        self.perform(#selector(navigateToLogin), with: nil, afterDelay: 3.0)
        
    }
    
    // This method use to set tabbar as root controller
    @objc func navigateToTabController() {
        
        let storyboard = UIStoryboard(name: "TabController", bundle: nil)
        let dashboardVC = storyboard.instantiateViewController(withIdentifier: "TabbarNavigation") as! UITabBarController
        dashboardVC.selectedIndex = 0
        
        self.window?.rootViewController = dashboardVC;
    }
    
    //MARK:-  This method use to navigate to login screen
    @objc func navigateToLogin() {
        let storyboard = UIStoryboard.init(name: "Auth", bundle: nil)
        self.window?.rootViewController = storyboard.instantiateViewController(withIdentifier: "navController")
        defaults.setValue("" , forKey: "access_token")
    }
    
    // use to setup navigation bar
    
    func setupNavigationBar(navigationBar:UINavigationController) {
        
        navigationBar.navigationBar.setBackgroundImage(UIImage.init(named: "navBar"), for: .default)
        navigationBar.navigationBar.tintColor = UIColor.white
        navigationBar.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white,NSAttributedStringKey.font:UIFont.init(name: "Gilroy-ExtraBold", size: 15) as Any]
    }
    
    @objc func navigateToLive(player:FKPlayerInfo?){
        
        // if socket connected then user already on live screen
        if FKPWebSocketHelper.sharedInstance.isConnected{
            return
        }
        
        if self.window?.rootViewController is UITabBarController {
            
            let stoaryboard = UIStoryboard.init(name: "BuddySection", bundle: nil)
            let  liveVideoVC = stoaryboard.instantiateViewController(withIdentifier: "liveVideoVC") as! FKLiveVideoVC
            liveVideoVC.player = player
            liveVideoVC.isFromNotification = true
            self.window?.rootViewController?.present(liveVideoVC, animated: true) {
                
            }
        }
    }
    
    
    func navigateToOrder(order: FKMyOrdersInfo?) {
        
        if self.window?.rootViewController is UITabBarController {
            let tabBarController = self.window?.rootViewController as! UITabBarController
            
            let navController =  tabBarController.viewControllers![tabBarController.selectedIndex] as! UINavigationController
            
            let controller =   navController.viewControllers.last
            if controller is FKOrderDetailVC {
                return
            } else {
                let stoaryboard = UIStoryboard.init(name: "Merchandise", bundle: nil)
                let orderDetailVC = stoaryboard.instantiateViewController(withIdentifier: "FKOrderDetailVC") as! FKOrderDetailVC
                orderDetailVC.isNavigateFromNotification = true
                orderDetailVC.orderDetail = order
                navController.pushViewController(orderDetailVC, animated: true)
            }
            print(navController.viewControllers)
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        //        if RemoteNotificationHandler.isPushNotificationEnabled == true {
        //            RemoteNotificationHandler.registerForRemoteNotification()
        //            self.webApiMethodToRegisterPushToken()
        //        }

    }
	

	
	
	
	
	
	
	
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler(UNNotificationPresentationOptions.alert)
    }
    
//    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        var deviceTokenString = ""
//        for i in 0..<deviceToken.count {
//            deviceTokenString = deviceTokenString + String(format: "%02.2hhx", arguments: [deviceToken[i]])
//        }
//        print("<<<< token: ",deviceTokenString," >>>>")
//        defaults.setValue(deviceTokenString, forKey: pDeviceToken)
//
//        webApiMethodToRegisterPushToken()
//    }
	
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        
			completionHandler(UIBackgroundFetchResult.newData)
			self.checkNotificationType(params: userInfo as! Dictionary<String, Any>, application: application)
			
			print("wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww",application.applicationState.rawValue)
			if self.window?.rootViewController is UITabBarController {
				let tabBarController = self.window?.rootViewController as! UITabBarController
				
				let navController = tabBarController.viewControllers![tabBarController.selectedIndex] as! UINavigationController
				
				let controller = navController.viewControllers.last
				if (controller is FKBuddyDetailVC) || (controller is FKBuddyVC) {
					NotificationCenter.default.post(name: NSNotification.Name(rawValue:PLAYER_ONLINE_NOTIFICATION ), object: userInfo as! Dictionary<String, Any>)
				}
			}

    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
	
    private func webApiMethodToRegisterPushToken() {
        
        var params = [String: Any]()
        params = ["device" : AppDelegate.Device()]
        ServiceHelper.request(params: params, method: .post, apiName: "users/sync_device", completionBlock: nil)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        application.applicationIconBadgeNumber = 0
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
//    func startUnity() {
//        if !isUnityRunning {
//            isUnityRunning = true
//            currentUnityController!.applicationDidBecomeActive(application!)
//        }
//    }
//
//    func stopUnity() {
//        if isUnityRunning {
//            currentUnityController!.applicationWillResignActive(application!)
//            isUnityRunning = false
//        }
//    }
	
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        
        if isPortraitOnly {
            return UIInterfaceOrientationMask.portrait
        } else {
            return UIInterfaceOrientationMask.landscape
        }
    }
    
    func changeOrientation(toPortrait:Bool) {
        
        print("changeOrientation ......",UIDevice.current.orientation.rawValue)
        self.isPortraitOnly = toPortrait
        if toPortrait {
            if UIDevice.current.orientation.rawValue == UIInterfaceOrientation.portrait.rawValue  {
                
                let value = UIInterfaceOrientation.portraitUpsideDown.rawValue
                UIDevice.current.setValue(value, forKey: "orientation")
            } else {
                let value = UIInterfaceOrientation.portrait.rawValue
                UIDevice.current.setValue(value, forKey: "orientation")
            }
        } else {
            if UIDevice.current.orientation.rawValue == UIInterfaceOrientation.landscapeRight.rawValue  {
                
                let value = UIInterfaceOrientation.landscapeLeft.rawValue
                UIDevice.current.setValue(value, forKey: "orientation")
            } else {
                let value = UIInterfaceOrientation.landscapeRight.rawValue
                UIDevice.current.setValue(value, forKey: "orientation")
            }
        }
        
        print("changeOrientation111111 ......",UIDevice.current.orientation.rawValue)
        
    }
    
    func checkNotificationType(params:Dictionary<String, Any>, application:UIApplication) {
        
        if application.applicationState == .background {
            return
        } else if application.applicationState == .inactive {
            
            self.perform(#selector(manageNotificationNavigation), with: params, afterDelay: 4.2)
            // manageNotificationNavigation(params: params)
        } else if application.applicationState == .active {
            
            let notificationType = params["notification_type"] as! String
            if  notificationType == "PlayerLive"{
                
                // Navigation to player Live
                
                let playerID = params["player_id"] as! String
                if notificationID == playerID
                {
                    notificationID = ""
                    manageNotificationNavigation(params: params)
                }
                notificationID = playerID
                if timer != nil{
                    timer?.invalidate()
                    timer = nil
                }
                // use to clear the notification id if user does not tap on notification
                timer =  Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: { (timer ) in
                    self.notificationID = ""
                })
                
            } else if (notificationType == "OrderPlaced" || notificationType == "OrderConfirmation" || notificationType == "Dispached"  || notificationType == "Cancelled" || notificationType == "Delivered" || notificationType == "Packed" || notificationType == "OrderConfirmation") {
                
                let orderID = params["order_id"] as! String
                
                if notificationID == orderID {
                    notificationID = ""
                    manageNotificationNavigation(params: params)
                }
                notificationID = orderID
                if timer != nil {
                    timer?.invalidate()
                    timer = nil
                }
                // use to clear the notification id if user does not tap on notification
                timer =  Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: { (timer ) in
                    self.notificationID = ""
                })
            }
        }
    }
    
    @objc func manageNotificationNavigation(params:Dictionary<String, Any>){
			
			print("Params notification:  \(params) ")
			
        let notificationType = params["notification_type"] as! String
        if  notificationType == "PlayerLive"{
            
            // Navigation to player Live
            
            let playerID = params["player_id"] as! String
            let player = FKPlayerInfo()
            player.id = playerID
            
            self.navigateToLive(player: player)
            
        } else if (notificationType == "OrderPlaced" || notificationType == "OrderConfirmation" || notificationType == "Dispatched"  || notificationType == "Cancelled" || notificationType == "Delivered" || notificationType == "Packed" || notificationType == "OrderConfirmation") {
            
            //OrderConfirmation Dispached  Cancelled  Delivered Packed
            let orderID = params["order_id"] as! String
            
            let order =  FKMyOrdersInfo()
            order.id = orderID
            navigateToOrder(order: order)
        }
    }
    
    func applicationDidReceiveMemoryWarning(_ application: UIApplication) {
        
        // use to remove thumbnail from memory if memory warnig received
        FKPlayerHelper.shared.removeThumbnail()
        SDImageCache.shared().clearMemory()
    }
	
	// FCM Methods.
	
	//MARK:- Notification Auth method
	func requestNotificationAuthorization(application: UIApplication) {
		
		if #available(iOS 10.0, *) {
			// For iOS 10 display notification (sent via APNS)
			UNUserNotificationCenter.current().delegate = self
			let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
			UNUserNotificationCenter.current().requestAuthorization(
				options: authOptions,
				completionHandler: {_, _ in })
			// For iOS 10 data message (sent via FCM
			
			
		} else {
			let settings: UIUserNotificationSettings =
				UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
			application.registerUserNotificationSettings(settings)
		}
		
		application.registerForRemoteNotifications()
//		FIRMessaging.messaging().remoteMessageDelegate = self
//		FIRMessaging.messaging().shouldEstablishDirectChannel = true
	}
	
	
	// NOTE: Need to use this when swizzling is disabled
	public func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
		var deviceTokenString = ""
		        for i in 0..<deviceToken.count {
		            deviceTokenString = deviceTokenString + String(format: "%02.2hhx", arguments: [deviceToken[i]])
		        }
		        print("<<<< token: ",deviceTokenString," >>>>")
		  defaults.setValue(deviceTokenString, forKey: pDeviceToken)
		
	//	FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.sandbox)
	//	FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.prod)
		FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.unknown)
		
		  //defaults.setValue(FIRInstanceID.instanceID().token(), forKey: pDeviceToken)
		
		if let token = FIRInstanceID.instanceID().token(){
			defaults.setValue(token, forKey: pDeviceToken)
			webApiMethodToRegisterPushToken()
		}

	}


	
	// The callback to handle data message received via FCM for devices running iOS 10 or above.
	func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
		print(remoteMessage.appData)
	}
	
	
}

