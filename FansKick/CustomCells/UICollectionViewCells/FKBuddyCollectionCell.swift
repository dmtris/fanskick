//
//  FKBuddyCollectionCell.swift
//  FansKick
//
//  Created by FansKick-Sunil on 21/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKBuddyCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView:UIImageView?
    @IBOutlet weak var channelBtn:IndexPathButton?
    @IBOutlet weak var nameLbl:UILabel?
    @IBOutlet weak var statusLbl:UILabel?

    @IBOutlet weak var rattingBar: AARatingBar?

}
