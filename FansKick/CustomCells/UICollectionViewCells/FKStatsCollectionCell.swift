//
//  FKStatsCollectionCell.swift
//  FansKick
//
//  Created by FansKick-Sunil on 24/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKStatsCollectionCell: UICollectionViewCell {
    @IBOutlet var imgView:UIImageView?
    @IBOutlet var numberLbl:UILabel?
    @IBOutlet var numberDesc:UILabel?

}
