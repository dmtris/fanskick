//
//  FKProductCell.swift
//  FansKick
//
//  Created by Sunil Verma on 11/11/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKProductCell: UICollectionViewCell {
    
    @IBOutlet weak var productNameLbl:UILabel?
    @IBOutlet weak var productPriceLbl:UILabel?
    @IBOutlet weak var imgView:UIImageView?
    @IBOutlet weak var shareBtn:IndexPathButton?
    @IBOutlet weak var likeBtn:IndexPathButton?
    @IBOutlet weak var discountedPriceLbl: UILabel!
}
