//
//  FKLineUpCell.swift
//  FansKick
//
//  Created by Sunil Verma on 07/12/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit


class FKLineUpCell: UICollectionViewCell {
    
    @IBOutlet var formationView:UIView!
    @IBOutlet var formationImageView:UIImageView!
    @IBOutlet var clubLogo:UIImageView!
    @IBOutlet var headerbg:UIImageView!
    @IBOutlet var clubName:UILabel!
    @IBOutlet var formationLbl:UILabel!
    
    var lineupInfo: FKLineUpFormationInfo?
    
    func homeFormation(){
        self.perform(#selector(homeFormationSetUp), with: nil, afterDelay: 0.1)
    }
    @objc func homeFormationSetUp(){
        
        // use to remove all subviews
        self.removeAllFormationObj()
        
        self.formationImageView.image = UIImage.init(named: "lineUpBg")
        self.headerbg.image = UIImage.init(named: "lineupheaderbg")
        self.formationLbl.text = lineupInfo?.homeFormation
        if (self.lineupInfo?.homeFormation.isEmpty)!{
            return
        }
        //  let lineFormationArray  =    self.lineupInfo?.homeFormation.components(separatedBy: CharacterSet.init(charactersIn: "-"))
        
        let array = FKLineUpGroupInfo.group(data: (self.lineupInfo?.homePlayer)!)
        
        let items = array.filter{ $0.groupName.contains("GK")}
        //GK
        
        if  items.count > 0{
            let group = items.first
            //     self.formation(xCount: 1, yValue: 20.0,data: (self.lineupInfo?.homePlayer)!)
            if DeviceType.IS_IPAD || DeviceType.IS_IPAD_PRO{
                self.formation(xCount: 1, yValue: 80.0,data: (group?.playes)!)
            }else{
                self.formation(xCount: 1, yValue: 20.0,data: (group?.playes)!)
            }
        }
        
        var groups = [FKLineUpGroupInfo]()
        for item  in array {
            if item.groupName.uppercased() != "GK" {
                groups.append(item)
            }
        }
        
        let verticalSpace = Float(self.formationView.frame.height) / (Float((groups.count)) + 1.0)
        var height = Float(0.0)
        if DeviceType.IS_IPAD || DeviceType.IS_IPAD_PRO{
            height = Float(30.0)
        }
        for item in groups{
            height = height + verticalSpace
            self.formation(xCount: Int(item.playes.count), yValue: height, data: (item.playes))
        }
    }
    
    func awayFormation(){
        self.perform(#selector(awayFormationSetUp), with: nil, afterDelay: 0.1)
    }
    @objc func awayFormationSetUp(){
        self.removeAllFormationObj()
        self.formationImageView.image = UIImage.init(named: "lineUpBg")
        self.headerbg.image = UIImage.init(named: "lineupheaderbg")
        self.formationLbl.text = lineupInfo?.awayFormation
        if (self.lineupInfo?.awayFormation.isEmpty)!{
            return
        }
        //  let lineFormationArray  =    self.lineupInfo?.awayFormation.components(separatedBy: CharacterSet.init(charactersIn: "-"))
        
        let array = FKLineUpGroupInfo.group(data: (self.lineupInfo?.awayPlayer)!)
        var groups = [FKLineUpGroupInfo]()
        for item  in array {
            if item.groupName.uppercased() != "GK" {
                groups.append(item)
            }
        }
        let verticalSpace = Float(self.formationView.frame.height) / (Float((groups.count)) + 1.0)
        var height = Float(30.0)
        if DeviceType.IS_IPAD || DeviceType.IS_IPAD_PRO{
            height = Float(120.0)
        }
        
        for item in groups{
            self.formation(xCount: Int(item.playes.count), yValue: height,data: (item.playes))
            height = height + verticalSpace
        }
        
        // GK
        let viewHeight = Float(formationView.frame.height)
        
        let items = array.filter{ $0.groupName.contains("GK")}
        
        //GK
        if  items.count > 0{
            let group = items.first
            if DeviceType.IS_IPAD || DeviceType.IS_IPAD_PRO{
                self.formation(xCount: 1, yValue: (viewHeight - 150),data: (group?.playes)!)
            }else{
                self.formation(xCount: 1, yValue: (viewHeight - 60),data: (group?.playes)!)
            }
        }
    }
    
    private func removeAllFormationObj(){
        
        for view in formationView.subviews{
            if view is UIButton {
                view.removeFromSuperview()
            }
            if view is FKLayerLabel{
                view.removeFromSuperview()
            }
        }
    }
    private func formation(xCount: Int, yValue:Float, data:Array<FKPlayerInfo>){
        
        let height = Float(formationView.frame.height)
        let width = Float(formationView.frame.width)
        let xSpace = Float((height - yValue)) * Float(cos(Int(71.2).degreesToRadians))
        let betweenSpace = (width - xSpace)/Float(xCount+1)
        
        for index in 1...xCount{
            
            // This is used to handle the exception in case data is not filled properly
            if index > data.count{
                break
            }
            
            let player = data[index-1]
            
            
            var zigzagHeight = 10.0 as Float
            let btn = UIButton.init(type: UIButtonType.custom)
            btn.isUserInteractionEnabled = false
            btn.imageView?.contentMode = .scaleAspectFit
            if DeviceType.IS_IPAD || DeviceType.IS_IPAD_PRO{
                zigzagHeight = 20.0
                btn.frame = CGRect.init(x: 0, y: 0, width: 80, height: 80)
            }else{
                btn.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
                
            }
            btn.backgroundColor = UIColor.clear
            var yPotion = yValue
            if xCount > 3{
                if index % 2 == 0{
                    yPotion = yPotion + zigzagHeight
                }else{
                    yPotion = yPotion - zigzagHeight
                }
            }
            
            btn.normalLoad(player.profileImage)
            let xValue =  Double(betweenSpace * Float(index) + xSpace/2.0)
            btn.center = CGPoint.init(x:xValue, y: Double(yPotion))
            btn.shadowOnView()
            formationView.addSubview(btn)
            formationView.bringSubview(toFront: btn)
            let lbl = FKLayerLabel.init()
            lbl.text = player.knowName
            
            if DeviceType.IS_IPAD || DeviceType.IS_IPAD_PRO{
                lbl.frame = CGRect.init(x: 0, y: 0, width: 80, height: 20)
                lbl.center = CGPoint.init(x:btn.center.x, y: (btn.center.y + 50.0))
                lbl.font = UIFont.systemFont(ofSize: 15.0)
            }else{
                lbl.frame = CGRect.init(x: 0, y: 0, width: 46, height: 12)
                lbl.center = CGPoint.init(x:btn.center.x, y: (btn.center.y + 20.0))
            }
            
            lbl.textAlignment = .center
            lbl.textColor = UIColor.white
            lbl.backgroundColor = UIColor.red
            lbl.lineBreakMode = .byWordWrapping
            lbl.font = UIFont.systemFont(ofSize: 10.0)
            formationView.addSubview(lbl)
            let numberLbl = FKLayerLabel.init(frame: CGRect.init(x: 0, y: 0, width: 24, height: 12))
            numberLbl.text = player.jerseyNumber
            numberLbl.center = CGPoint.init(x:btn.frame.origin.x - 13, y: (btn.center.y + 15.0))
            if DeviceType.IS_IPAD || DeviceType.IS_IPAD_PRO{
                numberLbl.frame = CGRect.init(x: 0, y: 0, width: 30, height: 20)
                numberLbl.center = CGPoint.init(x:btn.frame.origin.x - 9, y: (btn.center.y + 45.0))
                numberLbl.font = UIFont.boldSystemFont(ofSize: 14.0)
            }else{
                numberLbl.center = CGPoint.init(x:btn.frame.origin.x - 13, y: (btn.center.y + 15.0))
            }
            numberLbl.textAlignment = .center
            numberLbl.textColor = UIColor.black
            numberLbl.backgroundColor = UIColor.white
            numberLbl.font = UIFont.boldSystemFont(ofSize: 9.0)
            formationView.addSubview(numberLbl)
            
            
            //            if DeviceType.IS_IPAD || DeviceType.IS_IPAD_PRO{
            //                btn.frame = CGRect.init(x: 0, y: 0, width: 80, height: 80)
            //
            //                lbl.frame = CGRect.init(x: 0, y: 0, width: 80, height: 20)
            //                lbl.center = CGPoint.init(x:btn.center.x, y: (btn.center.y + 20.0))
            //                lbl.font = UIFont.systemFont(ofSize: 15.0)
            //
            //                numberLbl.frame = CGRect.init(x: 0, y: 0, width: 30, height: 20)
            //                numberLbl.center = CGPoint.init(x:btn.frame.origin.x - 13, y: (btn.center.y + 15.0))
            //                numberLbl.font = UIFont.boldSystemFont(ofSize: 14.0)
            //            }
        }
    }
    
}
