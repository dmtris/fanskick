//
//  FKInputTableViewCell.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 10/13/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKInputTableViewCell: UITableViewCell {

    @IBOutlet weak var textField: FKTextFieldView!
    @IBOutlet weak var downArrow: UIButton?
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
