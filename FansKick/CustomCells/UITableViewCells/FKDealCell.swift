//
//  FKDealCell.swift
//  FansKick
//
//  Created by FansKick-Sunil on 18/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKDealCell: UITableViewCell {

    @IBOutlet weak var nameLbl: UILabel?
    @IBOutlet weak var offersLbl: UILabel?
    @IBOutlet weak var descLbl: UILabel?
    @IBOutlet weak var imgView: UIImageView?
    @IBOutlet weak var redreamBtn: IndexPathButton?
    @IBOutlet weak var locationsBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
