//
//  FKLiveCell.swift
//  FansKick
//
//  Created by Sunil Verma on 10/31/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKLiveCell: UITableViewCell {

    @IBOutlet var titleLbl:UILabel?
    @IBOutlet var detailLbl:UILabel?
    @IBOutlet var imgView:UIImageView?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
