//
//  FKIndoorCell.swift
//  FansKick
//
//  Created by Sunil Verma on 21/11/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKIndoorCell: UITableViewCell {

    @IBOutlet weak var nameLbl:UILabel?
    @IBOutlet weak var locationLbl:UILabel?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
