//
//  FKOrderCell.swift
//  FansKick
//
//  Created by Sunil Verma on 22/11/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKOrderCell: UITableViewCell {
    @IBOutlet weak var orderIdLbl:UILabel?
    @IBOutlet weak var orderDateLbl:UILabel?
    @IBOutlet weak var totalPriceLbl:UILabel?
    @IBOutlet weak var orderStatusLbl:UILabel?


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
