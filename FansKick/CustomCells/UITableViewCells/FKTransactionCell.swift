//
//  FKTransactionCell.swift
//  FansKick
//
//  Created by Sunil Verma on 04/01/2018.
//  Copyright © 2018 FansKick Dev. All rights reserved.
//

import UIKit

class FKTransactionCell: UITableViewCell {

    @IBOutlet var transactionIdLbl:UILabel?
    @IBOutlet var transactionDateLbl:UILabel?
    @IBOutlet var transactionAmountLbl:UILabel?
    @IBOutlet var transactionTitleLbl:UILabel?
    @IBOutlet var transactionDescLbl:UILabel?
    @IBOutlet var transactionSourceImgView:UIImageView?
    @IBOutlet var statusLbl:UILabel?


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
