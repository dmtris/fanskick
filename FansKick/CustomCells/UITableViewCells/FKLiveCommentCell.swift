//
//  FKLiveCommentCell.swift
//  FansKick
//
//  Created by FansKick-Sunil on 23/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKLiveCommentCell: UITableViewCell {
    @IBOutlet var commentsLbl:UILabel?
    @IBOutlet var timeLbl:UILabel?
    @IBOutlet var nameLbl:UILabel?
    @IBOutlet var closeBtn:IndexPathButton?


    @IBOutlet var imgView:UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
