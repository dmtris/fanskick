//
//  FKStadiumCell.swift
//  FansKick
//
//  Created by FansKick-Sunil on 24/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKStadiumCell: UITableViewCell {

    @IBOutlet weak var imgView:UIImageView?
    @IBOutlet weak var nameLbl:UILabel?
    @IBOutlet weak var addresslbl:UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
