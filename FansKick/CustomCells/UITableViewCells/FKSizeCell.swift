//
//  FKSizeCell.swift
//  FansKick
//
//  Created by Sunil Verma on 11/11/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit


protocol SizeCellDelegate: class {
    func selectedIndex(index:Int)
}
class FKSizeCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView:UICollectionView?
    
    weak var delegate: SizeCellDelegate?
    var dataSourceArray = Array<String>()
    var selectedIndex = -1

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func reloadCellWithData(array:Array<String>) {
        
        self.dataSourceArray.removeAll()
        self.dataSourceArray.append(contentsOf: array);
        self.collectionView?.reloadData()
    }
    
    //MARK: UICollectionView Overrides
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSourceArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FKSizeCollectionCell", for: indexPath) as! FKSizeCollectionCell
        cell.titleTbl?.text = self.dataSourceArray[indexPath.row]
        
        if indexPath.row == selectedIndex {
            cell.titleTbl?.backgroundColor =  UIColor.RGB(r: 239, g: 98, b: 43, alpha: 0.5)
        } else {
            cell.titleTbl?.backgroundColor = UIColor.RGB(r: 48, g: 92, b: 44, alpha: 0.3)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedIndex = indexPath.row
        delegate?.selectedIndex(index: selectedIndex)
        self.collectionView?.reloadData()
        
        self.collectionView?.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.bottom, animated: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: 48, height: 44)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
