//
//  FKBuddyCell.swift
//  FansKick
//
//  Created by FansKick-Sunil on 21/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKBuddyCell: UITableViewCell {

    @IBOutlet weak var nameLbl:UILabel?
    @IBOutlet weak var clubNameLbl:UILabel?
    @IBOutlet weak var noOfGoalLbl:UILabel?

    @IBOutlet weak var imgView:UIImageView?
    @IBOutlet weak var clubImgView:UIImageView?
    @IBOutlet weak var rattingBar: AARatingBar?
    @IBOutlet weak var connectBtn: IndexPathButton?
    
    @IBOutlet weak var onlineLbl:UILabel?



    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
