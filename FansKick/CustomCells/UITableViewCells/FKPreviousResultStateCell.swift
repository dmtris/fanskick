//
//  FKPreviousResultStateCell.swift
//  FansKick
//
//  Created by Sunil Verma on 17/11/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKPreviousResultStateCell: UITableViewCell {

    @IBOutlet var nameLbl:UILabel?
    @IBOutlet var team1Lbl:UILabel?
    @IBOutlet var team2Lbl:UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
