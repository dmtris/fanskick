//
//  FKStoreCell.swift
//  FansKick
//
//  Created by Sunil Verma on 02/12/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKStoreCell: UITableViewCell {

    @IBOutlet weak var nameLbl:UILabel?
    @IBOutlet weak var addressLbl:UILabel?
    @IBOutlet weak var emailField:UITextView?
    @IBOutlet weak var navigationBtn:IndexPathButton?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
