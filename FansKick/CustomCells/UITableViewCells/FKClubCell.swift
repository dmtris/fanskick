//
//  FKClubCell.swift
//  FansKick
//
//  Created by Sunil Verma on 14/11/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKClubCell: UITableViewCell {

    @IBOutlet var nameLbl:UILabel?
    @IBOutlet var imgView:UIImageView?
    @IBOutlet var goalLbl:UILabel?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
