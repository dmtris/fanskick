//
//  FKDealDetailTitleDetailCell.swift
//  FansKick
//
//  Created by FansKick-Sunil on 20/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKDealDetailTitleDetailCell: UITableViewCell {
    @IBOutlet weak var titleLbl:UILabel?
    @IBOutlet weak var detailLbl:UILabel?
    @IBOutlet weak var detailTextView:UITextView?
    @IBOutlet weak var iconBtn:UIButton?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
