//
//  FKFixtureStatsCell.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 10/21/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKFixtureStatsCell: UITableViewCell {
    @IBOutlet weak  var totalMatchPlayedLbl:UILabel?
    @IBOutlet weak  var homeWinCountLbl:UILabel?
    @IBOutlet weak  var awayWinCountLbl:UILabel?
    @IBOutlet weak  var drawCountLbl:UILabel?
    @IBOutlet weak  var homeWinCountAtHomeLbl:UILabel?
    @IBOutlet weak  var awayWinCountAtHomeLbl:UILabel?
    @IBOutlet weak  var homeWinCountAtAwayLbl:UILabel?
    @IBOutlet weak  var awayWinCountAtAwayLbl:UILabel?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
