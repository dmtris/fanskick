//
//  FKTicketsDashboardVC.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 10/23/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKTicketsDashboardVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var allClubsbtn: UIButton!
    @IBOutlet weak var allLeaguebtn: UIButton!
    @IBOutlet weak var fixtureTableView: UITableView!
    var sectionHeaderArr = [String]()
    var DataArr = [Array<Any>]()
    var page = PAGE()
    var dataSourceArray = [FKFixtureGroup]()
    
    var allDataSourceArray = [FKFixtureListModel]()
    
    var leagueArray = [FKListInfo]()
    var clubArray = [FKListInfo]()
    
    var selectedLeague = ""
    var selectedClub = ""
    var isTicket = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "TICKETS"
        
        selectedLeague = "All Leagues"
        selectedClub = "All Clubs"
        page.startIndex = 1
        page.pageSize = 10
        
        leagueApiCall()
        clubApiCall()
        apiCallForFixtures()
        
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if DeviceType.IS_IPAD_PRO || DeviceType.IS_IPAD{
            allClubsbtn.titleLabel?.font = UIFont.init(name: "Gilroy-Light", size: 16)
            allLeaguebtn.titleLabel?.font = UIFont.init(name: "Gilroy-Light", size: 16)
            
        }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    // use to navigate back screen
    @objc func backBtnAction() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func applyFilter(){
        page.startIndex = 1
        page.pageSize = 10
        
        apiCallForFixtures()
    }
    
    //MARK:- Api call for league list
    func leagueApiCall() {
        var params      = Dictionary<String,Any>()
        params["page"]      = "1"
        params["per_page"]  = "500"
        
        ServiceHelper.request(params: params, method: .get, apiName: "fixtures/league_list", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            if error == nil {
                print(result ?? "no result")
                let data = result?["league_list"] as? Array<Dictionary<String, AnyObject>>
                if data != nil{
                    self.leagueArray.append(contentsOf: FKListInfo.list(data: (data)!))
                } else {
                }
            }
        }
    }
    
    //MARK:- Club List APi
    func clubApiCall() {
        
        var params          = Dictionary<String,Any>()
        params["page"]      = "1"
        params["per_page"]  = "500"
        
        ServiceHelper.request(params: params, method: .get, apiName: "fixtures/club_list", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if let data = results["league_list"] {
                    self.clubArray.append(contentsOf: FKListInfo.list(data: data as! Array<Dictionary<String, AnyObject>>))
                } else {
                    
                }
            }
        }
    }
    
    func getLeagueList() -> [String]{
        var array = [String]()
        array.append("All Leagues")
        for item in self.leagueArray{
            array.append(item.name)
        }
        return array
    }
    
    func leagueByName(name:String) -> FKListInfo {
        
        var league:FKListInfo?
        for item in self.leagueArray{
            if item.name == name {
                league = item
                break
            }
        }
        return league!
    }
    
    func getClubList() -> [String]{
        var array = [String]()
        array.append("All Clubs")
        for  item in self.clubArray{
            array.append(item.name)
        }
        return array
    }
    
    func clubByName(name:String) -> FKListInfo {
        var club:FKListInfo?
        for item in self.clubArray{
            if item.name == name{
                club = item
                break
            }
        }
        return club!
    }
    
    //MARK:-  Fixture api call
    func apiCallForFixtures() {
        
        var params      = Dictionary<String,Any>()
        var leagueDict  = Dictionary<String,Any>()
        var clubDict    = Dictionary<String,Any>()
        
        clubDict["id"] = ""
        leagueDict["id"] = ""
        params["type"]      = ""  // league, both, club
        
        if selectedLeague != "All Leagues"{
            let league =    leagueByName(name: selectedLeague)
            leagueDict["id"] = league.id
            params["type"]      = "league"
        }
        
        if selectedClub != "All Clubs"{
            let club =    clubByName(name: selectedClub)
            clubDict["id"] = club.id
            params["type"]      = "club"
        }
        
        if ((selectedLeague != "All Leagues") && (selectedClub != "All Clubs")){
            params["type"]      = "both"
        }
        
        params["page"]      = "\(page.startIndex)"
        params["per_page"]  = "\(page.pageSize)"
        params["league"]    = leagueDict
        params["club"]      = clubDict
        
        print(params)
        let apiname = "/fixtures/fixture_list"
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: MethodType.post, apiName: apiname) { (result, error, statusCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            DispatchQueue.main.async {
                if (error == nil) {
                    let results = result as! Dictionary<String, AnyObject>
                    if let data = results["fixture_list"] {
                        if self.page.startIndex == 1{
                            self.allDataSourceArray.removeAll()
                        }
                        
                        let list =  FKFixtureListModel.fixturesList(data: data as! Array<Dictionary<String, AnyObject>>)
                        
                        self.allDataSourceArray.append(contentsOf: list)
                        
                        print("============== == =   \(self.allDataSourceArray.count)  ........ \(data.count)")
                        let groups =  FKFixtureGroup.getGroupedFixture(array: self.allDataSourceArray)
                        self.dataSourceArray.removeAll()
                        
                        self.dataSourceArray.append(contentsOf: groups)
                        
                        if self.dataSourceArray.isEmpty{
                            MessageView.showMessage(message: "No upcoming fixture found for this filter, Please change filter criteria.", time: 5.0, verticalAlignment: .bottom)
                            
                        } else {
                            self.page.totalPage = result!["total_pages"] as! Int
                        }
                        self.fixtureTableView.reloadData()
                    }
                    
                    self.dataSourceArray.sort(by: { (group1, group2) -> Bool in
                        return group1.groupDate! < group2.groupDate!
                    })
                    self.fixtureTableView.reloadData()
                }
            }
        }
    }
    
    @IBAction func superLeagueBtn(_ sender: UIButton) {
        
        let dataArray = self.getLeagueList()
        
        RPicker.sharedInstance.selectOption(title: "Select League", dataArray: dataArray, selectedIndex: 0) { (selectedText, index) in
            if !selectedText.isEmpty{
                sender.setTitle(" "+selectedText, for: .normal)
                self.selectedLeague = selectedText
                self.page.startIndex = 1
                self.apiCallForFixtures()
            }
            print(selectedText)
        }
    }
    @IBAction func allClubsBtn(_ sender: UIButton) {
        
        let dataArray = self.getClubList()
        RPicker.sharedInstance.selectOption(title: "Select Club", dataArray: dataArray, selectedIndex: 0) { (selectedText, index) in
            
            if !selectedText.isEmpty{
                self.selectedClub = selectedText
                sender.setTitle(" "+selectedText, for: .normal)
                self.page.startIndex = 1
                
                self.apiCallForFixtures()
            }
            print(selectedText)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- Tableview Datasource
    public func numberOfSections(in tableView: UITableView) -> Int {
        return self.dataSourceArray.count // sectionHeaderArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FKTitleDetailCell") as! FKTitleDetailCell
        let group =    self.dataSourceArray[section]
        cell.titleLbl?.text = group.groupDate?.stringToDate()?.dateString(DATE_FORMAT)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let group = self.dataSourceArray[section]
        print("group.groupArray.count: ",group.groupArray.count)
        print("is Expanded: ",group.isExpanded)
        
        //   return group.groupArray.count
        
        if group.isExpanded {
            return group.groupArray.count
        }
        return (group.groupArray.count > 3) ? 3+1 : group.groupArray.count
    }
    
    @objc func viewAllMatches(sender:UIButton) {
        print("sender tag",sender.tag)
        let group = self.dataSourceArray[sender.tag]
        group.isExpanded = true
        fixtureTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let group = self.dataSourceArray[indexPath.section]
        print("group.groupArray.count: ",group.groupArray.count)
        print("is Expanded: ",group.isExpanded)
        
        let fixture = group.groupArray[indexPath.row]
        if group.groupArray.count > 3 {
            if group.isExpanded == false {
                //                if indexPath.row == group.groupArray.count-1 {
                if indexPath.row == 3 {
                    
                    let allFixtureCell = tableView.dequeueReusableCell(withIdentifier: "FKViewAllFixtureCell", for: indexPath) as! FKViewAllFixtureCell
                    allFixtureCell.viewAllBtn.tag = indexPath.section
                    allFixtureCell.viewAllBtn.addTarget(self, action: #selector(viewAllMatches), for: .touchUpInside)
                    return allFixtureCell
                }
            }
        }
        
        let cell : FKFixtureDetailTableViewCell!
        if DeviceType.IS_IPAD_PRO || DeviceType.IS_IPAD{
            cell = tableView.dequeueReusableCell(withIdentifier: "FKFixtureDetailTableViewCelliPad", for: indexPath) as! FKFixtureDetailTableViewCell
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "FKFixtureDetailTableViewCell", for: indexPath) as! FKFixtureDetailTableViewCell
        }
        cell.teamLbl1.text = fixture.club
        cell.teamImg1.normalLoad(fixture.clubImage)
        cell.teamLbl2.text = fixture.apponent
        cell.teamImg2.normalLoad(fixture.apponentImage)
        cell.stadiumLbl.text = fixture.stadium
        cell.timeLbl.setTitle(fixture.start_time.dateFromUTC()?.onlyTimeString(), for: .normal)
        cell.buyTicketBtn.addTarget(self, action: #selector(buyTicket), for: .touchUpInside)
        cell.buyTicketBtn.indexPath = indexPath
        return cell
    }
    
    @objc func buyTicket(sender: IndexPathButton) {
        let group = self.dataSourceArray[(sender.indexPath?.section)!]
        let fixture = group.groupArray[(sender.indexPath?.row)!]
        
        let buyTicketVC = self.storyboard?.instantiateViewController(withIdentifier: "buyTicketVC") as! FKBuyTicketVC
        buyTicketVC.fixture = fixture
        self.navigationController?.pushViewController(buyTicketVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        //        tableView.deselectRow(at: indexPath, animated: true)
        //        let fixtureDetailVc = self.storyboard?.instantiateViewController(withIdentifier: "FKFixtureDetailViewController") as! FKFixtureDetailViewController
        //        let group =    self.dataSourceArray[indexPath.section]
        //
        //        let fixture = group.groupArray[indexPath.row]
        //        fixtureDetailVc.fixtureItem = fixture
        //        self.navigationController?.pushViewController(fixtureDetailVc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let group = self.dataSourceArray[indexPath.section]
        
        if group.groupArray.count > 3 {
            if group.isExpanded == false {
                if indexPath.row == 3 {
                    return 40.0
                }
            }
        }
        if DeviceType.IS_IPAD_PRO || DeviceType.IS_IPAD {
            return 100.0
        } else {
            return 70.0
        }
    }
    
    //MARK:
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        // UITableView only moves in one direction, y axis
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        // Change 10.0 to adjust the distance from bottom
        if maximumOffset - currentOffset <= 10.0 {
            if page.totalPage > page.startIndex{
                page.startIndex = page.startIndex+1;
                self.apiCallForFixtures()
            }
        }
    }
    
    
    
    
    
    //    func getAlltickets() {
    //
    //        var arrData = dictResponse["data"] as? [Any]
    //        if arrData?.count > 0 {
    //            for dictData: [AnyHashable: Any] in arrData {
    //                var shouldSave = true
    //                var objSavedData = Data.mr_findAll(with: NSPredicate(format: "%K MATCHES %@", "strAddress", functionSaveResponse(inString: dictData["address"]))).first
    //                if objSavedData != nil {
    //                    shouldSave = false
    //                }
    //                if shouldSave {
    //                    var objData = Data.mr_createEntity()
    //                    if !dictData.isEmpty {
    //                        if !functionIsEmptyOrNull(dictData["address"]) {
    //                            objData.strAddress = functionSaveResponse(inString: dictData["address"])
    //                        }
    //                        if !functionIsEmptyOrNull(dictData["distance"]) {
    //                            objData.strDictance = functionSaveResponse(inString: dictData["distance"])
    //                        }
    //                        if !functionIsEmptyOrNull(dictData["g_img_1_200"]) {
    //                            objData.strImage = functionSaveResponse(inString: dictData["g_img_1_200"])
    //                        }
    //                        if !functionIsEmptyOrNull(dictData["location"]) {
    //                            objData.strLocation = functionSaveResponse(inString: dictData["location"])
    //                        }
    //                        NSManagedObjectContext.mr_default().mr_saveToPersistentStore(withCompletion: {(_ contextDidSave: Bool, _ error: Error?) -> Void in
    //                            if error != nil {
    //                                print("Error \(error?.description)")
    //                            }
    //                            else {
    //                                print("Saved")
    //                            }
    //                        })
    //                    }
    //                }
    //            }
    //            arrMainData = Data.mr_findAll()
    //            tblData.reloadData()
    //        }
    //    }
}

