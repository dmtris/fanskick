//
//  FKBBroadCastLiveVC.swift
//  FansKick
//
//  Created by Sunil Verma on 11/1/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit
import WebKit
import AVKit
class FKBroadCastLiveVC: UIViewController, WKNavigationDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var playerView: UIView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    private var playerViewController = AVPlayerViewController()
    private var  player: AVPlayer?

    var liveBroadastInfo: FKBroadcastInfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "FOOTBALL LIVE"
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        self.perform(#selector(loadVideoUrl), with: nil , afterDelay: 1.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
    }
    
    @objc func loadVideoUrl() {

        //fanskick
        let url = "\(HOST_ADDRESS):\(PORT_NUMBER)/fanskick1/\(self.liveBroadastInfo?.broadcastTitle ?? "")/playlist.m3u8"

        print("live=======",url)
            let videoURL = URL(string: url)
        if videoURL == nil{
            MessageView.showMessage(message: "Media type not suppoted", time: 4.0, verticalAlignment: .bottom)
            return
        }
        let playerItem = AVPlayerItem.init(url: videoURL!)
        let player = AVPlayer.init(playerItem: playerItem)
        playerViewController.view.frame = playerView.bounds
        playerViewController.player = player
        playerViewController.videoGravity = AVLayerVideoGravity.resizeAspectFill.rawValue

      //  playerViewController.delegate = self
        playerView.addSubview(playerViewController.view)
        playerViewController.player?.play()
    }
    
    // use to navigate back screen
    @objc func backBtnAction() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: UICOLLECTION VIEWS
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FKLiveOtherCollectionCell", for: indexPath) as! FKLiveOtherCollectionCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        FKPlayerHelper.shared.play(url: "http://192.168.0.197:1935/live/new/playlist.m3u8", controller: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: 120, height: 120)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        FKPlayerHelper.shared.stop()
        self.playerViewController.player?.pause()
        player?.pause()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
