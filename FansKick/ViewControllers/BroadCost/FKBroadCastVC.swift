//
//  FKBroadCostVC.swift
//  FansKick
//
//  Created by Sunil Verma on 10/31/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKBroadCastVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var segmentView: UISegmentedControl!
    var refreshControl: UIRefreshControl!
	 var nextPageToken = ""
    
    var dataSourceArray = [FKBroadcastInfo]()
    var page = PAGE()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "BROADCASTING CENTER"
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        tblView.tableFooterView = UIView.init()
        
        self.perform(#selector(apiCallForLive), with: nil, afterDelay: 0.1)
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblView.refreshControl = refreshControl
        
    }
    
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
        page.startIndex = 1
				self.nextPageToken = ""
        apiCall()
        refreshControl.endRefreshing()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    // use to navigate back screen
    @objc func backBtnAction() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func segmentControllAction(_ sender: UISegmentedControl) {
        self.dataSourceArray.removeAll()
        page.startIndex = 1
			self.nextPageToken = ""
        apiCall()
    }
    
    func apiCall() {
        
        if segmentView.selectedSegmentIndex == 0 {
				
         apiCallForLive()
        } else {
            apiCallForHighlighted()
        }
        self.tblView.reloadData()
    }
    
    func apiCallForHighlighted() {
        
        var params = Dictionary<String,Any>()
    //    params["page"] = "\(page.startIndex)"
      //  params["per_page"] = "\(page.pageSize)"
			
			params["page_token"] = self.nextPageToken
        MBProgressHUD.showAdded(to: self.view, animated: true)
			///highlights/highlighted_videos.json?page_token=CAoQAA

        ServiceHelper.request(params: params, method: .get, apiName: "highlights/highlighted_videos") { (result, error, responseCode) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                
                if let data = results["highlights"] {
                    if self.page.startIndex == 1 {
                        self.dataSourceArray.removeAll()
                    }
							//		let array = data["items"] as!  Array<Dictionary<String, AnyObject>>
									
									if let nextPage = data["nextPageToken"]{
										self.nextPageToken = nextPage as! String
									}else{
										self.nextPageToken = ""
									}
									
									let list =  FKBroadcastInfo.broadcastList(data: data["items"] as! Array<Dictionary<String, AnyObject>>)
                    self.dataSourceArray.append(contentsOf: list)
                    
              //      self.page.totalPage = result!["total_pages"] as! Int
                    
                }else {
                    self.dataSourceArray.removeAll()
                    MessageView.showMessage(message: "No highlights available for now.", time: 5.0, verticalAlignment: .bottom)
                }
            }
            self.dataSourceArray.sort(by: { (item1, item2) -> Bool in
                return item1.updateTime > item2.updateTime
            })
            self.tblView.reloadData()
        }
    }
    
    
    @objc func apiCallForLive() {
        
        let params = Dictionary<String,Any>()
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        ServiceHelper.request(params: params, method: .get, apiName: "highlights/live_streams") { (result, error, responseCode) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                let streams = results["streams"] as! Dictionary<String, AnyObject>
                
                if let _ = streams["instanceList"] {
                    let instanceArray = streams["instanceList"] as! Array<Dictionary<String, AnyObject>>
                    if instanceArray.count > 0 {
                        let incomingStreamsDict = instanceArray.first
                        let incomingStreamsArray = incomingStreamsDict!["incomingStreams"] as! Array<Dictionary<String, AnyObject>>
                        self.dataSourceArray.removeAll()
                        let list =  FKBroadcastInfo.livebroadcastList(data: incomingStreamsArray)
                        self.dataSourceArray.append(contentsOf: list)
                    }
                }
                if self.dataSourceArray.count == 0 {
                    MessageView.showMessage(message: "No stream available for now.", time: 5.0, verticalAlignment: .bottom)
                }
            }
            self.dataSourceArray.sort(by: { (item1, item2) -> Bool in
                return item1.broadcastTitle > item2.broadcastTitle
            })
            self.tblView.reloadData()
        }
    }
    // MARK: TableView overrdes
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSourceArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let broadcastInfo = self.dataSourceArray[indexPath.row]
        
        if self.segmentView.selectedSegmentIndex == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FKLiveCellBroadcast") as! FKLiveCell
            cell.titleLbl?.text = broadcastInfo.broadcastTitle
            return cell
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FKLiveCell") as! FKLiveCell
            cell.titleLbl?.text = broadcastInfo.broadcastTitle
            //cell.detailLbl?.text = broadcastInfo.broadcastDesc
					
					cell.imgView?.normalLoad(broadcastInfo.media)
					print(">>>>>>>>  \(broadcastInfo.media)")
//            FKPlayerHelper.captureImageFrom(url: broadcastInfo.media, completion: { (image ) in
//                cell.imgView?.image = image
//            })
            return cell;
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if segmentView.selectedSegmentIndex == 0 {
            return 60
        }
        return 120;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let broadcastInfo = self.dataSourceArray[indexPath.row]
        
        if segmentView.selectedSegmentIndex == 0 {
            
            let url = "\(HOST_ADDRESS):\(PORT_NUMBER)/fanskick1/\(broadcastInfo.broadcastTitle )/playlist.m3u8"
            
            let encodedUrl = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            print(">>>>>>>>>>  ", url)
            FKPlayerHelper.shared.playOnRootWindow(url: encodedUrl!)
            
            //            let liveVC = self.storyboard?.instantiateViewController(withIdentifier: "broadCastLiveVC") as! FKBroadCastLiveVC
            //            liveVC.liveBroadastInfo = broadcastInfo
            //            self.navigationController?.pushViewController(liveVC, animated: true)
        } else {
					
					let youTubeVC = self.storyboard?.instantiateViewController(withIdentifier: "FKYouTubeVC") as!  FKYouTubeVC
					youTubeVC.broadcastInfo = broadcastInfo
					self.navigationController?.pushViewController(youTubeVC, animated: true);
					
//					UIApplication.shared.open(URL.init(string: "https://www.youtube.com/watch?v=BRNvCsD8AB4")!, options: [:], completionHandler: { (sussess) in
//
//					})
					
          //  FKPlayerHelper.shared.playOnRootWindow(url: "https://www.youtube.com/embed/tgbNymZ7vqY")
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        // UITableView only moves in one direction, y axis
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        // Change 10.0 to adjust the distance from bottom
        if maximumOffset - currentOffset <= 10.0 {
					if segmentView.selectedSegmentIndex == 0 {

            if page.totalPage > page.startIndex{
                page.startIndex = page.startIndex+1;
							self.apiCall()
							
							}
					}else {
							if (self.nextPageToken.length > 0){
								page.startIndex = page.startIndex+1;
								self.apiCall()
								
							}
					}
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
