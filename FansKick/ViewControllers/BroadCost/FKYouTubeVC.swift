//
//  FKYouTubeVC.swift
//  FansKick
//
//  Created by Sunil Verma on 21/02/18.
//  Copyright © 2018 FansKick Dev. All rights reserved.
//

import UIKit
import WebKit
import youtube_ios_player_helper

class FKYouTubeVC: UIViewController, YTPlayerViewDelegate  {

//private	var webView = WKWebView()

	@IBOutlet var playerView: YTPlayerView?

	@IBOutlet var activityIndicator:UIActivityIndicatorView?
	
	var broadcastInfo:FKBroadcastInfo?
    override func viewDidLoad() {
        super.viewDidLoad()
			self.navigationItem.title = "HIGHLIGHTS"
			let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
			
			self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
			self.perform(#selector(loadVideo), with: nil , afterDelay: 1.0)

        // Do any additional setup after loading the view.
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.view.bringSubview(toFront: self.activityIndicator!)

		
	}
	// use to navigate back screen
	@objc func backBtnAction() {
		
		self.navigationController?.popViewController(animated: true)
	}
	@objc func loadVideo(){
		
		
		playerView?.load(withVideoId: (self.broadcastInfo?.id)!)
		playerView?.delegate = self
//		self.webView.allowsLinkPreview = true
//		self.webView.navigationDelegate = self
//		self.webView.uiDelegate = self
//		webView.allowsBackForwardNavigationGestures = true
//		self.webView.configuration.dataDetectorTypes = .link
//		self.webView.configuration.dataDetectorTypes = .phoneNumber
//		self.webView.configuration.dataDetectorTypes = .address
//		self.webView.frame = self.view.bounds
//		self.view.addSubview(self.webView)
//
//		self.activityIndicator?.startAnimating()
//		let htmlStr = "<html> <body><iframe width=\"100%\" height=\"500\" style=\"border:none;\" src=\"https://www.youtube.com/embed/\(self.broadcastInfo?.id ?? "")\"></iframe></body></html>"
//		webView.loadHTMLString(htmlStr, baseURL: nil);
		
		
	}
//	func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
//		self.activityIndicator?.stopAnimating()
//
//	}
//
//	func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
//		self.activityIndicator?.stopAnimating()
//	}
	
	func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
				self.activityIndicator?.stopAnimating()

	}
	
	func playerView(_ playerView: YTPlayerView, receivedError error: YTPlayerError) {
				self.activityIndicator?.stopAnimating()

	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
