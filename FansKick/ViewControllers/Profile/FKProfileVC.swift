//
//  FKProfileVC.swift
//  FansKick
//
//  Created by FansKick-Sunil on 17/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit
import SDWebImage


class FKProfileVC: UIViewController,UIPopoverPresentationControllerDelegate,UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate {
    
    typealias profileVcCompletionHandler = (_ success:Bool, _ callbackString:String) -> Void
    private var completionBlock : profileVcCompletionHandler? = nil

    @IBOutlet weak var frontImgBtn: UIButton!
    @IBOutlet weak var backImgBtn: UIButton!
    @IBOutlet weak var avatarView: UIView!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var sponsorsTbl: UITableView!
    @IBOutlet weak var clubsTbl: UITableView!
    @IBOutlet weak var kitTbl: UITableView!
    @IBOutlet weak var numbersTbl: UITableView!
    @IBOutlet weak var myTicketsBtn: UIButton!
    @IBOutlet weak var popOverWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var popOverVw: UIView!
    @IBOutlet weak var popOverViewHeightConstraint: NSLayoutConstraint!
    var titleNameArr = [String]()
    var upperImageArr = [UIImageView]()
    var contraintArray = [NSLayoutConstraint]()
    var timer = Timer()
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view5: UIView!
    var gestureScale = CGFloat()
    @IBOutlet weak var dd2ViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var dd3ViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var dd4ViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var dropDownHeight           : NSLayoutConstraint!
    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet weak var dropDownViewLeadingConstraint: NSLayoutConstraint!
    var isEditable = false
    var isAvatarCreated = false
    var saveBtn : UIButton?
    @IBOutlet weak var resignKeyBoardButton: UIButton!
    @IBOutlet weak var avatarScrollView: UIScrollView!
    @IBOutlet weak var avatarImageView: UIImageView!  // use to restrict the image size
    var responseMessage = ""
    var imagView: UIImageView!
    var fkAvatarInfo = FKAvatarInfo()
    var editedAvatarImage = UIImage()
    var avatarBackImage = UIImage()
    var isFlipped = false
    var clubArray = [FKListInfo]()
    var sponsorsArray = [FKListInfo]()
    @IBOutlet weak var avatarNameLbl: UILabel!
    var touchedScrollView = UIScrollView()
    var avatarNameImgVw = UIImageView()
    var kitArray = [Dictionary<String, UIImage>]()
    @IBOutlet weak var nameLabel: UILabel!
    var frontImageView  = UIImageView()
    var backImageView   = UIImageView()
    var textType = ""
    var imageTag = 0
    @IBOutlet weak var sponsorImg: UIImageView!
    @IBOutlet weak var numberLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var leagueLogoImg: UIImageView!
    @IBOutlet weak var kitImg: UIImageView!
    var selectedDropDown = ""
    var selectedKitIndex = 0
    var lastRotation: CGFloat = 0
    var angleSet = false
    
    func completionBlock(completion: @escaping profileVcCompletionHandler) {
        self.completionBlock = completion
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "MY PROFILE"
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        
        let profileBtn = UIButton.init(type: UIButtonType.custom)
        profileBtn.titleLabel?.font = UIFont.init(name: fanskickFontIcons, size: 22)
        profileBtn.setTitle("e", for: .normal)
        profileBtn.frame = CGRect.init(x: 0, y: 0, width: 24, height: 24)
        profileBtn.addTarget(self, action: #selector(menuOption), for: UIControlEvents.touchUpInside)
        
        let profileBar = UIBarButtonItem.init(customView: profileBtn)
        
        saveBtn = UIButton(type: .custom)
//        saveBtn?.titleLabel?.font = UIFont.init(name: "fanskick-font2-icon-set", size: 22)
        saveBtn?.setImage(#imageLiteral(resourceName: "save"), for: .normal)
//        saveBtn?.setTitle("D", for: .normal)
        saveBtn?.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        saveBtn?.tag = 5
        saveBtn?.addTarget(self, action: #selector(saveProfile), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: saveBtn!)
        
        self.navigationItem.rightBarButtonItems = [profileBar,item1]
        saveBtn?.isEnabled = false
        saveBtn?.setTitleColor(UIColor.lightGray, for: .normal)
        saveBtn?.setTitleColor(UIColor.lightGray, for: .normal)
        tableView.tableFooterView = UIView.init()
        popOverWidthConstraint.constant = 0
        if defaults.bool(forKey: "socialLogin") {
            popOverViewHeightConstraint.constant = 277
            titleNameArr = ["MY TICKETS","FK WALLET","MY ACCOUNT","SETTINGS","HELP","ABOUT US"]
        } else {
            titleNameArr = ["MY TICKETS","FK WALLET","MY ACCOUNT","CHANGE PASSWORD","SETTINGS","HELP","ABOUT US"]
        }
        
        contraintArray = [dropDownHeight,
                          dd2ViewHeightConstraint,
                          dd3ViewHeightConstraint,
                          dd4ViewHeightConstraint]
        
        callApiToGetAvatar()
        apiCallForTools()
        
        let avatarWhite = ["front":#imageLiteral(resourceName: "AvatarFront"),
                           "back":#imageLiteral(resourceName: "AvatarBack")]
        
        let avatarYellow = ["front":#imageLiteral(resourceName: "AvatarYellowFront"),
                            "back":#imageLiteral(resourceName: "AvatarYellowBack")]
        
        let avatarBlue = ["front":#imageLiteral(resourceName: "AvatarBlueFront"),
                          "back":#imageLiteral(resourceName: "AvatarBlueBack")]
        
        let avatarBrown = ["front":#imageLiteral(resourceName: "AvatarBrownFront"),
                           "back":#imageLiteral(resourceName: "AvatarBrownBack")]
        
        let avatarGreen = ["front":#imageLiteral(resourceName: "AvatarGreenFront"),
                           "back":#imageLiteral(resourceName: "AvatarGreenBack")]
        
        let avatarV = ["front":#imageLiteral(resourceName: "AvatarVioletFront"),
                       "back":#imageLiteral(resourceName: "AvatarVioletBack")]
        
        kitArray.append(avatarWhite)
        kitArray.append(avatarYellow)
        kitArray.append(avatarBlue)
        kitArray.append(avatarBrown)
        kitArray.append(avatarGreen)
        kitArray.append(avatarV)
    }
    
    func apiCallForTools()
    {
        sponsorsApiCall()
        clubApiCall()
    }
    
    //MARK: UITextField overrides
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        saveBtn?.isEnabled = true
        saveBtn?.setTitleColor(UIColor.white, for: .normal)
        addNameTextLabel(textField:textField)
    }
    
    
    /***
     This will add name on the image as view editing is ended an image will be createdf and will be added to imageview automaticaly.
     it can dragged anywhere onto the imageview
     ***/
    func addNameTextLabel(textField:UITextField) {
        if textField.text?.length != 0 {
            
            if textType == "name" {
                self.fkAvatarInfo.name = textField.text!
            } else {
                self.fkAvatarInfo.jerseyNo = textField.text!
            }
            
            isEditable = true
            self.resignKeyBoardButton.isHidden = true
            self.avatarNameLbl.text = textField.text
            
            let textHeight = textField.text!.heightOfString(withConstrainedWidth: avatarImageView.frame.size.width, font: UIFont(name: "Gilroy-ExtraBold", size: 18)!)
            let textWidth = textField.text!.width(withConstraintedHeight: textHeight, font: UIFont(name: "Gilroy-ExtraBold", size: 20)!)
            let imageCreatedFromText = imageFrom(text: textField.text!, size: CGSize(width:textWidth,height:textHeight))
            
            imagView = UIImageView(frame: CGRect(x:avatarScrollView.frame.size.width/2-50, y: avatarScrollView.frame.size.height/2-50, width: 150, height: 80))
            avatarNameImgVw = imagView
            imagView.removeFromSuperview()
            imagView.image = imageCreatedFromText
            imagView.tag = imageTag+1
            
            avatarScrollView.addSubview(imagView)
            
            if upperImageArr.contains(imagView) {
                upperImageArr.remove(at: upperImageArr.index(of: imagView)!)
            }
            upperImageArr.append(imagView)
            
            drawOutlineOnComponentAndCross()
            
            imagView.isUserInteractionEnabled = true
            let panGesture = UIPanGestureRecognizer(target: self, action: #selector(myPanAction(recognizer:)))
            imagView.addGestureRecognizer(panGesture)
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapOnImage(_:)))
            self.imagView.addGestureRecognizer(tapGesture)
        }
        textField.removeFromSuperview()
        
    }
    
    // MARK:- Textfield delegate method
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        
        if textField.tag == 3 { // handle name keyboard
            if !string.isEmpty && newString.length > 13 {
                return false
            }
        } else if textField.tag == 4 { // handle kit keyboard
            if !string.isEmpty && newString.length > 3 {
                return false
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    
    /***
     This will save all the changes which you have made, create a image and upload it to server.
     ***/
    @objc func saveProfile(_ sender: UIButton) {
        self.view.endEditing(true)
        hideAllDropDown()
        
        if isEditable {
            editedAvatarImage = self.avatarImage.image!
            for logoImage in upperImageArr {
                logoImage.borderColor = UIColor.clear
                
                for item in logoImage.subviews {
                    if item is UIButton {
                        item.removeFromSuperview()
                    }
                }
                let mergedImage = getEditedImageFromContext()//getMergedImage(bottomImage: editedAvatarImage, topImageView: logoImage)
                editedAvatarImage = mergedImage
            }
            self.responseMessage = "Avatar updated successfuly"
            if isFlipped { // Check if it flipped and upload the reversed image to server
                callAPiToUpdateAvatar(frontImage: self.frontImageView.image!, backImage: editedAvatarImage)
            } else {
                callAPiToUpdateAvatar(frontImage: editedAvatarImage, backImage: self.backImageView.image!)
            }
            isEditable = false
            sender.isEnabled = false
        } else {
            isEditable = true
            sender.isEnabled = true
        }
    }
    
    // Flip image back image.
    
    @IBAction func backSideImageBtn(_ sender: Any) {

        if isEditable { // check if it is editable, this bool becomes true whenever any changes are made onto the image.

            AlertController.actionSheet(title: "Confirmation!", message: "You have made some unsaved changes, Would you like to save the changes?", sourceView: backImgBtn, buttons: ["Yes","No","Cancel"]) { (action, index) in

                if index == 0 { //Yes
                    self.saveProfile(self.saveBtn!)
                    self.saveBtn?.isEnabled = false
                    self.saveBtn?.setTitleColor(UIColor.lightGray, for: .normal)
                    self.isEditable = false
                } else if index == 1 { //No
                    for img in self.upperImageArr {
                        img.removeFromSuperview()
                    }
                    self.upperImageArr.removeAll()
                    self.saveBtn?.isEnabled = false
                    self.saveBtn?.setTitleColor(UIColor.lightGray, for: .normal)
                    self.isEditable = false
                }
                print(index)
            }
        } else {
            self.flipImage()
        }
    }
    
    // MARK:- Flip image to front image
    @IBAction func frontSideImageBtn(_ sender: Any) {

        if isEditable { // check if it is editable, this bool becomes true whenever any changes are made onto the image.
            AlertController.actionSheet(title: "Confirmation!", message: "You have made some unsaved changes, Would you like to save the changes?", sourceView: backImgBtn, buttons: ["Yes","No","Cancel"]) { (action, index) in

                if index == 0 { //Yes
                    self.saveProfile(self.saveBtn!)
                    self.saveBtn?.isEnabled = false
                    self.saveBtn?.setTitleColor(UIColor.lightGray, for: .normal)
                    self.isEditable = false
                    print(index)
                } else if index == 1 { //No
                    for img in self.upperImageArr {
                        img.removeFromSuperview()
                    }
                    self.upperImageArr.removeAll()
                    self.saveBtn?.isEnabled = false
                    self.saveBtn?.setTitleColor(UIColor.lightGray, for: .normal)
                    self.isEditable = false
                    print(index)
                }
            }
        } else {
            self.flipImage()
        }
    }
    
    func flipImage() {
        
        if isFlipped {
            isFlipped = false
            let url = URL(string: self.fkAvatarInfo.frontImgUrl)
            UIView.transition(with: self.avatarImage,
                              duration: 0.5,
                              options: .transitionFlipFromLeft,
                              animations: {
                                self.avatarImage.sd_setImage(with: url, placeholderImage: self.kitArray[self.selectedKitIndex]["front"])
            },
                              completion: nil)
            
        } else {
            let url = URL(string: self.fkAvatarInfo.backImgUrl)
            isFlipped = true
            UIView.transition(with: self.avatarImage,
                              duration: 0.5,
                              options: .transitionFlipFromRight,
                              animations: {
                                self.avatarImage.sd_setImage(with: url, placeholderImage: self.kitArray[self.selectedKitIndex]["back"])
            },
                              completion: nil)
        }
    }
    
    /***
     Reset avatar Image to default image
     It will upload default image to server automaticaly.
     it will remove all the logos
     ***/
    @IBAction func resetAvatarBtn(_ sender: Any) {
        AlertController.alert(title: "Confirmation!", message: "Are you sure you want to reset the Avatar?\nThis action cannot be undone.", buttons: ["No","Yes"], tapBlock: { (action , index) in
            if index == 1 {
                /*self.kitArray[self.selectedKitIndex]["front"]
self.kitArray[self.selectedKitIndex]["back"]*/
                self.isAvatarCreated = true
                self.isFlipped = false
                let avatarPlaceholderImage = UIImage(named:"AvatarFront.png")
                self.editedAvatarImage = avatarPlaceholderImage!
                self.fkAvatarInfo.frontImgUrl = ""
                self.fkAvatarInfo.jerseyNo = ""
                self.fkAvatarInfo.name = ""
                self.responseMessage = "Avatar reset successfuly"
                self.callAPiToUpdateAvatar(frontImage: self.kitArray[self.selectedKitIndex]["front"]!, backImage: self.kitArray[self.selectedKitIndex]["back"]!)
            }
        })
    }
    
    //MARK:- Save Avatar
    func callAPiToUpdateAvatar(frontImage:UIImage,backImage:UIImage) {
        let userDict = NSMutableDictionary()
        let frontImageData = UIImagePNGRepresentation(frontImage)
        let backImageData = UIImagePNGRepresentation(backImage)
        
        userDict ["front_image"] = frontImageData
        userDict ["back_image"] = backImageData
        userDict ["jersey_no"]  = self.fkAvatarInfo.jerseyNo
        userDict ["name"]       = self.fkAvatarInfo.name
        
        var apiName = "avatars/upload_avatar.json"
        var methodType = "POST"
        
        if isAvatarCreated {
            apiName = "avatars/edit_avatar.json"
            methodType = "PUT"
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        HSServiceHelper.shared().multipartApiCall(withParameter: userDict, methodtype: methodType, apiName: apiName) { (result, error) in
            print(result ?? "no result")
            MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil {
                for img in self.upperImageArr {
                    img.removeFromSuperview()
                }
                self.upperImageArr.removeAll()
                
                self.callApiToGetAvatar()
                
                MessageView.showMessage(message: self.responseMessage, time: 2.0, verticalAlignment: .bottom)
            } else {
                MessageView.showMessage(message: "Unable to process your request, please try after some time.", time: 4.0, verticalAlignment: .bottom)
            }
        }
    }
    
    //MARK:- Get Avatar
    func callApiToGetAvatar() {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let params = Dictionary<String,Any>()
        ServiceHelper.request(params: params, method: .get, apiName: "avatars/view_avatar.json", hudType: loadingIndicatorType.withoutLoader) { (result, error, responseCode) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if responseCode == 200 {
                let results = result as! Dictionary<String, AnyObject>
                
                if let data = results["avatar"] {
                    
                    self.fkAvatarInfo = FKAvatarInfo.avatarImage(data as? Dictionary<String, AnyObject>)
                    
                    let frontImageUrl   = URL(string: self.fkAvatarInfo.frontImgUrl)
                    let backImageUrl    = URL(string: self.fkAvatarInfo.backImgUrl)
                    
                    self.nameLbl.text = self.fkAvatarInfo.name
                    self.numberLbl.text = self.fkAvatarInfo.jerseyNo
                   
//                    /clearMemory
                    SDImageCache.shared().removeImage(forKey: self.fkAvatarInfo.frontImgUrl, withCompletion: {
                        self.frontImageView.sd_setImage(with: frontImageUrl, placeholderImage: self.kitArray[self.selectedKitIndex]["front"])
                    })
                    SDImageCache.shared().removeImage(forKey: self.fkAvatarInfo.backImgUrl, withCompletion: {
                        self.backImageView.sd_setImage(with: backImageUrl, placeholderImage: self.kitArray[self.selectedKitIndex]["back"])
                    })
                    
                    self.kitImg.normalLoad(self.fkAvatarInfo.frontImgUrl)
                    
                    if self.isFlipped { // if avatar is updated while it is flipped
                        self.avatarImage.sd_setImage(with: backImageUrl, placeholderImage: self.kitArray[self.selectedKitIndex]["back"])
                    } else {
                        self.avatarImage.sd_setImage(with: frontImageUrl, placeholderImage:self.kitArray[self.selectedKitIndex]["front"])
                    }
                    self.isAvatarCreated = true
                    
                    self.saveBtn?.isEnabled = false
                    self.saveBtn?.setTitleColor(UIColor.lightGray, for: .normal)
                }
            } else {
                self.isAvatarCreated = false
                self.frontImageView.image   = self.kitArray[self.selectedKitIndex]["front"]//UIImage(named: "AvatarFront.png")!
                self.backImageView.image    = self.kitArray[self.selectedKitIndex]["back"]//UIImage(named: "AvatarBack.png")!
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.resignKeyBoardButton.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        print(self.avatarScrollView.frame.origin.y+self.avatarScrollView.frame.size.height/2)
        
        self.backImgBtn.frame   = CGRect(x:20,y:self.avatarScrollView.frame.origin.y+self.avatarScrollView.frame.size.height/2,width:self.backImgBtn.frame.size.width,height:self.backImgBtn.frame.size.height)
        
        self.frontImgBtn.frame  = CGRect(x:self.avatarScrollView.frame.origin.x+self.avatarScrollView.frame.size.width-40,y:self.avatarScrollView.frame.origin.y+self.avatarScrollView.frame.size.height/2,width:self.frontImgBtn.frame.size.width,height:self.frontImgBtn.frame.size.height)
        
        self.avatarImageView.frame = CGRect(x: 0, y: 0, width: self.avatarScrollView.frame.width/3, height: self.avatarScrollView.frame.height - 50 )
        self.avatarImageView.center = self.avatarScrollView.center
        
        let scrollViewTap = UITapGestureRecognizer(target: self, action: #selector(scrollViewTapped))
        scrollViewTap.numberOfTapsRequired = 1
        avatarScrollView.addGestureRecognizer(scrollViewTap)
        
        print(tableView.frame.size)
        print(popOverVw.frame.size)
    }
    
    @objc func scrollViewTapped() {
        
        for image in upperImageArr {
            for item in image.subviews {
                if item is UIButton {
                    item.removeFromSuperview()
                }
            }
            image.borderWidth = 0
            image.borderColor = .clear
        }
    }
    
    //MARK:- sponsors List APi
    func sponsorsApiCall() {
        
        var params          = Dictionary<String,Any>()
        params["page"]      = "1"
        params["per_page"]  = "500"
        
        ServiceHelper.request(params: params, method: .get, apiName: "/avatars/sponser_list", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if let data = results["sponser"] {
                    self.sponsorsArray.append(contentsOf: FKListInfo.list(data: data as! Array<Dictionary<String, AnyObject>>))
                    self.sponsorsTbl.reloadData()
                    
                    if self.sponsorsArray.count > 0 {
                        let club = self.sponsorsArray[0]
                        print(club.clubImg)
                        let clubImgUrl = URL(string: club.clubImg.trimWhiteSpace)
                        
                        SDImageCache.shared().removeImage(forKey: club.clubImg, withCompletion: {
                            self.sponsorImg.sd_setImage(with: clubImgUrl, placeholderImage: UIImage(named: "placeholder")!)
                        })
                    }
                }
            }
        }
    }
    
    //MARK:- Club List APi
    func clubApiCall() {
        
        var params          = Dictionary<String,Any>()
        params["page"]      = "1"
        params["per_page"]  = "500"
        
        ServiceHelper.request(params: params, method: .get, apiName: "fixtures/club_list", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if let data = results["league_list"] {
                    self.clubArray.append(contentsOf: FKListInfo.list(data: data as! Array<Dictionary<String, AnyObject>>))
                    self.clubsTbl.reloadData()
                    
                    if self.clubArray.count > 0 {
                        let club = self.clubArray[0]
                        let clubImgUrl = URL(string: club.clubImg.trimWhiteSpace)
                        print(clubImgUrl ?? "no club")
                        
                        SDImageCache.shared().removeImage(forKey: club.clubImg, withCompletion: {
                            self.leagueLogoImg.sd_setImage(with: clubImgUrl, placeholderImage: UIImage(named: "placeholder")!)
                        })
                    }
                }
            }
        }
    }
    
    @IBAction func clubDropDown(_ sender: UIButton) {
        self.view.endEditing(true)
        //        self.clubApiCall()
        selectedDropDown = "club"
        animateDropDown(senderBtn: sender,SenderConstraint: dropDownHeight)
    }
    
    @IBAction func kitDropDownBtn(_ sender: UIButton) {
        self.view.endEditing(true)
        animateDropDown(senderBtn: sender,SenderConstraint: dd2ViewHeightConstraint)
        selectedDropDown = "kit"
    }
    
    @IBAction func avatarNameDropDownBtn(_ sender: UIButton) {
        textType = "name"
        self.view.endEditing(true)
        hideAllDropDown()
        showNameEditor(placeholder: "NAME OF AVATAR")
        selectedDropDown = "name"
    }
    
    @IBAction func kitNumberDropDown(_ sender: UIButton) {
        textType = "jerseyNo"
        
        self.view.endEditing(true)
        hideAllDropDown()
        showNameEditor(placeholder: "Kit No")
        selectedDropDown = "number"
    }
    
    @IBAction func sponsorDropDown(_ sender: UIButton) {
        self.view.endEditing(true)
        //        sponsorsApiCall()
        animateDropDown(senderBtn: sender,SenderConstraint: dd4ViewHeightConstraint)
        selectedDropDown = "sponsor"
    }
    
    
    //MARK:-  Animate the dropdowns at the top
    func animateDropDown(senderBtn:UIButton,SenderConstraint:NSLayoutConstraint) {
        
        popOverWidthConstraint.constant = 0
        self.resignKeyBoardButton.isHidden = false
        for constraint in contraintArray {
            if constraint == SenderConstraint {
                if constraint.constant == 0 {
                    constraint.constant = 245
                } else {
                    constraint.constant = 0
                }
            } else {
                constraint.constant = 0
            }
        }
        UIView.animate(withDuration:0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func hideAllDropDown() {
        dd2ViewHeightConstraint.constant = 0
        dd3ViewHeightConstraint.constant = 0
        dd4ViewHeightConstraint.constant = 0
        dropDownHeight.constant = 0
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func menuOption() {
        self.resignKeyBoardButton.isHidden = false
        
        hideAllDropDown()
        if popOverWidthConstraint.constant == 0 {
            popOverWidthConstraint.constant = 210
        } else {
            popOverWidthConstraint.constant = 0
        }
        
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    // use to navigate back screen
    @objc func backBtnAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func myTickets() {
        
        let storyboard = UIStoryboard.init(name: "TicketSection", bundle: nil)
        self.navigationController?.pushViewController(storyboard.instantiateViewController(withIdentifier: "FKMyTicketsVC"), animated: true)
    }
    
    func fkWallet() {
        let storyboard = UIStoryboard.init(name: "FKWallet", bundle: nil)
        self.navigationController?.pushViewController(storyboard.instantiateViewController(withIdentifier: "FKWalletDashboard"), animated: true)
    }
    
    func myAccount() {
        let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
        self.navigationController?.pushViewController(storyboard.instantiateViewController(withIdentifier: "FKMyAccountViewController"), animated: true)
    }
    
    func settings() {
        let storyboard = UIStoryboard.init(name: "TabController", bundle: nil)
        self.navigationController?.pushViewController(storyboard.instantiateViewController(withIdentifier: "FKSettingsViewController"), animated: true)
    }
    
    func changePassword() {
        let storyboard = UIStoryboard.init(name: "Auth", bundle: nil)
        self.navigationController?.pushViewController(storyboard.instantiateViewController(withIdentifier: "FKChangePasswordVC"), animated: true)
    }
    
    func navigateToMyOrder() {
        let storyboard = UIStoryboard.init(name: "Merchandise", bundle: nil)
        let myOrderVC = storyboard.instantiateViewController(withIdentifier: "myOrderVC") as! FKMyOrderVC
        self.navigationController?.pushViewController(myOrderVC, animated: true)
        
    }
    
    //MARK:- UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.clubsTbl {
            return self.clubArray.count
        } else if tableView == self.kitTbl {
            return self.kitArray.count
        } else if tableView == self.numbersTbl {
            return self.clubArray.count
        } else if tableView == self.sponsorsTbl {
            return self.sponsorsArray.count
        } else {
            return self.titleNameArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableView {
            let cell:FKProfilePopoverCell = tableView.dequeueReusableCell(withIdentifier: "FKProfilePopoverCell", for: indexPath) as! FKProfilePopoverCell
            cell.titleLbl.text = titleNameArr[indexPath.row]
            return cell
        } else {
            
            let cell:FKDropDownCell = tableView.dequeueReusableCell(withIdentifier: "FKDropDownCell", for: indexPath) as! FKDropDownCell
            if tableView == self.clubsTbl {
                let club = self.clubArray[indexPath.row]
                cell.clubLogoBtn.addTarget(self, action: #selector(buttonAction(sender:)), for: UIControlEvents.touchUpInside)
                cell.clubLogoBtn.imageView?.contentMode = .scaleAspectFit
                cell.clubLogoBtn.clipsToBounds = true
                cell.clubLogoBtn.normalLoad(club.clubImg)
                
            } else if tableView == self.kitTbl {
                cell.kitBtn.addTarget(self, action: #selector(buttonAction(sender:)), for: UIControlEvents.touchUpInside)
                cell.kitBtn.imageView?.contentMode = .scaleAspectFit
                cell.kitBtn.clipsToBounds = true
                cell.kitBtn.tag = indexPath.row
                cell.kitBtn.setImage(self.kitArray[indexPath.row]["front"], for: .normal)//normalLoad(self.fkAvatarInfo.frontImgUrl)
                
            } else if tableView == self.numbersTbl {
                cell.numberBtn.addTarget(self, action: #selector(buttonAction(sender:)), for: UIControlEvents.touchUpInside)
                cell.numberBtn.imageView?.contentMode = .scaleAspectFit
                cell.numberBtn.clipsToBounds = true
                
            } else if tableView == self.sponsorsTbl {
                let club = self.sponsorsArray[indexPath.row]
                cell.sponsorBtn.addTarget(self, action: #selector(buttonAction(sender:)), for: UIControlEvents.touchUpInside)
                cell.sponsorBtn.imageView?.contentMode = .scaleAspectFit
                cell.sponsorBtn.normalLoad(club.clubImg)
                cell.sponsorBtn.clipsToBounds = true
            }
            return cell
        }
    }
    
    func createRotateGestureRecognizer(targetView:UIImageView) {
        let rotate = UIRotationGestureRecognizer(target: self, action: #selector(rotatedView(_:)))
        avatarScrollView.addGestureRecognizer(rotate)
    }
    
    @objc func rotatedView(_ sender: UIRotationGestureRecognizer) {
        var originalRotation = CGFloat()
        
        if sender.state == .began {
            print("begin")
            sender.rotation = lastRotation
            originalRotation = sender.rotation
            angleSet = false
        } else if sender.state == .changed {
            print("changing")
            let newRotation = sender.rotation + originalRotation
            print("newRotation",newRotation)
            imagView.transform = CGAffineTransform(rotationAngle: newRotation)
            
        } else if sender.state == .ended {
            print("end")
            lastRotation = sender.rotation
        }
    }
    
    /***
     This method will be called when any item is clicked from the dropdown and added to the keyboard
     */
    @objc func buttonAction(sender:UIButton) {
        
        if selectedDropDown == "kit" {
            
            selectedKitIndex = sender.tag
            self.fkAvatarInfo.frontImgUrl = ""
            self.fkAvatarInfo.backImgUrl = ""
            self.avatarImage.image = self.kitArray[sender.tag]["front"]
            frontImageView.image = self.kitArray[sender.tag]["front"]
            backImageView.image = self.kitArray[sender.tag]["back"]
            kitImg.image = self.kitArray[sender.tag]["front"]
            
        } else {
            
            imageTag += 1
            
            if selectedDropDown == "club" {
                self.leagueLogoImg.image = sender.currentImage
            } else if selectedDropDown == "sponsor" {
                self.sponsorImg.image = sender.currentImage
            }
            
            isEditable = true
            imagView = UIImageView(frame: CGRect(x:avatarScrollView.frame.size.width/2-50, y: avatarScrollView.frame.size.height/2-50, width: 100, height: 100))
            imagView.image = sender.currentImage
            imagView!.clipsToBounds = true
            imagView!.contentMode = .scaleAspectFit
            imagView.tag = imageTag
            avatarScrollView.addSubview(imagView!)
            upperImageArr.append(imagView)
            
            drawOutlineOnComponentAndCross()
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapOnImage(_:)))
            self.imagView.isUserInteractionEnabled = true
            let panGesture = UIPanGestureRecognizer(target: self, action: #selector(myPanAction(recognizer:)))
            self.imagView.addGestureRecognizer(panGesture)
            self.imagView.addGestureRecognizer(tapGesture)
            
        }
        
        //createRotateGestureRecognizer(targetView: imagView)
        saveBtn?.isEnabled = true
        saveBtn?.setTitleColor(UIColor.white, for: .normal)
        self.resignKeyBoardButton.isHidden = true
        popOverWidthConstraint.constant = 0
        hideAllDropDown()
    }
    
    // MARK:- Draw outline on item:::
    func drawOutlineOnComponentAndCross() {
        
        for image in upperImageArr {
            if image == imagView {
                image.borderWidth = 1.0
                image.borderColor = .red
                showCrossOnImage(component:image)
            } else {
                for item in image.subviews {
                    if item is UIButton {
                        item.removeFromSuperview()
                    }
                }
                image.borderWidth = 0
                image.borderColor = .clear
            }
        }
    }
    
    // MARK:- Tap on Image
    @objc func tapOnImage(_ sender: UITapGestureRecognizer) {
        
        imagView = sender.view as! UIImageView
        drawOutlineOnComponentAndCross()
    }
    
    func showCrossOnImage(component:UIImageView) {
        
        let crossBtn = UIButton.init(frame: CGRect(x:0,y:0,width:25,height:25))
        crossBtn.setTitle("X", for: .normal)
        crossBtn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        crossBtn.tag = component.tag + 2000
        crossBtn.addTarget(self, action: #selector(removeComponent(sender:)), for: .touchUpInside)
        crossBtn.backgroundColor = UIColor.red
        crossBtn.layer.cornerRadius = crossBtn.frame.size.width/2
        crossBtn.layer.borderColor = UIColor.red.cgColor
        component.addSubview(crossBtn)
    }
    
    @objc func removeComponent(sender:UIButton) {
        imagView.removeFromSuperview()
        
        if let index = upperImageArr.index(of:imagView) {
            upperImageArr.remove(at: index)
        }
        
        if upperImageArr.count == 0 {
            isEditable = false
            saveBtn?.isEnabled = false
            saveBtn?.setTitleColor(UIColor.lightGray, for: .normal)
            self.resignKeyBoardButton.isHidden = true
        }
        
        if upperImageArr.count > 0 {
            imagView = upperImageArr[0]
            drawOutlineOnComponentAndCross()
        }
    }
    
    // MARK:- Pan gesture Image
    @objc func myPanAction(recognizer: UIPanGestureRecognizer) {
        
        saveBtn?.isEnabled = true
        saveBtn?.setTitleColor(UIColor.white, for: .normal)
        isEditable = true
        
        imagView = recognizer.view as! UIImageView
        drawOutlineOnComponentAndCross()
        
        let xOrigin = self.avatarImageView.frame.origin.x
        let yOrigin = CGFloat(25.0) //self.avatarImageView.frame.origin.y
        let width = self.avatarImageView.frame.width
        let height = self.avatarImageView.frame.height
        
        let translation = recognizer.translation(in: self.avatarScrollView)
        
        if let myView = recognizer.view {
            myView.center = CGPoint(x: myView.center.x + translation.x, y: myView.center.y + translation.y)
            if myView.center.x <= xOrigin {
                myView.frame = CGRect(x:xOrigin - myView.frame.width/2,y:myView.frame.origin.y,width:myView.frame.size.width,height:myView.frame.size.height)
            }
            if myView.center.y <= yOrigin {
                myView.frame = CGRect(x:myView.frame.origin.x,y:yOrigin - myView.frame.height/2 ,width:myView.frame.size.width,height:myView.frame.size.height)
            }
            if myView.center.x >= xOrigin+width{
                myView.frame = CGRect(x:xOrigin+width - myView.frame.width/2 ,y:myView.frame.origin.y,width:myView.frame.size.width,height:myView.frame.size.height)
            }
            if myView.center.y >= yOrigin + height {
                myView.frame = CGRect(x:myView.frame.origin.x,y:yOrigin + height - myView.frame.height/2 ,width:myView.frame.size.width,height:myView.frame.size.height)
            }
            print("Image X Axis",myView.frame.origin.x)
            print("Image Y Axis",myView.frame.origin.y)
        }
        recognizer.setTranslation(CGPoint(x: 0, y: 0), in: self.avatarScrollView)
    }
    
    public func scrollViewDidZoom(_ scrollView: UIScrollView) {
        saveBtn?.isEnabled = true
        saveBtn?.setTitleColor(UIColor.white, for: .normal)
        isEditable = true
        print(self.imagView.frame.size)
//        if angleSet == false {
//            angleSet = true
//            imagView.transform = CGAffineTransform(rotationAngle: lastRotation)
//        }
    }
    
    func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        zoomRect.size.height = imagView.frame.size.height / scale
        zoomRect.size.width  = imagView.frame.size.width  / scale
        let newCenter = imagView.convert(center, from: avatarScrollView)
        zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
        return zoomRect
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        
        return self.imagView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if tableView == self.tableView {
            if defaults.bool(forKey: "socialLogin") {
                switch indexPath.row {
                case 0:
                    myTickets()
                    break
                case 1:
                    fkWallet()
                    break
                case 2:
                    myAccount()
                    break
                case 3:
                    settings()
                    break
                case 4:
                    navigateToStatic(type: STATIC_PAGE_TYPE.HELP)
                    //navigateToHelp()

                    break
                case 5:
                    navigateToStatic(type: STATIC_PAGE_TYPE.ABOUT)
                    break
                default:
                    break
                }
                menuOption()
            } else {
                switch indexPath.row {
                case 0:
                    myTickets()
                    break
                case 1:

                    fkWallet()
                    break
                case 2:
                    myAccount()
                    
                    break
                case 3:
                    changePassword()
                    break
                case 4:
                    settings()
                    break
                case 5:
                    navigateToStatic(type: STATIC_PAGE_TYPE.HELP)
                   // navigateToHelp()
                    break
                case 6:
                    navigateToStatic(type: STATIC_PAGE_TYPE.ABOUT)
                    break
                default:
                    break
                }
                menuOption()
            }
        }
    }
    
    func navigateToStatic(type:STATIC_PAGE_TYPE) {
        
        let storyboard = UIStoryboard.init(name: "TabController", bundle: nil)
        let staticVC = storyboard.instantiateViewController(withIdentifier: "staticWebPageVC") as! FKStaticWebPageVC
        staticVC.pageType = type
        self.navigationController?.pushViewController(staticVC, animated: true)
    }
    
   
    
    func navigateToHelp() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let walkThroughVC = storyboard.instantiateViewController(withIdentifier: "WalkThroughVC") as! WalkThroughVC
        self.present(walkThroughVC, animated: false, completion: nil)
        
        walkThroughVC.didTriggerWalkThroughAction = {
            () -> Void in
            Debug.log("walkThroughVC dismissed")
        }
    }
    
    // MARK: Button to Resign controles
    @IBAction func backgroundButtonToResignControles(_ sender: Any) {
        self.view.endEditing(true)
        self.resignKeyBoardButton.isHidden = true
        hideAllDropDown()
        popOverWidthConstraint.constant = 0
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK:- an Method to merge images and add overlay image on a imageview
    // currently its not in use will use it later
    func getMergedImage(bottomImage:UIImage,topImageView:UIImageView) -> UIImage {
        
        let topImage = topImageView.image
        let bottomImageSize = CGSize(width: avatarScrollView.frame.size.width, height: avatarScrollView.frame.size.height)
        
        UIGraphicsBeginImageContext(bottomImageSize)
        UIGraphicsBeginImageContextWithOptions(bottomImageSize, false, 0)
        UIColor.clear.setFill()
        
        let bottomImageAreaSize = CGRect(x: 0, y: 0, width: bottomImageSize.width, height: bottomImageSize.height)
        bottomImage.draw(in: bottomImageAreaSize)
        
        let topImageAreaSize = CGRect(x: topImageView.frame.origin.x, y: topImageView.frame.origin.y, width: topImageView.frame.size.width, height: topImageView.frame.size.height)
        topImage?.draw(in: topImageAreaSize, blendMode: .normal, alpha: 1.0)
        
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        let imgData =  UIImagePNGRepresentation(newImage)
        let newPngImageFromData = UIImage.init(data: imgData!)
        //        UIImageWriteToSavedPhotosAlbum(newPngImageFromData!, nil, nil, nil)
        
        UIGraphicsEndImageContext()
        return newPngImageFromData!
    }
    
    /***
     This method is used to create image from  text which is added from textfield.
     it will return the image which is created from the entered text.
     */
    func imageFrom(text: String , size:CGSize) -> UIImage {
        
        let renderer = UIGraphicsImageRenderer(size: size)
        let img = renderer.image { ctx in
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = .center
            
            let attrs = [NSAttributedStringKey.font: UIFont(name: "Gilroy-ExtraBold", size: 20)!, NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.paragraphStyle: paragraphStyle]
            text.draw(with: CGRect(x: 0, y: 0, width: size.width, height: size.height), options: .usesLineFragmentOrigin, attributes: attrs, context: nil)
        }
        return img
    }
    
    /***
     This method shows editor and opens the keyboard to enter the text.
     */
    func showNameEditor(placeholder:String) {
        
        hideAllDropDown()
        self.resignKeyBoardButton.isHidden = false
        let textField = UITextField.init(frame: CGRect(x:self.view.frame.size.width/2-75,y:self.view.frame.size.height/2-80,width:150,height:30))
        
        if placeholder == "NAME OF AVATAR" {
            textField.tag = 3
        } else {
            textField.tag = 4
            textField.keyboardType = .numberPad
        }
        textField.placeholder = placeholder
        textField.textAlignment = .center
        textField.delegate = self
        textField.backgroundColor = .white
        textField.font = UIFont.init(name: "Gilroy-Light", size: 15.0)
        
        self.view.addSubview(textField)
        textField.becomeFirstResponder()
    }
    
    /***
     This method is currently is use to create the image from the scrollview
     the image we edit we get that edited image from here.
     */
    func getEditedImageFromContext() -> UIImage {
        
        let subView: UIView? = avatarScrollView
        UIGraphicsBeginImageContextWithOptions((subView?.bounds.size)!, false, 0.0)
        let context: CGContext? = UIGraphicsGetCurrentContext()
        subView?.layer.render(in: context!)
        let snapshotImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let snapshotImageView = UIImageView(image: snapshotImage)
        
        return snapshotImageView.image!
    }
}
