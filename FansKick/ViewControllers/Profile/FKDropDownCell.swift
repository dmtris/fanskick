//
//  FKDropDownCell.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 11/24/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKDropDownCell: UITableViewCell {

    @IBOutlet weak var clubLogoBtn: UIButton!
    @IBOutlet weak var kitBtn: UIButton!
    @IBOutlet weak var numberBtn: UIButton!
    @IBOutlet weak var sponsorBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
