//
//  FKPreviousResultCell.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 10/21/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKPreviousResultCell: UITableViewCell {

    @IBOutlet weak var homeTeamName :UILabel?
    @IBOutlet weak var awayTeamName :UILabel?
    @IBOutlet weak var homeTeamLogo :UIImageView?
    @IBOutlet weak var awayTeamLogo :UIImageView?
    @IBOutlet weak var goalLbl :UILabel?
    @IBOutlet weak var timeLbl :UILabel?
    @IBOutlet weak var stadiumName :UILabel?


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
