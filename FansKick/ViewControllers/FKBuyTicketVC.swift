//
//  FKBuyTicketVC.swift
//  FansKick
//
//  Created by Sunil Verma on 11/7/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit
import CoreData
class FKBuyTicketVC: UIViewController {
    
    @IBOutlet weak var noofPersonBtn : UIButton?
    @IBOutlet weak var amountLbl : UILabel?
    @IBOutlet weak var teamNameLbl: UILabel!
    @IBOutlet weak var stadiumName: UILabel!
    @IBOutlet weak var fixtureDate: UILabel!
    var fixture : FKFixtureListModel?
    
    var numberOfPerson = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "TICKET"
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
    }
    
    // use to navigate back screen
    @IBAction func backBtnAction() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        teamNameLbl.text = "\(fixture!.club) Vs \(fixture!.apponent)"
        stadiumName.text = fixture!.stadium
        fixtureDate.text = fixture?.start_time.dateFromUTC()?.dateString(DATE_TIME_FORMAT)
    }
    
    @IBAction func  numberOfPersonBtnAction() {
        let data = ["1","2","3","4","5","6","7","8","9","10"]
        RPicker.sharedInstance.selectOption(title: "No. of persons", dataArray: data, selectedIndex: 0) { (selectedText, index) in
            
            if !selectedText.isEmpty {
                self.noofPersonBtn?.setTitle(selectedText, for: UIControlState.normal)
                self.numberOfPerson = Int(selectedText)!
                let price = 0 * Int(selectedText)!
                self.amountLbl?.text = "RM \(price)"
            }
        }
    }
    
    @IBAction func buyTicketsAction() {
        
        if isAllFieldVerified() {
         //   self.checkCreditBalence()
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.perform(#selector(saveDataInLocalDB), with: nil, afterDelay: 2.0)
            
        }
    }
    
    func isAllFieldVerified() -> Bool {
        
        if numberOfPerson == 0{
            MessageView.showMessage(message: "Please select number of tickets", time: 5.0, verticalAlignment: .bottom)
            return false
        }
        
        return true
    }
    
    
    // FKWallet overrides
    
    
    func checkCreditBalence() {
        
        if defaults.object(forKey: SENZPAY_USER_ID) != nil{
            apiCallToGetCredit()
        }else{
            self.apiCallForLogin()
        }
    }
    
    func apiCallForLogin() {
        
        let fkWalletStoryboard = UIStoryboard(name: "FKWallet", bundle: nil)
        let fkWalletLogin = fkWalletStoryboard.instantiateViewController(withIdentifier: "FKWalletLoginVC") as! FKWalletLoginVC
        let navController = UINavigationController(rootViewController: fkWalletLogin)
        self.present(navController, animated: true, completion: nil)
        fkWalletLogin.completionBlock { (success, response) in
            
            if success! {
                
                let status = response.validatedValue(SENZPAY_STATUS, expected:"" as AnyObject ) as! String
                if status == "false" {
                    // false case error messsage
                    MessageView.showMessage(message: response["msg"] as! String, time: 5.0, verticalAlignment: .bottom)
                } else {
                    // handle true response
                    
                    let availbleBalance = response[SENZPAY_BALANCE] as? String
                    
                    if ((Double("\(availbleBalance ?? "0")")!).isLess(than: Double("\(availbleBalance ?? "0")")!)) {
                        //  Not sufficient balance
                        MessageView.showMessage(message: "You have not enough balance in your FKWallet, Please recharge your FKWallet to continue..", time: 5.0, verticalAlignment: .bottom)
                    } else {
                        self.apiCallForPayment()
                    }
                }
            }
        }
    }
    
    
    func apiCallToGetCredit() {
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        MBProgressHUD.showAdded(to: appDel.window!, animated: true)
        
        var params = Dictionary<String,Any>()
        let email =  AESCrypto.aes256DecryptString(defaults.object(forKey: SENZPAY_EMAIL_DEFAULT) as! String, withKey: crypto256Key)
        let password =  AESCrypto.aes256DecryptString(defaults.object(forKey: SENZPAY_PASSWORD_DEFAULT) as! String, withKey: crypto256Key)
        
        params[SENZPAY_EMAIL] = email!
        params[SENZPAY_PASSWORD] = password!
        
        FKSenzPayHelper.senzpayRequest(params: params, method: .get, apiName: "user_login.php") { (result, error, responseCode) in
            MBProgressHUD.hide(for: appDel.window!, animated: true)
            let responseData = result as! Dictionary<String,AnyObject>
            let status      = responseData.validatedValue(SENZPAY_STATUS, expected:"" as AnyObject ) as! String
            if status == "false" {
                // false case error messsage
                MessageView.showMessage(message: responseData["msg"] as! String, time: 5.0, verticalAlignment: .bottom)
            } else {
                // handle true response
                
                let userID = responseData.validatedValue(SENZPAY_USER_ID, expected:"" as AnyObject ) as! String
                
                defaults.set(AESCrypto.aes256EncryptString(userID, withKey: crypto256Key), forKey: SENZPAY_USER_ID)
                
                let availbleBalance = responseData[SENZPAY_BALANCE] as? String
                
                let price = 0 * self.numberOfPerson
                
                if ((Double("\(availbleBalance ?? "0")")!).isLess(than: Double("\(price)")!)) {
                    //  Not sufficient balance
                    MessageView.showMessage(message: "You have not enough balance in your FKWallet, Please recharge your FKWallet to continue..", time: 5.0, verticalAlignment: .bottom)
                    
                } else {
                    self.apiCallForPayment()
                }
            }
        }
    }
    
    func apiCallForPayment() {
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        MBProgressHUD.showAdded(to: appDel.window!, animated: true)
        var params = Dictionary<String,Any>()
        let email =  AESCrypto.aes256DecryptString(defaults.object(forKey: SENZPAY_EMAIL_DEFAULT) as! String, withKey: crypto256Key)
        let password =  AESCrypto.aes256DecryptString(defaults.object(forKey: SENZPAY_PASSWORD_DEFAULT) as! String, withKey: crypto256Key)
        let userID =  AESCrypto.aes256DecryptString(defaults.object(forKey: SENZPAY_USER_ID) as! String, withKey: crypto256Key)
        
        params[SENZPAY_EMAIL] = email!
        params[SENZPAY_PASSWORD] = password!
        params[SENZPAY_USER_ID] = userID!
        
        let price = 0 * self.numberOfPerson
        
        params["amt"] = "\(price)"
        params["item_code"] = "\(fixture?.id ?? "")"
        params["item_desc"] = "\(fixture?.club ?? "") Vs \(fixture?.apponent ?? "")"
        params ["toemail"] = "hi@senzpay.asia"
        params ["touid"] = "2"
        params[SENZPAY_SKIP_SMS_ALERT] = "1"  //0 – To send the SMS. 1 – To Skip.
        
        FKSenzPayHelper.senzpayRequest(params: params, method: .post, apiName: "user_make_payment.php") { (result, error, responseCode) in
            MBProgressHUD.hide(for: appDel.window!, animated: true)
            let responseData = result as! Dictionary<String,AnyObject>
            
            let status        = responseData.validatedValue(SENZPAY_STATUS, expected: 0 as NSNumber) as! Bool
            
            if !status {
                // false case error messsage
                MessageView.showMessage(message: responseData["msg"] as! String, time: 5.0, verticalAlignment: .bottom)
            } else {
                // handle true response
                MessageView.showMessage(message: responseData["msg"] as! String, time: 5.0, verticalAlignment: .bottom)
              //  self.saveDataInLocalDB(transactionId: responseData.validatedValue("payment_id", expected: "" as AnyObject) as! String)
            }
        }
    }
    
   // func saveDataInLocalDB(transactionId:String){
    @objc func saveDataInLocalDB(){
        
        MBProgressHUD.hide(for: self.view, animated: true)

        let managedContext =   FKCoreDBHelper.coreDBHelper.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "FKTicket",
                                                in: managedContext)!
        
        let ticket = NSManagedObject(entity: entity,
                                     insertInto: managedContext) as! FKTicket
        
        ticket.apponentName = self.fixture?.apponent
        ticket.numberOfTickets = "\(numberOfPerson)"
        ticket.bookingDate = Date().UTCStringFromDate()
        ticket.matchDate = fixture?.start_time
        ticket.singleTicketPrice = "0"
        ticket.stadiumName = fixture?.stadium
        ticket.teamName = fixture?.club
        
        let price = 0 * self.numberOfPerson
        
        ticket.totalPrice = "\(price)"
        ticket.transactionId = "\(Date.timeIntervalBetween1970AndReferenceDate)"
        let qrCode = "\(self.fixture?.id ?? "")/\(ticket.transactionId ?? "")"
        
        let img =  UIImage.mdQRCode(for: qrCode, size: 120, fill: GREEN_COLOR)
        ticket.qrCode  = UIImageJPEGRepresentation(img!, 0.5)
        
        
        let currentUser = FKCoreDBHelper.coreDBHelper.getCurrentUser()
        
        currentUser?.addToUserToTicket(ticket)
        
        FKCoreDBHelper.coreDBHelper.saveContext()
        
        MessageView.showMessage(message: "Your ticket booked successfully.", time: 5.0, verticalAlignment: .bottom)

        let buyTicketVC = self.storyboard?.instantiateViewController(withIdentifier: "FKTicketDetailVC") as! FKTicketDetailVC
        buyTicketVC.ticketInfo = ticket
        self.navigationController?.pushViewController(buyTicketVC, animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
