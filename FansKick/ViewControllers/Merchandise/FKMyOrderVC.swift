//
//  FKMyOrderVC.swift
//  FansKick
//
//  Created by Sunil Verma on 16/11/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit
import CarbonKit

class FKMyOrderVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    @IBOutlet weak var tblView:UITableView?
    var dataSourceArray = [FKMyOrdersInfo] ()
    var page = PAGE()
    var refresh: CarbonSwipeRefresh?
    
    @IBOutlet weak var noOrderLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        tblView?.tableFooterView = UIView.init()
        self.navigationItem.title  = "MY ORDER"
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]

        callApiToGetMyOrders()
    }
    
    // MARK:- Get My Orders
    func callApiToGetMyOrders() {
        
        let params  = Dictionary<String,Any>()
        let apiname = "orders/my_orders"
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        ServiceHelper.request(params: params, method: .get, apiName: apiname) { (result, error, statusCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            print(result ?? "no result")
            
            if (result != nil) {
                let responseCode = result?["responseCode"] as! NSInteger
                if (error == nil) {
                    if responseCode == 200 {
                        let results = result as! Dictionary<String, AnyObject>
                        
                        if let data = results["order_list"] {
                            if self.page.startIndex == 1 {
                                self.dataSourceArray.removeAll()
                            }
                            self.dataSourceArray.append(contentsOf: FKMyOrdersInfo.orderList(data: data as! Array<Dictionary<String, AnyObject>>))
                            self.page.totalPage = result!["total_pages"] as! Int
                            self.tblView?.reloadData()
                            
                            if self.dataSourceArray.count == 0 {
                                self.noOrderLbl.isHidden = false
                                self.tblView?.isHidden = true
                            } else {
                                self.noOrderLbl.isHidden = true
                                self.tblView?.isHidden = false
                            }
                            
                        } else {
                            MessageView.showMessage(message: result!["responseMessage"] as! String, time: 5.0, verticalAlignment: .bottom)
                        }
                    }
                }
            }
        }
    }
    
    // use to navigate back screen
    @objc func backBtnAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: UITableView Overrides
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     return dataSourceArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: "FKOrderCell") as! FKOrderCell
        let order = self.dataSourceArray[indexPath.row]
        cell.orderIdLbl?.text = "Order No: \((order.id))"
        cell.totalPriceLbl?.text = "RM \(order.amountAfterDiscount)"
        cell.orderStatusLbl?.text = "Status: \((order.status))"
        cell.orderDateLbl?.text = "Placed On: \((order.orderDate.dateFromUTC()?.dateString())!)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let orderDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "FKOrderDetailVC") as! FKOrderDetailVC
        orderDetailVC.orderDetail   = self.dataSourceArray[indexPath.row]

        self.navigationController?.pushViewController(orderDetailVC, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
