//
//  FKMerchantClubCell.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 11/11/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKMerchantClubCell: UITableViewCell {

    @IBOutlet weak var clubBtn: UIButton!
    @IBOutlet weak var clubBtn2: UIButton!
    @IBOutlet weak var leftClubImage: UIImageView!
    @IBOutlet weak var rightClubImg: UIImageView!
    @IBOutlet weak var leftClubName: UILabel!
    @IBOutlet weak var leftclubState: UILabel!
    @IBOutlet weak var rightClubName: UILabel!
    @IBOutlet weak var rightClubState: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
