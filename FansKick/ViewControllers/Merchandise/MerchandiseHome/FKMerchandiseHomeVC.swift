//
//  FKMerchandiseHomeVC.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 11/11/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKMerchandiseHomeVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageDots: UIPageControl!
    @IBOutlet weak var popOverWidthConstraint: NSLayoutConstraint!
    var indexPathRow = 1
    @IBOutlet weak var moreView: UIView!
    var barButton : MJBadgeBarButton!
    
    @IBOutlet weak var headerViewHeightConstraints: NSLayoutConstraint?
   
    
    var isClubSelected  = false
    var clubsArray      = [String]()
    var featuredArray   = [FKProductInfo]()
    var clubArray       = [FKListInfo]()
    var categoryArray   = [FKListInfo]()
    var offersArray     = [FKSpecialOfferInfo]()
    var objImageArray   = [FKProductImageInfo]()
    var page = PAGE()
    var timer: Timer? = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        page.startIndex  = 1
        page.pageSize = 10
        popOverWidthConstraint.constant = -210
        
        initialise()
    }
    
    func initialise() {
        
        self.navigationItem.title = "MERCHANDISE"
        let cart = UIButton.init(type: UIButtonType.custom)
        cart.titleLabel?.font = UIFont.init(name: fanskickFontIcons, size: 22)
        cart.setTitle("h", for: .normal)
        cart.frame = CGRect.init(x: 0, y: 0, width: 24, height: 24)
        cart.addTarget(self, action: #selector(cartAction), for: UIControlEvents.touchUpInside)
        self.barButton = MJBadgeBarButton()
        self.barButton.setup(customButton: cart)
        self.barButton.badgeValue = ""
        
        let moreBtn = UIButton.init(type: UIButtonType.custom)
        moreBtn.titleLabel?.font = UIFont.init(name: fanskickFontIcons, size: 22)
        moreBtn.setTitle("e", for: .normal)
        moreBtn.frame = CGRect.init(x: 0, y: 0, width: 24, height: 24)
        moreBtn.addTarget(self, action: #selector(moreAction), for: UIControlEvents.touchUpInside)
        
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        
        let moreBar = UIBarButtonItem.init(customView: moreBtn)
        self.navigationItem.rightBarButtonItems = [moreBar,self.barButton]
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
    }
    
    // use to navigate back screen
    @objc func backBtnAction() {
        
        if isClubSelected {
            isClubSelected = false
            tableView.reloadData()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if DeviceType.IS_IPAD || DeviceType.IS_IPAD_PRO {
            
            headerViewHeightConstraints?.constant = 500
        }
        
        isClubSelected = false
        clubApiCall()
        featuredApiCall()
        callApiToGetSpecialOffers()
    }
    
    @objc func moreAction() {
        
        if popOverWidthConstraint.constant == 0 {
            popOverWidthConstraint.constant = -210
        } else {
            popOverWidthConstraint.constant = 0
        }
        
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        popOverWidthConstraint.constant = -210
        self.stopCarousal()
    }
    
    func callApiToGetSpecialOffers() {
        
        var params  =   Dictionary<String,Any>()
        params["page"]  =   "\(page.startIndex)"
        params["per_page"]  =   "\(page.pageSize)"
        
        ServiceHelper.request(params: params, method: .get, apiName: "products/special_offers", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                
                if let data = results["special_offers"] {
                    self.offersArray.removeAll()
                    self.offersArray.append(contentsOf: FKSpecialOfferInfo.offersList(data: data as! Array<Dictionary<String, AnyObject>>))
                    
                    if self.offersArray.count > 0 {
                        self.startCarousal()
                    } else {
                        self.stopCarousal()
                    }
                    
                    let appDelegate = UIApplication.shared.delegate as? AppDelegate
                    appDelegate?.cartCount = results.validatedValue("cart_products_count", expected: "" as AnyObject) as! String
                    
                    self.barButton.badgeValue = (appDelegate?.cartCount)!
                    self.barButton.badgeOriginX = 20.0
                    self.barButton.badgeOriginY = -4
                    
                    self.pageDots.numberOfPages = self.offersArray.count
                    self.collectionView.reloadData()
                } else {
                }
            }
        }
    }
    
    @objc func cartAction() {
        
        let productListVC = self.storyboard?.instantiateViewController(withIdentifier: "FKCartListVC") as! FKCartListVC
        self.navigationController?.pushViewController(productListVC, animated: true)
    }
    
    @IBAction func myFevoriteBtnAction() {
        moreAction()
        
        let favoriteVC = self.storyboard?.instantiateViewController(withIdentifier: "myFevoriteVC") as! FKMyFevoriteVC
        self.navigationController?.pushViewController(favoriteVC, animated: true)
    }
    
    @IBAction func myOrderBtnAction() {
        moreAction()
        let myOrderVC = self.storyboard?.instantiateViewController(withIdentifier: "myOrderVC") as! FKMyOrderVC
        self.navigationController?.pushViewController(myOrderVC, animated: true)
    }
    
    //MARK: Featured api Call
    func featuredApiCall() {
        
        var params          = Dictionary<String,Any>()
        params["page"] = "\(page.startIndex)"
        params["per_page"] = "\(page.pageSize)"
        
        ServiceHelper.request(params: params, method: .get, apiName: "products/feature_products", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                
                if let data = results["products"] {
                    self.featuredArray.removeAll()
                    self.featuredArray.append(contentsOf: FKProductInfo.productsList(data: data as! Array<Dictionary<String, AnyObject>>))
                    print(self.featuredArray)
                    self.tableView.reloadData()
                    
                } else {
                }
            }
        }
    }
    
    //MARK:- Club List APi
    func clubApiCall() {
        
        var params          = Dictionary<String,Any>()
        params["page"]      = "1"
        params["per_page"]  = "500"
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: .get, apiName: "fixtures/club_list", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                
                if let data = results["league_list"] {
                    self.clubArray.removeAll()
                    self.clubArray.append(contentsOf: FKListInfo.list(data: data as! Array<Dictionary<String, AnyObject>>))
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    //MARK:- Category List APi
    func categoryApiCallWithClubId(clubId:String) {
        
        var params = Dictionary<String,Any>()
        var id  = Dictionary<String,Any>()
        id["id"] = clubId
        
        params["club"] = id
        print(params)
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: .post, apiName: "products/category_list", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil {
                if let data = result!["category_list"] {
                    self.categoryArray.removeAll()
                    self.categoryArray.append(contentsOf: FKListInfo.categoriesList(data: data as! Array<Dictionary<String, AnyObject>>))
                    self.tableView.reloadData()
                }
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:  UICollectionView Overrides
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return offersArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        popOverWidthConstraint.constant = -210
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FKMerchantCollectionCell", for: indexPath) as! FKMerchantCollectionCell
        let offer = offersArray[indexPath.row]
        cell.bannerImage.normalLoad(offer.sponsorImage)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var size = CGSize()
        size.width  = self.view.frame.size.width
        size.height = self.view.frame.size.height
        
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let offer = offersArray[indexPath.row]
        
        print("clubId: ",offer.id)
        
        let productListVC = self.storyboard?.instantiateViewController(withIdentifier: "productListVC") as! FKProductListVC
        productListVC.productId = offer.id
        productListVC.isSpecialOffer = true
        self.navigationController?.pushViewController(productListVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var headerView: FKFixtureLableCell? = tableView.dequeueReusableCell(withIdentifier: "FKFixtureLableCell") as? FKFixtureLableCell
        if (headerView == nil) {
            headerView = FKFixtureLableCell(style:UITableViewCellStyle.subtitle, reuseIdentifier:"FKFixtureLableCell")
        }
        return headerView;
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if collectionView.indexPathsForVisibleItems.count > 0{
            let indexpath =  collectionView.indexPathsForVisibleItems.first
            self.pageDots.currentPage = (indexpath?.row)!
            indexPathRow = (indexpath?.row)!
            
        }
    }
    
    //MARK:- UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            if isClubSelected {
                let count = Int(ceil(Float(categoryArray.count)/2))
                return count
            } else {
                let count = Int(ceil(Float(clubArray.count)/2))
                return count
            }
        }
        print(featuredArray.count)
        let count = Int(ceil(Float(featuredArray.count)/2))
        print("featured array count:>> ",count)
        return count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        popOverWidthConstraint.constant = -210
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
        
        if indexPath.section == 0 {
            if isClubSelected {
                let categoryCell:FKCategoriesCell = tableView.dequeueReusableCell(withIdentifier: "FKCategoriesCell", for: indexPath) as! FKCategoriesCell
                categoryCell.categoryBtn1.addTarget(self, action: #selector(categoriesBtn(sender:)), for: .touchUpInside)
                categoryCell.categoryBtn2.addTarget(self, action: #selector(categoriesBtn(sender:)), for: .touchUpInside)
                
                let fkClubListForLeft = self.categoryArray[indexPath.row*2]
                categoryCell.categoryBtn1.setTitle(fkClubListForLeft.name, for: .normal)
                categoryCell.categoryBtn1.tag = indexPath.row*2 // left button tag
                categoryCell.categoryBtn2.tag = indexPath.row*2+1 // Right button tag

                let count = Int(ceil(Float(categoryArray.count)/2))
                
                if indexPath.row != count-1
                {
                    let fkClubListForRight = self.categoryArray[indexPath.row*2+1]
                    categoryCell.categoryBtn2.setTitle(fkClubListForRight.name, for: .normal)
                } else {
                    if categoryArray.count % 2 == 0 {
                        let fkClubListForRight = self.categoryArray[indexPath.row*2+1]
                        categoryCell.categoryBtn2.setTitle(fkClubListForRight.name, for: .normal)
                        categoryCell.categoryBtn2.isHidden = false
                    } else {
                        categoryCell.categoryBtn2.isHidden = true
                    }
                }
                return categoryCell
            } else {
                let merchantCell:FKMerchantClubCell = tableView.dequeueReusableCell(withIdentifier: "FKMerchantClubCell", for: indexPath) as! FKMerchantClubCell
                let fkClubListForLeft = self.clubArray[indexPath.row*2]
                
                merchantCell.leftClubName.text = fkClubListForLeft.name
                merchantCell.leftClubImage.normalLoad(fkClubListForLeft.clubImg)
                merchantCell.clubBtn.addTarget(self, action: #selector(clubBtn(sender:)), for: .touchUpInside)
                merchantCell.clubBtn2.addTarget(self, action: #selector(clubBtn(sender:)), for: .touchUpInside)
                
                merchantCell.clubBtn.tag = indexPath.row*2
                merchantCell.clubBtn2.tag = indexPath.row*2+1
                
                let count = Int(ceil(Float(clubArray.count)/2))
                
                if indexPath.row != count-1
                {
                    let fkClubListForRight = self.clubArray[indexPath.row*2+1]
                    merchantCell.rightClubName.text = fkClubListForRight.name
                    merchantCell.rightClubImg.normalLoad(fkClubListForRight.clubImg)
                    //                    merchantCell.clubBtn2.tag = 1000+indexPath.row
                } else {
                    if clubArray.count % 2 == 0 {
                        let fkClubListForRight = self.clubArray[indexPath.row*2+1]
                        merchantCell.rightClubName.text = fkClubListForRight.name
                        merchantCell.rightClubImg.normalLoad(fkClubListForRight.clubImg)
                        //                        merchantCell.clubBtn2.addTarget(self, action: #selector(clubBtn(sender:)), for: .touchUpInside)
                        //                        merchantCell.clubBtn2.tag = 1000+indexPath.row
                        merchantCell.clubBtn2.isHidden = false
                        
                    } else {
                        merchantCell.clubBtn2.isHidden = true
                    }
                }
                return merchantCell
            }
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FKFeaturesCell", for: indexPath) as! FKFeaturesCell
            let featured = self.featuredArray[indexPath.row*2]
            cell.featuredBtn.addTarget(self, action: #selector(featuredBtn(sender:)), for: .touchUpInside)
            cell.featuredBtn.indexPath = indexPath
            cell.featuredBtn.tag = 1
            cell.featuredBtn.normalLoad(featured.productImage)
            let count = Int(ceil(Float(featuredArray.count)/2))
            
            if indexPath.row != count-1 {
                let featured2 = self.featuredArray[indexPath.row*2 + 1]
                cell.featuredBtn2.addTarget(self, action: #selector(featuredBtn(sender:)), for: .touchUpInside)
                cell.featuredBtn2.tag = 2
                cell.featuredBtn2.indexPath = indexPath
                cell.featuredBtn2.normalLoad(featured2.productImage)
            } else {
                if featuredArray.count % 2 == 0 {
                    let featured2 = self.featuredArray[indexPath.row*2 + 1]
                    cell.featuredBtn2.addTarget(self, action: #selector(featuredBtn(sender:)), for: .touchUpInside)
                    cell.featuredBtn2.tag = 2
                    cell.featuredBtn2.indexPath = indexPath
                    cell.featuredBtn2.normalLoad(featured2.productImage)
                    cell.featuredBtn2.isHidden = false
                    
                } else {
                    cell.featuredBtn2.isHidden = true
                }
            }
            return cell
        }
    }
    
    @objc func clubBtn(sender:UIButton) {
        
        isClubSelected = true
        print("sender tag:> ",sender.tag)
        let fkClub = self.clubArray[sender.tag]
        let clubId = fkClub.id
        print("club name:",fkClub.name,"clubId is:> ",clubId)
        self.categoryApiCallWithClubId(clubId:clubId)
        tableView.reloadData()
    }
    
    @objc func categoriesBtn(sender:UIButton) {
        
        let fkClub = self.categoryArray[sender.tag]
        let clubId = fkClub.categoryId
        let productListVC = self.storyboard?.instantiateViewController(withIdentifier: "productListVC") as! FKProductListVC
        productListVC.productId = clubId
        productListVC.isSpecialOffer = false
        self.navigationController?.pushViewController(productListVC, animated: true)
        isClubSelected = false
    }
    
    @objc func featuredBtn(sender:IndexPathButton) {
        
        var featured: FKProductInfo?
        if sender.tag == 1{
            featured = self.featuredArray[(sender.indexPath?.row)!*2]
        } else {
            featured = self.featuredArray[(sender.indexPath?.row)!*2 + 1]
        }
        
        let productDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "productDetailVC") as! FKProductDetailVC
        productDetailVC.fkProduct = featured
        self.navigationController?.pushViewController(productDetailVC, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 1 {
            return 120
        } else {
            return 70
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        // UITableView only moves in one direction, y axis
        if scrollView == tableView {
            
            let currentOffset = scrollView.contentOffset.y
            let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
            
            // Change 10.0 to adjust the distance from bottom
            if maximumOffset - currentOffset <= 10.0 {
                
                if page.totalPage > page.startIndex{
                    page.startIndex = page.startIndex+1;
                    self.featuredApiCall()
                }
            }
        }
    }
    
    func startCarousal() {

        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 5.0,
                                         target: self,
                                         selector: #selector(scrollNextPage),
                                         userInfo: nil,
                                         repeats: true)
        }
    }
    
    func stopCarousal() {
        timer?.invalidate()
        timer = nil
    }
    
    @objc func scrollNextPage() {
        
        if indexPathRow < offersArray.count-1 {
            self.collectionView?.scrollToItem(at:IndexPath(item: indexPathRow, section: 0), at: .right, animated: true)
            indexPathRow = indexPathRow+1
        } else {
            indexPathRow = 0
            self.collectionView?.scrollToItem(at:IndexPath(item: indexPathRow, section: 0), at: .left, animated: true)
        }
    }
}
