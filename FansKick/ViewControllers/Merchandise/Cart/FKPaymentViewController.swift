//
//  FKPaymentViewController.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 11/14/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKPaymentViewController: UIViewController {

    @IBOutlet weak var totalAmountLbl: UILabel!
    var cartListArray = [FKCartInfo]()
    var shippingAddressId = ""
    var totalAmountToBePaid = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.totalAmountLbl.text = "RM \(totalAmountToBePaid)"
        
        self.navigationItem.title = "MAKE PAYMENT"
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
    }

    // use to navigate back screen
    @objc func backBtnAction() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func apiCallForPayment()  {
        
        MBProgressHUD.showAdded(to: appDelegate.window!, animated: true)
        var params = Dictionary<String,Any>()
        let email =  AESCrypto.aes256DecryptString(defaults.object(forKey: SENZPAY_EMAIL_DEFAULT) as! String, withKey: crypto256Key)
        let password =  AESCrypto.aes256DecryptString(defaults.object(forKey: SENZPAY_PASSWORD_DEFAULT) as! String, withKey: crypto256Key)
        let userID =  AESCrypto.aes256DecryptString(defaults.object(forKey: SENZPAY_USER_ID) as! String, withKey: crypto256Key)
        
        params[SENZPAY_EMAIL] = email!
        params[SENZPAY_PASSWORD] = password!
        params[SENZPAY_USER_ID] = userID!
        params["amt"] = totalAmountToBePaid
        params["item_code"] = self.getItemsIds()
        params["item_desc"] = self.getItemsNames()
        params ["toemail"] = "hi@senzpay.asia"
        params ["touid"] = "2"
        params[SENZPAY_SKIP_SMS_ALERT] = "1"  //0 – To send the SMS. 1 – To Skip.
        
        FKSenzPayHelper.senzpayRequest(params: params, method: .post, apiName: "user_make_payment.php") { (result, error, responseCode) in
            MBProgressHUD.hide(for: self.appDelegate.window!, animated: true)
            let responseData = result as! Dictionary<String,AnyObject>
            
            let status        = responseData.validatedValue(SENZPAY_STATUS, expected: 0 as NSNumber) as! Bool
            
            if !status {
                // false case error messsage
                MessageView.showMessage(message: responseData["msg"] as! String, time: 5.0, verticalAlignment: .bottom)
            } else {
                // handle true response
                self.callApiToMakePayment(tractionId:  responseData.validatedValue("payment_id", expected: "" as AnyObject) as! String)
                
            }
        }
    }
    
    func getItemsIds() -> String {
        
        var ids = ""
        var isFirst = true
        
        for cart in cartListArray {
            if !isFirst {
                ids.append("//")
                isFirst = false
            }
            ids.append(" \(cart.productId)")
        }
        return ids
    }
    
    func getItemsNames() -> String {
        var name = ""
        var isFirst = true
        for cart in cartListArray {
            if !isFirst{
                name.append("//")
                isFirst = false
            }
            name.append(" \(cart.product?.name ?? "")")
        }
        return name
    }
    func callApiToMakePayment(tractionId:String) {

        let shippingId = defaults.value(forKey: "shippingAddress")!
        var product = Dictionary<String, Any>()
        product["shipping_address_id"] = shippingId
        product["transaction_id"]  = tractionId
        let params  = ["order" : product]
        
        MBProgressHUD.showAdded(to: appDelegate.window!, animated: true)
        ServiceHelper.request(params: params, method: .post, apiName: "orders/buy_cart") { (result, error, responseCode) in
            MBProgressHUD.hide(for: self.appDelegate.window!, animated: true)
            
            if (error == nil) {
                let results = result as! Dictionary<String, AnyObject>

                if responseCode == 200 {
                    

                    if let data = results["order"] {
                        let user = data as! Dictionary<String, Any>
                        let writeReviewVc = self.storyboard?.instantiateViewController(withIdentifier: "FKWriteAReviewVC") as! FKWriteAReviewVC
                        writeReviewVc.cartListArray = self.cartListArray
                        writeReviewVc.orderId = user.validatedValue("id", expected: "" as AnyObject) as! String
                        self.navigationController?.pushViewController(writeReviewVc, animated: true)
                    }else{
                        if let data = results[kResponseMsg] {
                            MessageView.showMessage(message: data as! String, time: 5.0, verticalAlignment: .bottom)
                        }
                    }
                    
                } else {
                    
                    if let data = results[kResponseMsg] {
                        MessageView.showMessage(message: data as! String, time: 5.0, verticalAlignment: .bottom)
                    }
                }
            } else {
                let results = result as! Dictionary<String, AnyObject>
                if let data = results[kResponseMsg]  {
                    MessageView.showMessage(message: data as! String, time: 5.0, verticalAlignment: .bottom)
                }
            }
        }
    }
    
    func getTotalPrice() -> String {
        
        var price:Double = 0
        for cart in self.cartListArray {
            price = price +  Double(cart.price)!
        }
        return "RM \(String(describing: price))"
    }
    
    @IBAction func confirmBtn(_ sender: Any) {
        
        if defaults.object(forKey: SENZPAY_USER_ID) != nil{
            apiCallToGetCredit()
        }else{
            self.apiCallForLogin()
        }
}
    
    func apiCallForLogin()  {
        
        let fkWalletStoryboard = UIStoryboard(name: "FKWallet", bundle: nil)
        let fkWalletLogin = fkWalletStoryboard.instantiateViewController(withIdentifier: "FKWalletLoginVC") as! FKWalletLoginVC
        let navController = UINavigationController(rootViewController: fkWalletLogin)
        self.present(navController, animated: true, completion: nil)
        fkWalletLogin.completionBlock { (success, response) in
            if success! {

            let status      = response.validatedValue(SENZPAY_STATUS, expected:"" as AnyObject ) as! String
            if status == "false" {
                // false case error messsage
                MessageView.showMessage(message: response["msg"] as! String, time: 5.0, verticalAlignment: .bottom)
            } else {
                // handle true response
                
                let availbleBalance = response[SENZPAY_BALANCE] as? String
                
//                if ((Double("\(availbleBalance ?? "0")")!).isLess(than: Double("\(self.totalAmountToBePaid)")!)) {
//                    //  Not sufficient balance
//                    MessageView.showMessage(message: "You have not enough balance in your FKWallet, Please recharge your FKWallet to continue..", time: 5.0, verticalAlignment: .bottom)
//                }else{
                    self.apiCallForPayment()
            //    }
        }

    }
        }
}
    
    func apiCallToGetCredit() {
        
        MBProgressHUD.showAdded(to: appDelegate.window!, animated: true)
        var params = Dictionary<String,Any>()
        let email =  AESCrypto.aes256DecryptString(defaults.object(forKey: SENZPAY_EMAIL_DEFAULT) as! String, withKey: crypto256Key)
        let password =  AESCrypto.aes256DecryptString(defaults.object(forKey: SENZPAY_PASSWORD_DEFAULT) as! String, withKey: crypto256Key)
        
        params[SENZPAY_EMAIL] = email!
        params[SENZPAY_PASSWORD] = password!
        
        FKSenzPayHelper.senzpayRequest(params: params, method: .get, apiName: "user_login.php") { (result, error, responseCode) in
            MBProgressHUD.hide(for: self.appDelegate.window!, animated: true)
            let responseData = result as! Dictionary<String,AnyObject>
            let status      = responseData.validatedValue(SENZPAY_STATUS, expected:"" as AnyObject ) as! String
            if status == "false" {
                // false case error messsage
                MessageView.showMessage(message: responseData["msg"] as! String, time: 5.0, verticalAlignment: .bottom)
            } else {
                // handle true response
                
                let userID = responseData.validatedValue(SENZPAY_USER_ID, expected:"" as AnyObject ) as! String
                
                defaults.set(AESCrypto.aes256EncryptString(userID, withKey: crypto256Key), forKey: SENZPAY_USER_ID)
                
                let availbleBalance = responseData[SENZPAY_BALANCE] as? String
                
//                if ((Double("\(availbleBalance ?? "0")")!).isLess(than: Double("\(self.totalAmountToBePaid)")!)) {
//                    //  Not sufficient balance
//                    MessageView.showMessage(message: "You have not enough balance in your FKWallet, Please recharge your FKWallet to continue..", time: 5.0, verticalAlignment: .bottom)
//                }else{
                    self.apiCallForPayment()
               // }
            }
        }
    }
}
