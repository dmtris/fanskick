//
//  FKFKShippingAddressCell.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 11/13/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKFKShippingAddressCell: UITableViewCell {

    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var changeAddressBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
