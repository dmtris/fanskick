//
//  FKWriteAReviewVC.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 11/14/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKWriteAReviewVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var noReviewLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var dataSourceArray = [FKReviewList]()
    var cartListArray = [FKCartInfo]()
    var orderId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "REVIEWS"
        
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getProductReviewApi()
    }
    
    // use to navigate back screen
    @objc func backBtnAction() {
        
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is FKMerchandiseHomeVC {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    func getProductReviewApi() {
        
        var params = Dictionary<String,Any>()
        var product  = Dictionary<String,Any>()
        product["id"] = orderId
        params["order"] = product
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: .post, apiName: "orders/order_products_reviews", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if let data = results["reviews"] {
                    self.dataSourceArray.removeAll()
                    self.dataSourceArray.append(contentsOf: FKReviewList.reviewList(data:  data as! Array<Dictionary<String, AnyObject>>))
                    
                    if self.dataSourceArray.count == 0 {
                        self.noReviewLbl.isHidden = false
                        self.tableView.isHidden = true
                    } else {
                        self.noReviewLbl.isHidden = true
                        self.tableView.isHidden = false
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func writeReview(_ sender: Any) {
        
        let writeReviewPopUpVc = self.storyboard?.instantiateViewController(withIdentifier: "FKWriteReviewPopUpVC") as! FKWriteReviewPopUpVC
        writeReviewPopUpVc.cartListArray = cartListArray
        self.navigationController?.pushViewController(writeReviewPopUpVc, animated: true)
    }
    
    @IBAction func doneLikeList(_ sender: Any) {
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSourceArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:FKWriteReviewCell = tableView.dequeueReusableCell(withIdentifier: "FKWriteReviewCell", for: indexPath) as! FKWriteReviewCell
        let fkReview = self.dataSourceArray[indexPath.row]
        
        cell.reviewTitleCell.text = ""
        cell.reviewDescLbl.text = fkReview.desc
        cell.dateLbl.text = "by "+fkReview.userName+" "+(fkReview.dateCreated.dateFromUTC()?.dateString(DATE_TIME_FORMAT))!
        cell.likeBtn.setImage(UIImage(named:""), for: .normal)
        print(fkReview.rating)
        cell.ratingView?.value = CGFloat(Int(fkReview.rating)!)
        cell.ratingView?.ratingValueChange()
        return cell
    }
}
