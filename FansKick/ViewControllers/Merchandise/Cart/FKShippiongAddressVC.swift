//
//  FKShippiongAddressVC.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 11/14/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKShippiongAddressVC: UIViewController,UITableViewDelegate,UITableViewDataSource,FKTextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var picker = UIPickerView()
    var datePicker = UIDatePicker()
    
    @IBOutlet var  toolBar:UIToolbar!
    
    var placeholderTextArr = [String]()
    var fontIconArray = [String]()
    
    var indexPath:IndexPath?
    var stateArr = [String]()
    var stateArray = [FKListInfo]()
    
    var fkUser = FKUser()
    var selectedDate = Date()
    var selectedStateId = String()
    var errorTag = 50
    var cartListArray = [FKCartInfo]()
    var totalAmountToBePaid = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        placeholderTextArr = ["Email",
                              "Full Name",
                              "Address",
                              "State",
                              "City",
                              "Postal Code",
                              "Phone number"]
        
        fontIconArray = ["t",
                         "a",
                         "y",
                         "O",
                         "y",
                         "6",
                         "M"]
        
        self.navigationItem.title = "SHIPPING ADDRESS"
        
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        
        callApiToGetStates()
        getShippingAddressValues()
        
        self.tableView.reloadData() 
        self.initialise()
        // Do any additional setup after loading the view.
    }
    
    // use to navigate back screen
    @objc func backBtnAction() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //let value = UIInterfaceOrientation.portrait.rawValue
        // UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
    }
    
    func initialise() {
        
        self.toolBar = UIToolbar.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 44))
        let doneItem =   UIBarButtonItem.init(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(FKRegisterViewController.doneBtnAction))
        let cancelItem =   UIBarButtonItem.init(title: "Cancel", style: UIBarButtonItemStyle.done, target: self, action: #selector(FKRegisterViewController.cancelBtnAction))
        
        let space =  UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        self.toolBar.setItems([cancelItem, space, doneItem], animated: true)
        self.toolBar.barTintColor = UIColor.white
        self.toolBar.tintColor = UIColor.black
        
        self.picker.delegate = self
        self.picker.dataSource = self
        
        datePicker.maximumDate = Date()
        datePicker.datePickerMode = .date
    }
    
    func callApiToGetStates() {
        
        let params = Dictionary<String,Any>()
        
        ServiceHelper.request(params: params, method: .get, apiName: "users/state_list", hudType: loadingIndicatorType.withoutLoader) { (result, error, responseCode) in
            
            if (error == nil) {
                if responseCode == 200 {
                    let results = result as! Dictionary<String, AnyObject>
                    
                    if let data = results["state_list"] {
                        self.stateArray.append(contentsOf: FKListInfo.list(data: data as! Array<Dictionary<String, AnyObject>>))
                        for state in self.stateArray{
                            self.stateArr.append(state.name)
                        }
                    } else {
                        if let data = result![kResponseMsg]  {
                            MessageView.showMessage(message: data as! String, time: 5.0, verticalAlignment: .bottom)
                        }
                    }
                } else {
                }
            }
        }
    }
    
    func getStateByName(name:String)-> FKListInfo{
        var stateInfo:FKListInfo?
        for state in self.stateArray{
            if name == state.name {
                stateInfo = state
                break
            }
        }
        if stateInfo != nil {
            return stateInfo!
        } else {
            stateInfo?.name = ""
            stateInfo?.id = ""
            
            return stateInfo!
        }
    }
    
    @objc func doneBtnAction() {
        
        if indexPath?.row == 3 {
            // date
            fkUser.state =  stateArr[self.picker.selectedRow(inComponent: 0)]
            let cell = tableView.cellForRow(at: IndexPath.init(row: 3, section: 0)) as! FKInputTableViewCell
            cell.textField.setFKText(fkText: fkUser.state)
        }
        self.view.endEditing(true)
    }
    
    @objc func cancelBtnAction() {
        self.view.endEditing(true)
    }
    
    @IBAction func backBtn(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 70
    }
    
    //MARK:- UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.placeholderTextArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FKInputTableViewCell", for: indexPath) as! FKInputTableViewCell
        cell.textField.setFKPlaceHolder(placeHolder: self.placeholderTextArr[indexPath.row])
        
        cell.textField.leftPadingFontIcon(iconName: fontIconArray[indexPath.row])
        cell.textField.delegate = self
        cell.textField.setFKReturnKeyType(returnKeyType: .next)
        cell.textField.indexPath = indexPath
        cell.textField.setFKSecureTextEntry(secureTextEntry: false)
        cell.textField.setFKKeyboardType(keyboardType: .asciiCapable)
        cell.textField.maxLength = 0
        
        switch indexPath.row {
        case 0:
            cell.textField.text = fkUser.email
            cell.textField.setFKKeyboardType(keyboardType: .emailAddress)
            cell.textField.maxLength = 100
            break
        case 1:
            cell.textField.text = fkUser.name
            cell.textField.maxLength = 50
            break
        case 2:
            cell.textField.text = fkUser.address
            cell.textField.maxLength = 150
            break
        case 3:
            cell.textField.text = fkUser.state
            cell.textField.rightPadding(width: 30, image: UIImage.init(named: "arrowDown_icon"))
            break
        case 4:
            cell.textField.text = fkUser.city
            cell.textField.maxLength = 60

            break
        case 5:
            cell.textField.text = fkUser.postalCode
            cell.textField.setFKKeyboardType(keyboardType: .phonePad)
            cell.textField.maxLength = 6
            break
        case 6:
            cell.textField.text = fkUser.mobileNo
            cell.textField.maxLength = 13
            cell.textField.setFKKeyboardType(keyboardType: .numberPad)
            break
        default:
            break
        }
        
        return cell
    }
    
    //MARK: FKTextFieldView Overrides
    
    @objc func fkTextFieldDidBeginEditing(textField: FKTextFieldView){
        
        self.indexPath = textField.indexPath
        if textField.indexPath?.row == 3 {
            textField.setInputAccessoryView(inputAccessoryView:self.toolBar)
            textField.setInputView(inputView: picker)
        }
    }
    
    @objc func fkTextFieldShouldReturn(textField: FKTextFieldView) -> Bool
    {
        if textField.FKReturnKeyType() == .next{
            let indexPath = IndexPath.init(row: (textField.indexPath?.row)! + 1, section: 0)
            if !(tableView.indexPathsForVisibleRows?.contains(indexPath))!{
                tableView.scrollToRow(at: indexPath, at: .none, animated: false)
            }
            let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
            cell.textField.becomeFKFirstResponder()
        }else{
            textField.resignFKFirstResponder()
        }
        return true
    }
    
    @objc func fkTextFieldDidEndEditing(textField:FKTextFieldView)
    {
        if indexPath?.row == 0 {
            fkUser.email = textField.text
            
        }else if indexPath?.row == 1{
            fkUser.name = textField.text
            
        }else if indexPath?.row == 2{
            fkUser.address = textField.text
            
        }else if indexPath?.row == 4{
            fkUser.city = textField.text
            
        }else if indexPath?.row == 5{
            fkUser.postalCode = textField.text
            
        }else if indexPath?.row == 6{
            fkUser.mobileNo = textField.text
        }
    }
    
    
    // DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return stateArr.count
    }
    
    // Delegate
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return stateArr[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- Proceed to pay
    @IBAction func proceedToPay(_ sender: Any) {
        self.view.endEditing(true)
        
        if isAllFieldVerified() {
            
            callApiToAddShippingAddress()
        }
    }
    
    // MARK:- Api to register
    func callApiToAddShippingAddress() {
        
        if fkUser.mobileNo.contains("+") {
            fkUser.mobileNo = fkUser.mobileNo.replaceString("+", withString: "")
        }
        
        
        
        let state = getStateByName(name: fkUser.state)
        let userDict = ["full_name"       : fkUser.name,
                        pEmail     : fkUser.email,
                        "phone_number"    : fkUser.mobileNo.mobileNumberWithCountryCode,
                        "postal_code" : fkUser.postalCode,
                        pstate     : state.id ,
                        "city"     : fkUser.city ,
                        paddress   : fkUser.address] as [String : Any]
        
        let params  = ["shipping_address" : userDict]
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: .post, apiName: "products/shipping_address") { (result, error, responseCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if (error == nil) {
                if responseCode == 200 {
                    if let data = result!["shipping_address"]  {
                        
                        let user = data as! Dictionary<String, Any>
                        let paymentView = self.storyboard?.instantiateViewController(withIdentifier: "FKPaymentViewController") as! FKPaymentViewController
                        paymentView.cartListArray = self.cartListArray
                        paymentView.totalAmountToBePaid = self.totalAmountToBePaid
                        print(user.validatedValue("id", expected: "" as AnyObject) as! String)
                        defaults.setValue(user.validatedValue("id", expected: "" as AnyObject) as! String, forKey: "shippingAddress")
                        paymentView.shippingAddressId = user.validatedValue("id", expected: "" as AnyObject) as! String
                        self.navigationController?.pushViewController(paymentView, animated: true)
                    }
                }
            } else {
                let results = result as! Dictionary<String, AnyObject>
                
                if let data = results[kResponseMsg]  {
                    MessageView.showMessage(message: data as! String, time: 5.0, verticalAlignment: .bottom)
                }
            }
        }
    }
    
    func isAllFieldVerified() -> Bool{
        var isVerified = false
        
        if (!fkUser.email.isEmpty && !fkUser.email.isValidEmail()) { // email
            
            let indexPath = IndexPath.init(row: 0, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: false)
            
            let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
            cell.textField.setFKErrorMessage(message: "Please enter valid email")
        } else if fkUser.name.isEmpty {// name
            
            let indexPath = IndexPath.init(row: 1, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: false)
            
            let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
            cell.textField.setFKErrorMessage(message: "Please enter name")
        } else if fkUser.address.isEmpty { // address
            
            let indexPath = IndexPath.init(row: 2, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: false)
            
            let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
            cell.textField.setFKErrorMessage(message: "Please enter address")
        } else if fkUser.state.trimWhiteSpace.isEmpty { // state
            
            let indexPath = IndexPath.init(row: 3, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: false)
            
            let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
            cell.textField.setFKErrorMessage(message: "Please select state")
        } else if fkUser.city.isEmpty {
            
            let indexPath = IndexPath.init(row: 4, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: false)
            
            let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
            cell.textField.setFKErrorMessage(message: "Please enter city")
        } else if fkUser.postalCode.isEmpty {
            
            let indexPath = IndexPath.init(row: 5, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: false)
            
            let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
            cell.textField.setFKErrorMessage(message: "Please enter postal code")
        } else if fkUser.mobileNo.isEmpty {// Mob Number
            
            let indexPath = IndexPath.init(row: 6, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: false)
            
            let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
            cell.textField.setFKErrorMessage(message: "Please enter phone number")
        } else if (!fkUser.mobileNo.trimWhiteSpace.isMobileNumber)  { // Mobile Length
            
            let indexPath = IndexPath.init(row: 6, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: false)
            let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
            cell.textField.setFKErrorMessage(message: "Please enter valid phone number")
        } else {
            isVerified = true
        }
        return isVerified
    }
    
    func getShippingAddressValues() {
        
        fkUser.email        = defaults.value(forKey: "email") as! String
        fkUser.name         = defaults.value(forKey: "name") as! String
        fkUser.mobileNo     = defaults.value(forKey: "mob") as! String
        fkUser.postalCode   = defaults.value(forKey: "zipcode") as! String
        fkUser.stateId      = defaults.value(forKey: "stateId") as! String
        fkUser.state        = defaults.value(forKey: "state") as! String
        fkUser.address      = defaults.value(forKey: "address") as! String
        fkUser.city         = defaults.value(forKey: "city") as! String
    }
}
