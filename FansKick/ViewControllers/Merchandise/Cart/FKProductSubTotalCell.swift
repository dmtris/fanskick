//
//  FKProductSubTotalCell.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 11/13/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKProductSubTotalCell: UITableViewCell {

    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var separatorView2: UIView!
    @IBOutlet weak var valueLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var couponTextField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
