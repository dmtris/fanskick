//
//  FKCartProductCell.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 11/13/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKCartProductCell: UITableViewCell {

    @IBOutlet weak var upBtn: UIButton!
    @IBOutlet weak var downBtn: UIButton!
    @IBOutlet weak var crossBtn: UIButton!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var quantity: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var discountedPriceLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
