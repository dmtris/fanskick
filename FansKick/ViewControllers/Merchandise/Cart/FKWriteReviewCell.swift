//
//  FKWriteReviewCell.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 11/14/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKWriteReviewCell: UITableViewCell {

    @IBOutlet weak var ratingView: AARatingBar!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var likeListBtn: UIButton!
    @IBOutlet weak var reviewTitleCell: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var reviewDescLbl: UILabel!
    @IBOutlet weak var likerLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
