//
//  FKWriteReviewPopUpVC.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 11/15/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit
import CarbonKit

class FKWriteReviewPopUpVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextViewDelegate {
    
    @IBOutlet weak var pageDots: UIPageControl!
    @IBOutlet weak var homeTableView: UITableView!
    var newsDesc = String()//NSAttributedString()
    var newsHeading = String()
    var newsDateTime = String()
    var page = PAGE()
    var productDetailArr = [String]()
    var productId = String()
    var enteredReview = ""
    var rating = CGFloat()
    var cartListArray = [FKCartInfo]()
    var likeView = UIView()
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "RATE AND REVIEW"
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        
        if cartListArray.count > 0 {
            let objFkHomeModel = self.cartListArray[0]
            newsHeading = (objFkHomeModel.product?.name)!
            newsDesc = (objFkHomeModel.product?.detail)!
            productId = (objFkHomeModel.product?.id)!
//            rating = (objFkHomeModel.product?.id)!
            self.homeTableView.reloadData()
        }
        
        homeTableView.tableFooterView = UIView.init()
        collectionView.reloadData()
        pageDots.numberOfPages = cartListArray.count
    }
    
    @objc func backBtnAction() {
        
        self.navigationController?.popViewController(animated: true)
    }
    //MARK:- UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    //MARK:- Send Review APi
    func callApiToSendReview() {
        
        var params = Dictionary<String,Any>()
        var review = Dictionary<String,Any>()

        review["product_id"] = productId
        review["description"] = enteredReview
        review["rate"] = rating
        params["review"] = review
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: .post, apiName: "rating/rate_product", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                MessageView.showMessage(message: results["responseMessage"] as! String, time: 3.0, verticalAlignment: .bottom)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @objc func submitBtn() {
        
        self.view.endEditing(true)
        if self.rating != 0 {
            callApiToSendReview()
        } else {
            
            MessageView.showMessage(message: "Please select rating.", time: 3.0, verticalAlignment: .bottom)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:FKNewsCell = tableView.dequeueReusableCell(withIdentifier: "FKNewsCell", for: indexPath) as! FKNewsCell
        
        
        cell.starRating.ratingDidChange = { ratingValue in
            self.rating = ratingValue
        }
        cell.newsHeadingLbl.text        = newsHeading
        cell.newsDescriptionLbl.text    = newsDesc.html2String
        cell.submitBtn.addTarget(self, action: #selector(submitBtn), for: .touchUpInside)
        if enteredReview == "" {
            cell.reviewtextView.text = "Share your experience here.."
        } else {
            cell.reviewtextView.text = enteredReview
        }
        
        cell.reviewtextView.textColor = UIColor.lightGray
        cell.reviewtextView.delegate = self
        return cell
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            enteredReview = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Share your experience here.."
            textView.textColor = UIColor.lightGray
        } else {
            enteredReview = textView.text.trimWhiteSpace
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK:  UICollectionView Overrides
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cartListArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FKNewsImageCell", for: indexPath) as! FKNewsImageCell
        let fkCartList = cartListArray[indexPath.row]
        cell.newsImg.normalLoad((fkCartList.sponsorImage))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        let indexpath =  collectionView.indexPathsForVisibleItems.first
        let objFkHomeModel = self.cartListArray[(indexpath?.row)!]
        newsHeading = (objFkHomeModel.product?.name)!
        newsDesc = (objFkHomeModel.product?.detail)!
        productId = (objFkHomeModel.product?.id)!
        
        self.homeTableView.reloadData()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var size = CGSize()
        size.width  = self.view.frame.size.width
        size.height = self.view.frame.size.height
        
        return size
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

