//
//  FKCartListVC.swift
//  FansKick
//
//  Created by Sunil Verma on 11/11/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKCartListVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {

    @IBOutlet weak var cartEmptyLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var subTotalArray = [String]()
    var selectedQuantity:Int = 1
    var shippingPrice = ""
    var discountedPrice = "0"
    var totalPrice = "0"
    var dataSourceArray = [FKCartInfo]()
    var product: FKProductInfo?
    var totalAmountToBePaid = ""
    var barButton : MJBadgeBarButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "CART"
        
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        
        subTotalArray = ["SUB TOTAL",
                         "Shipping",
                         "GST 6%",
                      "Total"]
        
        self.shippingPrice = "0"
        self.discountedPrice = "0"
        self.totalPrice = "0"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.cartDetailApiCall()
    }
    // use to navigate back screen
    @objc func backBtnAction() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     * Call APi to get my cart
     */
    func cartDetailApiCall() {
        
        let params = Dictionary<String,Any>()
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: .get, apiName: "products/cart_details", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            self.dataSourceArray.removeAll()
            MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if let data = results["products"] {
                    self.dataSourceArray.removeAll()
                    self.dataSourceArray.append(contentsOf: FKCartInfo.cartList(data:  data as! Array<Dictionary<String, AnyObject>>))
                    
                    if self.dataSourceArray.count > 0 {
                        self.shippingPrice      = results.validatedValue("shipping_price", expected: "FREE" as AnyObject) as! String
                        self.discountedPrice    = results.validatedValue("discount_amount", expected: "0" as AnyObject) as! String
                        self.totalPrice         = results.validatedValue("total_price", expected: "0" as AnyObject) as! String
                    } else {
                        let appDelegate = UIApplication.shared.delegate as? AppDelegate
                        appDelegate?.cartCount = "0"//results.validatedValue("cart_products_count", expected: "" as AnyObject) as! String
                        
                        self.barButton.badgeValue = (appDelegate?.cartCount)!
                        self.barButton.badgeOriginX = 20.0
                        self.barButton.badgeOriginY = -4
                    }
                    if let shippingInfo = results["shipping_address"] as? [String : AnyObject] {
                        self.fillShippingAddress(shippingInfo: shippingInfo)
                    } else {
                        self.removeUserDefaults()
                    }
                    if self.dataSourceArray.count == 0 {
                        self.cartEmptyLbl.isHidden = false
                        self.tableView.isHidden = true
                    } else {
                        self.cartEmptyLbl.isHidden = true
                        self.tableView.isHidden = false
                    }
                    self.tableView.reloadData()
                } else {
                    if self.dataSourceArray.count == 0 {
                        self.cartEmptyLbl.isHidden = false
                        self.tableView.isHidden = true
                    } else {
                        self.cartEmptyLbl.isHidden = false
                        self.tableView.isHidden = true
                    }
                }
            }
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            appDelegate?.cartCount = String(self.dataSourceArray.count)
        }
    }
    
    // MARK:- Tableview Datasource
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return self.dataSourceArray.count
        } else {
            return subTotalArray.count
        }
    }
    
    // MARK:- Increase product count
    /**
     * increaseCount method increases the count of product -
     add to cart API is called with 1 product with product id, size and quantity(1)
     */
    @objc func increaseCount(sender:UIButton) {
        let cart = self.dataSourceArray[sender.tag]
        selectedQuantity = Int(cart.quantity)!
        updateCartWith(productId: cart.productId, size: cart.size,quantity: 1)
        self.tableView.reloadData()
    }
    
    /**
     * increaseCount method increases the count of product -
     add to cart API is called with -1 product with product id, size and quantity(1)
     */
    // MARK:- Decrease product count
    @objc func decreaseCount(sender:UIButton) {
        
        let cart = self.dataSourceArray[sender.tag]
        selectedQuantity = Int(cart.quantity)!
        if selectedQuantity > 1
        {
            updateCartWith(productId: cart.productId, size: cart.size,quantity: -1)
            self.tableView.reloadData()
        }
    }
    
    // MARK:- Delete Product from cart
    @objc func deleteProductFromCart(sender:UIButton) {
        
        let alertController: UIAlertController = UIAlertController(title: "", message: "Do you want to remove product from cart?", preferredStyle: .alert)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            
        }
        let yesAction: UIAlertAction = UIAlertAction(title: "Yes", style: .default) { action -> Void in
            
            let cart = self.dataSourceArray[sender.tag]
            self.callApiToDeleteProductFromCart(productId: cart.productId)
            self.tableView.reloadData()
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(yesAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func callApiToDeleteProductFromCart(productId:String) {
        
        var params = Dictionary<String,Any>()
        var product  = Dictionary<String,Any>()
        product["id"] = productId
        params["product"] = product
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: .post, apiName: "products/remove_product", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil {
                self.cartDetailApiCall()
            }
        }
    }
    
    /**
     * updateCartWith method increases/decreases the count of product into the cart -
     api written in it includes add to cart.
     whenever the quantity of the product is increased or decreased this method will be called with product quantity
     */
    func updateCartWith(productId:String,size:String,quantity:Int) {
        
        var params = Dictionary<String,Any>()
        var product  = Dictionary<String,Any>()
        product["id"] = productId
        product["quantity"] = quantity
        product["size"] = size
        params["product"] = product
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: .post, apiName: "products/add_product", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil {
                self.cartDetailApiCall()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            tableView.separatorColor = .white
            let cell = tableView.dequeueReusableCell(withIdentifier: "FKCartProductCell", for: indexPath) as! FKCartProductCell
            cell.upBtn.addTarget(self, action: #selector(increaseCount(sender:)), for: .touchUpInside)
            cell.downBtn.addTarget(self, action: #selector(decreaseCount(sender:)), for: .touchUpInside)
            cell.crossBtn.addTarget(self, action: #selector(deleteProductFromCart(sender:)), for: .touchUpInside)
            cell.crossBtn.tag = indexPath.row
            cell.upBtn.tag = indexPath.row
            cell.downBtn.tag = indexPath.row

            let cart = self.dataSourceArray[indexPath.row]
            cell.name.text = cart.product?.name
            
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\(cart.price)")
            attributeString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
            
            if cart.product!.discountedPrice != "0" {
                cell.discountedPriceLbl.isHidden = false
                cell.price.text = addRm(targetString: cart.product!.discountedPrice)
                cell.discountedPriceLbl.attributedText  = attributeString
                
            } else {
                cell.discountedPriceLbl.isHidden = true
                cell.price.text = addRm(targetString: "\(cart.price)")
            }
            cell.quantity.text = "QTY \(cart.quantity)"
            cell.imgView.normalLoad((cart.sponsorImage))
            return cell
        } else {
            var priceAfterDiscount = Float(self.totalPrice)!-Float(self.discountedPrice)!
            var gstAmount = Float()
            if priceAfterDiscount > 0 {
                gstAmount = Float((6 / 100) * priceAfterDiscount)
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "FKProductSubTotalCell", for: indexPath) as! FKProductSubTotalCell
            cell.titleLbl.text = subTotalArray[indexPath.row]
            cell.separatorView.isHidden = true
            cell.separatorView2.isHidden = true
            cell.couponTextField.isHidden = true
            cell.valueLbl.isHidden = false
            cell.titleLbl.font = UIFont.init(name: "Gilroy-Light", size: 15)
            tableView.separatorColor = .clear
            
            if indexPath.row == 0 {
                cell.titleLbl.font = UIFont.init(name: "Gilroy-ExtraBold", size: 15)
                if priceAfterDiscount < 0 {
                    priceAfterDiscount = 0
                }
                cell.valueLbl.text = addRm(targetString: "\(priceAfterDiscount)")
                
            } else if indexPath.row == 1 {
                if self.shippingPrice == "0" {
                    cell.valueLbl.text = "FREE"
                } else {
                    cell.valueLbl.text = addRm(targetString: self.shippingPrice)
                }
            } else if indexPath.row == 2 {
                let totalAmount = addRm(targetString: String(format: "%.2f", gstAmount))
                cell.valueLbl.text = totalAmount
                
            }  else {
                let shippingAddress = Float(self.shippingPrice)!
                priceAfterDiscount += gstAmount
                priceAfterDiscount += shippingAddress
                totalAmountToBePaid = String(format: "%.2f", priceAfterDiscount)
                cell.titleLbl.font = UIFont.init(name: "Gilroy-ExtraBold", size: 15)
                cell.valueLbl.font = UIFont.init(name: "Gilroy-ExtraBold", size: 15)

                cell.valueLbl.text = addRm(targetString: totalAmountToBePaid)
            }
            
            if indexPath.row == subTotalArray.count-1 {
                cell.separatorView.isHidden = false
                cell.separatorView2.isHidden = false
            }
            return cell
        }
    }
    
    func addRm(targetString:String) -> String {
			
			let untiPrice = Float(targetString)

			let obtainedString = "RM \(String(format: "%.2f", untiPrice!))"
        return obtainedString
    }
    
//     MARK:- Proceed to pay
    @IBAction func proceedToPayBtn(_ sender: Any) {
        
        if self.dataSourceArray.count > 0 {
            let shippingAddressVc = self.storyboard?.instantiateViewController(withIdentifier: "FKShippiongAddressVC") as! FKShippiongAddressVC
            shippingAddressVc.cartListArray = dataSourceArray
            shippingAddressVc.totalAmountToBePaid = totalAmountToBePaid

            self.navigationController?.pushViewController(shippingAddressVc, animated: true)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
        textField.resignFirstResponder()
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fillShippingAddress(shippingInfo:[String:AnyObject]) {
        
        defaults.setValue(shippingInfo.validatedValue("email",         expected: "" as AnyObject) as! String, forKey: "email")
        defaults.setValue(shippingInfo.validatedValue("full_name",     expected: "" as AnyObject) as! String, forKey: "name")
        defaults.setValue(shippingInfo.validatedValue("phone_number",  expected: "" as AnyObject) as! String, forKey: "mob")
        defaults.setValue(shippingInfo.validatedValue("postal_code",   expected: "" as AnyObject) as! String, forKey: "zipcode")
        defaults.setValue(shippingInfo.validatedValue("state_id",      expected: "" as AnyObject) as! String, forKey: "stateId")
        defaults.setValue(shippingInfo.validatedValue("state_name",    expected: "" as AnyObject) as! String, forKey: "state")
        defaults.setValue(shippingInfo.validatedValue("address",       expected: "" as AnyObject) as! String, forKey: "address")
        defaults.setValue(shippingInfo.validatedValue("city",          expected: "" as AnyObject) as! String, forKey: "city")
        
    }
    
    func removeUserDefaults() {
        defaults.setValue("", forKey: "email")
        defaults.setValue("",forKey: "name")
        defaults.setValue("",forKey: "mob")
        defaults.setValue("",forKey: "zipcode")
        defaults.setValue("",forKey: "stateId")
        defaults.setValue("",forKey: "state")
        defaults.setValue("",forKey: "address")
        defaults.setValue("",forKey: "city")
    }
}
