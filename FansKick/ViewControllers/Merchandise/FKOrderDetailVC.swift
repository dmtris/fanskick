//
//  FKOrderDetailVC.swift
//  FansKick
//
//  Created by Sunil Verma on 22/11/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKOrderDetailVC: UIViewController,UITableViewDelegate, UITableViewDataSource  {
    
    var dataSourceArray = [FKProductInfo]()
    var orderDetail : FKMyOrdersInfo?
    
    var isNavigateFromNotification = false
    
    @IBOutlet weak var tblView:UITableView?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title  = "ORDER DETAIL"
        tblView?.tableFooterView = UIView.init()
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        
        if isNavigateFromNotification {
            orderDetailApiCall()
        } else {
            dataSourceArray = (orderDetail?.productsArray)!
            self.tblView?.reloadData()
        }
        
//        orderDetailApiCall()

    }
    
    // use to navigate back screen
    @objc func backBtnAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func orderDetailApiCall() {
        
        var params  = Dictionary<String,Any>()
        var order = Dictionary<String,Any>()
        order["id"] =  self.orderDetail?.id
        params["order"] = order
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: .post, apiName: "orders/order_detail") { (result, error, statusCode) in
         
            MBProgressHUD.hide(for: self.view, animated: true)
            if (result != nil) {
                let responseCode = result?["responseCode"] as! NSInteger
                if (error == nil) {
                    if responseCode == 200 {
                        let results = result as! Dictionary<String, AnyObject>
                        if let data = results["order"] {
                            self.orderDetail?.updateOrder(data:  data as! Dictionary<String, AnyObject>)
                            self.dataSourceArray = (self.orderDetail?.productsArray)!

                            self.tblView?.reloadData()
                        } else {
                            MessageView.showMessage(message: result!["responseMessage"] as! String, time: 5.0, verticalAlignment: .bottom)
                        }
                    }
                }
            }
        }
    }
    
    //MARK: UITableView Overrides
    func numberOfSections(in tableView: UITableView) -> Int{
        
        if dataSourceArray.isEmpty{
            return 0
        }
        
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (section == 1) ? dataSourceArray.count : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell  = tableView.dequeueReusableCell(withIdentifier: "FKOrderCell") as! FKOrderCell
            cell.orderIdLbl?.text = "Order No: \((orderDetail?.id)!)"
            cell.totalPriceLbl?.text = "RM \((orderDetail?.amountAfterDiscount)!)"
            cell.orderStatusLbl?.text = "Status: \((orderDetail?.status)!)"
            cell.orderDateLbl?.text = "Placed On: \((orderDetail?.orderDate.dateFromUTC()?.dateString())!)"
            
            return cell
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "FKCartProductCell", for: indexPath) as! FKCartProductCell
            let product = self.dataSourceArray[indexPath.row]
            cell.name.text = product.name
            
            print("unitPrice: ",Float(product.unitPrice)!)
            print("discount: ",Float(product.discount)!)
            print("quantity: ",Float(product.quantity)!)
            
            let priceAfterDiscount = Float(product.unitPrice)!-Float(product.discount)!
            let priceNoOfProductsWithPrice = priceAfterDiscount * Float(product.quantity)!
            let totalShippingPrice = Float(product.shippingPrice)! * Float(product.quantity)!
            let totalPrice = priceNoOfProductsWithPrice+totalShippingPrice
            
            let gstAmount = Float((6 / 100) * priceNoOfProductsWithPrice)
            
            cell.price.text = "RM \(totalPrice+gstAmount)"
            cell.quantity.text = "QTY \(product.quantity)"
            cell.imgView.normalLoad((product.productImage))
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 90
        }else{
            return 80
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
