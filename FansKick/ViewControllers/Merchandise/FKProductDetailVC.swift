//
//  FKProductDetailVC.swift
//  FansKick
//
//  Created by Sunil Verma on 11/11/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKProductDetailVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource, SizeCellDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tblView: UITableView!
    var fkProduct: FKProductInfo?
    @IBOutlet weak var pageControll: UIPageControl!
    public var selectedQuantity:Int = 1
    var selectedSize = ""
    var isFav = false
    var barButton : MJBadgeBarButton!
    
    @IBOutlet weak var favBtn: IndexPathButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "PRODUCT DETAILS"//fkProduct?.name
        self.pageControll.numberOfPages = (fkProduct?.productImageArr.count)!
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        
        let cart = UIButton.init(type: UIButtonType.custom)
        cart.titleLabel?.font = UIFont.init(name: fanskickFontIcons, size: 22)
        cart.setTitle("h", for: .normal)
        cart.frame = CGRect.init(x: 0, y: 0, width: 24, height: 24)
        cart.addTarget(self, action: #selector(cartAction), for: UIControlEvents.touchUpInside)
        let cartBar = UIBarButtonItem.init(customView: cart)
        self.barButton = MJBadgeBarButton()
        self.barButton.setup(customButton: cart)
        self.navigationItem.rightBarButtonItems = [cartBar]
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        
        self.barButton.badgeValue = (appDelegate?.cartCount)!
        self.barButton.badgeOriginX = 20.0
        self.barButton.badgeOriginY = -4
        
        callApiToGetProductDetails()
    }
    
    @objc func cartAction() {
        
        let productListVC = self.storyboard?.instantiateViewController(withIdentifier: "FKCartListVC") as! FKCartListVC
        self.navigationController?.pushViewController(productListVC, animated: true)
    }
    
    // use to navigate back screen
    @objc func backBtnAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Product Details
    func callApiToGetProductDetails() {
        
        var params = Dictionary<String,Any>()
        var product  = Dictionary<String,Any>()
        product["id"] = fkProduct?.id
        
        params["product"] = product
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: .post, apiName: "products/product_details", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if results["is_favourite"] != nil {
                    
                    self.isFav = results.validatedValue("is_favourite", expected: false as AnyObject ) as! Bool
                    
                    if self.isFav {
                        self.isFav = false
                        self.favBtn.setImage(UIImage(named:"greeanHeart.png"), for: .normal)
                    } else {
                        self.isFav = true
                        self.favBtn.setImage(UIImage(named:"whiteImage.png"), for: .normal)
                    }
                } else {
                }
            }
        }
    }
    
    //MARK:- product List API
    func callApiToLikeDislike() {
        
        var params = Dictionary<String,Any>()
        var product  = Dictionary<String,Any>()
        product["id"] = fkProduct?.id
        
        params["product"] = product
        ServiceHelper.request(params: params, method: .post, apiName: "products/favourite_product", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if results["responseMessage"] != nil {
                }
            }
        }
    }
    
    @IBAction func likeBtnAction() {
        
        if self.isFav {
            self.isFav = false
            self.favBtn.setImage(UIImage(named:"greeanHeart.png"), for: .normal)
        } else {
            self.isFav = true
            self.favBtn.setImage(UIImage(named:"whiteImage.png"), for: .normal)
        }
        callApiToLikeDislike()
    }
    
    @IBAction func shareBtnAction() {
        
        var imageToShare = [Any]()
        
        let message = "Check out the awesome product \((fkProduct?.name)!) at fanscick.com"
        
        do {
            let data =   try Data.init(contentsOf: URL.init(string: (fkProduct?.productImage)!)!)
            let image =    UIImage.init(data: data)
            
            imageToShare = [ message as Any, image as Any]
            
        } catch  {
            imageToShare = [message as Any]
        }
        
        FKShareHelper.share.openActivity(options: imageToShare , controller: self)
    }
    
    //MARK Size cell Delegate
    func selectedIndex(index:Int){
        
        selectedSize = (fkProduct?.availedSize[index])!
        
    }
    
    @IBAction func addTiCartBtnAction() {
        
        if (fkProduct?.availedSize.count)! > 0 {
            if selectedSize.isEmpty {
                MessageView.showMessage(message: "Please select size", time: 3.0, verticalAlignment: .bottom)
                return
            }
        } else {
            selectedSize = ""
        }
        
        var params = Dictionary<String,Any>()
        var product  = Dictionary<String,Any>()
        product["id"] = fkProduct?.id
        product["quantity"] = selectedQuantity
        product["size"] = selectedSize
        params["product"] = product
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: .post, apiName: "products/add_product", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if results["responseMessage"] != nil {
                    let responseCode = result?["responseCode"] as! NSInteger
                    
                    if responseCode == 200 {
                        let appDelegate = UIApplication.shared.delegate as? AppDelegate
                        appDelegate?.cartCount = results.validatedValue("cart_products_count", expected: "" as AnyObject) as! String
                        
                        self.barButton.badgeValue = (appDelegate?.cartCount)!
                        self.barButton.badgeOriginX = 20.0
                        self.barButton.badgeOriginY = -4
                    }
                    
                    self.selectedQuantity = 1
                    self.tblView.reloadData()
                    MessageView.showMessage(message: results["responseMessage"] as! String, time: 5.0, verticalAlignment: .bottom)
                } else {
                }
            }
        }
    }
    
    func updatePagerValue() {
    }
    
    @IBAction func minusBtnAction() {
        if selectedQuantity > 1
        {
            selectedQuantity = selectedQuantity - 1
            self.IncreaseCount()
        }
        //        self.tblView.reloadData()
    }
    
    @IBAction func plushBtnAction() {
        selectedQuantity  = selectedQuantity + 1
        //        self.tblView.reloadData()
        self.IncreaseCount()
    }
    
    func IncreaseCount() {
        
        let quantityLbl = self.view.viewWithTag(55) as! UILabel
        quantityLbl.text = "\(selectedQuantity)"
    }
    
    //MARK: UICollectionView Overrides
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (fkProduct?.productImageArr.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FKProductCell", for: indexPath) as! FKProductCell
        cell.productNameLbl?.text = fkProduct?.name
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "RM \((fkProduct?.unitPrice)!)")
        attributeString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        
        if fkProduct?.discountedPrice != "0" {
            cell.discountedPriceLbl.isHidden = false
            cell.productPriceLbl?.text = addRm(targetString: (fkProduct?.discountedPrice)!)
            cell.discountedPriceLbl.attributedText  = attributeString
            
        } else {
            cell.discountedPriceLbl.isHidden = true
            cell.productPriceLbl?.text = addRm(targetString: (fkProduct?.unitPrice)!)
        }
        
        cell.shareBtn?.indexPath = indexPath
        cell.imgView?.normalLoad((fkProduct?.productImage)!)
        cell.likeBtn?.indexPath = indexPath
        return cell
    }
    
    func addRm(targetString:String) -> String {
			let untiPrice = Float(targetString)
			let obtainedString = "RM \(String(format: "%.2f", untiPrice!))"
       // let obtainedString = "RM \(String(describing: targetString))"
        return obtainedString
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        let indexpath =  collectionView.indexPathsForVisibleItems.first
        
        self.pageControll.currentPage = (indexpath?.row)!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: self.view.frame.width, height: 220)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 1 {
            return 2
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return (section == 1) ? 40 : 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FKDetailTitleCell") as! FKDetailTitleCell
            
            return cell
        }else{
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FKTitleDetailCell") as! FKTitleDetailCell
            cell.titleLbl?.text = fkProduct?.name
            cell.detailLbl?.text = fkProduct?.detail.html2String
            return cell
        } else {
            
            if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "FKQuantiryCell") as! FKQuantiryCell
                cell.quantityLbl?.tag = 55
                cell.quantityLbl?.text = "\(selectedQuantity)"
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "FKSizeCell") as! FKSizeCell
                cell.delegate = self
                cell.reloadCellWithData(array: (fkProduct?.availedSize)!)
                
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 1 {
            return 44
        }else{
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
