//
//  FKProductListVC.swift
//  FansKick
//
//  Created by Sunil Verma on 11/11/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKProductListVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var filterBtn:UIButton?
    
    @IBOutlet weak var noProductsLbl: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    var productsArray = [FKProductInfo]()
    var searchText = ""
    var isSpecialOffer = false
    var productId = ""
    
    var barButton : MJBadgeBarButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "PRODUCTS"
        
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        
        let cart = UIButton.init(type: UIButtonType.custom)
        cart.titleLabel?.font = UIFont.init(name: fanskickFontIcons, size: 22)
        cart.setTitle("h", for: .normal)
        cart.frame = CGRect.init(x: 0, y: 0, width: 24, height: 24)
        cart.addTarget(self, action: #selector(cartAction), for: UIControlEvents.touchUpInside)
        let cartBar = UIBarButtonItem.init(customView: cart)
        self.navigationItem.rightBarButtonItems = [cartBar]
        
        self.barButton = MJBadgeBarButton()
        self.barButton.setup(customButton: cart)
        
        productListApi()
    }
    
    @objc func cartAction() {
        
        let productListVC = self.storyboard?.instantiateViewController(withIdentifier: "FKCartListVC") as! FKCartListVC
        self.navigationController?.pushViewController(productListVC, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        self.barButton.badgeValue = (appDelegate?.cartCount)!
        self.barButton.badgeOriginX = 20.0
        self.barButton.badgeOriginY = -4
    }
    
    //MARK:- product List APi
    func productListApi() {
        var params          = Dictionary<String,Any>()
        params["page"]      = "1"
        params["per_page"]  = "500"
        params["search"]    = searchText

        var id  = Dictionary<String,Any>()
        id["id"] = productId
        
        print(params)
        
        var apiName = ""
        if isSpecialOffer {
            params["special_offer"] = id
            apiName = "products/special_products"
        } else {
            params["product_category"] = id
            apiName = "products/product_list"
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: .post, apiName: apiName, hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil {
                if let data = result!["products"] {
                    self.productsArray.removeAll()
                    self.productsArray.append(contentsOf: FKProductInfo.productsList(data: data as! Array<Dictionary<String, AnyObject>>))
                    self.collectionView.reloadData()
                    
                    if self.productsArray.count == 0 {
                        self.noProductsLbl.isHidden = false
                        self.collectionView.isHidden = true
                    } else {
                        self.noProductsLbl.isHidden = true
                        self.collectionView.isHidden = false
                    }
                } else {
                }
            }
        }
    }
    
    // use to navigate back screen
    @objc func backBtnAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func filterBtnAction(){
        
        let dataArray = ["All Products","Low to high price", "High to low price","Review and rating"];
        RPicker.sharedInstance.selectOption(title: "Select filter", dataArray: dataArray, selectedIndex: 0) { (selectedText, index) in
            if !selectedText.isEmpty {
                if selectedText == "Low to high price" {
                    self.searchText = "LTH"
                } else if selectedText == "Low to high price" {
                    self.searchText = "LTH"
                } else if selectedText == "Review and rating"{
                    self.searchText = "rating"
                } else {
                    self.searchText = ""
                }
                
                self.productListApi()
                self.filterBtn?.setTitle(selectedText, for: .normal)
            }
        }
    }
    
    //MARK: UICollectionView Overrides
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.productsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FKProductCell", for: indexPath) as! FKProductCell
        
        let fkProduct = self.productsArray[indexPath.row]
        cell.productNameLbl?.text = fkProduct.name
        cell.productPriceLbl?.text = fkProduct.unitPrice
        cell.imgView?.normalLoad(fkProduct.productImage)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let productDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "productDetailVC") as! FKProductDetailVC
        let fkProduct = self.productsArray[indexPath.row]
        productDetailVC.fkProduct = fkProduct
        self.navigationController?.pushViewController(productDetailVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: self.view.frame.width/2 - 10, height: self.view.frame.width/2 + 30)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
