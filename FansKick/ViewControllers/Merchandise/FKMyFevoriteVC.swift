//
//  FKMyFevoriteVC.swift
//  FansKick
//
//  Created by Sunil Verma on 16/11/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKMyFevoriteVC: UIViewController,UITableViewDelegate, UITableViewDataSource {

    var dataSourceArray = [FKProductInfo]()

    @IBOutlet weak var noFavLbl: UILabel!
    @IBOutlet weak var favTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title  = "MY FAVORITES"
        favTableView?.tableFooterView = UIView.init()
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
    }
    
    // use to navigate back screen
    @objc func backBtnAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        callApiToGetFavList()
    }
    //MARK:- fav List APi
    func callApiToGetFavList() {
        
        let params = Dictionary<String,Any>()
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: .get, apiName: "rating/favourite_list", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil {
                if let data = result!["favourite_list"] {
                    if (data != nil) {
                        self.dataSourceArray.removeAll()
                        self.dataSourceArray.append(contentsOf: FKProductInfo.favoriteList(data: data as! Array<Dictionary<String, AnyObject>>))
                        self.favTableView.reloadData()
                        
                        if self.dataSourceArray.count == 0 {
                            self.noFavLbl.isHidden = false
                            self.favTableView.isHidden = true
                        } else {
                            self.noFavLbl.isHidden = true
                            self.favTableView.isHidden = false
                        }
                    }else{
                        self.noFavLbl.isHidden = false
                        self.favTableView.isHidden = true
                    }
                } else {
                    self.noFavLbl.isHidden = false
                    self.favTableView.isHidden = true
                }
            }
        }
    }
    
    //MARK: UITableView Overrides
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourceArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FKCartProductCell", for: indexPath) as! FKCartProductCell
        let product = self.dataSourceArray[indexPath.row]
        cell.name.text = product.name
        cell.price.text = "RM \(product.unitPrice)"
        //cell.quantity.text = "" //"QTY \(product.quantity)"
        cell.imgView.normalLoad((product.productImage))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let productDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "productDetailVC") as! FKProductDetailVC
        let fkProduct = self.dataSourceArray[indexPath.row]
        productDetailVC.fkProduct = fkProduct
        self.navigationController?.pushViewController(productDetailVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
