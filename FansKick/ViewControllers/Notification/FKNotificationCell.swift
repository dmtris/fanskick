//
//  FKNotificationCell.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 11/1/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKNotificationCell: UITableViewCell {

    @IBOutlet weak var notificationLbl:UILabel?
    @IBOutlet weak var dateLbl:UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
