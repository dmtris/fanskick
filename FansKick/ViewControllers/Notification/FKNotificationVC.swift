//
//  FKNotificationVC.swift
//  FansKick
//
//  Created by FansKick-Sunil on 25/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKNotificationVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var notificationArray = [FKNotificationInfo]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "NOTIFICATIONS"
        
        tableView.tableFooterView = UIView.init()
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
       
        self.perform(#selector(apiCallForNotification), with: nil, afterDelay: 0.1)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    // use to navigate back screen
    @objc func backBtnAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func apiCallForNotification(){
        
        let params = Dictionary<String, Any>()
        
        ServiceHelper.request(params: params, method: .get, apiName: "notifications/notification_list") { (result, error, responseCode) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if (error == nil){
                let results = result as! Dictionary<String, AnyObject>
                let data = results["notification_list"]
                if  data is Array<Dictionary<String, AnyObject>>{
                    self.notificationArray =  FKNotificationInfo.list(data: data as! Array<Dictionary<String, AnyObject>>)
                }
                
                if self.notificationArray.count == 0{
                    MessageView.showMessage(message: "Right now you don't have pending notification", time: 5.0, verticalAlignment: .bottom)
                }
                
                self.tableView?.reloadData()
            }
        }
    }
    
    // MARK:- tableview datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FKNotificationCell") as! FKNotificationCell
        let notification = self.notificationArray[indexPath.row]
        cell.notificationLbl?.text = notification.content
        cell.dateLbl?.text =  notification.notificationDate.dateFromUTC()?.dateString(DATE_TIME_FORMAT)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let notification = self.notificationArray[indexPath.row]

        if notification.notificationType == "Order"
        {
            let order =  FKMyOrdersInfo()
            order.id = notification.notificationID
            let stoaryboard = UIStoryboard.init(name: "Merchandise", bundle: nil)
            let orderDetailVC = stoaryboard.instantiateViewController(withIdentifier: "FKOrderDetailVC") as! FKOrderDetailVC
         
            orderDetailVC.isNavigateFromNotification = true
            orderDetailVC.orderDetail = order
            self.navigationController?.pushViewController(orderDetailVC, animated: true)
        }
        
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool{
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            notificationArray.remove(at: indexPath.row)
            tableView.reloadData()
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
