//
//  FKLineUpViewController.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 11/8/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

extension UIView {
    
    func shadowOnView(_ color: UIColor = UIColor.black) {
        self.layer.shadowColor = color.cgColor;
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = 1
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 4.5, height: -1.5)
    }
}
extension BinaryInteger {
    var degreesToRadians: CGFloat { return CGFloat(Int(self)) * .pi / 180 }
}

extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}


class FKLineUpViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
 

    var fixture:FKFixtureListModel?
    var lineupInfo: FKLineUpFormationInfo?
    var dataSourceArray = [FKLineUpGroupInfo]()
  


    @IBOutlet weak var collectionView:UICollectionView?


    override func viewDidLoad() {
        super.viewDidLoad()
        lineupApiCall()
    }

    override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
    }

    
    func lineupApiCall(){
        
        var params          = Dictionary<String,Any>()
        var fixture     = Dictionary<String,Any>()
        fixture["id"] = self.fixture?.id
        params["fixture"] = fixture
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: .post, apiName: "fixtures/lineup_formation", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if statusCode == 200 {
                    self.lineupInfo = FKLineUpFormationInfo.lineupInfo(data: results)
                 //   self.dataSourceArray = FKLineUpGroupInfo.group(data: (self.lineupInfo?.homePlayer)!)
                    self.collectionView?.reloadData()
                }else{
                    MessageView.showMessage(message: "Line-up information not updated yet.", time: 5.0, verticalAlignment: .bottom)
                }
            }
        }
    }
    
    
    //MARK: UIcollectionView Overrides
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if self.lineupInfo == nil{
            return 0
        }
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FKLineUpCell", for: indexPath) as! FKLineUpCell
        
        cell.lineupInfo = self.lineupInfo
        if indexPath.item == 0{
        
            cell.clubLogo.normalLoad((fixture?.clubImage)!)
            cell.clubName.text = fixture?.club
            cell.homeFormation()
        }else{
            cell.clubLogo.normalLoad((fixture?.apponentImage)!)
            cell.clubName.text = fixture?.apponent
            cell.awayFormation()
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
      return  CGSize.init(width: self.view.frame.width, height: self.view.frame.height)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
