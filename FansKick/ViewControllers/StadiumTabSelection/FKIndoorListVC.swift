//
//  FKIndoorListVC.swift
//  FansKick
//
//  Created by Sunil Verma on 21/11/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit
import IndoorAtlas



class FKIndoorListVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    
    @IBOutlet weak var tblView:UITableView?
    @IBOutlet weak var searchBarView: UISearchBar?

    var dataSourceArray = [FKStadiumGroupInfo]()
    var localDataSourceArray = [FKStadiumGroupInfo]()

		var selectedIndex = -1
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "STADIUMS"
        
        tblView?.tableFooterView = UIView.init()
        tblView?.rowHeight = UITableViewAutomaticDimension
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        
        self.apiCallForStadium()
        
        
    }
    
    func apiCallForStadium(){
        
        
        var params = Dictionary<String,Any>()
        params["page"] = "1"
        params["per_page"] = "500"
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: MethodType.get, apiName: "stadiums/stadium_list") { (result, error, statusCode) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if (error == nil){
                
                if statusCode == 200{
                    let results = result as! Dictionary<String, AnyObject>
                    
                    if let data = results["stadiums"] {
                        self.dataSourceArray.append(contentsOf: FKStadiumInfo.list(data: data as! Array<Dictionary<String, AnyObject>>))
                    }
                    
                    self.localDataSourceArray.removeAll()
                    
                    self.localDataSourceArray.append(contentsOf: self.dataSourceArray)
                    
                    self.tblView?.reloadData()
                }
            }
        }
    }
    
    // use to navigate back screen
    @objc func backBtnAction(){
        
        self.navigationController?.popViewController(animated: true)
    }
	
	@objc func headerTapAction(sender:UIButton) {
		
		if sender.tag == selectedIndex {
			selectedIndex = -1
		}else{
			selectedIndex = sender.tag
		}
		
		tblView?.reloadData()
	}
	
    //MARK: UITableView Overrides
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "FKDetailTitleCell") as! FKDetailTitleCell
        let group = self.localDataSourceArray[section]
        cell.titleLbl?.text = group.stadiumName.uppercased()
			cell.tapBtn?.tag = section
			
			cell.tapBtn?.addTarget(self, action: #selector(headerTapAction), for: .touchUpInside)
			
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.localDataSourceArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let group = self.localDataSourceArray[section]
			
			if section == selectedIndex {
				return group.floors.count
			}
			
			return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: "FKIndoorCell") as! FKIndoorCell
        let group = self.localDataSourceArray[indexPath.section]
        let stadium =  group.floors [indexPath.row]
        cell.nameLbl?.text = stadium.floorName.uppercased()
        cell.locationLbl?.text = stadium.stadiumAddress
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let indoorVC = self.storyboard?.instantiateViewController(withIdentifier: "FKIndoorMapVC") as! FKIndoorMapVC
        
        let group = self.localDataSourceArray[indexPath.section]
        indoorVC.floorInfo = group.floors [indexPath.row]
        self.navigationController?.pushViewController(indoorVC, animated: true)
    }
    
    //MARK: UISearchBar Overrides
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = ""
        self.searchWithText(searchText: (searchBar.text?.trimWhiteSpace)!)

    }
    
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.searchWithText(searchText: (searchBar.text?.trimWhiteSpace)!)
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.searchWithText(searchText: (searchBar.text?.trimWhiteSpace)!)
    }
    
    func searchWithText(searchText:String){
        localDataSourceArray.removeAll()
			selectedIndex = -1
        if searchText.isEmpty{
            localDataSourceArray.append(contentsOf: self.dataSourceArray)
        }else{
            let data = self.dataSourceArray.filter{ ($0.stadiumName.contains(searchText))}
            localDataSourceArray.append(contentsOf: data)
            
        }
        tblView?.reloadData()

       
        
      //  let data = array.filter{ ($0.start_time.dateFromUTC()?.dateString("yyyy-MM-dd").contains(startDate))!}

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
