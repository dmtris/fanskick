//
//  FKStadiuVC.swift
//  FansKick
//
//  Created by FansKick-Sunil on 17/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKStadiumVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblView: UITableView!
    var isFromMore:Bool = false
    
    
    var dataArr = [String]()
    var iconArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "STADIUM"
        dataArr = ["TICKETS","BROADCASTING", "BUDDY", "STATS","TEAM RANKING" ,"FIXTURES","INDOOR MAP"]
        
      
        let appDel = UIApplication.shared.delegate as! AppDelegate
        
        appDel.setupNavigationBar(navigationBar: self.navigationController!)
        iconArray = ["c","Z","3", "d","V","U","L"]
        if isFromMore{
            let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
            
            self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        }else{
            self.navigationItem.leftBarButtonItem = AppUtility.fanskickLogo()

        }
        
        self.tblView.tableFooterView = UIView.init()
    }
    override func viewWillAppear(_ animated: Bool) {
        //let value = UIInterfaceOrientation.portrait.rawValue
       // UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    // use to navigate back screen
    @objc func backBtnAction(){
        self.navigationController?.popViewController(animated: true)
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let moreCell:FKMoreCell = tableView.dequeueReusableCell(withIdentifier: "FKMoreCell", for: indexPath) as! FKMoreCell
        moreCell.titleLbl.text = dataArr[indexPath.row].uppercased()
        moreCell.omgIcon.setTitle(iconArray[indexPath.row], for: .normal)
        return moreCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if indexPath.row == 0 {
            let storyboard = UIStoryboard.init(name: "TicketSection", bundle: nil)
            let ticketVC = storyboard.instantiateViewController(withIdentifier: "FKTicketsDashboardVC") as! FKTicketsDashboardVC
            ticketVC.isTicket = true
            self.navigationController?.pushViewController(ticketVC, animated: true)
            
        } else if indexPath.row == 1 {
            
            let storyboard = UIStoryboard.init(name: "Broadcost", bundle: nil)
            let broadcost = storyboard.instantiateViewController(withIdentifier: "broadCostVC") as! FKBroadCastVC
            self.navigationController?.pushViewController(broadcost, animated: true)
            
        } else if indexPath.row == 2 {
            self.navigationController?.pushViewController((self.storyboard?.instantiateViewController(withIdentifier: "FKBuddyVC"))!, animated: true)
        } else if indexPath.row == 3 {
            let storyboard = UIStoryboard.init(name: "StatsSection", bundle: nil)
            self.navigationController?.pushViewController(storyboard.instantiateViewController(withIdentifier: "statsVC"), animated: true)
        } else if indexPath.row == 4 {
            let rankingVC = self.storyboard?.instantiateViewController(withIdentifier: "FKRankingViewController") as! FKRankingViewController
            self.navigationController?.pushViewController(rankingVC, animated: true)
            
        }
        else if indexPath.row == 5 {
            self.navigationController?.pushViewController((self.storyboard?.instantiateViewController(withIdentifier: "FKFixturesViewController"))!, animated: true)
        } else if indexPath.row == 6 {
            let storyboard = UIStoryboard.init(name: "IndoorMapSection", bundle: nil)
            self.navigationController?.pushViewController(storyboard.instantiateViewController(withIdentifier: "FKIndoorListVC"), animated: true)
        }
        
        //Broadcost
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
