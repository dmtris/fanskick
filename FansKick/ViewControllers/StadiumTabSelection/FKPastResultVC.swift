//
//  FKPastResultVC.swift
//  FansKick
//
//  Created by Sunil Verma on 02/02/18.
//  Copyright © 2018 FansKick Dev. All rights reserved.
//

import UIKit

class FKPastResultVC: UIViewController, UITableViewDelegate,UITableViewDataSource {
	
	@IBOutlet weak var tblView: UITableView!
	
	var dataSourceArray = [FKPreviousResultInfo]()
	var refereshControl = UIRefreshControl()
	
	var page = PAGE()
	override func viewDidLoad() {
		super.viewDidLoad()
		
		
		self.navigationItem.title = "RESULTS"
		//  self.navigationItem.title = floorInfo?.stadiumName

        refereshControl.tintColor = GREEN_COLOR
		refereshControl.addTarget(self, action: #selector(refereshControlAction), for: .valueChanged)
		tblView.addSubview(refereshControl)
		
		self.perform(#selector(apiCallForPreviousResult), with: nil, afterDelay: 0.1)
	}
	
	@objc func  refereshControlAction() {
		page.startIndex = 1
		apiCallForPreviousResult()
	}
	@objc func apiCallForPreviousResult(){
		var params          = Dictionary<String,Any>()
		params["page"]      = "\(page.startIndex)"
		params["per_page"]  = "\(page.pageSize)"
		
		
		var league = Dictionary <String, Any>()
		league["id"] = ""
		
		var club = Dictionary <String, Any>()
		club["id"] = ""
		
		params["league"] = league
		params["club"] = club
		params["type"] = ""
		
		MBProgressHUD.showAdded(to: self.view, animated: true)
		ServiceHelper.request(params: params, method: .post, apiName: "fixtures/past_fixture_list", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
			MBProgressHUD.hide(for: self.view, animated: true)
			self.refereshControl.endRefreshing()
			if error == nil {
				let results = result as! Dictionary<String, AnyObject>
				if statusCode == 200 {
					
					if let data = results["fixture_list"]{
						
						if self.page.startIndex == 1 {
							self.dataSourceArray.removeAll()
						}
						
					//	self.dataSourceArray =  FKPreviousResultInfo.previousResultList(data: data as! Array<Dictionary<String, AnyObject>>)
						
						self.dataSourceArray.append(contentsOf: FKPreviousResultInfo.previousResultList(data: data as! Array<Dictionary<String, AnyObject>>))
						
						if self.dataSourceArray.isEmpty{
							MessageView.showMessage(message: "No past result found.", time: 5.0, verticalAlignment: .bottom)
							
						} else {
							self.page.totalPage = result!["total_pages"] as! Int
						}
					}
				}
				self.tblView.reloadData()
			}
		}
	}
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
	}
	
	
	
	// MARK:- tableview datasource
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return  dataSourceArray.count
	}
	
	
	
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		
		let cell : FKPreviousResultCell!
		if DeviceType.IS_IPAD_PRO || DeviceType.IS_IPAD {
			cell  = tableView.dequeueReusableCell(withIdentifier: "FKPreviousResultCelliPad", for: indexPath) as! FKPreviousResultCell
			
		}else {
			cell  = tableView.dequeueReusableCell(withIdentifier: "FKPreviousResultCell", for: indexPath) as! FKPreviousResultCell
		}
		
		let result = self.dataSourceArray[indexPath.row]
		cell.homeTeamName?.text = result.homeTeam
		cell.homeTeamLogo?.normalLoad(result.homeTeamLogo)
		cell.awayTeamName?.text = result.awayTeam
		cell.awayTeamLogo?.normalLoad(result.awayTeamLogo)
		cell.timeLbl?.text = result.kickOffTime.dateFromUTC()?.dateString(DATE_TIME_FORMAT)
	//	cell.goalLbl?.text = "\(result.homeTeamScore?.score ?? "0") : \(result.awayTeamScore?.score ?? "0")"
		
			cell.goalLbl?.text = "\(result.homeTeamScoreString) : \(result.awayTeamScoreString)"
		cell.stadiumName?.text = result.stadiumName
		return cell
		
		
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
	{
		
		if DeviceType.IS_IPAD_PRO || DeviceType.IS_IPAD{
			return 120.0
		}else{
			return 90.0
		}
		
	}
	
	// MARK:- tableview delegate
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)	{
		let fixtureDetailVc = self.storyboard?.instantiateViewController(withIdentifier: "FKPreviousResultVC") as! FKPreviousResultVC
		fixtureDetailVc.previousResult = self.dataSourceArray[indexPath.row]
		self.navigationController?.pushViewController(fixtureDetailVc, animated: true)
		
	}
	
	//MARK:
	func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
		
		// UITableView only moves in one direction, y axis
		let currentOffset = scrollView.contentOffset.y
		let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
		
		// Change 10.0 to adjust the distance from bottom
		if maximumOffset - currentOffset <= 10.0 {
			if page.totalPage > page.startIndex{
				page.startIndex = page.startIndex+1;
				self.apiCallForPreviousResult()
			}
		}
	}
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
	}
	*/
	
}
