//
//  FKIndoorMapVC.swift
//  FansKick
//
//  Created by FansKick-Sunil on 17/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//



import UIKit
import MapKit
import IndoorAtlas


// Function to convert degrees to radians
func degreesToRadians(_ x:Double) -> Double {
    return (Double.pi * x / 180.0)
}
// Blue dot & accuracy circle annotation class
class LocationAnnotation: MKPointAnnotation {
    enum LocationType {
        case blueDot
        case accuracyCircle
    }
    
    var radius: Double
    var locationType: LocationType
    
    required init(locationType: LocationType, radius: Double) {
        self.radius = radius
        self.locationType = locationType
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


// Class for map overlay object
class MapOverlay: NSObject, MKOverlay {
    var coordinate: CLLocationCoordinate2D
    var boundingMapRect: MKMapRect
    
    
    // Initializer for the class
    init(floorPlan: IAFloorPlan, andRotatedRect rotated: CGRect) {
        coordinate = floorPlan.center
        
        // Area coordinates for the overlay
        let topLeft = MKMapPointForCoordinate(floorPlan.topLeft)
        boundingMapRect = MKMapRectMake(topLeft.x + Double(rotated.origin.x), topLeft.y + Double(rotated.origin.y), Double(rotated.size.width), Double(rotated.size.height))
    }
}


// Class for rendering map overlay objects
class MapOverlayRenderer: MKOverlayRenderer {
    var overlayImage: UIImage
    var floorPlan: IAFloorPlan
    var rotated: CGRect
    
    init(overlay:MKOverlay, overlayImage:UIImage, fp: IAFloorPlan, rotated: CGRect) {
        self.overlayImage = overlayImage
        self.floorPlan = fp
        self.rotated = rotated
        super.init(overlay: overlay)
    }
    
    override func draw(_ mapRect: MKMapRect, zoomScale: MKZoomScale, in ctx: CGContext) {
        
        // Width and height in MapPoints for the floorplan
        let mapPointsPerMeter = MKMapPointsPerMeterAtLatitude(floorPlan.center.latitude)
        let rect = CGRect(x: 0, y: 0, width: Double(floorPlan.widthMeters) * mapPointsPerMeter, height: Double(floorPlan.heightMeters) * mapPointsPerMeter)
        ctx.translateBy(x: -rotated.origin.x, y: -rotated.origin.y)
        
        // Rotate around top left corner
        ctx.rotate(by: CGFloat(degreesToRadians(floorPlan.bearing)));
        
        // Draw the floorplan image
        UIGraphicsPushContext(ctx)
        overlayImage.draw(in: rect, blendMode: CGBlendMode.normal, alpha: 1.0)
        UIGraphicsPopContext();
    }
}

// View controller for Apple Maps Overlay Example
class FKIndoorMapVC: UIViewController, IALocationManagerDelegate, MKMapViewDelegate {
    
    
    var floorInfo:FKStadiumInfo?
    
    var floorPlanFetch:IAFetchTask!
    var imageFetch:AnyObject!
    
    var fpImage = UIImage()
    
    var map = MKMapView()
    var camera = MKMapCamera()
    var circle = MKCircle()
    var currentCircle: LocationAnnotation? = nil
    var currentAccuracyCircle: MKCircle? = nil
    var currentLocation: CLLocation? = nil
    var flooorPlanOverlay: MapOverlay? = nil
    var updateCamera = true
    
    var floorPlan = IAFloorPlan()
    var locationManager = IALocationManager.sharedInstance()
    var resourceManager = IAResourceManager()
    
    var rotated = CGRect()
    
    // User defind
    var indoorNavigationOn = false
    var isInsideTheReason = false
    
    @IBOutlet weak var navigateBtn:UIButton?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "INDOOR MAP"
        //  self.navigationItem.title = floorInfo?.stadiumName
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        
//        Timer.scheduledTimer(withTimeInterval: 30.0, repeats: true) { (timer) in
//
//            self.isInsideTheReason = !self.isInsideTheReason
//        }
        

        
    }
    
    // Called when view will appear and sets up the map view and its bounds and delegate. Also requests location
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.addMapView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateCamera = true
        UIApplication.shared.isIdleTimerDisabled = true
    }
    
    // use to navigate back screen
    @objc func backBtnAction(){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func navigationModeOnOff(){
        
        indoorNavigationOn = !indoorNavigationOn
        
        if indoorNavigationOn{
            self.navigateBtn?.backgroundColor = UIColor.red
        }else{
            self.navigateBtn?.backgroundColor = GREEN_COLOR
        }
    }
    
   @objc func addMapView(){
        //map = MKMapView(frame: self.view.bounds)
        map.removeOverlays(map.overlays)
        //        map.removeAnnotations(map.annotations)
        map.removeFromSuperview()
        map.frame = self.view.bounds
        map.delegate = self
        map.isPitchEnabled = false
        self.view.addSubview(map)
        self.view.sendSubview(toBack: map)
        map.showsUserLocation = true
    
        requestLocation()
        
        MBProgressHUD.showAdded(to: self.view, animated: true, titile: "Loading map...")
        self.perform(#selector(updateMap), with: nil, afterDelay: 5.0)
    }
    
    
    @objc func updateMap() {
        
			print("=================",(floorInfo?.floorId) ?? "")
        let floor  =  IALocation.init(floorPlanId: (floorInfo?.floorId)!)
        // let floor  =  IALocation.init(floorPlanId: "e8bce38b-1a85-465b-b565-4f4258ac9584")
        
			print(">>>>>>>>>>>",floor.region?.identifier ?? "")
        
        floorPlanFetch = self.resourceManager.fetchFloorPlan(withId: floor.region?.identifier, andCompletion: { (floorplan, error) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if (error == nil) {
                self.floorPlan = floorplan!
                print("Floor plan image ==============",self.floorPlan.imageUrl ?? "")
                self.fetchImage(floorplan!)
                let location =    CLLocation.init(latitude: (floorplan?.center.latitude)!, longitude: (floorplan?.center.longitude)!)
                self.initialiseLocation(location: location)
            } else {
                MessageView.showMessage(message: "There was an error during floorplan update", time: 0, verticalAlignment: .bottom)
                print("There was an error during floorplan fetch: ", error as Any)
            }
        })
    }
    // Function to change the map overlay
    func changeMapOverlay() {
        
        //Width and height in MapPoints for the floorplan
        let mapPointsPerMeter = MKMapPointsPerMeterAtLatitude(floorPlan.center.latitude)
        let widthMapPoints = floorPlan.widthMeters * Float(mapPointsPerMeter)
        let heightMapPoints = floorPlan.heightMeters * Float(mapPointsPerMeter)
        
        print("   =================  \(widthMapPoints)  \(heightMapPoints)  \(mapPointsPerMeter)")
        let cgRect = CGRect(x: 0, y: 0, width: CGFloat(widthMapPoints), height: CGFloat(heightMapPoints))
        let a = degreesToRadians(self.floorPlan.bearing)
        rotated = cgRect.applying(CGAffineTransform(rotationAngle: CGFloat(a)));
        flooorPlanOverlay = MapOverlay(floorPlan: floorPlan, andRotatedRect: rotated)
        map.add(flooorPlanOverlay!)
        print("sssssssss",map.overlays);
        updateCircles()
    }
    
    // Function for rendering overlay objects
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        var circleRenderer:MKCircleRenderer!
        
        print("=============11==========\(overlay)")
        // If it is possible to convert overlay to MKCircle then render the circle with given properties. Else if the overlay is class of MapOverlay set up its own MapOverlayRenderer. Else render red circle.
        if overlay is MapOverlay {
            let overlayView = MapOverlayRenderer(overlay: overlay, overlayImage: fpImage, fp: floorPlan, rotated: rotated)
            return overlayView
            
        } else {
            circleRenderer = MKCircleRenderer(overlay: overlay)
            circleRenderer.fillColor = UIColor.red//UIColor.init(red: 0.08627, green: 0.5059, blue: 0.9843, alpha: 0.4)
            return circleRenderer
        }
    }
    
    func indoorLocationManager(_ manager: IALocationManager, didUpdateLocations locations: [Any]) {
        
        // Convert last location to IALocation
        //        let l = locations.last as! IALocation
        //
        //        // Check that the location is not nil
        //        if let newLocation = l.location?.coordinate {
        //
        //            SVProgressHUD.dismiss()
        //            currentLocation = l.location
        //
        //            if currentAccuracyCircle != nil {
        //                map.remove(currentAccuracyCircle!)
        //            }
        //
        //            currentAccuracyCircle = MKCircle(center: newLocation, radius: (l.location?.horizontalAccuracy)!)
        //            map.add(currentAccuracyCircle!)
        //
        //            // Remove the previous circle overlay and set up a new overlay
        //            if currentCircle == nil {
        //                currentCircle = LocationAnnotation(locationType: .blueDot, radius: 25)
        //                map.addAnnotation(currentCircle!)
        //            }
        //            currentCircle?.coordinate = newLocation
        //
        //            if updateCamera {
        //                // Ask Map Kit for a camera that looks at the location from an altitude of 300 meters above the eye coordinates.
        //                camera = MKMapCamera(lookingAtCenter: (l.location?.coordinate)!, fromEyeCoordinate: (l.location?.coordinate)!, eyeAltitude: 300)
        //
        //                // Assign the camera to your map view.
        //                map.camera = camera
        //                updateCamera = false
        //            }
        //        }
        
        
    }
    
    func updateCircles() {
        //        if currentAccuracyCircle != nil {
        //            map.remove(currentAccuracyCircle!)
        //        }
        
        //        if currentCircle == nil {
        //            currentCircle = LocationAnnotation(locationType: .blueDot, radius: 25)
        //            map.addAnnotation(currentCircle!)
        //        }
        //
        //        currentAccuracyCircle = MKCircle(center: (currentLocation?.coordinate)!, radius: (currentLocation?.horizontalAccuracy)!)
        //        map.add(currentAccuracyCircle!)
        //        currentCircle?.coordinate = (currentLocation?.coordinate)!
    }
    
    // Fetches image with the given IAFloorplan
    func fetchImage(_ floorPlan:IAFloorPlan) {
        imageFetch = self.resourceManager.fetchFloorPlanImage(with: floorPlan.imageUrl!, andCompletion: { (data, error) in
            if (error != nil) {
                print(error as Any)
            } else {
                self.fpImage = UIImage.init(data: data!)!
                self.changeMapOverlay()
            }
        })
    }
    
    func indoorLocationManager(_ manager: IALocationManager, didEnter region: IARegion) {
        
        isInsideTheReason = true
        
        switch region.type {
        case .iaRegionTypeVenue:
            map.showsUserLocation = true
            print("Enter region \(region.identifier)")
            break
        case .iaRegionTypeFloorPlan:
            
            updateCamera = true
            
            if (floorPlanFetch != nil) {
                floorPlanFetch.cancel()
                floorPlanFetch = nil
            }
            
            // Fetches the floorplan for the given region identifier
            floorPlanFetch = self.resourceManager.fetchFloorPlan(withId: region.identifier, andCompletion: { (floorplan, error) in
                
                if (error == nil) {
                    self.floorPlan = floorplan!
                    self.fetchImage(floorplan!)
                } else {
                    print("There was an error during floorplan fetch: ", error as Any)
                }
            })
            break
        default:
            return
        }
    }
    
    func indoorLocationManager(_ manager: IALocationManager, didExitRegion region: IARegion) {
        
        isInsideTheReason = false
        
        switch region.type {
        case .iaRegionTypeVenue:
            print("Exit Venue \(region.identifier)")
            map.showsUserLocation = true
            if currentCircle != nil {
                map.removeAnnotation(currentCircle!)
            }
            if currentAccuracyCircle != nil {
                map.remove(currentAccuracyCircle!)
            }
            break
        case .iaRegionTypeFloorPlan:
            if flooorPlanOverlay != nil {
                map.remove(flooorPlanOverlay!)
            }
            break
        default:
            return
        }
    }
    // Authenticate to IndoorAtlas services and request location updates
    func requestLocation() {
        
        locationManager.delegate = self
        resourceManager = IAResourceManager(locationManager: locationManager)!
        locationManager.startUpdatingLocation()
    }
    
    
    
    //MARK: MKMap view overrides
    // Called when view will disappear and will remove the map from the view and sets its delegate to nil
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        UIApplication.shared.isIdleTimerDisabled = false
        
        locationManager.stopUpdatingLocation()
        locationManager.delegate = nil
        
        map.delegate = nil
        map.removeFromSuperview()
        
        MessageView.hide()
        
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        if currentCircle != nil {
            //    map.removeAnnotation(currentCircle!)
        }
        if currentAccuracyCircle != nil {
            //   map.remove(currentAccuracyCircle!)
        }
        
        var spanDelta = 0.005  // default value
        
       // if isInsideTheReason {
            spanDelta = 0.001
      //  }
        
        if indoorNavigationOn {
            let span = MKCoordinateSpan(latitudeDelta: spanDelta, longitudeDelta: spanDelta)
            let location = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
            let region = MKCoordinateRegion(center: location, span: span)
            
            currentLocation =    CLLocation.init(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
            
            map.setRegion(region, animated: true)
        }
        
    }
    
    func initialiseLocation(location:CLLocation){
        
        // currentLocation = location
        
        if currentAccuracyCircle != nil {
            map.remove(currentAccuracyCircle!)
        }
        
        currentAccuracyCircle = MKCircle(center: location.coordinate, radius: (location.horizontalAccuracy))
        map.add(currentAccuracyCircle!)
        
        // Remove the previous circle overlay and set up a new overlay
        if currentCircle == nil {
            currentCircle = LocationAnnotation(locationType: .blueDot, radius: 25)
            map.addAnnotation(currentCircle!)
        }
        currentCircle?.coordinate = location.coordinate
        
        if updateCamera {
            // Ask Map Kit for a camera that looks at the location from an altitude of 300 meters above the eye coordinates.
            camera = MKMapCamera(lookingAtCenter: (location.coordinate), fromEyeCoordinate: (location.coordinate), eyeAltitude: 300)
            
            // Assign the camera to your map view.
            map.camera = camera
            updateCamera = false
        }
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? LocationAnnotation {
            var type = ""
            let color = UIColor(red: 0, green: 125/255, blue: 1, alpha: 1)
            var alpha: CGFloat = 1.0
            
            var borderWidth:CGFloat = 0
            var borderColor = UIColor(red: 0, green: 30/255, blue: 80/255, alpha: 1)
            
            switch annotation.locationType {
            case LocationAnnotation.LocationType.blueDot:
                type = "blueDot"
                //  borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
                borderColor =   UIColor.red//UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 1)
                borderWidth = 3
                break
            case LocationAnnotation.LocationType.accuracyCircle:
                type = "accuracyCircle"
                alpha = 0.2
                borderWidth = 0 // 1
                break
            default:
                break
            }
            
            let annotationView: MKAnnotationView = map.dequeueReusableAnnotationView(withIdentifier: type) ?? MKAnnotationView.init(annotation: annotation, reuseIdentifier: type)
            
            annotationView.annotation = annotation
            annotationView.frame = CGRect(x: 0, y: 0, width: annotation.radius, height: annotation.radius)
            annotationView.backgroundColor = color
            annotationView.alpha = alpha
            annotationView.layer.borderWidth = borderWidth
            annotationView.layer.borderColor = borderColor.cgColor
            annotationView.layer.cornerRadius = annotationView.frame.size.width / 2
            
            let mask = CAShapeLayer()
            mask.path = UIBezierPath(ovalIn: annotationView.frame).cgPath
            annotationView.layer.mask = mask
            
            return annotationView
            
        }
        return nil
    }
    
    
}
