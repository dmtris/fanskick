//
//  FKTicketsCell.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 10/24/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKTicketsCell: UITableViewCell {

    @IBOutlet weak var team1Lbl: UILabel!
    @IBOutlet weak var team2Lbl: UILabel!
    @IBOutlet weak var team1ImgVw: UIImageView!
    @IBOutlet weak var team2Img: UIImageView!
    @IBOutlet weak var buyTicketBtn: IndexPathButton!
    @IBOutlet weak var timeLbl: UIButton!
    @IBOutlet weak var stadiumNameLbl: UILabel!
    @IBOutlet weak var playBtn: UIButton?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
