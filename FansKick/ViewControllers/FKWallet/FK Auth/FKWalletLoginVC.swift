//
//  FKWalletLoginVC.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 1/3/18.
//  Copyright © 2018 FansKick Dev. All rights reserved.
//

import UIKit

let NOTIFICATION_FOR_DISMISS = "NOTIFICATION_FOR_DISMISS"

typealias FKWalletCompletionBlock = (Bool?, Dictionary<String, Any>) -> Void

class FKWalletLoginVC: UIViewController, UITextFieldDelegate, FKTextFieldDelegate {
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var emailFieldView: FKTextFieldView!
    @IBOutlet weak var passwordFieldView: FKTextFieldView!
    @IBOutlet weak var googleBtnAction: UIButton!
    @IBOutlet weak var fbButton: UIButton!
    
    private var completionBlock : FKWalletCompletionBlock? = nil
    
    //MARK:- UIViewController life cycle >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Email field initialization
        emailFieldView.setFKKeyboardType(keyboardType: .emailAddress)
        emailFieldView.setFKReturnKeyType(returnKeyType: .next)
        emailFieldView.setFKAutocorrectionType(autocorrectionType: .no)
        emailFieldView.setFKAutocapitalizationType(autocapitalizationType: .none)
        emailFieldView.delegate = self
        
        // Password field initialization
        passwordFieldView.setFKSecureTextEntry(secureTextEntry: true)
        passwordFieldView.setFKReturnKeyType(returnKeyType: .default)
        passwordFieldView.delegate = self
        
        self.navigationItem.title = "FKWALLET LOGIN"
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
//        emailFieldView.setFKText(fkText: "vijv@b")
//        passwordFieldView.setFKText(fkText: "11111111")
        
        NotificationCenter.default.removeObserver(self, name:  NSNotification.Name(rawValue: NOTIFICATION_FOR_DISMISS), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(notifyToDissmiss), name: NSNotification.Name(rawValue: NOTIFICATION_FOR_DISMISS), object: nil)
        
    }
    
    func completionBlock(completion: @escaping FKWalletCompletionBlock) {
        self.completionBlock = completion
    }
    
    @objc func notifyToDissmiss(noti:Notification?) {
        
        print(noti as Any)
        print(noti?.object as Any)
        
        if noti?.object != nil{
            dismissAction(data: noti?.object as! Dictionary<String, Any>)
        }
    }
    
    @objc func dismissAction(data:Dictionary<String,Any>) {
        
        print("==============",data)
        if completionBlock != nil {
            completionBlock!(true, data)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    // use to navigate back screen
    @objc func backBtnAction() {
        
        self.dismiss(animated: true, completion: nil)
        if completionBlock != nil {
            completionBlock!(false, Dictionary<String,Any>())
        }
    }
	
	
	@IBAction func forgotPasswordAction(){
	
	}
	
    override func viewWillAppear(_ animated: Bool) {
       
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.setupNavigationBar(navigationBar: self.navigationController!)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        emailFieldView.leftPadingFontIcon(iconName: "a")
        passwordFieldView.leftPadingFontIcon(iconName: "6")
    }
    
    func callApiToLogin() {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        var params = Dictionary<String,Any>()
        params[SENZPAY_EMAIL] = self.emailFieldView.text.trimWhiteSpace
        params[SENZPAY_PASSWORD] = self.passwordFieldView.text.trimWhiteSpace
        
        //user_login.php
        //"user_signup.php"
        FKSenzPayHelper.senzpayRequest(params: params, method: .get, apiName: "user_login.php") { (result, error, responseCode) in
            print(result ?? "")
            MBProgressHUD.hide(for: self.view, animated: true)
            print(responseCode)
            
            let responseData = result as! Dictionary<String,AnyObject>
            let status      = responseData.validatedValue(SENZPAY_STATUS, expected:"" as AnyObject ) as! String
            if status == "false" {
                // false case error messsage
                MessageView.showMessage(message: responseData["msg"] as! String, time: 5.0, verticalAlignment: .bottom)
            } else {
                // handle true response
                
                let userID = responseData.validatedValue(SENZPAY_USER_ID, expected:"" as AnyObject ) as! String
                let email = responseData.validatedValue(SENZPAY_EMAIL, expected:"" as AnyObject ) as! String
                let phone = responseData.validatedValue(SENZPAY_PHONE, expected:"" as AnyObject ) as! String
               
                defaults.set(AESCrypto.aes256EncryptString(userID, withKey: crypto256Key), forKey: SENZPAY_USER_ID)
                defaults.set(AESCrypto.aes256EncryptString(email, withKey: crypto256Key), forKey: SENZPAY_EMAIL_DEFAULT)
                defaults.set(AESCrypto.aes256EncryptString(self.passwordFieldView.text.trimWhiteSpace, withKey: crypto256Key), forKey: SENZPAY_PASSWORD_DEFAULT)
                defaults.set(AESCrypto.aes256EncryptString(phone, withKey: crypto256Key), forKey: SENZPAY_PHONE)
           
                self.dismissAction(data: responseData)
            }
        }
    }
	
	
    
    @IBAction func loginBtn(_ sender: Any) {
        self.view.endEditing(true)
        
        if allFieldVerified() {
            callApiToLogin()
        }
    }
    
    func allFieldVerified()-> Bool {
        
        var isVerified = false
        
        if emailFieldView.text.trimWhiteSpace.isEmpty {
            
            emailFieldView.setFKErrorMessage(message: "Please enter Email")
        } else {
            if !emailFieldView.text.trimWhiteSpace.isEmail {
                emailFieldView.setFKErrorMessage(message: "Please enter valid Email")
                
            } else {
                if passwordFieldView.text.isEmpty {
                    passwordFieldView.setFKErrorMessage(message: "Please enter Password")
                    
                }else if passwordFieldView.text.trimWhiteSpace.length > 5 {
                    isVerified = true
                } else {
                    passwordFieldView.setFKErrorMessage(message: "Please enter valid Password")
                }
            }
        }
        return isVerified
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.returnKeyType == UIReturnKeyType.next {
            let txtField = self.view.viewWithTag(textField.tag + 1) as! UITextField
            txtField.becomeFirstResponder()
        }else {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    
    @IBAction func registerButton(_ sender: Any) {
        let fkWalletSignup = self.storyboard?.instantiateViewController(withIdentifier: "FKWalletSignupVC") as! FKWalletSignupVC
        self.navigationController?.pushViewController(fkWalletSignup, animated: true)
    }
    
    //MARK: FKTextFieldView Overrides
    @objc  func fkTextFieldDidBeginEditing(textField: FKTextFieldView){
        
    }
    @objc func fkTextFieldShouldReturn(textField: FKTextFieldView) -> Bool
    {
        
        if textField.FKReturnKeyType() == .next{
            let nextFieldView = self.view.viewWithTag(textField.tag + 1) as! FKTextFieldView
            nextFieldView.becomeFKFirstResponder()
        }else{
            textField.resignFKFirstResponder()
        }
        
        return true
    }
    @objc func fkTextFieldDidEndEditing(textField:FKTextFieldView)
    {
        
    }
    @objc  func textField(textField: FKTextFieldView, fkShouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        return true
    }
    
    
    // MARK:- Memory Management function
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
