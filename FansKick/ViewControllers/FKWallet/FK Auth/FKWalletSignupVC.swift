//
//  FKWalletSignupVC.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 1/3/18.
//  Copyright © 2018 FansKick Dev. All rights reserved.
//

import UIKit

class FKWalletSignupVC:UIViewController,UITableViewDelegate,UITableViewDataSource,FKTextFieldDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    let placeholderTextArr = ["First name",
                              "Last name",
                              "Email",
                              "Phone number",
                              "Password",
                              "Confirm password"]
    let fontIconArray = [
        "a",// first name
        "a",// last name
        "t",// email
        "M", // phone
        "6", // password
        "6"] // confirm password
    
    var indexPath:IndexPath?
    var stateArr = [String]()
    var stateArray = [FKListInfo]()
    
    let fkUser = FKUser()
    var selectedDate = Date()
    var selectedStateId = String()
    var errorTag = 50
    
    // @IBOutlet weak var pickerMainView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        callApiToGetStates()
        self.initialise()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //let value = UIInterfaceOrientation.portrait.rawValue
        // UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
    }
    
    private func initialise() {
        
        self.navigationItem.title = "FKWALLET SIGNUP"
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        
    }
    
    // use to navigate back screen
    @objc func backBtnAction() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    private func pickerToolBar() -> UIToolbar {
        let toolBar = UIToolbar.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 44))
        let doneItem =   UIBarButtonItem.init(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(FKRegisterViewController.doneBtnAction))
        let cancelItem =   UIBarButtonItem.init(title: "Cancel", style: UIBarButtonItemStyle.done, target: self, action: #selector(FKRegisterViewController.cancelBtnAction))
        let space =  UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        toolBar.setItems([cancelItem, space, doneItem], animated: true)
        toolBar.barTintColor = UIColor.white
        toolBar.tintColor = UIColor.black
        
        return toolBar
    }
    
    private func numberPadToolBar() -> UIToolbar {
        let toolbar = UIToolbar.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 44))
        let doneItem =   UIBarButtonItem.init(title: "Next", style: UIBarButtonItemStyle.done, target: self, action: #selector(FKRegisterViewController.nextBtnAction))
        let space =  UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        toolbar.setItems([space, doneItem], animated: true)
        toolbar.barTintColor = UIColor.white
        toolbar.tintColor = UIColor.black
        
        return toolbar
    }
    
    func callApiToGetStates() {
        
        let params = Dictionary<String,Any>()
        
        ServiceHelper.request(params: params, method: .get, apiName: "users/state_list", hudType: loadingIndicatorType.withoutLoader) { (result, error, responseCode) in
            
            if (error == nil) {
                if responseCode == 200 {
                    let results = result as! Dictionary<String, AnyObject>
                    if let data = results["state_list"] {
                        self.stateArray.append(contentsOf: FKListInfo.list(data: data as! Array<Dictionary<String, AnyObject>>))
                        for state in self.stateArray{
                            self.stateArr.append(state.name)
                        }
                    } else {
                        if let data = result![kResponseMsg] {
                            MessageView.showMessage(message: data as! String, time: 5.0, verticalAlignment: .bottom)
                        }
                    }
                }else{
                }
            }
        }
    }
    
    @IBAction func termAndCondition(_ sender: Any) {
        
        let staticVC = self.storyboard?.instantiateViewController(withIdentifier: "staticContentVC")
        self.navigationController?.pushViewController(staticVC!, animated: true)
        
        
    }
    
    func getStateByName(name:String)-> FKListInfo {
        var stateInfo:FKListInfo?
        for state in self.stateArray{
            if name == state.name{
                stateInfo = state
                break
            }
        }
        if stateInfo != nil {
            return stateInfo!
        } else {
            stateInfo?.name = ""
            stateInfo?.id = ""
            
            return stateInfo!
        }
    }
    
    //MARK:- UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.placeholderTextArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:FKInputTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FKInputTableViewCell", for: indexPath) as! FKInputTableViewCell
        cell.textField.setFKPlaceHolder(placeHolder: self.placeholderTextArr[indexPath.row])
        
        cell.textField.leftPadingFontIcon(iconName: fontIconArray[indexPath.row])
        cell.textField.delegate = self
        cell.textField.setFKReturnKeyType(returnKeyType: .next)
        cell.textField.indexPath = indexPath
        cell.textField.setFKSecureTextEntry(secureTextEntry: false)
        cell.textField.setFKKeyboardType(keyboardType: .asciiCapable)
        cell.textField.maxLength = 0
        cell.textField.setFKAutocapitalizationType(autocapitalizationType: .none)
        
        switch indexPath.row {
        case 0:
            cell.textField.text = fkUser.firstName
            cell.textField.maxLength = 60
            cell.textField.setFKAutocapitalizationType(autocapitalizationType: UITextAutocapitalizationType.words)
            break
        case 1:
            cell.textField.text = fkUser.lastName
            cell.textField.maxLength = 60
            cell.textField.setFKAutocapitalizationType(autocapitalizationType: UITextAutocapitalizationType.words)
            break
        case 2:
            cell.textField.text = fkUser.email
            cell.textField.setFKKeyboardType(keyboardType: .emailAddress)
            cell.textField.maxLength = 60
            break
        case 3:
            cell.textField.text = fkUser.mobileNo
            cell.textField.setFKKeyboardType(keyboardType: .numberPad)
            cell.textField.maxLength = 13
            break
        case 4:
            cell.textField.text = fkUser.password
            cell.textField.setFKSecureTextEntry(secureTextEntry: true)
            cell.textField.maxLength = 16
            break
        case 5:
            cell.textField.text = fkUser.confrmPassword
            cell.textField.setFKSecureTextEntry(secureTextEntry: true)
            cell.textField.setFKReturnKeyType(returnKeyType: .default)
            cell.textField.maxLength = 16
            break
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 70
    }
    
    @objc func nextBtnAction(){
        
        let indexPath = IndexPath.init(row: 2, section: 0)
        if !(tableView.indexPathsForVisibleRows?.contains(indexPath))!{
            tableView.scrollToRow(at: indexPath, at: .none, animated: false)
        }
        let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
        cell.textField.becomeFKFirstResponder()
    }
    
    //MARK: FKTextFieldView Overrides
    
    @objc  func fkTextFieldDidBeginEditing(textField: FKTextFieldView){
        
        self.indexPath = textField.indexPath
        if textField.indexPath?.row == 3 {
            textField.setInputAccessoryView(inputAccessoryView:self.numberPadToolBar())
        }
    }
    
    @objc func fkTextFieldShouldReturn(textField: FKTextFieldView) -> Bool {
        
        if textField.FKReturnKeyType() == .next{
            let indexPath = IndexPath.init(row: (textField.indexPath?.row)! + 1, section: 0)
            if !(tableView.indexPathsForVisibleRows?.contains(indexPath))!{
                tableView.scrollToRow(at: indexPath, at: .none, animated: false)
            }
            let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
            cell.textField.becomeFKFirstResponder()
        } else {
            textField.resignFKFirstResponder()
        }
        
        return true
    }
    @objc func fkTextFieldDidEndEditing(textField:FKTextFieldView)
    {
        switch indexPath!.row {
        case 0:
            fkUser.firstName = textField.text
            break
        case 1:
            fkUser.lastName = textField.text
            break
        case 2:
            fkUser.email = textField.text
            break
        case 3:
            fkUser.mobileNo = textField.text
            break
        case 4:
            fkUser.password = textField.text
            break
        case 5:
            fkUser.confrmPassword = textField.text
            break
        default:
            break
        }
    }
    
    @objc  func textField(textField: FKTextFieldView, fkShouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        return true
    }
    
    // MARK:- Submit Button
    @IBAction func submitBtn(_ sender: Any) {
        self.view.endEditing(true)

        if isAllFieldVerified(){
            callApiToRegister()
        }
    }
    
    // MARK:- Api to register
    private func callApiToRegister() {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        var params = Dictionary<String,Any>()
        params[SENZPAY_EMAIL] = fkUser.email
        params[SENZPAY_PASSWORD] = fkUser.password
        
        let phoneNo = fkUser.mobileNo.mobileNumberWithCountryCode
        
        params[SENZPAY_PHONE] = phoneNo.replaceString("+", withString: "")
        params[SENZPAY_FNAME] = fkUser.firstName
        params[SENZPAY_LNAMEE] = fkUser.lastName
        
        
        FKSenzPayHelper.senzpayRequest(params: params, method: .post, apiName: "user_signup.php") { (result, error, responseCode) in
            MBProgressHUD.hide(for: self.view, animated: true)

            let responseData = result as! Dictionary<String,AnyObject>
            let status = responseData.validatedValue("status", expected: 0 as NSNumber) as! Bool
            
            if status {
                // handle true response
                
                //Store credentials details in user defaut for future
                let userID = responseData.validatedValue(SENZPAY_USER_ID, expected:"" as AnyObject ) as! String
                let email = responseData.validatedValue(SENZPAY_EMAIL, expected:"" as AnyObject ) as! String
                let phone = responseData.validatedValue(SENZPAY_PHONE, expected:"" as AnyObject ) as! String
                
                defaults.set(AESCrypto.aes256EncryptString(userID, withKey: crypto256Key), forKey: SENZPAY_USER_ID)
                defaults.set(AESCrypto.aes256EncryptString(email, withKey: crypto256Key), forKey: SENZPAY_EMAIL_DEFAULT)
                defaults.set(AESCrypto.aes256EncryptString(phone, withKey: crypto256Key), forKey: SENZPAY_PHONE)
                defaults.set(AESCrypto.aes256EncryptString(self.fkUser.password, withKey: crypto256Key), forKey: SENZPAY_PASSWORD_DEFAULT)

                self.navigationController?.popViewController(animated: true)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTIFICATION_FOR_DISMISS), object: responseData)

            } else {
                // false case error messsage
                MessageView.showMessage(message: responseData["msg"] as! String, time: 5.0, verticalAlignment: .bottom)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func isAllFieldVerified() -> Bool {
        
        /*["First name",
         "Last name",
         "Email",
         "Phone number",
         "Password",
         "Confirm password"]
         */
        var isVerified = false
        if fkUser.firstName.isEmpty {
            
            let indexPath = IndexPath.init(row: 0, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: false)
            let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
            cell.textField.setFKErrorMessage(message: "Please enter first name")
        } else if fkUser.lastName.isEmpty {
            let indexPath = IndexPath.init(row: 1, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: false)
            let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
            cell.textField.setFKErrorMessage(message: "Please enter last name")
            
        } else if (!fkUser.email.isEmpty && !fkUser.email.isEmail) {
            let indexPath = IndexPath.init(row: 2, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: false)
            let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
            cell.textField.setFKErrorMessage(message: "Please enter valid email")
            
        } else if fkUser.mobileNo.isEmpty {
            let indexPath = IndexPath.init(row: 3, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: false)
            let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
            cell.textField.setFKErrorMessage(message: "Please enter phone number")
            
        } else if (!fkUser.mobileNo.trimWhiteSpace.isMobileNumber) {
            let indexPath = IndexPath.init(row: 3, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: false)
            let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
            cell.textField.setFKErrorMessage(message: "Please enter valid phone number")
            
        } else if fkUser.password.isEmpty {
            let indexPath = IndexPath.init(row: 4, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: false)
            let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
            cell.textField.setFKErrorMessage(message: "Please enter password")
            
        } else if fkUser.password.trimWhiteSpace.length < 6 {
            let indexPath = IndexPath.init(row: 4, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: false)
            let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
            cell.textField.setFKErrorMessage(message: "Please should be at least 6 characters")
            
        } else if fkUser.password.trimWhiteSpace != fkUser.confrmPassword.trimWhiteSpace {
            MessageView.showMessage(message: "Password fields mismatched", time: 5, verticalAlignment: .top)
            
        } else {
            isVerified = true
        }
        return isVerified
    }
}
