//
//  FKWalletAddAmountCell.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 1/4/18.
//  Copyright © 2018 FansKick Dev. All rights reserved.
//

import UIKit

class FKWalletAddAmountCell: UITableViewCell {

    @IBOutlet weak var checkBoxImgVw: UIImageView!
    @IBOutlet weak var imgView: UIImageView?
    @IBOutlet weak var nameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
