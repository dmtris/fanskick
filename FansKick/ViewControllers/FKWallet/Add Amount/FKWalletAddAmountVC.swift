//
//  FKWalletAddAmountVC.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 1/4/18.
//  Copyright © 2018 FansKick Dev. All rights reserved.
//

import UIKit

class FKWalletAddAmountVC: UIViewController,UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var currencyBtn: UIButton!
    @IBOutlet weak var dateBtn: UIButton!

    @IBOutlet weak var addAmountTableView: UITableView!
    var dataSourceArray = [String]()
    var selectedIndexPathArray = [IndexPath]()
    
    private var selectedDate:Date?
    @IBOutlet weak var proceedBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSourceArray.append("SenzPay Manual CDM")
//        dataSourceArray.append("JomPAY")
//        dataSourceArray.append("Credit Card")
//        dataSourceArray.append("Online Banking")
        initialise()
    }
    
    private func initialise() {
        
        self.navigationItem.title = "ADD MONEY"
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        amountTextField.setLeftPaddingPoints(10.0)
       // self.addAmountTableView.tableFooterView = UIView.init()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        amountTextField.attributedPlaceholder = NSAttributedString(string: "Amount",
                                                                   attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
    }
    // use to navigate back screen
    @objc func backBtnAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
  @IBAction func dateBtnAction() {
    
    self.view.endEditing(true)
    RPicker.selectDate(title: " Select deposit date", hideCancel: false, datePickerMode: UIDatePickerMode.dateAndTime, selectedDate: Date(), minDate: nil, maxDate: Date()) { (date) in
        self.selectedDate = date
        let depositDate = self.selectedDate?.dateString("yyyy-MM-dd hh:mm:ss")
        self.dateBtn.setTitle( "  \(depositDate ?? "")", for: .normal)
        }
    }
    
    // use to navigate back screen
    @IBAction func proceedAction(_ sender: Any) {
        
        self.view.endEditing(true)
        if allFieldsVerified() {
            if defaults.object(forKey: SENZPAY_USER_ID) != nil{
                apiCallToRequestCredit()
            } else {
                let fkWalletLogin = self.storyboard?.instantiateViewController(withIdentifier: "FKWalletLoginVC") as! FKWalletLoginVC
                let navController = UINavigationController(rootViewController: fkWalletLogin)
                self.present(navController, animated: true, completion: nil)
                fkWalletLogin.completionBlock { (success, response) in
                    
                    if success! {

                    self.apiCallToRequestCredit()
                    }
                }
            }
        } else {
            MessageView.showMessage(message: "Please enter valid amount.", time: 3.0, verticalAlignment: .bottom)
        }
    }
    
    func allFieldsVerified() -> Bool {
        
        guard let text = amountTextField.text?.trimWhiteSpace, !text.isEmpty,text.isNumeric else {
            MessageView.showMessage(message: "Please enter amount.", time: 4.0, verticalAlignment: .bottom)
            return false
        }
        
        if Float(text)! > 0 {
            if selectedDate == nil{
                MessageView.showMessage(message: "Please select deposit date.", time: 4.0, verticalAlignment: .bottom)
                return false
            }
            if selectedIndexPathArray.count == 0 {
                MessageView.showMessage(message: "Please select payment method.", time: 4.0, verticalAlignment: .bottom)
                return false
            }
           
            return true
        } else {
            MessageView.showMessage(message: "Please enter valid amount.", time: 4.0, verticalAlignment: .bottom)
            return false
        }
    }
    
    func apiCallToRequestCredit() {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        var params = Dictionary<String,Any>()
        let email =  AESCrypto.aes256DecryptString(defaults.object(forKey: SENZPAY_EMAIL_DEFAULT) as! String, withKey: crypto256Key)
        let password =  AESCrypto.aes256DecryptString(defaults.object(forKey: SENZPAY_PASSWORD_DEFAULT) as! String, withKey: crypto256Key)
        let userID =  AESCrypto.aes256DecryptString(defaults.object(forKey: SENZPAY_USER_ID) as! String, withKey: crypto256Key)
        
        params[SENZPAY_EMAIL] = email!
        params[SENZPAY_PASSWORD] = password!
        params[SENZPAY_USER_ID] = userID!
        params["amt"] = amountTextField.text?.trimWhiteSpace
        
        let indexPath = selectedIndexPathArray.first
        params[SENZPAY_TOPUP_METHOD] = dataSourceArray[(indexPath?.row)!]
        params[SENZPAY_TOPUP_METHOD_INFO] = "Wallet recharge"
        params[SENZPAY_SKIP_SMS_ALERT] = "1"  //0 – To send the SMS. 1 – To Skip.
        params[SENZPAY_TOPUP_DATE] = self.selectedDate?.dateString("yyyy-MM-dd hh:mm:ss")  //Date().dateString("yyyy-MM-dd hh:mm:ss")  //2018-01-15 13:12:23
     
        FKSenzPayHelper.senzpayRequest(params: params, method: .post, apiName: "user_wallet_topup.php") { (result, error, responseCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let responseData = result as! Dictionary<String,AnyObject>
            
            let status        = responseData.validatedValue(SENZPAY_STATUS, expected: 0 as NSNumber) as! Bool

            if !status {
                // false case error messsage
                MessageView.showMessage(message: responseData["msg"] as! String, time: 5.0, verticalAlignment: .bottom)
            } else {
                // handle true response
                MessageView.showMessage(message: responseData["msg"] as! String, time: 5.0, verticalAlignment: .bottom)
                
                defaults.set(responseData.validatedValue(SENZPAY_DEPOSIT_ID, expected: "" as AnyObject) as! String, forKey: SENZPAY_DEPOSIT_ID)
                
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK:- Textfield delegate method
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        
        if !string.isEmpty && newString.length > 8 {
            return false
        }
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getPamentMethod() -> String {
        var method = ""
        let indexPath = selectedIndexPathArray.first
        method =   dataSourceArray[(indexPath?.row)!]
        return method
    }
    
    @IBAction func currencyBtn(_ sender: UIButton) {
        
        return
//        RPicker.sharedInstance.selectOption(title: "Currency", dataArray: ["MYR","USD","INR","SGD","IDR"], selectedIndex: 0) { (selectedText, index) in
//            if !selectedText.isEmpty{
//                sender.setTitle("  "+selectedText, for: .normal)
//            }
//        }
    }
    
    //MARK: UITableView Overrides
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourceArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FKWalletAddAmountCell", for: indexPath) as! FKWalletAddAmountCell
        cell.imgView?.image = #imageLiteral(resourceName: "topup")
        cell.nameLbl.text = dataSourceArray[indexPath.row]
        
        if selectedIndexPathArray.contains(indexPath) {
            cell.checkBoxImgVw.image = #imageLiteral(resourceName: "checkbox.png")
        } else {
            cell.checkBoxImgVw.image = #imageLiteral(resourceName: "uncheck.png")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if selectedIndexPathArray.contains(indexPath){
            selectedIndexPathArray.removeAll()
        }else{
            selectedIndexPathArray.append(indexPath)
        }
        
        tableView.reloadData()
        self.view.endEditing(true)
    }
}
