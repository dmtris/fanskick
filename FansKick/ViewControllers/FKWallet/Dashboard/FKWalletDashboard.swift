//
//  FKWalletDashboard.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 1/4/18.
//  Copyright © 2018 FansKick Dev. All rights reserved.
//

import UIKit

class FKWalletDashboard: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var dashboardTableView: UITableView!
    @IBOutlet weak var creditBalanceLbl: UILabel!
    var dataSourceArray = [Dictionary<String, AnyObject>]()
    var timer:Timer?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let creditCard = ["image":#imageLiteral(resourceName: "cdm"),
                          "name":"Credit Top Up"] as [String : Any]
        
        let reports = ["image":#imageLiteral(resourceName: "report"),
                       "name":"Transaction Reports"] as [String : Any]
        
        dataSourceArray.append(creditCard as [String : AnyObject])
        dataSourceArray.append(reports as [String : AnyObject])
        
        // Do any additional setup after loading the view.
        
        
        let logOutBtn = UIButton.init(type: UIButtonType.custom)
        logOutBtn.titleLabel?.font = UIFont.init(name: fanskickFontIcons, size: 22)
        logOutBtn.setTitle("Q", for: .normal)
        logOutBtn.frame = CGRect.init(x: 0, y: 0, width: 24, height: 24)
        logOutBtn.addTarget(self, action: #selector(logOutActionAction), for: UIControlEvents.touchUpInside)
        
        let logOutBar = UIBarButtonItem.init(customView: logOutBtn)
        self.navigationItem.rightBarButtonItem = logOutBar
        
        initialise()
        self.perform(#selector(checkCreditBalance), with: nil, afterDelay: 0.2)
        self.checkToupBalance()
        
    }
    
    private func initialise() {
        
        self.navigationItem.title = "FKWALLET"
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
    }
    
    @objc func logOutActionAction(){
        
        AlertController.alert(title: "Confirmation!", message: "Are you sure, you want to logout from FKWallet account?", buttons: ["No","Yes"], tapBlock: { (action , index) in
            if index == 1{
                defaults.removeObject(forKey: SENZPAY_USER_ID)
                self.timer?.invalidate()
                self.navigationController?.popViewController(animated:true)
            }
        })
    }
    
    
    func checkToupBalance() {
        
        if defaults.object(forKey: SENZPAY_USER_ID) == nil {
            return
        }
        
        timer = Timer.scheduledTimer(withTimeInterval: 300.0, repeats: true) { (timer) in
           
            if defaults.object(forKey: SENZPAY_DEPOSIT_ID) != nil{
                var params = Dictionary<String,Any>()
                let email =  AESCrypto.aes256DecryptString(defaults.object(forKey: SENZPAY_EMAIL_DEFAULT) as! String, withKey: crypto256Key)
                let password =  AESCrypto.aes256DecryptString(defaults.object(forKey: SENZPAY_PASSWORD_DEFAULT) as! String, withKey: crypto256Key)
                
                params[SENZPAY_EMAIL] = email!
                params[SENZPAY_PASSWORD] = password!
                params[SENZPAY_DEPOSIT_ID] = defaults.object(forKey: SENZPAY_DEPOSIT_ID) as! String
                
                FKSenzPayHelper.senzpayRequest(params: params, method: .post, apiName: "user_wallet_topup_check.php") { (result, error, responseCode) in
                    
                    let responseData = result as! Dictionary<String,AnyObject>
                    let status      = responseData.validatedValue(SENZPAY_STATUS, expected:"false" as AnyObject ) as! String
                    if status == "false" {
                        // false case error messsage
                        defaults.removeObject(forKey: SENZPAY_DEPOSIT_ID)
                        timer.invalidate()
                    } else {
                        // handle true response
                        self.creditBalanceLbl.text = "RM \(String(describing: responseData[SENZPAY_BALANCE]!))"
                    }
                }
            }
        }
    }
    
    @objc func checkCreditBalance() {
        
        if defaults.object(forKey: SENZPAY_USER_ID) != nil{
            apiCallToGetCredit()
        }else{
            let fkWalletLogin = self.storyboard?.instantiateViewController(withIdentifier: "FKWalletLoginVC") as! FKWalletLoginVC
            let navController = UINavigationController(rootViewController: fkWalletLogin)
            self.present(navController, animated: true, completion: nil)
            
            fkWalletLogin.completionBlock { (success, response) in
                if success! {
                    self.creditBalanceLbl.text = "RM \(String(describing: response[SENZPAY_BALANCE]!))"
                } else {
                    self.backBtnAction()
                }
            }
        }
    }
    
    func apiCallToGetCredit() {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        var params = Dictionary<String,Any>()
        
        let email =  AESCrypto.aes256DecryptString(defaults.object(forKey: SENZPAY_EMAIL_DEFAULT) as! String, withKey: crypto256Key)
        let password =  AESCrypto.aes256DecryptString(defaults.object(forKey: SENZPAY_PASSWORD_DEFAULT) as! String, withKey: crypto256Key)
        
        params[SENZPAY_EMAIL] = email!
        params[SENZPAY_PASSWORD] = password!
        
        FKSenzPayHelper.senzpayRequest(params: params, method: .get, apiName: "user_login.php") { (result, error, responseCode) in
            print(result ?? "")
            MBProgressHUD.hide(for: self.view, animated: true)
            print(responseCode)
            
            let responseData = result as! Dictionary<String,AnyObject>
            let status      = responseData.validatedValue(SENZPAY_STATUS, expected:"" as AnyObject ) as! String
            if status == "false" {
                // false case error messsage
                MessageView.showMessage(message: responseData["msg"] as! String, time: 5.0, verticalAlignment: .bottom)
            } else {
                
                let userID = responseData.validatedValue(SENZPAY_USER_ID, expected:"" as AnyObject ) as! String
                
                defaults.set(AESCrypto.aes256EncryptString(userID, withKey: crypto256Key), forKey: SENZPAY_USER_ID)
                // handle true response
                self.creditBalanceLbl.text = "RM \(String(describing: responseData[SENZPAY_BALANCE]!))"
            }
        }
    }
    
    
    // use to navigate back screen
    @objc func backBtnAction() {
        self.timer?.invalidate()
        self.navigationController?.popViewController(animated: true)
    }
    
    func navigateToTransaction() {
        
        let transactonVC = self.storyboard?.instantiateViewController(withIdentifier: "FKTransactionVC") as! FKTransactionVC
        self.navigationController?.pushViewController(transactonVC, animated: true)
    }
    
    func navigateToAddCredit() {
        
        let transactonVC = self.storyboard?.instantiateViewController(withIdentifier: "FKWalletAddAmountVC") as! FKWalletAddAmountVC
        self.navigationController?.pushViewController(transactonVC, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: UITableView Overrides
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourceArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FKWalletDashboardTblCell", for: indexPath) as! FKWalletDashboardTblCell
        cell.imgView.image = (dataSourceArray[indexPath.row]["image"] as! UIImage)
        cell.textLbl.text = (dataSourceArray[indexPath.row]["name"] as! String)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 0 {
            navigateToAddCredit()
        } else {
            navigateToTransaction()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
    }
}
