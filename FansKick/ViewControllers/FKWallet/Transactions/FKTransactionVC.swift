//
//  FKTransactionVC.swift
//  FansKick
//
//  Created by Sunil Verma on 04/01/2018.
//  Copyright © 2018 FansKick Dev. All rights reserved.
//

import UIKit

class FKTransactionVC: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tblView:UITableView?
    private var dataSourceArray = [FKTransationInfo]()
    var fromDate = Date()
    var toDate  = Date()
    @IBOutlet weak var fromDateBtn: UIButton!
    @IBOutlet weak var toDateBtn: UIButton!
    
    @IBOutlet weak var noTransactionLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView?.rowHeight = UITableViewAutomaticDimension
        tblView?.estimatedRowHeight = 90
        tblView?.tableFooterView = UIView.init()
        self.navigationItem.title = "TRANSACTIONS"
        
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        
        //        let minDate = Date().dateBySubtractingYears(47)
        
        fromDate = Date().dateBySubtractingMonths(1)
        fromDateBtn.setTitle("  \(Date().dateBySubtractingMonths(1).dateString(DATE_FORMAT))", for: .normal)
        toDateBtn.setTitle("  \(Date().dateString(DATE_FORMAT))", for: .normal)
        
        self.perform(#selector(apiCallForTransaction), with: nil, afterDelay: 0.1)

    }
    
    @IBAction func applyBtn(_ sender: Any) {
        apiCallForTransaction()
    }
    // use to navigate back screen
    @objc func backBtnAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func fromBtn(_ sender: UIButton) {
        
        RPicker.selectDate(selectedDate: Date(), maxDate: Date()) { (date: Date) in
            self.fromDate = date
            sender.setTitle("  \(date.dateString(DATE_FORMAT))", for: .normal)
        }
    }
    
    @IBAction func toBtn(_ sender: UIButton) {

        RPicker.selectDate(selectedDate: Date(), minDate:self.fromDate, maxDate: Date()) { (date: Date) in
            self.toDate = date
            sender.setTitle("  \(date.dateString(DATE_FORMAT))", for: .normal)
        }
    }
    
    @objc func apiCallForTransaction() {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        var params = Dictionary<String,Any>()
        let email =  AESCrypto.aes256DecryptString(defaults.object(forKey: SENZPAY_EMAIL_DEFAULT) as! String, withKey: crypto256Key)
        let password =  AESCrypto.aes256DecryptString(defaults.object(forKey: SENZPAY_PASSWORD_DEFAULT) as! String, withKey: crypto256Key)
        params[SENZPAY_EMAIL] = email!
        params[SENZPAY_PASSWORD] = password!
        params["frm_date"] = fromDate.dateString("yyyy-MM-dd")
        params["to_date"] = toDate.dateString("yyyy-MM-dd")
        
        FKSenzPayHelper.senzpayRequest(params: params, method: .get, apiName: "user_report.php") { (result, error, responseCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            self.dataSourceArray.removeAll()
            let responseData = result as! Dictionary<String,AnyObject>
            let status      = responseData.validatedValue(SENZPAY_STATUS, expected:"" as AnyObject ) as! String
            if status == "false" {
                // false case error messsage
                MessageView.showMessage(message: responseData["msg"] as! String, time: 5.0, verticalAlignment: .bottom)
            } else {
                
                let data = responseData["data"]
                
                if data is Array<Dictionary<String,AnyObject>>{
                    self.dataSourceArray.append(contentsOf:(FKTransationInfo.transactionList(data: data as! Array<Dictionary<String,AnyObject>>)))
                }
                if self.dataSourceArray.count == 0 {
                    MessageView.showMessage(message: "You have not made any transaction so far.", time: 5.0, verticalAlignment: .bottom)
                    self.noTransactionLbl.isHidden = false
                } else {
                    self.noTransactionLbl.isHidden = true
                }
                self.dataSourceArray.sort(by: { (transtion1, transtion2) -> Bool in
                    return transtion1.transactionTime > transtion2.transactionTime
                })
                
                self.tblView?.reloadData()
            }
        }
    }
    
    // MARK:- Tableview Datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSourceArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =    tableView.dequeueReusableCell(withIdentifier: "FKTransactionCell") as! FKTransactionCell
        cell.transactionSourceImgView?.image = UIImage.init(named: "placeholder")
        let transaction = self.dataSourceArray[indexPath.row]
        
        cell.transactionIdLbl?.text = "SD_ID: \(transaction.transactionId)"
        cell.transactionDateLbl?.text = transaction.transactionTime
        cell.transactionTitleLbl?.text = transaction.sender
        cell.transactionDescLbl?.text = transaction.userComment
        cell.transactionAmountLbl?.text = "RM \(transaction.amount)"
        cell.statusLbl?.text = transaction.statusText
        if transaction.status == "5" {
            cell.statusLbl?.textColor = UIColor.red
        }else if transaction.status == "1"  {
            cell.statusLbl?.textColor = UIColor.yellow
        }else if transaction.status == "2"  {
            cell.statusLbl?.textColor = GREEN_COLOR
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableViewAutomaticDimension
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
