//
//  FKFixtureDetailViewController.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 10/20/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit
import CarbonKit

class FKFixtureDetailViewController: UIViewController,CarbonTabSwipeNavigationDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var stadiumNameLbl: UILabel!
    @IBOutlet weak var teamName1: UILabel!
    @IBOutlet weak var teamName2: UILabel!
    @IBOutlet weak var hrsLbl: UILabel!
    @IBOutlet weak var daysLbl: UILabel!
    @IBOutlet weak var minsLbl: UILabel!
    @IBOutlet weak var teamImg1: UIImageView!
    @IBOutlet weak var teamImg2: UIImageView!
    @IBOutlet weak var kickOfftimeLbl: UILabel!
    
    var fixtureItem:FKFixtureListModel?
    var currentTab = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "FIXTURE DETAIL"
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        setMatchDetails()
        setupSegment()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
    }
    
    // use to navigate back screen
    @objc func backBtnAction(){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupSegment() {
        
        let items = ["Related", "Stats", "Line-up"]
        let carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: mainView)
        let widthOfTabIcons = self.view.frame.size.width/3
        carbonTabSwipeNavigation.toolbar.barTintColor = .clear
        carbonTabSwipeNavigation.setNormalColor(.white, font: .systemFont(ofSize: 14.0))
        carbonTabSwipeNavigation.carbonSegmentedControl!.indicator.backgroundColor = .white
        carbonTabSwipeNavigation.setSelectedColor(.white, font: .systemFont(ofSize: 14.0))
        carbonTabSwipeNavigation.carbonSegmentedControl!.backgroundColor = .clear
        carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(widthOfTabIcons, forSegmentAt: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(widthOfTabIcons, forSegmentAt: 1)
        carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(widthOfTabIcons, forSegmentAt: 2)
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        guard let storyboard = storyboard else { return UIViewController() }
        if index == 0 {
            let fixtureRelatedVc = storyboard.instantiateViewController(withIdentifier: "FKFixtureRelatedVC") as! FKFixtureRelatedVC
            fixtureRelatedVc.fixtureItem = fixtureItem
            return fixtureRelatedVc
        } else if index == 1 {
            
            let fixtureState = storyboard.instantiateViewController(withIdentifier: "FKStatsViewController") as! FKStatsViewController
            fixtureState.fixture = fixtureItem
            return fixtureState
        } else {
            
            let lineUp = storyboard.instantiateViewController(withIdentifier: "FKLineUpViewController") as! FKLineUpViewController
            lineUp.fixture = fixtureItem
            return lineUp
        }
    }
    
    func setMatchDetails() {
        
        teamName1.text          = fixtureItem?.club //"JDT"//
        teamName2.text          = fixtureItem?.apponent//"KL"//
        dateLbl.text            = fixtureItem?.start_time.dateFromUTC()?.dateString(DATE_FORMAT)//"01 Nov 2017"//
        stadiumNameLbl.text     = fixtureItem?.stadium//"Johor National Stadium"//
        teamImg1.normalLoad((fixtureItem?.clubImage)!)//UIImage(named:"lg1")//
        teamImg2.normalLoad((fixtureItem?.apponentImage)!)//UIImage(named:"lg2")//
        kickOfftimeLbl.text     = "Kick Off: "+(fixtureItem?.start_time.dateFromUTC()?.dateString("hh:mm a"))!//"Kick Off: 04:00 PM "//
        
        if (fixtureItem?.start_time.dateFromUTC()?.daysFrom(Date()))! < 0 {
            daysLbl.text = "0"
        } else {
            daysLbl.text = "\(String(describing: (fixtureItem?.start_time.dateFromUTC()?.daysFrom(Date()))!))"//"2"//
        }
        
        if (fixtureItem?.start_time.dateFromUTC()?.hoursFrom(Date()))! < 0 {
            hrsLbl.text = "0"
        } else {
            hrsLbl.text = "\(String(describing: (fixtureItem?.start_time.dateFromUTC()?.hoursFrom(Date()))! % 24 ))"//"19"//
        }
        
        if (fixtureItem?.start_time.dateFromUTC()?.minutesFrom(Date()))! < 0 {
            minsLbl.text = "0"
        } else {
            minsLbl.text = "\(String(describing: (fixtureItem?.start_time.dateFromUTC()?.minutesFrom(Date()))! % 60))"//"45"//
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension UIProgressView {
    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        let newSize = CGSize(width: frame.size.width, height: 5)
        return newSize
    }
}
