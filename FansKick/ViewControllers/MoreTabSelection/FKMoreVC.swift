//
//  FKMoreVC.swift
//  FansKick
//
//  Created by FansKick-Sunil on 17/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKMoreVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var dataArr = [String]()
    var iconArray = [String]()
    @IBOutlet weak var tblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        tblView.tableFooterView = UIView.init()
        self.navigationItem.title = "MORE"
        
        //dataArr = ["Merchandise", "Notifications", ,"Logout"]
         dataArr = ["MERCHANDISE", "NOTIFICATIONS","LOGOUT"]
        iconArray = ["h","&", "Q"] 
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        
        appDel.setupNavigationBar(navigationBar: self.navigationController!)
        self.navigationItem.leftBarButtonItem = AppUtility.fanskickLogo()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
    }
    
    // MARK:- tableview datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let moreCell:FKMoreCell = tableView.dequeueReusableCell(withIdentifier: "FKMoreCell", for: indexPath) as! FKMoreCell
        moreCell.titleLbl.text = dataArr[indexPath.row]
        moreCell.omgIcon.setTitle(iconArray[indexPath.row], for: .normal)
        return moreCell
    }
    
    // MARK:- tableview delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        tableView.deselectRow(at: indexPath, animated: true)

        if indexPath.row == 0 {
            let storyboard = UIStoryboard.init(name: "Merchandise", bundle: nil)
            self.navigationController?.pushViewController(storyboard.instantiateViewController(withIdentifier: "FKMerchandiseHomeVC"), animated: true)
            
        } else if (indexPath.row == 1){
            
            let notificationVc = self.storyboard?.instantiateViewController(withIdentifier: "FKNotificationVC") as! FKNotificationVC
            self.navigationController?.pushViewController(notificationVc, animated: true)
        
        }  else if indexPath.row == 2{
				
            AlertController.alert(title: "Confirmation!", message: "Are you sure, you want to logout?", buttons: ["No","Yes"], tapBlock: { (action , index) in
                
                if index == 1{
                    self.logoutApiCall()
                }
            })
        }
        
        /*
         else if indexPath.row == 1 {
         let storyboard = UIStoryboard.init(name: "StatsSection", bundle: nil)
         self.navigationController?.pushViewController(storyboard.instantiateViewController(withIdentifier: "statsVC"), animated: true)
         } else if indexPath.row == 2 {
         let storyboard = UIStoryboard.init(name: "IndoorMapSection", bundle: nil)
         self.navigationController?.pushViewController(storyboard.instantiateViewController(withIdentifier: "FKIndoorListVC"), animated: true)
         } else if indexPath.row == 3 {
         self.navigationController?.pushViewController((self.storyboard?.instantiateViewController(withIdentifier: "FKFixturesViewController"))!, animated: true)
         }
 */
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func logoutApiCall() {
        
        FKGoogleSignIn.sharedInstance.signOut()
        FacebookManager.facebookManager.logoutFromFacebook()
        defaults.removeObject(forKey: SENZPAY_USER_ID)
        defaults.removeObject(forKey:kUserId)
        defaults.removeObject(forKey: kOAuthToken)
        
        let params = Dictionary<String, Any>()
        MBProgressHUD.showAdded(to: self.view, animated: true)

        ServiceHelper.request(params: params, method: MethodType.delete, apiName: "users/logout", hudType: loadingIndicatorType.withoutLoader) { (result, error, responseCode) in
            MBProgressHUD.hide(for: self.view, animated: true)

            if (error == nil) {
                let appDel = UIApplication.shared.delegate as! AppDelegate
                appDel.navigateToLogin()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

