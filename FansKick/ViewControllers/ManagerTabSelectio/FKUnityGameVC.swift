//
//  FKUnityGameVC.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 11/4/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKUnityGameVC: UIViewController {
    
    var isFirstTime = true
    //var unityView = UnityGetGLView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("UnityReady"), object: nil)
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("FANSKICK_UNITY_NOTIFICATION"), object: nil)
//
//        NotificationCenter.default.addObserver (
//            self,
//            selector: #selector(handleUnityReady),
//            name: NSNotification.Name("UnityReady"),
//            object: nil)
//
//        NotificationCenter.default.addObserver(
//            self,
//            selector: #selector(backToNative),
//            name: NSNotification.Name("FANSKICK_UNITY_NOTIFICATION"),
//            object: nil)
//

       // let appDelegate = UIApplication.shared.delegate as? AppDelegate
//        appDelegate?.allocateAndInitializeUnityAppController()
        
//        navigateToUnityGame()
//        if (appDelegate?.isUnityInitialized)! {
//            handleUnityReady()
//        }
//
//        if !(appDelegate?.isUnityFirstTimeLaunch)! {
//            UnitySendMessage("FKManager", "MessageFromNative", "Refresh")
//        } else {
//            appDelegate?.isUnityFirstTimeLaunch = false
//        }
//        self.perform(#selector(openUnity), with: nil, afterDelay: 5.0)
    }
    
//    @objc func openUnity() {
//        navigateToUnityGame()
//        //        if (appDelegate?.isUnityInitialized)! {
//        handleUnityReady()
//        //        }
//    }
//
//    @objc func backToNative(notification: NSNotification) {
//        print(notification.object ?? "no value")
//        let serviceName = "\(notification.object ?? "")"
//
//        let appDelegate = UIApplication.shared.delegate as? AppDelegate
//        let data = serviceName.components(separatedBy: CharacterSet.init(charactersIn: ";"))
//        var methodName = ""
//        methodName = data.first!
//
//        if methodName == "BackTONAtiveSignout" {
//            // needed to log out the
//            appDelegate?.stopUnity()
//            appDelegate?.navigateToLogin()
//            self.dismiss(animated: true, completion: nil)
//
//        } else if methodName == "SHARETEXT" {
//            // Needed to share only text
//            if data.count > 1 {
//                let message = data [1]
//                shareViaUnity(message: message+"\nFanskick is an awesome application, Please try below link to install the application on your phone \nClick here: https://appurl.io/jcef8dvj", image: nil)
//            }
//        } else if methodName == "SHARETEXTWITHIMAGE" {
//            // Needed to share text and image both
//
//            let img = self.captureScreenshot() //UIImage.init(named: "qr")
//            if data.count > 1{
//                let message = data [1]
//                shareViaUnity(message: message.trimWhiteSpace, image: img)
//            }
//        } else if methodName == "HideStatusBar" {
//            UIApplication.shared.isStatusBarHidden = true
//        } else {
//            // this will exit from unity
//            appDelegate?.stopUnity()
//            self.dismiss(animated: true, completion: nil)
//        }
//    }
	
    func captureScreenshot() -> UIImage? {
        
        UIGraphicsBeginImageContext(self.view.bounds.size)  //Change this line only
        self.view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
    
    func shareViaUnity(message: String, image:UIImage?){
        
        var messageToShare = [Any]()
        if image != nil{
            messageToShare.append(image as Any)
        }
        if !message.isEmpty{
            messageToShare.append(message as Any)
        }
        
        FKShareHelper.share.openActivity(options: messageToShare , controller: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if isFirstTime {
            let appDell = UIApplication.shared.delegate as! AppDelegate
            appDell.changeOrientation(toPortrait: false)
            isFirstTime = false
        }
    }
    
    //    override func viewDidAppear(_ animated: Bool) {
    //        navigateToUnityGame()
    //        let appDelegate = UIApplication.shared.delegate as? AppDelegate
    //        if (appDelegate?.isUnityInitialized)! {
    //            handleUnityReady()
    //        }
    //    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func navigateToUnityGame() {
//        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
//            appDelegate.startUnity()
//        }
//    }
    
//    @objc func handleUnityReady() {
//        if let unityView = UnityGetGLView() {
//
//            print(unityView.frame.size.width)
//            print(unityView.frame.size.height)
//            unityView.frame = self.view.bounds
//            // insert subview at index 0 ensures unity view is behind current UI view
//            self.navigationController?.isNavigationBarHidden = true
//            let appDelegate = UIApplication.shared.delegate as? AppDelegate
//            appDelegate?.isUnityInitialized = true
//            view?.insertSubview(unityView, at: 0)
//            //            view?.addSubview(unityView)
//        }
//    }
}
