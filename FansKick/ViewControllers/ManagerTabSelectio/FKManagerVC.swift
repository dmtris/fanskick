//
//  FKManagerVC.swift
//  FansKick
//
//  Created by FansKick-Sunil on 17/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKManagerVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
                
        let appDel = UIApplication.shared.delegate as! AppDelegate
        
        appDel.setupNavigationBar(navigationBar: self.navigationController!)
        
        self.navigationItem.title = "FK MANAGER"
        self.navigationItem.leftBarButtonItem = AppUtility.fanskickLogo()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func playbtn(_ sender: Any) {
        
        MessageView.showMessage(message: "Something awesome is coming, Stay tuned with us!", time: 10.0, verticalAlignment: .bottom)
//        let value = UIInterfaceOrientation.landscapeLeft.rawValue
//        UIDevice.current.setValue(value, forKey: "orientation")
//        let fkManagerVc = self.storyboard?.instantiateViewController(withIdentifier: "FKUnityGameVC") as! FKUnityGameVC
//        self.present(fkManagerVc, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let appDell = UIApplication.shared.delegate as! AppDelegate
        appDell.changeOrientation(toPortrait: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
