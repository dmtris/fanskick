//
//  FKTopPerformerVC.swift
//  FansKick
//
//  Created by Sunil Verma on 14/11/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKTopPerformerVC: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var segmentControll: UISegmentedControl!
    var page = PAGE()

    @IBOutlet weak var seasonBtn: UIButton!
    

    
    var seasonArray = [FKListInfo]()
    var dataSourceArray = [FKPlayerInfo]()
    var clubArray = [FKClubInfo]()
    
    var selectedSeson  = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        page.startIndex = 1
        page.pageSize = 20
        tblView.tableFooterView = UIView.init()
        self.navigationItem.title = "TOP PERFORMERS"
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        
        self.clubApiCall()
        self.seasonApiCall()
        self.topPlayerApiCall()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    // use to navigate back screen
    @objc func backBtnAction() {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func sesonBtnAction(_ sender: Any) {
       
        let dataArray = getSeasonList()
        RPicker.sharedInstance.selectOption(title: "Select season", dataArray: dataArray, selectedIndex: 0) { (selectedText, index) in
            if !selectedText.isEmpty{
                self.seasonBtn.setTitle(selectedText, for: .normal)
                self.selectedSeson = selectedText
                self.page.startIndex = 1
                
                if self.segmentControll.selectedSegmentIndex == 0{
                self.clubApiCall()
                } else {
                    self.topPlayerApiCall()
                }
            }
        }
    }
   
    
    @IBAction func  segmentAction(){
    
        tblView.reloadData()
    }
   
    func seasonApiCall(){
        
        var params          = Dictionary<String,Any>()
        params["page"]      = "1"
        params["per_page"]  = "500"
        
        ServiceHelper.request(params: params, method: .get, apiName: "stats/season_list", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if let data = results["seasons"] {
                    self.seasonArray.append(contentsOf: FKListInfo.list(data: data as! Array<Dictionary<String, AnyObject>>))
                    
                    if self.seasonArray.count > 0{
                        let season = self.seasonArray.first
                        self.selectedSeson = (season?.name)!
                        self.seasonBtn.setTitle(self.selectedSeson, for: .normal)
                    }
                    self.tblView.reloadData()
                } else {
                    
                }
            }
        }
    }
    
    
    //MARK:- Club List APi
    func clubApiCall() {
        
        var params          = Dictionary<String,Any>()
        params["page"]      = "1"
        params["per_page"]  = "500"

        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: .get, apiName: "stats/top_clubs", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if let data = results["clubs"] {
                    if self.page.startIndex == 1{
                        self.clubArray.removeAll()
                    }
                    self.clubArray.append(contentsOf: FKClubInfo.clubList(data: data as! Array<Dictionary<String, AnyObject>>))
                    self.tblView.reloadData()
                } else {
                    
                }
            }
        }
    }
    
    func topPlayerApiCall() {
        
        
        var params          = Dictionary<String,Any>()
        
        var season =  Dictionary<String,Any>()
        
        if (!selectedSeson.isEmpty){
            let sean = SeasonByName(name: selectedSeson)
            season["id"] = sean.id
        } else {
            season["id"] = ""
            
        }
        params["page"]      = "\(page.startIndex)"
        params["per_page"]  = "\(page.pageSize)"
        
        params["season"] = season
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: .post, apiName: "stats/top_players", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if let data = results["players"] {
                    
                    if self.page.startIndex == 1{
                        self.dataSourceArray.removeAll()
                    }
                    self.dataSourceArray.append(contentsOf: FKPlayerInfo.playerList(data: data as! Array<Dictionary<String, AnyObject>>))
                    self.tblView.reloadData()
                } else {
                    
                }
            }
        }
    }
    
    
    func getSeasonList() -> [String]{
        var array = [String]()
        for  item in self.seasonArray{
            array.append(item.name)
        }
        return array
    }
    
    func SeasonByName(name:String) -> FKListInfo {
        var club:FKListInfo?
        for item in self.seasonArray{
            if item.name == name{
                club = item
                break
            }
        }
        return club!
    }
    
    //MARK: UItableView Overrides
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  (segmentControll.selectedSegmentIndex == 0) ? clubArray.count : self.dataSourceArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if segmentControll.selectedSegmentIndex == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FKClubCell") as! FKClubCell
            
            let club = self.clubArray[indexPath.row]
            cell.nameLbl?.text = club.name
            cell.imgView?.normalLoad(club.clubImg)
            cell.goalLbl?.text = club.goal
            cell.accessoryType = .none
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FKBuddyCell") as! FKBuddyCell
            
            cell.accessoryType = .disclosureIndicator

            let player = self.dataSourceArray[indexPath.row]
            
            cell.nameLbl?.text = player.name
            cell.clubNameLbl?.text = player.clubName
            cell.clubImgView?.normalLoad(player.clubImage)
            cell.imgView?.normalLoad(player.profileImage)
            cell.noOfGoalLbl?.text = player.goal
            return cell
        }        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (segmentControll.selectedSegmentIndex == 0) ? 60 : 80
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if segmentControll.selectedSegmentIndex == 0{
//            let playerVC = self.storyboard?.instantiateViewController(withIdentifier: "statePlayerVC") as! FKStatePlayerVC
//            playerVC.club = self.clubArray[indexPath.row]
//
//            self.navigationController?.pushViewController(playerVC, animated: true)
        }else{
            let playerVC = self.storyboard?.instantiateViewController(withIdentifier: "statisticVC") as! FKStatisticVC
            playerVC.player =  self.dataSourceArray[indexPath.row]
            self.navigationController?.pushViewController(playerVC, animated: true)
        }
      
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if segmentControll.selectedSegmentIndex == 0 {
            return
        }
        // UITableView only moves in one direction, y axis
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        // Change 10.0 to adjust the distance from bottom
        if maximumOffset - currentOffset <= 10.0 {
            if page.totalPage > page.startIndex{
                page.startIndex = page.startIndex+1;
                self.topPlayerApiCall()
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
