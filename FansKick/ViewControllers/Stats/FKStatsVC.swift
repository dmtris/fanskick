//
//  FKStatsVC.swift
//  FansKick
//
//  Created by FansKick-Sunil on 24/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKStatsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tblView: UITableView!
    
    var dataSourceArray = [FKListInfo]()
    override func viewDidLoad() {
        super.viewDidLoad()

        tblView.tableFooterView = UIView.init()
        self.navigationItem.title = "STATS"
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        
        self.clubApiCall()
    }
    override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
    }
    
    // use to navigate back screen
    @objc func backBtnAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- Club List APi
    func clubApiCall() {
        
        var params          = Dictionary<String,Any>()
        params["page"]      = "1"
        params["per_page"]  = "500"
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: .get, apiName: "fixtures/club_list", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if let data = results["league_list"] {
                    self.dataSourceArray.append(contentsOf: FKListInfo.list(data: data as! Array<Dictionary<String, AnyObject>>))
                    self.tblView.reloadData()
                } else {
                    
                }
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.dataSourceArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FKClubCell") as! FKClubCell
        let club = self.dataSourceArray[indexPath.row]
        cell.nameLbl?.text = club.name
        cell.imgView?.normalLoad(club.clubImg)
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let playerVC = self.storyboard?.instantiateViewController(withIdentifier: "statePlayerVC") as! FKStatePlayerVC
        playerVC.club = self.dataSourceArray[indexPath.row]
       self.navigationController?.pushViewController(playerVC, animated: true)
    }

    @IBAction func topPerformerAction() {
        let playerVC = self.storyboard?.instantiateViewController(withIdentifier: "topPerformerVC") as! FKTopPerformerVC
        self.navigationController?.pushViewController(playerVC, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
