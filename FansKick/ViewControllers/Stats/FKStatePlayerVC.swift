//
//  FKStatePlayerVC.swift
//  FansKick
//
//  Created by Sunil Verma on 14/11/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKStatePlayerVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblView: UITableView!
    var dataSourceArray = [FKPlayerInfo]()
    var club : FKListInfo?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "PLAYERS"
        
        self.tblView.tableFooterView = UIView.init()
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        playerApiCall()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    // use to navigate back screen
    @objc func backBtnAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: Player api call
    
    func playerApiCall(){
        
        
        var params          = Dictionary<String,Any>()
        
        var club =  Dictionary<String,Any>()
        club["id"] = self.club?.id       
        params["page"]      = "1"
        params["per_page"]  = "500"
        params["club"] = club
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: .post, apiName: "stats/stats_details", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if let data = results["players"] {
                    self.dataSourceArray.append(contentsOf: FKPlayerInfo.playerList(data: data as! Array<Dictionary<String, AnyObject>>))
                    self.tblView.reloadData()
                } else {
                    
                }
            }
        }
    }
    
    //MARK: UItableView Overrides
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.dataSourceArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FKBuddyCell") as! FKBuddyCell
        
        let player = self.dataSourceArray[indexPath.row]
        
        cell.nameLbl?.text = player.name
        cell.clubNameLbl?.text = player.clubName
        cell.clubImgView?.normalLoad(player.clubImage)
        cell.imgView?.normalLoad(player.profileImage)
        cell.noOfGoalLbl?.text = player.goal
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let playerVC = self.storyboard?.instantiateViewController(withIdentifier: "statisticVC") as! FKStatisticVC
        playerVC.player =  self.dataSourceArray[indexPath.row]
        self.navigationController?.pushViewController(playerVC, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
