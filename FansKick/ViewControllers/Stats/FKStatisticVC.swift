//
//  FKStatisticVC.swift
//  FansKick
//
//  Created by Sunil Verma on 14/11/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKStatisticVC: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var clubLbl: UILabel!
    @IBOutlet weak var clubImgView: UIImageView!
    @IBOutlet weak var appearanceLbl: UILabel!

    var dataSourceArray = [String]()
    
    @IBOutlet weak var goalLbl: UILabel!
    @IBOutlet weak var jersyImgView: UIImageView!
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var yelloCardBtn: UIButton!
    @IBOutlet weak var redCardBtn: UIButton!
    
    var player: FKPlayerInfo?
    var statsImageHeight: CGFloat = 160.0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "STATS"
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        
    }
    // use to navigate back screen
    @objc func backBtnAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        reloadDataContent()
        initaliseContent()
    }
    
    func initaliseContent() {
        
        self.nameLbl.text = player?.name
        self.clubLbl.text = player?.clubName
        self.clubImgView.normalLoad((player?.clubImage)!)
        self.profileImgView.normalLoad((player?.profileImage)!)
        self.goalLbl.text = "\(player?.goal ?? "") Goal"
        
        if DeviceType.IS_IPHONE_5 {
            self.appearanceLbl.font = UIFont.init(name: "Gilroy-Light", size: 12.0)
        }
        
        self.appearanceLbl.text = "Appearance\n\(self.player?.appearances ?? "") Matches"
        self.redCardBtn.setTitle(self.player?.redCard, for: .normal)
        self.yelloCardBtn.setTitle(self.player?.yellowCared, for: .normal)
    }
    
    func reloadDataContent() {
        
        if (self.player != nil) {
            dataSourceArray = (self.player?.getStatsInfo())!

        }else{
            return
        }
        
    if (self.player?.statsImage)!.length <= 1{
        statsImageHeight = 0.0
    }
    else if ScreenSize.SCREEN_HEIGHT > 568 {
        statsImageHeight = 240
        }

    let count = Int(ceil(Float(dataSourceArray.count)/2))
        
        print(count, "" , dataSourceArray.count)
        for index in 0 ..< count {
            leftLabel(yPosition: index*50, text: dataSourceArray[index*2])
            
            if index < dataSourceArray.count/2{
                rightLabel(yPosition: index*50, text: dataSourceArray[index*2+1])

            }
//            if (dataSourceArray.count % 2 == 0){
//                rightLabel(yPosition: index*50, text: dataSourceArray[index*2+1])
//            }
        }
        scrollView.contentSize = CGSize.init(width: 10, height: (count*50)+Int(statsImageHeight+10.0) )
        centerLabel()
        self.insertStatsImage()
    }
    
    func leftLabel(yPosition:Int, text:String) {
        
        print(text.extractNumber)
        let lbl = UILabel.init(frame: CGRect.init(x: 0, y: yPosition, width: (Int(self.view.frame.width/2 - 80)), height: 30))
        lbl.textColor = UIColor.white
        lbl.attributedText =     attributtedString(string: text)
        lbl.textAlignment = .right
        scrollView.addSubview(lbl)
        
        let lineLbl = UILabel.init(frame: CGRect.init(x: 0, y: yPosition+30, width: (Int(self.view.frame.width/2 - 55)), height: 1))
        lineLbl.backgroundColor = GREEN_COLOR
        scrollView.addSubview(lineLbl)
        
        let circleLbl = UILabel.init(frame: CGRect.init(x: (Int(self.view.frame.width/2 - 65)), y: yPosition+20, width: 20, height: 20))
        circleLbl.backgroundColor = UIColor.white
        circleLbl.layer.cornerRadius = 10
        circleLbl.layer.borderColor = GREEN_COLOR.cgColor
        circleLbl.borderWidth = 2
        circleLbl.layer.masksToBounds = true
        scrollView.addSubview(circleLbl)
    }
    
    func rightLabel(yPosition:Int, text:String) {
        
        let lbl = UILabel.init(frame: CGRect.init(x: (Int(self.view.frame.width/2 + 80)), y: yPosition, width: (Int(self.view.frame.width/2 - 60)), height: 30))
        lbl.textColor = UIColor.white
        lbl.attributedText = attributtedString(string: text)
        lbl.textAlignment = .left
        scrollView.addSubview(lbl)
        
        let lineLbl = UILabel.init(frame: CGRect.init(x: (Int(self.view.frame.width/2 + 65)), y: yPosition+30, width: (Int(self.view.frame.width/2 - 55)), height: 1))
        lineLbl.backgroundColor = GREEN_COLOR
        scrollView.addSubview(lineLbl)
        
        let circleLbl = UILabel.init(frame: CGRect.init(x: (Int(self.view.frame.width/2 + 50)), y: yPosition+20, width: 20, height: 20))
        circleLbl.backgroundColor = UIColor.white
        circleLbl.layer.cornerRadius = 10
        circleLbl.layer.borderColor = GREEN_COLOR.cgColor
        circleLbl.borderWidth = 2
        circleLbl.layer.masksToBounds = true
        scrollView.addSubview(circleLbl)
    }
    
    func centerLabel() {
        
        let lbl = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: 90, height: 100))
        lbl.textAlignment = .center
        lbl.textColor = UIColor.white
        lbl.attributedText = attributtedStringCenter(string:"\(self.player?.appearances ?? "") MATCHES PLAYED")
        lbl.numberOfLines = 0
        lbl.center = CGPoint.init(x: self.view.frame.width/2, y: scrollView.contentSize.height/2 - statsImageHeight/2)
        scrollView.addSubview(lbl)
        
        let lblV1 = UILabel.init(frame: CGRect.init(x: (Int(self.view.frame.width/2)), y: 5, width: 1, height: Int(lbl.frame.origin.y - 5)))
        lblV1.backgroundColor = UIColor.white
        scrollView.addSubview(lblV1)
        let lblV2 = UILabel.init(frame: CGRect.init(x: (Int(self.view.frame.width/2)), y: Int(lbl.frame.origin.y + lbl.frame.height), width: 1, height: Int(lbl.frame.origin.y-10)))
        lblV2.backgroundColor = UIColor.white
        scrollView.addSubview(lblV2)
    }
    
    func attributtedString(string:String) -> NSAttributedString {

        let formatted = NSMutableAttributedString(string: string, attributes: [.font: UIFont.init(name: "Gilroy-Light", size: 14)!])
        if let range = string.range(of: string.extractNumber) {
            let nsRange = NSRange(range, in: string)
            formatted.addAttribute(.font, value: UIFont.init(name: "Gilroy-ExtraBold", size: 16)!, range: nsRange)
            formatted.addAttribute(.foregroundColor, value: GREEN_COLOR, range: nsRange)
        }
        return formatted
    }
    
    func attributtedStringCenter(string:String) -> NSAttributedString {
        
        let formatted = NSMutableAttributedString(string: string, attributes: [.font: UIFont.init(name: "Gilroy-Light", size: 20)!])
        if let range = string.range(of: string.extractNumber) {
            let nsRange = NSRange(range, in: string)
            formatted.addAttribute(.font, value: UIFont.init(name: "Gilroy-ExtraBold", size: 22)!, range: nsRange)
            formatted.addAttribute(.foregroundColor, value: UIColor.white, range: nsRange)
        }
        return formatted
    }
    
    func insertStatsImage() {
        let width: CGFloat = ScreenSize.SCREEN_WIDTH - 20
        let yPos = self.scrollView.contentSize.height - statsImageHeight;
        let xPos: CGFloat = self.view.frame.width - (width/2)
        let imageView = UIImageView.init(frame: CGRect.init(x: xPos, y: yPos, width: width, height: statsImageHeight))
        imageView.center = CGPoint.init(x: self.view.frame.width/2, y: scrollView.contentSize.height - statsImageHeight/2)
        imageView.contentMode = .scaleAspectFit
        imageView.normalLoad((player?.statsImage)!)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        imageView.addGestureRecognizer(tap)
        imageView.isUserInteractionEnabled = true

        scrollView.addSubview(imageView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        FKImagePreview.shared.previewImage(url: (player?.statsImage)!, controller: self)
    }

}
