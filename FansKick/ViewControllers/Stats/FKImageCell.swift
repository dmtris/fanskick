//
//  FKImageCell.swift
//  FansKick
//
//  Created by shivam on 31/01/18.
//  Copyright © 2018 FansKick Dev. All rights reserved.
//

import UIKit

class FKImageCell: UITableViewCell {

    @IBOutlet weak var bannerImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
