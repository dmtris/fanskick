//
//  FKSettingsViewController.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 10/21/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKSettingsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblView:UITableView?
    
    var dataArr = [String]()

    var userSetting = FKNotificationSetting()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "SETTINGS"
        dataArr = ["Match Reminder",
                   "Kick Off",
                   "Goals",
                   "Red Card",
                   "Half Time Score", "Player Online","Match Score"
        ]
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        self.perform(#selector(settingApiCall), with: nil, afterDelay: 0.1)
        
    }
    @objc func backBtnAction(){
        
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- tableview datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let settingCell = tableView.dequeueReusableCell(withIdentifier: "FKSettingsCell", for: indexPath) as! FKSettingsCell
        settingCell.titleLbl.text = dataArr[indexPath.row]
        settingCell.switchBtn.indexPath = indexPath
        settingCell.switchBtn.addTarget(self, action: #selector(settingChange), for: .valueChanged)
        
        if indexPath.row == 0{
            settingCell.switchBtn.setOn((userSetting.matchReminder), animated: true)
        }else if (indexPath.row == 1){
            settingCell.switchBtn.setOn((userSetting.kickOff), animated: true)
        }else if indexPath.row == 2{
            settingCell.switchBtn.setOn((userSetting.goal), animated: true)
        }else if indexPath.row == 3 {
            settingCell.switchBtn.setOn((userSetting.redCard), animated: true)
        }else if indexPath.row == 4 {
            settingCell.switchBtn.setOn((userSetting.halfTimeScore), animated: true)
        }else if indexPath.row == 5 {
            settingCell.switchBtn.setOn((userSetting.playerOnLineReminder), animated: true)
        }else if indexPath.row == 6 {
            settingCell.switchBtn.setOn((userSetting.matchScore), animated: true)
        }
        
        return settingCell
    }
    
    
    @objc func settingApiCall() {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let params = Dictionary<String, Any>()

        ServiceHelper.request(params: params, method: .get, apiName: "notifications/get_status") { (result, error, responseCode) in

            MBProgressHUD.hide(for: self.view, animated: true)

            if (error == nil){
                let results = result as! Dictionary<String, AnyObject>
                let data = results["status"]
                if  data is Dictionary<String, AnyObject>{
                    self.userSetting = FKNotificationSetting.userSetting(params: data as! Dictionary<String, Any>)
                }
                self.tblView?.reloadData()
            }
        }
    }
    
    
    
    @objc func settingChange(sender: FKSwitch){
        
        var params = Dictionary<String, Any>()
        if sender.indexPath?.row == 0{
            params["match_reminder"] = sender.isOn
        }else if (sender.indexPath?.row == 1){
            params["kick_off"] = sender.isOn
        }else if (sender.indexPath?.row == 2){
            params["goal"] = sender.isOn
        }else if (sender.indexPath?.row == 3){
            params["red_card"] = sender.isOn
        }else if (sender.indexPath?.row == 4){
            params["half_time_score"] = sender.isOn
        }else if (sender.indexPath?.row == 5){
            params["player_online_reminder"] = sender.isOn
        }else if (sender.indexPath?.row == 6){
            params["match_score"] = sender.isOn
            
        }
        var data = Dictionary<String, Any>()
        data ["user_setting"] = params
        self .apiCallForUpdateSetting(params: data)
    }
    
    func apiCallForUpdateSetting(params:Dictionary<String, Any>)  {

        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: .put, apiName: "notifications/notification_status") { (result, error, responseCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if (error == nil){
                let results = result as! Dictionary<String, AnyObject>
                let data = results["user_setting"]
                if  data is Dictionary<String, AnyObject>{
                    self.userSetting = FKNotificationSetting.userSetting(params: data as! Dictionary<String, Any>)
                }
                self.tblView?.reloadData()
                }
            }
    }
    
    // MARK:- tableview delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){

    }
}
