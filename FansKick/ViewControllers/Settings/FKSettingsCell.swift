//
//  FKSettingsCell.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 10/21/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKSettingsCell: UITableViewCell {

    @IBOutlet weak var switchBtn: FKSwitch!
    @IBOutlet weak var titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
