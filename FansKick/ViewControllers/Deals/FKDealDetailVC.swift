//
//  FKDealDetailVC.swift
//  FansKick
//
//  Created by FansKick-Sunil on 20/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

protocol RedeemDealDelegate: class {
    func backFromRedeem()
}

class FKDealDetailVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var redeemBtn: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var redreemHeightConstraints: NSLayoutConstraint!
    weak var delegate:RedeemDealDelegate?
    
   private var isRedeemCalled = false
    
    var dealItem:FKDeal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.tableFooterView = UIView.init()
        self.navigationItem.title = "DEALS"
        
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        
        if (dealItem?.isRedeemed)!{
            redreemHeightConstraints.constant = 0
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }

    // use to navigate back screen
    @objc func backBtnAction() {
        
        if  isRedeemCalled {
            self.delegate?.backFromRedeem()
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    // action of redeem button
    @IBAction func redeemAction(_ sender: UIButton) {
        AlertController.alert(title: "Confirmation!", message: "Are you sure you want to redeem deal?", buttons: ["No", "Yes"]) { (action, index) in
            if index == 1{
                self.redeemDealApiCall()
            }
        }
    }
    
    func redeemDealApiCall() {
        isRedeemCalled = true
        
        var params = Dictionary<String,Any>()
        var deal = Dictionary<String,Any>()
        deal["id"] = dealItem?.id
        params["deal"] = deal
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: MethodType.post, apiName: "deals/redeem_deal") { (result, error, statusCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if (error == nil) {
                let message = result![kResponseMsg] as! String
                MessageView.showMessage(message: message, time: 5.0, verticalAlignment: .bottom)
                self.dealItem?.isRedeemed = true
                self.tblView.reloadData()
                if (self.dealItem?.isRedeemed)!{
                    self.redreemHeightConstraints.constant = 0
                }
                //                        self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    //MARK: UITableView overrides
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (dealItem?.isRedeemed)! ? 5 : 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FKDetailTitleCell") as! FKDetailTitleCell
            cell.titleLbl?.text = dealItem?.dealName
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FKDealDetailTitleDetailCell") as! FKDealDetailTitleDetailCell
            cell.detailTextView?.isUserInteractionEnabled = true

            if indexPath.row == 1 {
                cell.titleLbl?.text = "LOCATION"
                cell.detailTextView?.text  = "\(dealItem!.stores.count) Location(s)"
                cell.iconBtn?.setTitle("C", for: .normal)
                cell.detailTextView?.isUserInteractionEnabled = false
                
            } else if indexPath.row == 2 {
                cell.iconBtn?.setTitle("'", for: .normal)
                if (dealItem?.isRedeemed)!{
                    cell.titleLbl?.text = "REDEEM ON"
                    if dealItem?.redeemDate.dateFromUTC()?.dateString(DATE_TIME_FORMAT) != nil{
                        cell.detailTextView?.text = dealItem?.redeemDate.dateFromUTC()?.dateString(DATE_TIME_FORMAT)
                    }else{
                        cell.detailTextView?.text = Date().dateString(DATE_TIME_FORMAT)
                    }
                } else {
                    cell.titleLbl?.text = "TIME LEFT"
                    let remainingTime = (dealItem?.endDate.dateFromUTC())?.offsetFrom(Date())
                    cell.detailTextView?.text = (remainingTime == "now") ? "Expired" : remainingTime
                }
            } else if indexPath.row == 3{
                cell.titleLbl?.text = "TERMS AND CONDITIONS"
                cell.detailTextView?.text = dealItem?.temsAndServices.html2String
                cell.iconBtn?.setTitle("(", for: .normal)
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "FKQRCodeCell") as! FKQRCodeCell
                cell.imgView?.normalLoad((dealItem?.qrImage)!)
                return cell
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 1 {
            if (dealItem?.stores.isEmpty)! {
                return
            }
            let storeList = self.storyboard?.instantiateViewController(withIdentifier: "FKStoreVC") as! FKStoreVC
            storeList.dataSourceArray = (dealItem?.stores)!
            self.navigationController?.pushViewController(storeList, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
