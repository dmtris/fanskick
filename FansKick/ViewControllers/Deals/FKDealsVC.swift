//
//  FKDealsVC.swift
//  FansKick
//
//  Created by FansKick-Sunil on 17/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit
import CarbonKit

class FKDealsVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, RedeemDealDelegate{
    
    @IBOutlet weak var searchView: UISearchBar!
    @IBOutlet weak var segmentView: UISegmentedControl!
    @IBOutlet weak var tblView: UITableView!
    
    var page = PAGE()
    var dataSourceArray = [FKDeal]()
    var refresh: CarbonSwipeRefresh?
    
    @objc func refreshTable() {
        apiCallForDeal()
        refresh?.endRefreshing()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !(refresh!.superview != nil) {
            view.superview?.addSubview(refresh!)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refresh = CarbonSwipeRefresh.init(scrollView: tblView)
        refresh?.colors = [UIColor.blue, UIColor.red, UIColor.orange, UIColor.green]
        // default tintColor
        // If your ViewController extends to UIViewController
        // else see below
        tblView.addSubview(refresh!)
        refresh?.addTarget(self, action: #selector(refreshTable), for: .valueChanged)
        
        self.segmentView.selectedSegmentIndex = 0;
        page.pageSize = 10
        page.totalPage = 1
        tblView.tableFooterView = UIView.init()
        tblView.rowHeight = UITableViewAutomaticDimension
        
        self.navigationItem.title = "DEALS"
        
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        
        self.perform(#selector(apiCallForDeal), with: nil, afterDelay: 0.1)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //let value = UIInterfaceOrientation.portrait.rawValue
       // UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    // use to navigate back screen
    @objc func backBtnAction(){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func backFromRedeem(){
        
        searchView.text = ""
        tblView.reloadData()
        page.startIndex = 1

        apiCallForDeal()
    }
    
    @IBAction func segmentControllAction(_ sender: UISegmentedControl) {
        page.startIndex = 1
        searchView.text = ""
        tblView.reloadData()
        apiCallForDeal()
    }
    
    @objc func apiCallForDeal() {
        var params = Dictionary<String,Any>()
        params["page"] = "\(page.startIndex)"
        params["per_page"] = "\(page.pageSize)"
        params["search"] = "\(String(describing: searchView.text!.trimWhiteSpace))"
        var apiname = "deals/deal_list"
        if segmentView.selectedSegmentIndex == 1 {
            apiname = "deals/redeem_list"
        }
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: MethodType.post, apiName: apiname) { (result, error, statusCode) in
            self.refresh?.endRefreshing()

            MBProgressHUD.hide(for: self.view, animated: true)
            if (error == nil){
                if self.page.startIndex == 1{
                    self.dataSourceArray.removeAll();
                }
                
                if statusCode == 200{
                    let results = result as! Dictionary<String, AnyObject>
                    
                    if let data = results["deals"] {
                        self.dataSourceArray.append(contentsOf: FKDeal.dealList(data: data as! Array<Dictionary<String, AnyObject>>))
                    }else{
                        self.dataSourceArray.append(contentsOf: FKDeal.dealList(data: result!["redeem_list"] as! Array<Dictionary<String, AnyObject>>))
                    }
                    self.page.totalPage = result!["total_pages"] as! Int
                }
                self.tblView.reloadData()
            }
            
            if self.dataSourceArray.count == 0 {
                
                var message = ""
                
                if self.segmentView.selectedSegmentIndex == 0{
                    message = "Right now no deal available"
                }else{
                    message = "You have not redeemed any deal so far, please redeem deal under promo section."
                }
                
                MessageView.showMessage(message: message, time: 5.0, verticalAlignment: .bottom)
            }
        }
    }
    
    
    //MARK: UISegmentControll Overrides
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        searchBar.setShowsCancelButton(true, animated: true)
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = ""
        page.startIndex = 1
        apiCallForDeal()
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        
        if !(searchBar.text?.trimWhiteSpace.isEmpty)! {
            searchApiCall(searchText: (searchBar.text?.trimWhiteSpace)!)
        }
        
    }
    // MARK: TableView overrdes
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSourceArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FKDealCell") as! FKDealCell
        
        let dealItem = self.dataSourceArray[indexPath.row]
        cell.nameLbl?.text = dealItem.dealName
       // cell.offersLbl?.text = dealItem.dealDetails
        cell.locationsBtn.setTitle("\(dealItem.stores.count) Location(s)", for: .normal)
        cell.redreamBtn?.indexPath = indexPath
        
        cell.imgView?.normalLoad(dealItem.imageUrl)
        if segmentView.selectedSegmentIndex == 1{
            cell.redreamBtn?.setTitle("REDEEMED", for: .normal)
            
        } else {
            cell.redreamBtn?.setTitle("VIEW DEAL", for: .normal)
        }
        cell.redreamBtn?.addTarget(self, action: #selector(redreamBtnAction), for: .touchUpInside)
        cell.locationsBtn?.addTarget(self, action: #selector(locationsBtn), for: .touchUpInside)

        return cell;
    }
    
    @objc func locationsBtn(sender:IndexPathButton) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dealItem = self.dataSourceArray[indexPath.row]
        if dealItem.stores.isEmpty{
            return
        }
        let storeList = self.storyboard?.instantiateViewController(withIdentifier: "FKStoreVC") as! FKStoreVC
        storeList.dataSourceArray = dealItem.stores
        self.navigationController?.pushViewController(storeList, animated: true)
        
    }
    @objc func redreamBtnAction(sender:IndexPathButton)  {
        let dealItem = self.dataSourceArray[(sender.indexPath?.row)!]
        if self.segmentView.selectedSegmentIndex == 0 {
            dealItem.isRedeemed = false
        }else {
            dealItem.isRedeemed = true
        }
        
        let dealDetail = self.storyboard?.instantiateViewController(withIdentifier: "dealDetailVC") as! FKDealDetailVC
        dealDetail.dealItem = dealItem
        dealDetail.delegate = self
        self.navigationController?.pushViewController(dealDetail, animated: true)
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        // UITableView only moves in one direction, y axis
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        // Change 10.0 to adjust the distance from bottom
        if maximumOffset - currentOffset <= 10.0 {
            
            if page.totalPage > page.startIndex{
                page.startIndex = page.startIndex+1;
                self.apiCallForDeal()
            }
        }
    }
    
    func searchApiCall(searchText:String) {
        
        self.page.startIndex = 1
        apiCallForDeal()
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
