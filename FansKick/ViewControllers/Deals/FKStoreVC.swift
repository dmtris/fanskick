//
//  FKStoreVC.swift
//  FansKick
//
//  Created by Sunil Verma on 02/12/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit
import MapKit

class FKStoreVC: UIViewController,  UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblView:UITableView?
    
    var dataSourceArray = [FKStoreInfo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "STORES"
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        
        
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        
        print(">>>>>>>>>>>>>>   ",dataSourceArray.count)
        
        print(dataSourceArray)
        self.tblView?.tableFooterView = UIView.init()
        self.tblView?.rowHeight = UITableViewAutomaticDimension
        //
        //        self.tblView?.rowHeight = 100
        
    }
    
    @objc func backBtnAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: TableView overrdes
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSourceArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FKStoreCell") as! FKStoreCell
        let store = self.dataSourceArray[indexPath.row]
        
        cell.nameLbl?.text  =  store.name
        cell.emailField?.text = "\(store.address)\n\n\(store.email)\n\n\(store.url)"
        cell.navigationBtn?.addTarget(self, action: #selector(navigateToMap), for: .touchUpInside)
        cell.navigationBtn?.indexPath = indexPath
        return cell;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    @objc func navigateToMap(sender:IndexPathButton) {
        let store = self.dataSourceArray[(sender.indexPath?.row)!]
        
        //        let latitude: CLLocationDegrees = 37.2
        //        let longitude: CLLocationDegrees = 22.9
        //        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        
        if CLLocationCoordinate2DIsValid(store.cordinate2D)
        {
            
            print(store.cordinate2D)
            let options = [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving]
            let placemark = MKPlacemark(coordinate: store.cordinate2D, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
            mapItem.name = store.name
            mapItem.openInMaps(launchOptions: options)
        }else{
            MessageView.showMessage(message: "Store address is not valid, Please update correct latitude/logitude of the store.", time: 5.0, verticalAlignment: .bottom)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
