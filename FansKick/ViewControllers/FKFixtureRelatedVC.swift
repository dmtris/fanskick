//
//  FKFixtureRelatedVC.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 11/8/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit
import CarbonKit

class FKFixtureRelatedVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var dataSourceArray = [FKNewsInfo]()
    var fixtureItem:FKFixtureListModel?
    var page = PAGE()
    var refresh: CarbonSwipeRefresh?

    var isVideoTapped = false
    @IBOutlet weak var noRelatedFound: UILabel!
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 90
        tableView.tableFooterView = UIView.init()
        refresh = CarbonSwipeRefresh.init(scrollView: tableView)
        refresh?.colors = [UIColor.blue, UIColor.red, UIColor.orange, UIColor.green]
        // default tintColor
        // If your ViewController extends to UIViewController
        // else see below
        view.addSubview(refresh!)
        refresh?.addTarget(self, action: #selector(refreshTable), for: .valueChanged)
        noRelatedFound.isHidden = true

        relatedApiCall()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //let value = UIInterfaceOrientation.portrait.rawValue
       // UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    @objc func refreshTable() {
        refresh?.endRefreshing()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !(refresh!.superview != nil) {
            view.superview?.addSubview(refresh!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // api calls
    func relatedApiCall() {
        
        var params  = Dictionary<String,Any>()
        var fixtre  = Dictionary<String,Any>()
        
        fixtre["id"] = fixtureItem?.id
        
        params["page"]      = "\(page.startIndex)"
        params["per_page"]  = "\(page.pageSize)"
        params["fixture"]    = fixtre
        let apiname = "fixtures/fixture_news"
        
        //fixtures/fixture_news
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        ServiceHelper.request(params: params, method: MethodType.post, apiName: apiname) { (result, error, statusCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            //            print(result ?? "no result")
            if (error == nil) {
                let responseCode = result!["responseCode"] as! NSInteger

                if responseCode == 200 {
                    let results = result as! Dictionary<String, AnyObject>
                    
                    if let data = results["related_news"] {
                        if self.page.startIndex == 1{
                            self.dataSourceArray.removeAll()
                        }
                        self.dataSourceArray.append(contentsOf: FKNewsInfo.list(data: data as! Array<Dictionary<String, AnyObject>>))
                        self.page.totalPage = result!["total_pages"] as! Int
                        self.tableView.reloadData()
                        if self.dataSourceArray.count == 0 {
                            self.noRelatedFound.isHidden = false
                        } else {
                            self.noRelatedFound.isHidden = true
                        }
                    } else {
                        
                        MessageView.showMessage(message: result!["responseMessage"] as! String, time: 5.0, verticalAlignment: .bottom)
                    }
                }
            }
        }
    }
    
    // MARK:- tableview datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.dataSourceArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let relatedCell = tableView.dequeueReusableCell(withIdentifier: "FKFixtureDetailCell", for: indexPath) as! FKFixtureDetailCell
        let newsInfo = self.dataSourceArray[indexPath.row]
        
        DispatchQueue.main.async {
            relatedCell.contentTextView?.text = newsInfo.contentText.html2String
            relatedCell.contentTextView?.setContentOffset(CGPoint.init(x: 0, y: -1.0), animated: false)
        }
        relatedCell.imgView?.normalLoad(newsInfo.imageUrl)
        if !newsInfo.imageUrl.isImageUrl(){
							FKPlayerHelper.captureImageFrom(url: newsInfo.imageUrl, completion: { (image ) in
                relatedCell.imgView?.image = image
            })
        }
        
        relatedCell.titleLbl?.text = newsInfo.titleText.html2String
        relatedCell.playBtn?.isHidden = newsInfo.imageUrl.isImageUrl()
        return relatedCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    // MARK:- tableview delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        // added this timer to avoid the multiple tap
        if !isVideoTapped{
            Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false, block: { (timer) in
                self.isVideoTapped = false
            })
            isVideoTapped = true
            
        let newsInfo = self.dataSourceArray[indexPath.row]
        if  !newsInfo.imageUrl.isImageUrl()  {
            
            FKPlayerHelper.shared.play(url: newsInfo.imageUrl, controller: self)

        } else {
            FKImagePreview.shared.previewImage(url: newsInfo.imageUrlLorge, controller: self)
        }
        }
    }
}
