//
//  GenericContentVC.swift
//  FansKick
//
//  Created by FansKick Dev on 12/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit


enum ContentType {
    case ContentType_Unknown, ContentType_TOS, ContentType_PrivacyPolicy, ContentType_AboutUs
}

class GenericContentVC: UIViewController {

    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet var webView: UIWebView!
    @IBOutlet weak var navBarTitleLabel: UILabel!
    
    var contentType: ContentType = .ContentType_Unknown

    var isFromMenu = false {
        
        didSet {
            
        }
    }
    
    // MARK: - UIViewController Life Cycle Functions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
    }
    override func viewWillAppear(_ animated: Bool) {
        //let value = UIInterfaceOrientation.portrait.rawValue
       // UIDevice.current.setValue(value, forKey: "orientation")
    }
    // MARK: - Private Functions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    
    fileprivate func initialSetup() {
        
        // load local file
        if let url = Bundle.main.url(forResource: "DummyHtml", withExtension: "html") {
            webView.loadRequest(URLRequest(url: url))
        }
        
        if contentType == .ContentType_TOS {
            navBarTitleLabel.text = "TOS"
        } else if contentType == .ContentType_PrivacyPolicy {
            navBarTitleLabel.text = "Privacy Policy"
        }
        
        self.activityIndicatorView.isHidden = true
    }
    
    // MARK: - IBActions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    @IBAction func onBack(_ sender: Any) {
        
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    // MARK: - UIWebView Delegates

    func webViewDidStartLoad(_ webView: UIWebView) {
        /*DispatchQueue.main.sync(execute: { () -> Void in
            self.activityIndicatorView.startAnimating()
        })*/
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        /*DispatchQueue.main.sync(execute: { () -> Void in
            self.activityIndicatorView.stopAnimating()
        })*/
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        /*DispatchQueue.main.sync(execute: { () -> Void in
            self.activityIndicatorView.stopAnimating()
        })*/
    }
    
    // MARK: - Memory Managment >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
