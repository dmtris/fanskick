//
//  FKRatingViewController.swift
//  FansKick
//
//  Created by shivam on 31/01/18.
//  Copyright © 2018 FansKick Dev. All rights reserved.
//

import UIKit
import CarbonKit

class FKRankingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var refresh: CarbonSwipeRefresh?
    var dataSourceArray = [String]()
    
    var page = PAGE()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "TEAM RANKING"
        
        tableView.tableFooterView = UIView.init()
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        
        refresh = CarbonSwipeRefresh.init(scrollView: tableView)
        refresh?.colors = [UIColor.blue, UIColor.red, UIColor.orange, UIColor.green]
        view.addSubview(refresh!)
        refresh?.addTarget(self, action: #selector(refreshTable), for: .valueChanged)
        /*
        page.startIndex = 1
        page.pageSize = 20
        
        refresh = CarbonSwipeRefresh.init(scrollView: tableView)
        refresh?.colors = [UIColor.blue, UIColor.red, UIColor.orange, UIColor.green]
        view.addSubview(refresh!)
        refresh?.addTarget(self, action: #selector(refreshTable), for: .valueChanged)
        
        tableView.tableFooterView = UIView.init()
 */
        apiCallForStates()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    // use to navigate back screen
    @objc func backBtnAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func refreshTable() {
        apiCallForStates()
        refresh?.endRefreshing()
    }
    
    func apiCallForStates(){
        let params = Dictionary<String,Any>()
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: .get, apiName: "stadiums/team_rankings", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil {
                //let results = result as! Dictionary<String, AnyObject>
                if statusCode == 200 {
                    if let data = result!["team_rankings"] {
                        self.dataSourceArray = data as! [String]
                        self.tableView.reloadData()
                    }
                    
                }
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !(refresh!.superview != nil) {
            view.superview?.addSubview(refresh!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- tableview datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourceArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: "FKImageCell", for: indexPath) as! FKImageCell
        cell.bannerImage.normalLoad(dataSourceArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableView.frame.height
    }
    
    // MARK:- tableview delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        FKImagePreview.shared.previewImage(url: dataSourceArray[indexPath.row], controller: self)
    }
    
}
