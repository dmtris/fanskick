//
//  WalkThroughVC.swift
//  ProjectTemplate
//
//  Created by Raj Kumar Sharma on 21/07/17.
//  Copyright © 2017 mobiloitte. All rights reserved.
//

import UIKit

class WalkThroughVC: UIViewController {
    
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var skipButton: UIButton!

    typealias WalkThroughAction = () -> Void
    
    var didTriggerWalkThroughAction: WalkThroughAction?

    private var arrayDataSource = [
        
        WalkThroughItem(title: "Search tutor based on location, gender, subject, rate etc. We have recommended tutors for you based on your information.", imageName: "tutorial_1"),
        WalkThroughItem(title: "View the details of tutor profile before booking. Bookmark and share tutor.", imageName: "tutorial_2"),
        WalkThroughItem(title: "Get your bookmark list", imageName: "tutorial_3"),
        WalkThroughItem(title: "Check availability of tutor and book those based on your suitable time.", imageName: "tutorial_4"),

        WalkThroughItem(title: "Communicate with tutor for any query and hurdle.", imageName: "tutorial_5"),

        WalkThroughItem(title: "You will get to know what you are booking for.", imageName: "tutorial_6"),
        WalkThroughItem(title: "View your lesson history and details.", imageName: "tutorial_7")

    ]
    
    // MARK: - UIViewController Life Cycle Functions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup() // manage walk throuhj
    }
    
    // MARK: - Private Functions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    fileprivate func initialSetup() {
        
        pageControl.numberOfPages = arrayDataSource.count
        
        // as screen visited, upadte user default status
        //UserDefaults.standard.set(true, forKey: "isFirstTime")
    }
    
    fileprivate func dismissWalkThrough() {
        
        if let triggeredWalkThroughAction = didTriggerWalkThroughAction {
            triggeredWalkThroughAction()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - IBActions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    @IBAction func onSkipButton(_ sender: Any) {
        dismissWalkThrough()
    }
    
    // MARK: - UICollectionViewDataSource Protocols >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WalkThroughCell", for: indexPath) as! WalkThroughCell
        
        let walkThroughItem = arrayDataSource[indexPath.row]
        
        cell.titleLabel.text = walkThroughItem.title
        cell.collectionImageView.image = walkThroughItem.image
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate Protocols >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        return collectionView.frame.size
    }
    
    // MARK: - UIScrollViewDelegate Protocols >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let pageWidth = collectionView.frame.size.width
        //Switch the indicator when more than 50% of the previous/next page is visible
        
        let currentPageIndex = Int(floor((scrollView.contentOffset.x - pageWidth * 0.5) / pageWidth)) + 1
        
        pageControl.currentPage = currentPageIndex
        
    }
    
    // MARK: - Memory Managment >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

class WalkThroughItem {
    
    var title = ""
    var image = UIImage(named: "")
    
    init(title: String, imageName: String) {
        self.title = title
        self.image = UIImage(named: imageName)
    }
}
