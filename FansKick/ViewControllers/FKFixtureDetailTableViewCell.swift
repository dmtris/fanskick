//
//  FKFixtureDetailTableViewCell.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 10/18/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKFixtureDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var teamLbl1: UILabel!
    @IBOutlet weak var teamLbl2: UILabel!
    @IBOutlet weak var stadiumLbl: UILabel!
    @IBOutlet weak var teamImg1: UIImageView!
    @IBOutlet weak var teamImg2: UIImageView!
    @IBOutlet weak var timeLbl: UIButton!
    @IBOutlet weak var buyTicketBtn: IndexPathButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
