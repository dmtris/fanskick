//
//  FKNewsImageCell.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 10/25/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKNewsImageCell: UICollectionViewCell {
    
    @IBOutlet weak var newsImg: UIImageView!
    @IBOutlet weak var playBtn: UIButton?

}
