//
//  FKHomeDashboardVC.swift
//  FansKick
//
//  Created by FansKick-Raj on 13/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit
import CarbonKit
import Crashlytics

class FKHomeDashboardVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITabBarDelegate,UITabBarControllerDelegate {
    @IBOutlet weak var pageDots: UIPageControl!
    @IBOutlet weak var homeTableView: UITableView!
    var page = PAGE()
    var dataSourceArray = [FKHomeModel]()
    var fixtureDataSource = [FKFixtureListModel]()
    var dataSourceArrayGroup = [FKFixtureGroup]()
    var objFkHomeModel = FKHomeModel()
    @IBOutlet weak var collectionView: UICollectionView!
    var refresh: CarbonSwipeRefresh?
    
    @objc func refreshTable() {
        apiCallForNews()
        apiCallForFixtures()
        refresh?.endRefreshing()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if DeviceType.IS_IPAD || DeviceType.IS_IPAD_PRO{
            homeTableView.tableHeaderView?.frame.size = CGSize(width:homeTableView.frame.width, height: CGFloat(500))
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !(refresh!.superview != nil) {
            view.superview?.addSubview(refresh!)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.delegate = self
        refresh = CarbonSwipeRefresh.init(scrollView: homeTableView)
        refresh?.colors = [UIColor.blue, UIColor.red, UIColor.orange, UIColor.green]
        homeTableView.addSubview(refresh!)
        refresh?.addTarget(self, action: #selector(refreshTable), for: .valueChanged)
        
        self.navigationItem.title = "HOME"
        let appDel = UIApplication.shared.delegate as! AppDelegate
        appDel.setupNavigationBar(navigationBar: self.navigationController!)
        
        initialise()
        homeTableView.tableFooterView = UIView.init()
        collectionView.reloadData()
        apiCallForFixtures()
        apiCallForNews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    func initialise() {
        
        let deal = UIButton.init(type: UIButtonType.custom)
        deal.titleLabel?.font = UIFont.init(name: fanskickFontIcons, size: 22)
        deal.setTitle("8", for: .normal)
        deal.frame = CGRect.init(x: 0, y: 0, width: 24, height: 24)
        deal.addTarget(self, action: #selector(dealAction), for: UIControlEvents.touchUpInside)
        
        let notificationBar = UIBarButtonItem.init(customView: deal)
        let profileBtn = UIButton.init(type: UIButtonType.custom)
        profileBtn.titleLabel?.font = UIFont.init(name: fanskickFontIcons, size: 22)
        profileBtn.setTitle("a", for: .normal)
        profileBtn.frame = CGRect.init(x: 0, y: 0, width: 24, height: 24)
        profileBtn.addTarget(self, action: #selector(profileAction), for: UIControlEvents.touchUpInside)
        
        let profileBar = UIBarButtonItem.init(customView: profileBtn)
        self.navigationItem.rightBarButtonItems = [profileBar,notificationBar]
        self.navigationItem.leftBarButtonItem = AppUtility.fanskickLogo()
        
    }
    
    // Fixture api call
    func apiCallForFixtures() {
        
        MBProgressHUD.hide(for: self.view, animated: true)
        MBProgressHUD.showAdded(to: self.view, animated: true)
        var params      = Dictionary<String,Any>()
        var leagueDict  = Dictionary<String,Any>()
        var clubDict    = Dictionary<String,Any>()
        
        clubDict["id"]      = ""
        leagueDict["id"]    = ""
        params["type"]      = ""  // league, both, club
        params["page"]      = "\(page.startIndex)"
        params["per_page"]  = "\(page.pageSize)"
        params["league"]    = leagueDict
        params["club"]      = clubDict
        
        print(params)
        let apiname = "fixtures/fixture_list"
        
        ServiceHelper.request(params: params, method: MethodType.post, apiName: apiname) { (result, error, statusCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            print(result ?? "No result")
            if (error == nil) {
                let results = result as! Dictionary<String, AnyObject>
                
                if let data = results["fixture_list"] {
                    
                    self.fixtureDataSource.removeAll()
                    self.dataSourceArrayGroup.removeAll()
                    let list = FKFixtureListModel.fixturesList(data: data as! Array<Dictionary<String, AnyObject>>)
                    self.fixtureDataSource.append(contentsOf: list)
                    let groups =  FKFixtureGroup.getGroupedFixture(array: self.fixtureDataSource)
                    self.dataSourceArrayGroup.append(contentsOf: groups)
                    
                    if self.dataSourceArrayGroup.isEmpty {
                        MessageView.showMessage(message: noFixtures, time: 5.0, verticalAlignment: .bottom)
                    } else {
                        self.page.totalPage = result!["total_pages"] as! Int
                    }
                }
                
                self.dataSourceArrayGroup.sort(by: { (group1, group2) -> Bool in
                    return group1.groupDate! < group2.groupDate!
                })
                self.homeTableView.reloadData()
            }
        }
    }
    
    @objc func dealAction() {
        
        let storyboard = UIStoryboard.init(name: "DealsSection", bundle: nil)
        self.navigationController?.pushViewController((storyboard.instantiateViewController(withIdentifier: "dealsVC")), animated: true);
    }
    
    @objc func profileAction() {
        let storyboard = UIStoryboard.init(name: "TabController", bundle: nil)
        let profileVc = storyboard.instantiateViewController(withIdentifier: "FKProfileVC") as! FKProfileVC
        self.navigationController?.pushViewController(profileVc, animated: true)
    }
    
    func apiCallForNews() {
        
        MBProgressHUD.hide(for: self.view, animated: true) 
        MBProgressHUD.showAdded(to: self.view, animated: true)
        var params = Dictionary<String,Any>()
        params["page"] = "\(page.startIndex)"
        params["per_page"] = "\(page.pageSize)"
        
        let apiname = "news/news"
        
        ServiceHelper.request(params: params, method: MethodType.get, apiName: apiname) { (result, error, statusCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if (error == nil) {
                self.dataSourceArray.removeAll()
                let results = result as! Dictionary<String, AnyObject>
                
                if let data = results["news"] {
                    self.dataSourceArray.append(contentsOf: FKHomeModel.homeList(data: data as! Array<Dictionary<String, AnyObject>>))
                }
                
                self.dataSourceArray.sort(by: { (news1, news2) -> Bool in
                    return news1.newsDate > news2.newsDate
                })
                
                self.page.totalPage = result!["total_pages"] as! Int
                if self.dataSourceArray.count > 0 {
                    
                    self.objFkHomeModel = self.dataSourceArray[0]
                    self.pageDots.numberOfPages = self.dataSourceArray.count
                    delay(0.1, closure: {
                        self.homeTableView.reloadData()
                    })
                }
                
                DispatchQueue.main.async {
                    self.homeTableView.reloadData()
                    self.collectionView.reloadData()
                }
            }
        }
    }
    
    //MARK:- UITableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            if self.dataSourceArray.count > 0 {
                return 1
            }
            return 0
        } else if section == 1 {
            if dataSourceArrayGroup.count > 0 {
                return 1
            }
            return 0
        } else if section == 2 {
            
            if dataSourceArrayGroup.count > 0 {
                
                let group = self.dataSourceArrayGroup[0]
                if group.groupArray.count > 3 {
                    return 3
                }
                return group.groupArray.count
            } else {
                return 0
            }
        } else if section == 3 {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FKNewsCell", for: indexPath) as! FKNewsCell
            
            cell.readMoreBtn.addTarget(self, action: #selector(readMoreBtnAction), for: .touchUpInside)
            cell.newsHeadingLbl.text    = objFkHomeModel.title
            cell.dateTimeLbl.text       =  (objFkHomeModel.newsDate.dateFromUTC()?.dateString(DATE_TIME_FORMAT))!
            cell.readMoreBtn.isHidden = true
            
            if self.objFkHomeModel.details.html2String.length > 150 {
                cell.readMoreBtn.isHidden = false
            }
            cell.reviewtextView.text = self.objFkHomeModel.details.html2String
            
            if self.objFkHomeModel.isExpanded == false {
                if self.objFkHomeModel.details.html2String.length > 150 {
                    
                    cell.reviewtextView.textContainer.maximumNumberOfLines = 3
                    cell.readMoreBtn.setTitle("READ MORE", for: .normal)
                } else {
                    cell.readMoreBtn.setTitle("READ LESS", for: .normal)
                    cell.reviewtextView.textContainer.maximumNumberOfLines = 0
                }
            } else {
                cell.readMoreBtn.setTitle("READ LESS", for: .normal)
                cell.reviewtextView.textContainer.maximumNumberOfLines = 0
            }
            return cell
        } else if indexPath.section == 1 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "FKHomeFixtureCellTableViewCell", for: indexPath) as! FKHomeFixtureCellTableViewCell
            if self.dataSourceArrayGroup.count > 0 {
                let group = self.dataSourceArrayGroup[0]
                cell.dateLbl.text = group.groupDate?.stringToDate()?.dateString(DATE_FORMAT)
            }
            return cell
        } else if indexPath.section == 2 {
            
            let group = self.dataSourceArrayGroup[0]
            
            let cell : FKFixtureListCell!
            if DeviceType.IS_IPAD_PRO || DeviceType.IS_IPAD{
                cell = tableView.dequeueReusableCell(withIdentifier: "FKFixtureListCelliPad", for: indexPath) as! FKFixtureListCell
            }else{
                cell = tableView.dequeueReusableCell(withIdentifier: "FKFixtureListCell", for: indexPath) as! FKFixtureListCell
            }
            
            // let cell = tableView.dequeueReusableCell(withIdentifier: "FKFixtureListCell", for: indexPath) as! FKFixtureListCell
            let fixture = group.groupArray[indexPath.row]
            cell.team1Lbl.text   = fixture.club
            cell.team2.text      =  fixture.apponent
            cell.team1Img.normalLoad(fixture.clubImage)
            cell.team2Img.normalLoad(fixture.apponentImage)
            cell.timeBtn.setTitle(fixture.start_time.dateFromUTC()?.onlyTimeString(), for: .normal)
            cell.stadiumNameLbl.text    =  fixture.stadium
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FKViewAllFixtureCell", for: indexPath) as! FKViewAllFixtureCell
            cell.viewAllMatcheBtn.addTarget(self, action: #selector(nextBtnAction), for: .touchUpInside)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 2 {
            if dataSourceArrayGroup.count > 0 {
                let group = self.dataSourceArrayGroup[0]
                let fixtureDetailVc = self.storyboard?.instantiateViewController(withIdentifier: "FKFixtureDetailViewController") as! FKFixtureDetailViewController
                let fixture = group.groupArray[indexPath.row]
                fixtureDetailVc.fixtureItem = fixture
                self.navigationController?.pushViewController(fixtureDetailVc, animated: true)
            }
        }
    }
    
    @objc func nextBtnAction() {
        
        self.navigationController?.pushViewController((self.storyboard?.instantiateViewController(withIdentifier: "FKFixturesViewController"))!, animated: true)
    }
    
    @objc func readMoreBtnAction(sender:UIButton) {
        
        if self.objFkHomeModel.isExpanded {
            sender.setTitle("READ MORE", for: .normal)
        } else {
            sender.setTitle("READ LESS", for: .normal)
        }
        self.objFkHomeModel.isExpanded = !self.objFkHomeModel.isExpanded
        let indexPath = IndexPath(item: 0, section: 0)
        homeTableView.beginUpdates()
        homeTableView.reloadRows(at: [indexPath], with: .automatic)
        homeTableView.endUpdates()
        
    }
    
    //MARK:  UICollectionView Overrides
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSourceArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        let indexpath =  collectionView.indexPathsForVisibleItems.first
        if (indexpath != nil) {
            self.pageDots.currentPage = (indexpath?.row)!
            
            if self.dataSourceArray.count > 0 {
                objFkHomeModel = self.dataSourceArray[(indexpath?.row)!]
                objFkHomeModel.isExpanded = false
                delay(0.2, closure: {
                    self.homeTableView.reloadData()
                })
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FKNewsImageCell", for: indexPath) as! FKNewsImageCell
        let objFkHomeModel = self.dataSourceArray[indexPath.row]
			cell.newsImg.normalLoad(objFkHomeModel.imageUrl)
			//cell.newsImg.normalLoad(objFkHomeModel.imageUrlLorge)

        if !objFkHomeModel.imageUrl.isImageUrl(){
            FKPlayerHelper.captureImageFrom(url: objFkHomeModel.imageUrl, completion: { (image ) in
                cell.newsImg?.image = image
            })
        }
			

        cell.playBtn?.isHidden = objFkHomeModel.imageUrl.isImageUrl()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let objFkHomeModel = self.dataSourceArray[indexPath.row]
        if  !objFkHomeModel.imageUrl.isImageUrl()  {
            
            FKPlayerHelper.shared.playOnRootWindow(url: objFkHomeModel.imageUrl)
        } else {
            FKImagePreview.shared.previewImage(url: objFkHomeModel.imageUrlLorge, controller: self)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var size = CGSize()
        size.width  = self.view.frame.size.width
        size.height = self.view.frame.size.height
        return size
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
