//
//  FKFixtureListCell.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 10/30/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKFixtureListCell: UITableViewCell {

    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var timeBtn: UIButton!
    @IBOutlet weak var team1Lbl: UILabel!
    @IBOutlet weak var team2: UILabel!
    @IBOutlet weak var team1Img: UIImageView!
    @IBOutlet weak var team2Img: UIImageView!
    
    @IBOutlet weak var stadiumNameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
