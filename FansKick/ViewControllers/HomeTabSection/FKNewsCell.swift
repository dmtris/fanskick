//
//  FKNewsCell.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 10/21/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKNewsCell: UITableViewCell {

    @IBOutlet weak var starRating: AARatingBar!
    @IBOutlet weak var reviewtextView: UITextView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var dateTimeLbl: UILabel!
    @IBOutlet weak var newsHeadingLbl: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var readMoreBtn: UIButton!
    @IBOutlet weak var newsDescriptionLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
