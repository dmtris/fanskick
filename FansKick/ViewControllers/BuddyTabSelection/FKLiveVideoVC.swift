//
//  FKLiveVideoVC.swift
//  FansKick
//
//  Created by FansKick-Sunil on 21/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit
import WebKit

//let HOST_ADDRESS = "http://52.77.166.60"   // staging
let HOST_ADDRESS = "http://52.221.153.72"  // PRODUCTION URL
let PORT_NUMBER = 1935
let APPLICATION_NAME = "live";
let STREAM_NAME = "myStream";
class FKLiveVideoVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate  {
	
	var dataSourceArray = [FKCommetInfo]()
	@IBOutlet  weak var tblView: UITableView?
	@IBOutlet  weak var inputTextView: UIView?
	@IBOutlet weak var liveBtn :UIButton?
	@IBOutlet weak var viewerCount:UIButton?
	
	@IBOutlet weak var crossBtn :UIButton?
	@IBOutlet  weak var textView: FKTextView?
	
	var player : FKPlayerInfo?
	var isFromNotification = false
	private  var isDragging = true
	private var isNotifyCalled = false
	
	
	@IBOutlet weak var textViewBottmConstraints:NSLayoutConstraint?
	override func viewDidLoad() {
		super.viewDidLoad()
		
		UIApplication.shared.isIdleTimerDisabled = true
		
		self.perform(#selector(loadVideoUrl), with: nil , afterDelay: 1.0)
		
		textViewBottmConstraints?.constant = 0
		tblView?.tableFooterView = UIView.init()
		tblView?.rowHeight = UITableViewAutomaticDimension
		
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(keyboardWillShow),
			name: NSNotification.Name.UIKeyboardWillShow,
			object: nil
		)
		
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: MESSAGE_RECEIVE_NOTIFICATION), object: nil)
		
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: MESSAGE_CONNECT_NOTIFICATION), object: nil)
		
		NotificationCenter.default.addObserver(self, selector: #selector(playerDidFailNotification), name: NSNotification.Name(rawValue: PLAYER_FAIL_NOTIFICATION), object: nil)
		
		
		
		NotificationCenter.default.addObserver(self, selector: #selector(messageReceive), name: NSNotification.Name(rawValue: MESSAGE_RECEIVE_NOTIFICATION), object: nil)
		
		
		NotificationCenter.default.addObserver(self, selector: #selector(socketConnectNotify), name: NSNotification.Name(rawValue: MESSAGE_CONNECT_NOTIFICATION), object: nil)
		
		// Initialise socket
		
		if !isFromNotification {
			self.initialiseSocket()
		}else{
			self.apiCallForPlayerDetail()
		}
	}
	
	@objc func socketConnectNotify(){
		if !isNotifyCalled {
			isNotifyCalled = true
			
			var params = Dictionary<String,Any>()
			params["user_id"] = defaults.value(forKey: kUserId)
			params["room_id"] = self.player?.roomID
			ServiceHelper.request(params: params, method: .post, apiName: "highlights/increase_live") { (result, error, responseCode) in
				if error == nil {
					let results = result as! Dictionary<String, AnyObject>
					if let data = results["room"] {
						let room = data as! Dictionary<String, AnyObject>
						let userCount =		room.validatedValue("live_user_count", expected: "0" as AnyObject)
						self.updateViewCount(count: userCount as! String);
					}
				}
			}
		}
	}
	func updateViewCount(count:String){
		
		self.viewerCount?.setTitle("\(count)    ", for: .normal);
	}
	func initialiseSocket(){
		FKPWebSocketHelper.sharedInstance.connect()
	}
	
	func apiCallForPlayerDetail() {
		
		var params = Dictionary<String,Any>()
		var playerDict = Dictionary<String,Any>()
		playerDict["id"] = player?.id
		params["player"] = playerDict
		MBProgressHUD.showAdded(to: self.view, animated: true)
		ServiceHelper.request(params: params, method: .post, apiName: "players/player_details") { (result, error, responseCode) in
			MBProgressHUD.hide(for: self.view, animated: true)
			if error == nil{
				let results = result as! Dictionary<String, AnyObject>
				
				if let data = results["player_details"] {
					self.player?.updatePlayerInfo(data: data as! Dictionary<String, AnyObject>)
				}
				
				let group = results["player_group"]
				
				if  group is Dictionary<String, AnyObject>
				{
					let groupID = group as! Dictionary<String, AnyObject>
					self.player?.groupID =  groupID["id"] as! String
				}
				let room = results["player_room"]
				if room is Dictionary<String, AnyObject>{
					let roomID = room as! Dictionary<String, AnyObject>
					self.player?.roomID =  roomID["id"] as! String
				}
				
				self.initialiseSocket()
			}
			
		}
	}
	
	@objc func messageReceive(data:Notification){
		
		let message = data.object! as! String
		guard let data = message.data(using: .utf8) else {
			return
		}
		
		let anyResult = try? JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String, Any>
		
		let roomID = anyResult!["room_id"] as! String
		let userCount        = anyResult?.validatedValue("user_count", expected: 0 as NSNumber)

		self.updateViewCount(count: "\(userCount ?? "0" as AnyObject)")

		if  roomID == self.player?.roomID {
			let comments =  FKCommetInfo.messageWith(params: anyResult!)
			//            self.dataSourceArray.insert(comments, at: 0)
			self.dataSourceArray.append(comments)
			if isDragging {
				scrollToBottom()
			}
			tblView?.reloadData()
		}
	}
	
	@objc func playerDidFailNotification() {
		
		//  MessageView.showMessage(message: "It seems player is  offline now, please try after some time", time: 0, verticalAlignment: .bottom)
	}
	@objc func keyboardWillShow(_ notification: Notification) {
		if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
			let keyboardRectangle = keyboardFrame.cgRectValue
			let keyboardHeight = keyboardRectangle.height
			textViewBottmConstraints?.constant = keyboardHeight
			
			UIView.animate(withDuration: 0.2) {
				self.view.layoutIfNeeded()
			}
		}
	}
	@IBAction func crossBtnAction() {
		
		FKPlayerHelper.shared.stop()
		UIApplication.shared.isIdleTimerDisabled = false
		
		self.dismiss(animated: true) {
			
			var params = Dictionary<String,Any>()
			params["user_id"] = defaults.value(forKey: kUserId)
			params["room_id"] = self.player?.roomID
			ServiceHelper.request(params: params, method: .post, apiName: "highlights/decrease_live") { (result, error, responseCode) in
				
			}
		}
	}
	
	@IBAction func liveBtnAction() {
		
	}
	
	@objc func loadVideoUrl() {
		
		
		let url = "\(HOST_ADDRESS):\(PORT_NUMBER)/\(APPLICATION_NAME)/\(self.player?.username ?? "")/playlist.m3u8"
		//http://192.168.0.106:1935/live/myStream/playlist.m3u8
		FKPlayerHelper.shared.embedPlayer(url: url, view: self.view)
		
		self.view.bringSubview(toFront: tblView!)
		self.view.bringSubview(toFront: inputTextView!)
		self.view.bringSubview(toFront: liveBtn!)
		self.view.bringSubview(toFront: crossBtn!)
		self.view.bringSubview(toFront: viewerCount!)
	}
	
	override func viewDidDisappear(_ animated: Bool) {
		
		FKPlayerHelper.shared.stop()
		FKPWebSocketHelper.sharedInstance.disconnect()
	}
	
	//MARK: UITableView Overrides
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.dataSourceArray.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let cell = tableView.dequeueReusableCell(withIdentifier: "FKLiveCommentCell") as! FKLiveCommentCell
		
		let message = self.dataSourceArray[indexPath.row]
		cell.commentsLbl?.text = message.message
		cell.nameLbl?.text = message.senderName
		cell.timeLbl?.text = message.messageDate.dateFromUTC()?.dateString("hh:mm a")
		cell.imgView?.normalLoad(message.senderImage)
		cell.closeBtn?.addTarget(self, action: #selector(clearBtnAction), for: .touchUpInside)
		cell.closeBtn?.indexPath = indexPath
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableViewAutomaticDimension
	}
	
	
	func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool{
		return true
	}
	
	func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
			dataSourceArray.remove(at: indexPath.row)
			tblView?.reloadData()
		}
	}
	
	//MARK:  UITextView overrdes
	
	func textViewDidBeginEditing(_ textView: UITextView) {
	}
	//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
	//        if (text == "\n") {
	//
	//            if !textView.text.trimWhiteSpace.isEmpty{
	//                dataSourceArray.append(textView.text.trimWhiteSpace)
	//                textView.text = ""
	//                tblView?.reloadData()
	//            }
	//            textView.resignFirstResponder()
	//            return false
	//        }
	//        return true
	//    }
	
	func textViewDidEndEditing(_ textView: UITextView) {
		
		textViewBottmConstraints?.constant = 0
		
		UIView.animate(withDuration: 0.2) {
			self.view.layoutIfNeeded()
		}
	}
	
	@IBAction func sendBtnAction() {
		
		self.view.endEditing(true)
		if !(textView?.text.trimWhiteSpace.isEmpty)! {
			let appDel = UIApplication.shared.delegate as! AppDelegate
			if !appDel.isReachable {
				MessageView.showMessage(message: NO_INTERNET_CONNECTION, time: 4.0, verticalAlignment: .top)
				return
			}
			//  self.dataSourceArray.insert((textView?.text.trimWhiteSpace)!, at: 0)
			sendDataToSocket(data: (textView?.text.trimWhiteSpace)!)
			textView?.text = ""
			tblView?.reloadData()
		}
	}
	
	@objc func clearBtnAction(sender:IndexPathButton) {
		
		dataSourceArray.remove(at: (sender.indexPath?.row)!)
		tblView?.reloadData()
	}
	
	func sendDataToSocket(data:String) {
		
		var params = Dictionary<String, Any>()
		params["receiver_id"]   =   self.player?.groupID
		params["room_id"]       =   self.player?.roomID
		params["body"]          =   data
		params["upload_type"]   =   ""
		params["is_group"]      =   true
		params["sender_id"]     =   defaults.value(forKey: kUserId)
		params["type"]          =   "Joined"
		FKPWebSocketHelper.sharedInstance.sendData(data: params)
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
	}
	
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		
		let height = scrollView.frame.size.height
		let contentYoffset = scrollView.contentOffset.y
		let distanceFromBottom = scrollView.contentSize.height - contentYoffset
		
		if distanceFromBottom < height {
			print("you reached end of the table")
		}
		if distanceFromBottom > 240.0 {
			isDragging = false
			
		} else {
			isDragging = true
		}
	}
	
	func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
		
		let height = scrollView.frame.size.height
		let contentYoffset = scrollView.contentOffset.y
		let distanceFromBottom = scrollView.contentSize.height - contentYoffset
		print("scrollViewDidEndDragging distanceFromBottom:> ",distanceFromBottom)
		
		if distanceFromBottom < height {
			print("scrollViewDidEndDragging you reached end of the table")
		}
		
		if distanceFromBottom > 240.0 {
			isDragging = false
		} else {
			isDragging = true
		}
	}
	
	func scrollToBottom() {
		DispatchQueue.main.async {
			let indexPath = IndexPath(row: self.dataSourceArray.count-1, section: 0)
			self.tblView?.scrollToRow(at: indexPath, at: .bottom, animated: true)
		}
	}
}
