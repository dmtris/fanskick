//
//  FKBuddyDetailVC.swift
//  FansKick
//
//  Created by FansKick-Sunil on 21/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKBuddyDetailVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    
    @IBOutlet var descLbl:UITextView?
    @IBOutlet var imgView:UIImageView?
    @IBOutlet weak var collectionView:UICollectionView?
    @IBOutlet weak var segmentControll:UISegmentedControl?
    var dataSourceArray = [FKStoryInfo]()
    var player : FKPlayerInfo?
    
    @IBOutlet weak var errorLbl: UILabel!
    var isChatScreenPreseting = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        
        self.navigationItem.title = self.player?.name
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        NotificationCenter.default.addObserver(self, selector: #selector(playerOnlineNotification), name: NSNotification.Name(rawValue: PLAYER_ONLINE_NOTIFICATION), object: nil)

        playerDetailApiCall()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    // use to navigate back screen
    @objc func backBtnAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
  private func initialiseContent(){
        self.descLbl?.text = self.player?.bio
        self.imgView?.normalLoad((player?.profileImage)!)
        self.descLbl?.setContentOffset(CGPoint.init(x: 0, y: -10), animated: false)
        self.addRightBarBtn()

    }
    
    @objc func playerOnlineNotification(noti:NSNotification){
        
        let params = noti.object as! Dictionary<String,Any>
        let playerID = params["player_id"] as! String
        
        
            if self.player?.id == playerID {
                self.player?.isOnline = true
                self.addRightBarBtn()
        }
    }
    func addRightBarBtn() {
        
        let chat = UIButton.init(type: UIButtonType.custom)
        chat.titleLabel?.font = UIFont.init(name: "fanskick-font2-icon-set", size: 22)
        chat.setTitle("x", for: .normal)
        chat.frame = CGRect.init(x: 0, y: 0, width: 24, height: 24)

        if (self.player?.isOnline)!{
            chat.setTitleColor(GREEN_COLOR, for: .normal)
        }
        chat.addTarget(self, action: #selector(liveChatAction), for: UIControlEvents.touchUpInside)
        let chatBar = UIBarButtonItem.init(customView: chat)
        self.navigationItem.rightBarButtonItem = chatBar
    }
    
    func playerDetailApiCall() {
        
        var params = Dictionary<String,Any>()
        var playerDict = Dictionary<String,Any>()
        playerDict["id"] = player?.id
        params["player"] = playerDict
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: .post, apiName: "players/player_details") { (result, error, responseCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                
                if let data = results["player_details"] {
                    self.player?.updatePlayerInfo(data: data as! Dictionary<String, AnyObject>)
                    self.dataSourceArray = (self.player?.storyArray)!
                    self.collectionView?.reloadData()
                    self.initialiseContent()
                }
                
                if (self.player?.storyArray)!.count == 0 {
                    self.errorLbl.isHidden = false
                    self.errorLbl.text = "No stories found!"
                }
                
                let group = results["player_group"]
                
                if  group is Dictionary<String, AnyObject>
                {
                    let groupID = group as! Dictionary<String, AnyObject>
                    self.player?.groupID =  groupID["id"] as! String
                }
                let room = results["player_room"]
                if room is Dictionary<String, AnyObject>{
                    let roomID = room as! Dictionary<String, AnyObject>
                    self.player?.roomID =  roomID["id"] as! String
                }
            }
            
        }
    }
    
    @IBAction func segmentAction() {
        dataSourceArray.removeAll()
        errorLbl.isHidden = true
        
        if  self.segmentControll?.selectedSegmentIndex == 0 {
            dataSourceArray = (self.player?.storyArray)!
            if (self.player?.storyArray)!.count == 0 {
                errorLbl.isHidden = false
                errorLbl.text = "No stories found!"
            }
        } else {
            dataSourceArray = (self.player?.videoArray)!
            if (self.player?.videoArray)!.count == 0 {
                errorLbl.isHidden = false
                errorLbl.text = "No videos found!"
            }
        }
        collectionView?.reloadData()
    }
    
    //MARK:  UICollectionView Overrides
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSourceArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FKBuddyStoryCell", for: indexPath) as! FKBuddyStoryCell
        
        let story = self.dataSourceArray[indexPath.row]
        
        cell.playBtn?.isHidden = true
        
        if (story.imageUrl.isImageUrl()) {
            cell.imgView?.normalLoad((story.imageUrl))
            
        } else {
            FKPlayerHelper.captureImageFrom(url: story.imageUrl, completion: { (image ) in
                cell.imgView?.image = image
            })
            cell.playBtn?.isHidden = false
            
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let story = self.dataSourceArray[indexPath.row]
        
        if  !story.imageUrl.isImageUrl()  {

            FKPlayerHelper.shared.playOnRootWindow(url: story.imageUrl)
        }else{
            FKImagePreview.shared.previewImage(url: story.imageUrl, controller: self)
            
        }
        //
        //        let storyDetail = self.storyboard?.instantiateViewController(withIdentifier: "storyDetailVC") as! FKStoryDetailVC
        //        storyDetail.story = self.dataSourceArray[indexPath.row]
        //        self.navigationController?.pushViewController(storyDetail, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: self.view.frame.width/3 - 10, height:  self.view.frame.width/3 - 10)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func liveChatAction() {
			
        if (self.player?.isOnline)! {
            
            let  liveVideoVC = self.storyboard?.instantiateViewController(withIdentifier: "liveVideoVC") as! FKLiveVideoVC
            liveVideoVC.player = self.player
            
            self.present(liveVideoVC, animated: true) {
                self.isChatScreenPreseting = false
            }
        } else {
            MessageView.showMessage(message: "Player is not available right now.", time: 5.0, textAlignment: .center, verticalAlignment: .bottom)
        }
    }
}
