//
//  FKStoryDetailVC.swift
//  FansKick
//
//  Created by Sunil Verma on 17/11/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit
import WebKit
class FKStoryDetailVC: UIViewController {

    var story: FKStoryInfo?
    
    @IBOutlet weak var textView:UITextView?
    @IBOutlet weak var imgView:UIImageView?
    @IBOutlet weak var crosBtn:UIImageView?
    @IBOutlet weak var previewView:UIView?
    
    @IBOutlet weak var webView:WKWebView?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Story"
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        
        self.perform(#selector(initialise), with: nil, afterDelay: 0.1)
        
      let tapGesture =   UITapGestureRecognizer.init(target: self, action: #selector(imageViewTapAction))
        
        imgView?.addGestureRecognizer(tapGesture)
        
    }

    // use to navigate back screen
    @objc func backBtnAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    
    @objc func initialise(){
       self.textView?.text = story?.content.html2String

        if (story?.imageUrl.isImageUrl())! {
            self.imgView?.normalLoad((story?.imageUrl)!)
            previewView?.isHidden = true
            imgView?.isHidden = false

        } else {
            previewView?.isHidden = false
            imgView?.isHidden = true

            FKPlayerHelper.shared.play(url: (story?.imageUrl)!, view: previewView!)
        }
        
     //   self.webView?.loadHTMLString((story?.content)!, baseURL: nil)
//    FKPlayerHelper.shared.play(url: "http://res.cloudinary.com/di8lsuqdb/video/upload/v1510901619/ewrp7lmrzrafrkrd4ngw.mp4", view: previewView!)

    }
    
    @objc func imageViewTapAction(){
        
        FKImagePreview.shared.previewImage(url: (story?.imageUrl)!, controller: self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        FKPlayerHelper.shared.stop()

    }
  
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
