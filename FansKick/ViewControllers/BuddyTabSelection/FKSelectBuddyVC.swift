//
//  FKSelectBuddyVC.swift
//  FansKick
//
//  Created by FansKick-Sunil on 21/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKSelectBuddyVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var ratingBarView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var ratingBarMain: AARatingBar!
    @IBOutlet weak var ratingBar0: AARatingBar!
    @IBOutlet weak var ratingBar1: AARatingBar!
    @IBOutlet weak var ratingBar2: AARatingBar!
    @IBOutlet weak var ratingBar3: AARatingBar!
    @IBOutlet weak var ratingBar4: AARatingBar!
    @IBOutlet weak var ratingBar5: AARatingBar!
    
    var leagueArray = [FKListInfo]()
    var page = PAGE()
    var dataSourceArray = [FKPlayerInfo]()
    var selectedLeague = ""
    
    
    var isRattingDisplay = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "PLAYERS"
        selectedLeague = "All Leagues"
        page.startIndex = 1
        page.pageSize = 10
        
        tblView.tableFooterView = UIView.init()
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        
        let mainGesture = UITapGestureRecognizer.init(target: self, action: #selector(mainRattingAction))
        
        let gesture0 = UITapGestureRecognizer.init(target: self, action: #selector(rattingViewAction0))
        let gesture1 = UITapGestureRecognizer.init(target: self, action: #selector(rattingViewAction1))
        let gesture2 = UITapGestureRecognizer.init(target: self, action: #selector(rattingViewAction2))
        let gesture3 = UITapGestureRecognizer.init(target: self, action: #selector(rattingViewAction3))
        let gesture4 = UITapGestureRecognizer.init(target: self, action: #selector(rattingViewAction4))
        let gesture5 = UITapGestureRecognizer.init(target: self, action: #selector(rattingViewAction5))
        
        
        self.ratingBarMain.addGestureRecognizer(mainGesture)
        self.ratingBar0.addGestureRecognizer(gesture0)
        self.ratingBar1.addGestureRecognizer(gesture1)
        self.ratingBar2.addGestureRecognizer(gesture2)
        self.ratingBar3.addGestureRecognizer(gesture3)
        self.ratingBar4.addGestureRecognizer(gesture4)
        self.ratingBar5.addGestureRecognizer(gesture5)
        
        leagueApiCall()
        playerApiCall()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    //MARK: APis call
    
    func leagueApiCall(){
        
        var params      = Dictionary<String,Any>()
        params["page"]      = "1"
        params["per_page"]  = "500"
        
        ServiceHelper.request(params: params, method: .get, apiName: "fixtures/league_list", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            if error == nil {
                let data = result?["league_list"] as? Array<Dictionary<String, AnyObject>>
                if data != nil{
                    self.leagueArray.append(contentsOf: FKListInfo.list(data: (data)!))
                } 
            }
        }
    }
    
    
    func getLeagueList() -> [String]{
        var array = [String]()
        array.append("All Leagues")
        for item in self.leagueArray{
            array.append(item.name)
        }
        return array
        
    }
    
    func leagueByName(name:String) -> FKListInfo {
        
        var league:FKListInfo?
        for item in self.leagueArray{
            if item.name == name {
                league = item
                break
            }
        }
        return league!
    }
    
    func playerApiCall(){
        
        var leageDict = Dictionary<String,Any>()
        
        leageDict["id"] = ""  // initialise with blank
        
        var type = ""
        if selectedLeague != "All Leagues"{
            let league =    leagueByName(name: selectedLeague)
            leageDict["id"] = league.id
            type = "league"
        }
        
        var player = Dictionary<String, Any>()
        player["rating"] = ""
        
        if ratingBarMain.value > 0 {
            type = "rating"
            player["rating"] = "\(ratingBarMain.value)"
            
        }
        
        if (selectedLeague != "All Leagues") && (ratingBarMain.value > 0){
            type = "both"
        }
        
        var params = Dictionary<String,Any>()
        
        params["league"] = leageDict
        params["player"] = player
        
        params["type"] = type
        params["page"] = "\(page.startIndex)"
        params["per_page"] = "\(page.pageSize)"
        
        if (searchBar.text?.isEmpty)!{
            params["search"] = ""
        }else{
            params["search"] = searchBar.text
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        ServiceHelper.request(params: params, method: .post, apiName: "players/player_list") { (result, error, responseCode) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if error == nil {
                
                let results = result as! Dictionary<String, AnyObject>
                if let data = results["players"] {
                    if self.page.startIndex == 1 {
                        self.dataSourceArray.removeAll()
                    }
                    let list =  FKPlayerInfo.playerList(data: data as! Array<Dictionary<String, AnyObject>>)
                    self.dataSourceArray.append(contentsOf: list)
                    
                    if self.dataSourceArray.isEmpty{
                        MessageView.showMessage(message: "No player found, please change filter criteria.", time: 5.0, verticalAlignment: .bottom)

                    } else {
                        self.page.totalPage = result!["total_pages"] as! Int
                    }
                    self.tblView.reloadData()
                }
            }
        }
    }
    
    //MARK: Ratting action
    @objc func mainRattingAction(sender: AARatingBar?){
        isRattingDisplay = !isRattingDisplay
        self.ratingBarView.isHidden = isRattingDisplay
        
    }
    
    func updateRatting(value:CGFloat) {
        isRattingDisplay = !isRattingDisplay
        self.ratingBarView.isHidden = isRattingDisplay
        ratingBarMain.value = value
        ratingBarMain.ratingValueChange()
    }
    
    
    @objc func rattingViewAction0(sender: AARatingBar?){
        updateRatting(value: 0)
        rattingChange()

    }
    @objc func rattingViewAction1(sender: AARatingBar?){
        updateRatting(value: 1)
        rattingChange()

    }
    @objc func rattingViewAction2(sender: AARatingBar?){
        updateRatting(value: 2)
        rattingChange()

    }
    @objc func rattingViewAction3(sender: AARatingBar?){
        updateRatting(value: 3)
        rattingChange()

    }
    @objc func rattingViewAction4(sender: AARatingBar?){
        updateRatting(value: 4)
        rattingChange()

    }
    @objc func rattingViewAction5(sender: AARatingBar?){
        updateRatting(value: 5)
        rattingChange()
    }
    
    func rattingChange(){
        page.startIndex = 1
        playerApiCall()
    }
    // use to navigate back screen
    @objc func backBtnAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func leagueBtn(_ sender: UIButton) {
        
        
        let dataArray = self.getLeagueList()
        
        RPicker.sharedInstance.selectOption(title: "Select League", dataArray: dataArray, selectedIndex: 0) { (selectedText, index) in
            if !selectedText.isEmpty{
                sender.setTitle(selectedText, for: .normal)
                self.selectedLeague = selectedText
                self.page.startIndex = 1
                self.playerApiCall()
            }
            print(selectedText)
        }
        
    }
    // MARK: TableView overrdes
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.dataSourceArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FKBuddyCell") as! FKBuddyCell
        let player = self.dataSourceArray[indexPath.row]
        cell.nameLbl?.text = player.name
        cell.rattingBar?.value = CGFloat(Int(player.rating)!)
        cell.rattingBar?.ratingValueChange()
        cell.imgView?.normalLoad(player.profileImage)
        cell.clubNameLbl?.text = player.clubName
        cell.clubImgView?.normalLoad(player.clubImage)
        return cell;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80;
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let subscriptionVC = self.storyboard?.instantiateViewController(withIdentifier: "buddySubscriptionVC") as! FKBuddySubscriptionVC
        subscriptionVC.player = self.dataSourceArray[indexPath.row]
        self.navigationController?.pushViewController(subscriptionVC, animated: true)
        
    }
    
    //MARK: ScrollView overrdes
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        // UITableView only moves in one direction, y axis
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        // Change 10.0 to adjust the distance from bottom
        if maximumOffset - currentOffset <= 10.0 {
            
            if page.totalPage > page.startIndex{
                page.startIndex = page.startIndex+1;
                self.playerApiCall()
            }
        }
    }
    
    
    
    //MARK: UISegmentControll Overrides
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        searchBar.setShowsCancelButton(true, animated: true)
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        page.startIndex = 1
        searchBar.text = ""
        playerApiCall()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if !(searchBar.text?.trimWhiteSpace.isEmpty)! {
            page.startIndex = 1
            playerApiCall()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
