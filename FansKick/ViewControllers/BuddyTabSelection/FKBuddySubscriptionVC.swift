//
//  FKBuddySubscriptionVC.swift
//  FansKick
//
//  Created by FansKick-Sunil on 24/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKBuddySubscriptionVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var clubImageView: UIImageView!
    @IBOutlet weak var clubNameLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var priceBtn: UIButton!
    var dataSourceArray = [FKListInfo]()
    
    var placeHolderArray = [String]()
    var player:FKPlayerInfo?
    var dataArray = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblView.tableFooterView  = UIView.init()
        self.navigationItem.title = "PLAYER DETAILS"
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        
        let data = self.player?.getDetailArray()
        self.dataSourceArray = data!
        initialiseData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func initialiseData() {
        
        let goal = (player?.goal.isEmpty)! ? "0" : player?.goal
        
        if !(self.player?.debut.isEmpty)!
        {
            self.nameLbl.text =
            "\(player?.name ?? "")\n\(player?.clubName ?? "")\n\(goal ?? "") Goals\nPL Debut \((player?.debut.dateFromUTC()?.dateString(DATE_FORMAT))!)"
            
        }else{
            self.nameLbl.text =
            "\(player?.name ?? "")\n\(player?.clubName ?? "")\n\(goal ?? "") Goals"
        }
        
        self.imgView?.normalLoad((player?.profileImage)!)
        self.clubImageView?.normalLoad((player?.clubImage)!)
        self.priceBtn.setTitle("RM \((player?.price)!)", for: .normal)
    }
    
    // use to navigate back screen
    @objc func backBtnAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FKTitleDetailCellHeader") as! FKTitleDetailCell
            cell.titleLbl?.text = "PERSONAL DETAILS"
            return cell
        }
        
        return UIView.init()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (section == 1) ? self.dataSourceArray.count : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FKTitleDetailCell") as! FKTitleDetailCell
            
            let listInfo = self.dataSourceArray[indexPath.row]
            cell.titleLbl?.text = listInfo.name
            cell.detailLbl?.text = listInfo.detail
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FKTitleDetailCellSubscription") as! FKTitleDetailCell
            cell.indexButton?.setTitle("ADD BUDDY", for: .normal)
            cell.indexButton?.addTarget(self, action: #selector(subscribeBtnAction), for: .touchUpInside)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (indexPath.section == 1) ? 40 : 60
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return (section == 1) ? 40 : 0.1
        
    }
    
    //MARK: Subscribe button action
    @objc func subscribeBtnAction() {
        
        AlertController.alert(title: "Confirmation!", message: "Are you sure you want to subscribe?", buttons: ["No", "Yes"]) { (action , index) in
            if index == 1 {
                
                  let playerPrice =   Double("\(self.player?.price ?? "0")")
                if playerPrice! > 0.0 {
                    self.checkCreditBalence()
                }else {
                    self.subscribePlayerApiCall(transactionId: (self.player?.id)!)
                }
            }
        }
    }
    
    func checkCreditBalence() {
        
        if defaults.object(forKey: SENZPAY_USER_ID) != nil{
            apiCallToGetCredit()
        }else{
            self.apiCallForLogin()
        }
    }
    
    func apiCallForLogin()  {
        
        let fkWalletStoryboard = UIStoryboard(name: "FKWallet", bundle: nil)
        let fkWalletLogin = fkWalletStoryboard.instantiateViewController(withIdentifier: "FKWalletLoginVC") as! FKWalletLoginVC
        let navController = UINavigationController(rootViewController: fkWalletLogin)
        self.present(navController, animated: true, completion: nil)
        fkWalletLogin.completionBlock { (success, response) in
            
            if success! {
            let status      = response.validatedValue(SENZPAY_STATUS, expected:"" as AnyObject ) as! String
            if status == "false" {
                // false case error messsage
                MessageView.showMessage(message: response["msg"] as! String, time: 5.0, verticalAlignment: .bottom)
            } else {
                // handle true response
                
                let availbleBalance = response[SENZPAY_BALANCE] as? String
                
                if ((Double("\(availbleBalance ?? "0")")!).isLess(than: Double("\(self.player?.price ?? "0")")!)) {
                    //  Not sufficient balance
                    MessageView.showMessage(message: "You have not enough balance in your FKWallet, Please recharge your FKWallet to continue..", time: 5.0, verticalAlignment: .bottom)
                    
                }else{
                    self.apiCallForPayment()
                }
                
            }
            }
        }
    }
    
    
    func apiCallToGetCredit() {
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        MBProgressHUD.showAdded(to: appDel.window!, animated: true)
        
        var params = Dictionary<String,Any>()
        let email =  AESCrypto.aes256DecryptString(defaults.object(forKey: SENZPAY_EMAIL_DEFAULT) as! String, withKey: crypto256Key)
        let password =  AESCrypto.aes256DecryptString(defaults.object(forKey: SENZPAY_PASSWORD_DEFAULT) as! String, withKey: crypto256Key)
        
        params[SENZPAY_EMAIL] = email!
        params[SENZPAY_PASSWORD] = password!
        
        FKSenzPayHelper.senzpayRequest(params: params, method: .get, apiName: "user_login.php") { (result, error, responseCode) in
            MBProgressHUD.hide(for: appDel.window!, animated: true)
            let responseData = result as! Dictionary<String,AnyObject>
            let status      = responseData.validatedValue(SENZPAY_STATUS, expected:"" as AnyObject ) as! String
            if status == "false" {
                // false case error messsage
                MessageView.showMessage(message: responseData["msg"] as! String, time: 5.0, verticalAlignment: .bottom)
            } else {
                // handle true response
                
                let userID = responseData.validatedValue(SENZPAY_USER_ID, expected:"" as AnyObject ) as! String
                
                defaults.set(AESCrypto.aes256EncryptString(userID, withKey: crypto256Key), forKey: SENZPAY_USER_ID)
                
                let availbleBalance = responseData[SENZPAY_BALANCE] as? String
                
                if ((Double("\(availbleBalance ?? "0")")!).isLess(than: Double("\(self.player?.price ?? "")")!)) {
                    //  Not sufficient balance
                    MessageView.showMessage(message: "You have not enough balance in your FKWallet, Please recharge your FKWallet to continue..", time: 5.0, verticalAlignment: .bottom)
                    
                }else{
                    self.apiCallForPayment()
                }
            }
        }
    }
    
    func apiCallForPayment()  {
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        MBProgressHUD.showAdded(to: appDel.window!, animated: true)
        var params = Dictionary<String,Any>()
        let email =  AESCrypto.aes256DecryptString(defaults.object(forKey: SENZPAY_EMAIL_DEFAULT) as! String, withKey: crypto256Key)
        let password =  AESCrypto.aes256DecryptString(defaults.object(forKey: SENZPAY_PASSWORD_DEFAULT) as! String, withKey: crypto256Key)
        let userID =  AESCrypto.aes256DecryptString(defaults.object(forKey: SENZPAY_USER_ID) as! String, withKey: crypto256Key)
        
        params[SENZPAY_EMAIL] = email!
        params[SENZPAY_PASSWORD] = password!
        params[SENZPAY_USER_ID] = userID!
        params["amt"] = player?.price
        params["item_code"] = player?.id
        params["item_desc"] = player?.name
        params ["toemail"] = "hi@senzpay.asia"
        params ["touid"] = "2"
        params[SENZPAY_SKIP_SMS_ALERT] = "1"  //0 – To send the SMS. 1 – To Skip.
        
        FKSenzPayHelper.senzpayRequest(params: params, method: .post, apiName: "user_make_payment.php") { (result, error, responseCode) in
            MBProgressHUD.hide(for: appDel.window!, animated: true)
            let responseData = result as! Dictionary<String,AnyObject>
            
            let status        = responseData.validatedValue(SENZPAY_STATUS, expected: 0 as NSNumber) as! Bool
            
            if !status {
                // false case error messsage
                MessageView.showMessage(message: responseData["msg"] as! String, time: 5.0, verticalAlignment: .bottom)
            } else {
                // handle true response
                MessageView.showMessage(message: responseData["msg"] as! String, time: 5.0, verticalAlignment: .bottom)
                self.subscribePlayerApiCall(transactionId: responseData.validatedValue("payment_id", expected: "" as AnyObject) as! String)
            }
        }
    }
    
    func subscribePlayerApiCall(transactionId:String) {
        
        var params = Dictionary<String,Any>()
        var player = Dictionary<String, Any>()
        player["id"] = self.player?.id
        player["transaction_id"]  = transactionId
        params["player"] = player
        let appDel = UIApplication.shared.delegate as! AppDelegate
        MBProgressHUD.showAdded(to: appDel.window!, animated: true)
        
        ServiceHelper.request(params: params, method: .post, apiName: "players/add_buddy") { (result, error, responseCode) in
            MBProgressHUD.hide(for: appDel.window!, animated: true)
            
            if error == nil {
                if responseCode == 200{
                    self.perform(#selector(self.navigateToMyBuddy), with: nil, afterDelay: 0.10)
                }
                let message = result![kResponseMsg] as! String
                MessageView.showMessage(message: message, time: 3.0, verticalAlignment: .bottom)
            }
        }
    }
    
    @objc func navigateToMyBuddy(){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: REFERESH_PLAYER), object: nil)
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
