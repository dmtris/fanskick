//
//  FKStoryVC.swift
//  FansKick
//
//  Created by FansKick-Sunil on 21/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit
import WebKit

class FKStoryVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, WKNavigationDelegate {

    @IBOutlet weak var collectionView:UICollectionView?
    
    var dataSourceArray = [FKStoryInfo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        //let value = UIInterfaceOrientation.portrait.rawValue
       // UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    
   func  updateData(array:Array<FKStoryInfo>){
    
    dataSourceArray = array
    self.collectionView?.reloadData()
    }
    
    //MARK:  UICollectionView Overrides
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSourceArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FKBuddyStoryCell", for: indexPath) as! FKBuddyStoryCell
      // cell.imgView?.image = UIImage.init(named: "placeholder")
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        let storyDetail = self.storyboard?.instantiateViewController(withIdentifier: "storyDetailVC") as! FKStoryDetailVC
        
        self.navigationController?.pushViewController(storyDetail, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: self.view.frame.width/3 - 10, height:  self.view.frame.width/3 - 10)
    }
    
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
