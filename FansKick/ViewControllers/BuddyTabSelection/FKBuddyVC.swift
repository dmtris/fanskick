//
//  FKBuddyViewController.swift
//  FansKick
//
//  Created by FansKick-Sunil on 17/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

let REFERESH_PLAYER = "REFERESH_PLAYER"

class FKBuddyVC: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var addBuddyView: UIView!
    @IBOutlet weak var buddyView: UIView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    var refereshControl = UIRefreshControl()
    
    var page = PAGE()
    var dataSourceArray = [FKPlayerInfo]()
    
    var isFirst:Bool = true  // this needed to remove
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "MY BUDDY"
        
        page.startIndex = 1
        page.pageSize = 10
        
        self.tblView.tableFooterView = UIView.init()
        let appDel = UIApplication.shared.delegate as! AppDelegate
        
        appDel.setupNavigationBar(navigationBar: self.navigationController!)
        
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]

        refereshControl.tintColor = GREEN_COLOR
        refereshControl.addTarget(self, action: #selector(refereshControlAction), for: .valueChanged)
        tblView.addSubview(refereshControl)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(myBuddyApiCall), name: NSNotification.Name(rawValue: REFERESH_PLAYER), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerOnlineNotification), name: NSNotification.Name(rawValue: PLAYER_ONLINE_NOTIFICATION), object: nil)

        self.perform(#selector(myBuddyApiCall), with: nil, afterDelay: 0.1)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // myBuddyApiCall()
    }

    // use to navigate back screen
    @objc func backBtnAction(){
        
        self.navigationController?.popViewController(animated: true)
    }

    
    @objc func playerOnlineNotification(noti:NSNotification){
        
        let params = noti.object as! Dictionary<String,Any>
        let playerID = params["player_id"] as! String
        
        for player in self.dataSourceArray {
            if player.id == playerID {
                player.isOnline = true
            }
        }
        
        tblView.reloadData()
        
    }
    @objc func  refereshControlAction() {
        page.startIndex = 1
        myBuddyApiCall()
    }
    
    func updateView() {
        
        addBuddyView.isHidden = !self.isFirst
        buddyView.isHidden = self.isFirst
        
        if !self.isFirst {
            self.navigationItem.rightBarButtonItem =   UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.add, target: self, action: #selector(addBuddyAction))
            
        } else {
            self.navigationItem.rightBarButtonItem =   nil
        }
    }
    
    @IBAction func addBuddyAction() {
        
        isFirst = true
        let stoaryboard = UIStoryboard.init(name: "BuddySection", bundle: nil)
        
        let selectBuddyVC = stoaryboard.instantiateViewController(withIdentifier: "selectBuddyVC") as! FKSelectBuddyVC
        self.navigationController?.pushViewController(selectBuddyVC, animated: true)
    }
    
    @objc func myBuddyApiCall() {
        
        var params = Dictionary<String,Any>()
        
        params ["search"] = searchBar.text?.trimWhiteSpace
        params["page"] = "\(page.startIndex)"
        params["per_page"] = "\(page.pageSize)"
        MBProgressHUD.hide(for: self.view, animated: true)
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        ServiceHelper.request(params: params, method: .post, apiName: "players/buddy_list") { (result, error, responseCode) in
            
            self.refereshControl.endRefreshing()
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                
                if let data = results["buddy_list"] {
                    if self.page.startIndex == 1{
                        self.dataSourceArray.removeAll()
                    }
                    let list =  FKPlayerInfo.playerList(data: data as! Array<Dictionary<String, AnyObject>>)
                    self.dataSourceArray.append(contentsOf: list)
                    
                    if !self.isFirst{
                        if self.dataSourceArray.isEmpty{
                            MessageView.showMessage(message: "No player found, please change filter criteria.", time: 5.0, verticalAlignment: .bottom)
                        } else {
                            self.page.totalPage = result!["total_pages"] as! Int
                        }
                    }
                }else{
                    
                    self.dataSourceArray.removeAll()
                    
                    MessageView.showMessage(message: "No player found, please change filter criteria.", time: 5.0, verticalAlignment: .bottom)

                }
                
                if self.dataSourceArray.count > 0{
                    self.isFirst = false
                }
                self.updateView()
                self.tblView.reloadData()
            }else{
                self.updateView()
                self.tblView.reloadData()
                
            }
        }
    }
    
    //MARK: UITableView overrides
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSourceArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FKBuddyCell") as! FKBuddyCell
        let player = self.dataSourceArray[indexPath.row]
        cell.nameLbl?.text = player.name
        cell.clubNameLbl?.text = player.clubName
        cell.clubImgView?.normalLoad(player.clubImage)
        cell.rattingBar?.value = CGFloat(Int(player.rating)!)
        cell.rattingBar?.ratingValueChange()
        cell.connectBtn?.indexPath = indexPath
        
        cell.onlineLbl?.isHidden = true
        if !player.isOnline {
            cell.onlineLbl?.backgroundColor = UIColor.clear
            cell.connectBtn?.backgroundColor = UIColor.lightGray
        } else {
            cell.connectBtn?.backgroundColor = GREEN_COLOR
            cell.onlineLbl?.backgroundColor = GREEN_COLOR
        }
        
        cell.imgView?.normalLoad(player.profileImage)
        cell.connectBtn?.addTarget(self, action: #selector(connectBtnAction), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let stoaryboard = UIStoryboard.init(name: "BuddySection", bundle: nil)
        let buddyDetailVC = stoaryboard.instantiateViewController(withIdentifier: "buddyDetailVC") as! FKBuddyDetailVC
        buddyDetailVC.player = self.dataSourceArray[indexPath.row]
        self.navigationController?.pushViewController(buddyDetailVC, animated: true)
    }
    
    //MARK: UISegmentControll Overrides
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        searchBar.setShowsCancelButton(true, animated: true)
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        page.startIndex = 1
        searchBar.text = ""
        myBuddyApiCall()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if !(searchBar.text?.trimWhiteSpace.isEmpty)! {
            page.startIndex = 1
            myBuddyApiCall()
        }
    }
    
    @objc func connectBtnAction(sender:IndexPathButton) {
        
        let player = self.dataSourceArray[(sender.indexPath?.row)!]
        if player.isOnline {
            let stoaryboard = UIStoryboard.init(name: "BuddySection", bundle: nil)
            let  liveVideoVC = stoaryboard.instantiateViewController(withIdentifier: "liveVideoVC") as! FKLiveVideoVC
            liveVideoVC.player = player
            self.present(liveVideoVC, animated: true) {
            }
        } else {
            MessageView.showMessage(message: "Player is not available right now.", time: 5.0, verticalAlignment: .bottom)
        }
    }
    
    //MARK: ScrollView overrdes
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        // UITableView only moves in one direction, y axis
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        // Change 10.0 to adjust the distance from bottom
        if maximumOffset - currentOffset <= 10.0 {
            
            if page.totalPage > page.startIndex{
                page.startIndex = page.startIndex+1;
                self.myBuddyApiCall()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
