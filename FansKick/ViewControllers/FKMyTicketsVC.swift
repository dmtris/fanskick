//
//  FKMyTicketsVC.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 10/24/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit
import CoreData
class FKMyTicketsVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblView:UITableView?
    
    @IBOutlet weak var noTicketLabel: UILabel!
    var dataSourceArray = [NSManagedObject]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "MY TICKETS"
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        
        tblView?.tableFooterView = UIView.init()
        noTicketLabel.isHidden = true

        self.fetchRecordFromDB()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
    }
    
    func fetchRecordFromDB() {
        
        
        
        let currentUser = FKCoreDBHelper.coreDBHelper.getCurrentUser()

        let array = currentUser?.userToTicket?.allObjects as! Array<NSManagedObject>
        
        dataSourceArray =  array
        self.dataSourceArray.sort(by: { (obj1, obj2) -> Bool in
            let item1 = obj1 as! FKTicket
            let item2 = obj2 as! FKTicket
            return item1.bookingDate! > item2.bookingDate!
        })
        tblView?.reloadData()
        if dataSourceArray.count == 0 {
            noTicketLabel.isHidden = false
        }

//
//        let managedContext =   FKCoreDBHelper.coreDBHelper.persistentContainer.viewContext
//
//        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FKTicket")
//        let sortDescriptor = NSSortDescriptor(key: "bookingDate", ascending: false)
//        fetchRequest.sortDescriptors = [sortDescriptor]
//
//        do {
//            let   tickets  = try managedContext.fetch(fetchRequest)
//            dataSourceArray = tickets
//
//            if dataSourceArray.count == 0 {
//                noTicketLabel.isHidden = false
//            }
//
//            print(">>>>>>>>>>>  ",tickets)
//
//            tblView?.reloadData()
//
//        } catch let error as NSError {
//            print("Could not fetch. \(error), \(error.userInfo)")
//        }
    }
    
    @objc func backBtnAction() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSourceArray.count
    }
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FKMyTicketsCell", for: indexPath) as! FKMyTicketsCell
        let ticket = self.dataSourceArray[indexPath.row] as! FKTicket
        cell.opponentTeamName?.text = ticket.apponentName
        cell.bookingTime?.text  = ticket.bookingDate?.dateFromString("yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSS'Z")?.dateString(DATE_TIME_FORMAT)

        //ticket.bookingDate!.dateFromUTC()!.dateString(DATE_TIME_FORMAT)
        cell.fixtureDate?.text  = ticket.matchDate!.dateFromUTC()!.dateString(DATE_TIME_FORMAT)
        cell.ticketsCount?.text = "\(ticket.numberOfTickets!) tickets"
        cell.stadiumName?.text  = ticket.stadiumName
        cell.teamName1?.text    = ticket.teamName
        cell.totalPrice?.text   = "RM \(ticket.totalPrice!)"

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        let ticket = self.dataSourceArray[indexPath.row] as! FKTicket
        let buyTicketVC = self.storyboard?.instantiateViewController(withIdentifier: "FKTicketDetailVC") as! FKTicketDetailVC
        buyTicketVC.ticketInfo = ticket
        self.navigationController?.pushViewController(buyTicketVC, animated: true)
    }
    
    

}
