//
//  FKMyTicketsCell.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 10/24/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKMyTicketsCell: UITableViewCell {

    @IBOutlet weak var teamName1: UILabel!
    @IBOutlet weak var teamLogo: UIImageView!
    @IBOutlet weak var opponentTeamLogo: UIImageView!
    @IBOutlet weak var opponentTeamName: UILabel!
    @IBOutlet weak var stadiumName: UILabel!
    @IBOutlet weak var fixtureDate: UILabel!
    @IBOutlet weak var ticketsCount: UILabel!
    @IBOutlet weak var bookingTime: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
