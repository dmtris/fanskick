//
//  FKTicketDetailVC.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 10/24/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKTicketDetailVC: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var matchName:UILabel?
    @IBOutlet weak var matchDetail:UILabel?
    var ticketInfo : FKTicket?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationItem.title = "TICKET DETAIL"
        
        matchName?.text = "JDT vs Pahang"
        matchDetail?.text = "Stadium Larkin Johor \n15 Dec 11:00 PM \n2Tickets P2"
        
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        
        self.navigationItem.rightBarButtonItem =   UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.action, target: self, action: #selector(shareBtnAction))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //let value = UIInterfaceOrientation.portrait.rawValue
       // UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
    }
    
    @objc func shareBtnAction() {
        
        let img =  UIImage.init(data: ticketInfo!.qrCode!)
        let imageToShare = [img as Any]
        FKShareHelper.share.openActivity(options: imageToShare , controller: self)
    }
    
    // use to navigate back screen
    @IBAction func backBtnAction() {
        
            var viewController :UIViewController?
            for obj in (self.navigationController?.viewControllers)! {
                if  obj is FKTicketsDashboardVC{
                    viewController = obj
                    break
                }
            }
            
            if viewController != nil {
                self.navigationController?.popToViewController(viewController!, animated: true)
            }else{
                self.navigationController?.popViewController(animated: true)

            }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    @objc func viewAllMatches() {
        print("view all")
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FKMyTicketsCell", for: indexPath) as! FKMyTicketsCell
            cell.opponentTeamName?.text = ticketInfo?.apponentName
            cell.bookingTime?.text  = ticketInfo?.bookingDate!.dateFromUTC()!.dateString(DATE_TIME_FORMAT)
            cell.fixtureDate?.text  = ticketInfo?.matchDate!.dateFromUTC()!.dateString(DATE_TIME_FORMAT)
            cell.ticketsCount?.text = "\(ticketInfo!.numberOfTickets!) tickets"
            cell.stadiumName?.text  = ticketInfo?.stadiumName
            cell.teamName1?.text    = ticketInfo?.teamName
            cell.totalPrice?.text   = "RM \(ticketInfo!.totalPrice!)"
            return cell
        } else {
            let qrCodeCell = tableView.dequeueReusableCell(withIdentifier: "FKTicketDetailCell", for: indexPath) as! FKTicketDetailCell
           
            let img =  UIImage.init(data: ticketInfo!.qrCode!)
            qrCodeCell.qrImageView?.image  = img
            return qrCodeCell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 150
        } else {
            return 240
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
//        let storyboard = UIStoryboard.init(name: "TabController", bundle: nil)
//        self.navigationController?.pushViewController(storyboard.instantiateViewController(withIdentifier: "FKFixtureDetailViewController"), animated: true)
    }
    
}
