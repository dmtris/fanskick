//
//  FKViewAllFixtureCell.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 10/23/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKViewAllFixtureCell: UITableViewCell {

    @IBOutlet weak var viewAllBtn: UIButton!
    @IBOutlet weak var viewAllMatcheBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
