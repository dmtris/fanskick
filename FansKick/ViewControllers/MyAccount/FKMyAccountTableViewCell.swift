//
//  FKMyAccountTableViewCell.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 11/9/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKMyAccountTableViewCell: UITableViewCell {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var iconBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
