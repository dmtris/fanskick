//
//  FKChangePasswordVC.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 11/7/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKChangePasswordVC: UIViewController,UITableViewDelegate,UITableViewDataSource,FKTextFieldDelegate {

    @IBOutlet weak var tableView: UITableView!

    @IBOutlet var  toolBar:UIToolbar!
    
    var placeholderTextArr = [String]()
    
    var indexPath:IndexPath?
    var stateArr = [String]()
    var stateArray = [FKListInfo]()
    let fkUser = FKUser()

    var selectedDate = Date()
    var selectedStateId = String()
    var errorTag = 50
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        placeholderTextArr = ["Old Password",
                              "New Password",
                              "Confirm Password"]
        
        self.navigationItem.title = "CHANGE PASSWORD"
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
    }
    override func viewWillAppear(_ animated: Bool) {
        //let value = UIInterfaceOrientation.portrait.rawValue
       // UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        tableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnAction(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.placeholderTextArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:FKInputTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FKInputTableViewCell", for: indexPath) as! FKInputTableViewCell
        cell.textField.setFKPlaceHolder(placeHolder: self.placeholderTextArr[indexPath.row])
        
        cell.textField.leftPadingFontIcon(iconName: "6")
        cell.textField.delegate = self
        cell.textField.setFKReturnKeyType(returnKeyType: .next)
        cell.textField.indexPath = indexPath
        cell.textField.setFKSecureTextEntry(secureTextEntry: false)
        cell.textField.setFKKeyboardType(keyboardType: .asciiCapable)

        if indexPath.row == 0 {
            cell.textField.text = fkUser.oldPassword
            cell.textField.setFKSecureTextEntry(secureTextEntry: true)
            
        } else if indexPath.row == 1{
            
            cell.textField.text = fkUser.password
            cell.textField.setFKSecureTextEntry(secureTextEntry: true)
            
        } else if indexPath.row == 2{
            cell.textField.text = fkUser.confrmPassword
            cell.textField.setFKSecureTextEntry(secureTextEntry: true)
            cell.textField.setFKReturnKeyType(returnKeyType: .default)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 70
    }
    
    @objc func fkTextFieldShouldReturn(textField: FKTextFieldView) -> Bool
    {
        
        if textField.FKReturnKeyType() == .next{
            
            let cell = tableView.cellForRow(at: IndexPath.init(row: (textField.indexPath?.row)! + 1, section: 0)) as! FKInputTableViewCell
            cell.textField.becomeFKFirstResponder()
        }else{
            textField.resignFKFirstResponder()
        }
        
        return true
    }
    @objc func fkTextFieldDidEndEditing(textField:FKTextFieldView)
    {
        
        if textField.indexPath?.row == 0 {
            fkUser.oldPassword = textField.text
        } else if textField.indexPath?.row == 1{
            fkUser.password = textField.text
        } else if textField.indexPath?.row == 2{
            fkUser.confrmPassword = textField.text
        }
    }
    @objc  func textField(textField: FKTextFieldView, fkShouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        return true
    }
    // MARK:- Submit Button
    @IBAction func submitBtn(_ sender: Any) {
        self.view.endEditing(true)
        
        if isAllFieldVerified(){
            
            callApiToChangePassword()
            
            //            let dashboardVC = self.storyboard?.instantiateViewController(withIdentifier: "FKOtpViewController") as! FKOtpViewController
            //            dashboardVC.enteredEmail = fkUser.mobileNo
            //            self.navigationController!.pushViewController(dashboardVC, animated: true)
        }
    }
    // MARK:- Api to Change password
    private func callApiToChangePassword() {
        
        var params = Dictionary<String, Any>()
        
        var user  = Dictionary<String, Any>()
        
        user["password"] = fkUser.oldPassword
        user["new_password"] = fkUser.password
        
        params ["user"] = user
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        ServiceHelper.request(params: params, method: .post, apiName: "passwords/change_password", hudType: loadingIndicatorType.withoutLoader) { (result, error, responseCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if (error == nil) {
                if let data = result![kResponseMsg]  {
                    
                    MessageView.showMessage(message: data as! String, time: 5.0, verticalAlignment: .bottom)
                    self.perform(#selector(self.backBtnAction(_:)), with: nil, afterDelay: 1.0)
                }
            }
        }
    }
    
    func isAllFieldVerified() -> Bool{
        var isVerified = false
        
        if fkUser.oldPassword.isEmpty {
            let cell = tableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as! FKInputTableViewCell
            cell.textField.setFKErrorMessage(message: "Please enter old paasword")
        }else if fkUser.password.isEmpty{
            let cell = tableView.cellForRow(at: IndexPath.init(row: 1, section: 0)) as! FKInputTableViewCell
            cell.textField.setFKErrorMessage(message: "Please enter password")
        }else if fkUser.password.trimWhiteSpace.length < 6{
            let cell = tableView.cellForRow(at: IndexPath.init(row: 1, section: 0)) as! FKInputTableViewCell
            cell.textField.setFKErrorMessage(message: "Please should be at least 6 characters")
        } else if fkUser.password.trimWhiteSpace != fkUser.confrmPassword.trimWhiteSpace{
            MessageView.showMessage(message: "Password fields mismatched", time: 5.0, verticalAlignment: .bottom)

        }else{
            isVerified = true
        }
        return isVerified
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
