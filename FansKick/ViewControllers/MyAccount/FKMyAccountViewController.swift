//
//  FKMyAccountViewController.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 11/9/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKMyAccountViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var cameraBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var  toolBar:UIToolbar!
    
    @IBOutlet weak var profileImgBtn: UIButton!
  
    
    var indexPath:IndexPath?
    var stateArr = [String]()
    var stateArray = [FKListInfo]()
    var fkUser = FKUser()
    var stateId = ""
    var selectedDate : Date?
    var selectedStateId = String()
    var errorTag = 50
    var isEditable = false
    var base64Image = ""
    let  placeholderTextArr = ["Name",
                               "Email",
                               "DD/MM/YYYY",
                               "State",
                               "Address"]
    let  fontIconArray = ["a",//name
        "t",//email
        "9",//dob
        "O",//state
        "y"]//address

    var captureData:Data? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        
        self.navigationItem.title = "MY ACCOUNT"
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        
        let saveBtn = UIButton(type: .custom)
        saveBtn.titleLabel?.font = UIFont.init(name: "fanskick-font2-icon-set", size: 22)
        saveBtn.setTitle("E", for: .normal)

       // saveBtn.setImage(UIImage(named: "edit_icon"), for: .normal)
        saveBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        saveBtn.tag = 5
        saveBtn.addTarget(self, action: #selector(saveProfile), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: saveBtn)
        
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        self.navigationItem.rightBarButtonItems = [item1]
        
        tableView.isUserInteractionEnabled = false
        tableView.tableFooterView = UIView.init()
        callApiToGetStates()
        callApiToGetProfile()
         
    }
    
    @objc func saveProfile(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if isEditable {
            if isAllFieldVerified() {
                isEditable = false
               // sender.setImage(UIImage(named:"edit_icon"), for: .normal)

                sender.setTitle("E", for: .normal)
                tableView.isUserInteractionEnabled = false
                callApiToUpdateProfile()
            }
        } else {
            isEditable = true
            tableView.isUserInteractionEnabled = true
          //  sender.setImage(UIImage(named:"save_icon"), for: .normal)
           

            sender.setTitle("D", for: .normal)

        }
        
    }
    
    func callApiToGetStates() {
        
        let params = Dictionary<String,Any>()
        
        ServiceHelper.request(params: params, method: .get, apiName: "users/state_list", hudType: loadingIndicatorType.withoutLoader) { (result, error, responseCode) in
            
            if (error == nil) {
                if responseCode == 200{
                    if let data = result!["state_list"]{
                        self.stateArray.append(contentsOf: FKListInfo.list(data: data as! Array<Dictionary<String, AnyObject>>))
                        for state in self.stateArray{
                            self.stateArr.append(state.name)
                        }
                    } else {
                        if let data = result![kResponseMsg]  {
                            MessageView.showMessage(message: data as! String, time: 5.0, verticalAlignment: .bottom)
                        }
                    }
                } else {
                }
            }
        }
    }
    
    func callApiToGetProfile() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let params = Dictionary<String,Any>()
        ServiceHelper.request(params: params, method: .get, apiName: "users/view_profile", hudType: loadingIndicatorType.withoutLoader) { (result, error, responseCode) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if responseCode == 200 {
                let results = result as! Dictionary<String, AnyObject>
                
                if let data = results["user"] {
                   
                    self.fkUser = FKUser.user(data as? Dictionary<String, AnyObject>)
                    self.profileImgBtn.normalLoad(self.fkUser.imgUrl)
                    
                    self.tableView.reloadData()
                    
                } else {
                    if let data = result![kResponseMsg]  {
                        MessageView.showMessage(message: data as! String, time: 5.0, verticalAlignment: .bottom)
                    }
                }
            } else {
            }
        }
    }
    
    func callApiToUpdateProfile() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let userDict = NSMutableDictionary()

        if !fkUser.name.isEmpty
        {
            userDict ["name"] = fkUser.name
        }

        if !fkUser.email.isEmpty
        {
            userDict ["email"] = fkUser.email
        }

        if !fkUser.dob.isEmpty{
            userDict ["dob"] = fkUser.dob
        }

        if !fkUser.stateId.isEmpty{
            userDict ["state_id"] = fkUser.stateId
        }

        if !fkUser.address.isEmpty{
            userDict ["address"] = fkUser.address
        }
        
        if captureData != nil{
            userDict ["image"] = captureData
        }
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        
        if !appDel.isReachable {
            
            MessageView.showMessage(message: NO_INTERNET_CONNECTION, time: 5.0, verticalAlignment: .bottom)
            return
        }
        
        if isAllFieldVerified(){
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            HSServiceHelper.shared().multipartApiCall(withParameter: userDict, methodtype: "PUT", apiName: "users/edit_profile.json") { (result, error) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if error == nil{
                    MessageView.showMessage(message: "Your profile updated successfully", time: 4.0, verticalAlignment: .bottom)
                }
            }
        }
        
        
//        let data  = UIImageJPEGRepresentation((self.profileImgBtn.currentImage)!, 0.01)! as NSData
//     //   let data =    UIImagePNGRepresentation(self.profileImgBtn.currentImage!) as NSData?
//
////        let imageArr = ["image":data] as Dictionary<String,AnyObject>
//
//        var imageData = Dictionary<String,AnyObject>()
//            imageData["data"] = data
//        imageData ["fileType"] = "image" as AnyObject
//        imageData ["keyAtServerSide"] = "image" as AnyObject
//
//
//
//        ServiceHelper.multiPartRequest(params: userDict, method: .put, apiName: "users/edit_profile", hudType: loadingIndicatorType.withoutLoader, mediaArray: [imageData], isUsingFilePathUpload: false) { (result, error, responseCode) in
//            MBProgressHUD.hide(for: self.view, animated: true)
//
//            if (error == nil) {
//                if responseCode == 200 {
//                    if let data = result!["user"] {
//
//                        if let data = result![kResponseMsg]  {
//                            MessageView.showMessage(message: data as! String, time: 5.0)
//                        }
//                    } else {
//                        if let data = result![kResponseMsg]  {
//                            MessageView.showMessage(message: data as! String, time: 5.0)
//                        }
//                    }
//                } else {
//                }
//            }
//        }
    }
    
    @IBAction func profileImgBtn(_ sender: UIButton) {
        openActionSheet()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnAction(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    //MARK:- UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.placeholderTextArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FKMyAccountTableViewCell", for: indexPath) as! FKMyAccountTableViewCell
        
        cell.textField.delegate = self
        cell.textField.returnKeyType = .next
        cell.textField.keyboardType = .asciiCapable
        cell.titleLbl.text = placeholderTextArr[indexPath.row]
        cell.textField.attributedPlaceholder = NSAttributedString(string: placeholderTextArr[indexPath.row],
                                                               attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        cell.textField.tag = indexPath.row
        
        cell.iconBtn.setTitle(fontIconArray[indexPath.row], for: .normal)
        
        cell.textField.autocapitalizationType = .none
        switch indexPath.row {
        case 0:
            cell.textField.text = fkUser.name
            cell.textField.autocapitalizationType = .words

            break
        case 1:
            cell.textField.text = fkUser.email
            break
        case 2:
            cell.titleLbl.text = "Date Of Birth"
            cell.textField.text = fkUser.dob.dateFromUTC()?.dateString(DATE_FORMAT)
            break
        case 3:
            cell.textField.text = fkUser.state
            break
        case 4:
            cell.textField.text = fkUser.address
            cell.textField.returnKeyType = .done
            break
        default:
            break
        }
        

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField.returnKeyType == .next {
            let cell = tableView.cellForRow(at: IndexPath.init(row: (textField.tag) + 1, section: 0)) as! FKMyAccountTableViewCell
            cell.textField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    @objc func fkTextFieldShouldReturn(textField: FKTextFieldView) -> Bool
    {
        if textField.FKReturnKeyType() == .next{
            
            let cell = tableView.cellForRow(at: IndexPath.init(row: (textField.indexPath?.row)! + 1, section: 0)) as! FKInputTableViewCell
            cell.textField.becomeFKFirstResponder()
        } else {
            textField.resignFKFirstResponder()
        }
        return true
    }
    
    func datePicker(_ textField:UITextField) {
        
        let minDate = Date().dateBySubtractingYears(120)
        let preDate = selectedDate == nil ? minDate : selectedDate
        
        RPicker.selectDate(selectedDate: preDate, minDate: minDate, maxDate: Date()) { (date: Date) in
            self.selectedDate = date
            self.fkUser.dob =   date.UTCStringFromDate()!
            textField.text = date.dateString(DATE_FORMAT)
        }
    }
    
    func selectStates(_ textField:UITextField) {
        RPicker.sharedInstance.selectOption(title: "Stats", dataArray: self.stateArr, selectedIndex: 0) { (selectedText, index) in
            self.fkUser.state = selectedText
            let fkList = self.stateArray[index]
            self.fkUser.stateId = fkList.id
            textField.text = selectedText
            
            self.tableView.reloadData()
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField.tag == 2 {
            self.view.endEditing(true)
            datePicker(textField)
            return false
        } else if textField.tag == 3 {
            self.view.endEditing(true)
            selectStates(textField)
            return false
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if textField.tag == 0 {
            fkUser.name = textField.text!
        } else if textField.tag == 1{
            fkUser.email = textField.text!
        } else if textField.tag == 2{
            fkUser.dob = textField.text!
        } else if textField.tag == 3{
            fkUser.state = textField.text!
        } else if textField.tag == 4{
            fkUser.address = textField.text!
        }
    }
    
    @objc func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)

        if textField.tag == 0 || textField.tag == 1{
            if !string.isEmpty && newString.length > 60{
                return false
            }
        }
        
        return true
    }
    
    // MARK:- Api to Change password
    private func callApiToChangePassword() {
        
        var params = Dictionary<String, Any>()
        
        var user  = Dictionary<String, Any>()
        
        user["password"] = fkUser.oldPassword
        user["new_password"] = fkUser.password
        
        params ["user"] = user
        MBProgressHUD.showAdded(to: self.view, animated: true)

        ServiceHelper.request(params: params, method: .post, apiName: "passwords/change_password", hudType: loadingIndicatorType.withoutLoader) { (result, error, responseCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if (error == nil) {
                if let data = result![kResponseMsg]  {
                    MessageView.showMessage(message: data as! String, time: 5.0, verticalAlignment: .bottom)
                }
            }
        }
    }
    
    func isAllFieldVerified() -> Bool{
        var isVerified = false
        
        if fkUser.name.isEmpty {
            MessageView.showMessage(message: "Please enter name", time: 5.0, verticalAlignment: .bottom)
        } else if !fkUser.email.isEmpty {
            if !fkUser.email.isValidEmail() {
                MessageView.showMessage(message: "Please enter valid email", time: 5.0, verticalAlignment: .bottom)

            } else if fkUser.state.isEmpty {
                MessageView.showMessage(message: "Please enter state", time: 5.0, verticalAlignment: .bottom)

            }else{
                isVerified = true
            }
        } else if fkUser.state.isEmpty {
            MessageView.showMessage(message: "Please enter state", time: 5.0, verticalAlignment: .bottom)
        }
        else {
            isVerified = true
        }
        return isVerified
    }
    
    func openActionSheet() {
        //Create the AlertController and add Its action like button in Actionsheet
        
        AlertController.actionSheet(title: "Please select", message: "", sourceView: cameraBtn, buttons: ["Camera","Gallery","Cancel"]) { (action, index) in
            
            if index == 0{
                self.openCamera()

            }else if index == 1{
                self.openGallery()

            }
            print(index)
        }
        
//        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select", message: nil, preferredStyle: .actionSheet)
//
//        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
////            print("Cancel")
//        }
//        actionSheetControllerIOS8.addAction(cancelActionButton)
//
//        let saveActionButton = UIAlertAction(title: "Camera", style: .default)
//        { _ in
////            print("Camera")
//            self.openCamera()
//        }
//        actionSheetControllerIOS8.addAction(saveActionButton)
//
//        let deleteActionButton = UIAlertAction(title: "Gallery", style: .default)
//        { _ in
////            print("Gallery")
//            self.openGallery()
//        }
//        actionSheetControllerIOS8.addAction(deleteActionButton)
////        self.present(actionSheetControllerIOS8, animated: true) {
////
////        }
    }
    
    func openGallery() {
        FKMediaPicker.mediaPicker.pickMediaFromGallery(galleryBlock: { (info: [String : Any], pickedImage: UIImage?) in
            if let pickedImage = pickedImage {
                delay(delay: 0.2) {
                    print(pickedImage.size)
                    self.profileImgBtn.setImage(pickedImage, for: .normal)

                    self.captureData = UIImageJPEGRepresentation(pickedImage, 0.5)

                    //                    self.callAPIToUploadImages([pickedImage], hudType: hudType, completionBlock: { (response: AnyObject?, error: Error?, httpCode: Int) in
                    //                        completionBlock(info, response, error, httpCode)
                    //                    })
                }
            }
        })
    }
    
    func openCamera() {
        FKMediaPicker.mediaPicker.pickMediaFromCamera { (cameraBlock, pickedImage) in
            print(pickedImage ?? "No image picked")
            self.profileImgBtn.setImage(pickedImage, for: .normal)
            
            self.captureData = UIImageJPEGRepresentation(pickedImage!, 0.5)
            
        }
    }
}
