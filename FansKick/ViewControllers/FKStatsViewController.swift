//
//  FKStatsViewController.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 11/8/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit
import CarbonKit

class FKStatsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var refresh: CarbonSwipeRefresh?
    var fixture:FKFixtureListModel?
    var statInfo = FKFixtureStatsInfo()
    var dataSourceArray = [FKPreviousResultInfo]()
    
    var page = PAGE()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        page.startIndex = 1
        page.pageSize = 20
        
        refresh = CarbonSwipeRefresh.init(scrollView: tableView)
        refresh?.colors = [UIColor.blue, UIColor.red, UIColor.orange, UIColor.green]
        view.addSubview(refresh!)
        refresh?.addTarget(self, action: #selector(refreshTable), for: .valueChanged)
        
        tableView.tableFooterView = UIView.init()
        apiCallForStates()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        tableView.reloadData()
    }
    
    @objc func refreshTable() {
        refresh?.endRefreshing()
    }
    
    
    func apiCallForStates(){
        var params          = Dictionary<String,Any>()
        params["page"]      = "\(page.startIndex)"
        params["per_page"]  = "\(page.pageSize)"
        var fixture     = Dictionary<String,Any>()
        fixture["id"] = self.fixture?.id
        params["fixture"] = fixture
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: .post, apiName: "fixtures/fixture_stats", hudType: loadingIndicatorType.withoutLoader) { (result, error, statusCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if statusCode == 200 {
                    self.statInfo = FKFixtureStatsInfo.fixtureState(data: results)
                    self.dataSourceArray = (self.statInfo.previousResult)
                }
               self.tableView.reloadData()
            }
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !(refresh!.superview != nil) {
            view.superview?.addSubview(refresh!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- tableview datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1//return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return 1 // return (section == 1) ? (dataSourceArray.count) : 1
    }
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 55
//    }
//
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
//        let cell  = tableView.dequeueReusableCell(withIdentifier: "FKTitleDetailCell") as! FKTitleDetailCell
//
//        if section == 1{
//            cell.titleLbl?.text = "Previous results"
//        }else{
//            cell.titleLbl?.text = "Head-to-Head"
//
//        }
//        return cell
//
//    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: "FKImageCell", for: indexPath) as! FKImageCell
			
			cell.bannerImage.sd_setShowActivityIndicatorView(true)
			cell.bannerImage.sd_setIndicatorStyle(.whiteLarge)
			
			if let url = URL(string: self.statInfo.fixtureImage) {
				cell.bannerImage.sd_setImage(with: url, placeholderImage: UIImage(named: "")!)

			}
			
     //   cell.bannerImage.normalLoad((self.statInfo.fixtureImage))
        return cell
        /*
        if indexPath.section == 0 {
            let cell  = tableView.dequeueReusableCell(withIdentifier: "FKFixtureStatsCell", for: indexPath) as! FKFixtureStatsCell
         
            cell.totalMatchPlayedLbl?.text = statInfo?.totalMatchPlayed
            cell.homeWinCountLbl?.text = statInfo?.homeWinCount
            cell.awayWinCountLbl?.text = statInfo?.awayWinCount
            cell.homeWinCountAtHomeLbl?.text = statInfo?.homeWinCountAtHome
            cell.homeWinCountAtAwayLbl?.text = statInfo?.homeWinCountAtAway
            cell.awayWinCountAtHomeLbl?.text = statInfo?.awayWinCountAtHome
            cell.awayWinCountAtAwayLbl?.text = statInfo?.awayWinCountAtAway
            cell.drawCountLbl?.text = "Drawn \(statInfo?.drawCount ?? "")"
            
            return cell
            
        } else {
            
            let cell : FKPreviousResultCell!
            if DeviceType.IS_IPAD_PRO || DeviceType.IS_IPAD{
                cell  = tableView.dequeueReusableCell(withIdentifier: "FKPreviousResultCelliPad", for: indexPath) as! FKPreviousResultCell

            }else{
                cell  = tableView.dequeueReusableCell(withIdentifier: "FKPreviousResultCell", for: indexPath) as! FKPreviousResultCell
            }
     
            let result = self.dataSourceArray[indexPath.row]
            cell.homeTeamName?.text = result.homeTeam
            cell.homeTeamLogo?.normalLoad(result.homeTeamLogo)
            cell.awayTeamName?.text = result.awayTeam
            cell.awayTeamLogo?.normalLoad(result.awayTeamLogo)
            cell.timeLbl?.text = result.kickOffTime.dateFromUTC()?.dateString(DATE_FORMAT)
            cell.goalLbl?.text = "\(result.homeTeamScore?.score ?? "0") : \(result.awayTeamScore?.score ?? "0")"
            cell.stadiumName?.text = result.stadiumName
            return cell
        }
 */
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return self.view.frame.height
//        if indexPath.section == 0 {
//            return 212.0
//        } else {
//            if DeviceType.IS_IPAD_PRO || DeviceType.IS_IPAD{
//                return 120.0
//            }else{
//                return 90.0
//            }
//        }
    }
    
    // MARK:- tableview delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        FKImagePreview.shared.previewImage(url: self.statInfo.fixtureImage, controller: self)
//        tableView.deselectRow(at: indexPath, animated: true)
//        if indexPath.section == 1{
//            let fixtureDetailVc = self.storyboard?.instantiateViewController(withIdentifier: "FKPreviousResultVC") as! FKPreviousResultVC
//
//            fixtureDetailVc.previousResult = self.dataSourceArray[indexPath.row]
//            self.navigationController?.pushViewController(fixtureDetailVc, animated: true)
//        }
    }
}
