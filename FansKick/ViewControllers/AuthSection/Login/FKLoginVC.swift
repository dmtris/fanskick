//
//  FKLoginVC.swift
//  FansKick
//
//  Created by FansKick-Raj on 13/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit
import CoreData
class FKLoginVC: UIViewController, UITextFieldDelegate, FKTextFieldDelegate {
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var emailFieldView: FKTextFieldView!
    @IBOutlet weak var passwordFieldView: FKTextFieldView!
    @IBOutlet weak var googleBtnAction: UIButton!
    @IBOutlet weak var fbButton: UIButton!
    @IBOutlet weak var logoImageView: UIImageView?

	@IBOutlet weak var tblView: UITableView?

    //MARK:- UIViewController life cycle >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Email field initialization
        emailFieldView.setFKKeyboardType(keyboardType: .emailAddress)
        emailFieldView.setFKReturnKeyType(returnKeyType: .next)
        emailFieldView.delegate = self
        // Password field initialization
        passwordFieldView.setFKSecureTextEntry(secureTextEntry: true)
        passwordFieldView.setFKReturnKeyType(returnKeyType: .default)
        passwordFieldView.delegate = self
					
			if ScreenSize.SCREEN_HEIGHT  <= 568.0{
				self.tblView?.isScrollEnabled = true
			}
			
        //   emailFieldView.setFKText(fkText: "60146318751")
        // passwordFieldView.setFKText(fkText: "11111111")
    }
    override func viewWillAppear(_ animated: Bool) {
        //let value = UIInterfaceOrientation.portrait.rawValue
        // UIDevice.current.setValue(value, forKey: "orientation")
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        emailFieldView.leftPadingFontIcon(iconName: "a")
        passwordFieldView.leftPadingFontIcon(iconName: "6")
    }
    
    //MARK:- Private functions
    private func initialSetup() {
        showWalkThrough()
    }
    
    func callApiToLogin() {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        var userDict  = [String : Any]()
        
        if self.emailFieldView.text.trimWhiteSpace.isMobileNumber {
            
            userDict["mobile"] = self.emailFieldView.text.mobileNumberWithCountryCode
            
        } else {
            userDict[pEmail] = self.emailFieldView.text.trimWhiteSpace
        }
        
        userDict[pPassword] = self.passwordFieldView.text.trimWhiteSpace
        
        let dataDict  = [puser : userDict, pdevice : AppDelegate.Device()] as [String : Any]
        print(dataDict.toJsonString())
        
        ServiceHelper.request(params: dataDict, method: .post, apiName: "users/login") { (result, error, responseCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if (error == nil) {
                if responseCode == 200 {
                    if let data = result!["user"]  {
                        defaults.set(false, forKey: "socialLogin")
                        let user = data as! Dictionary<String, Any>
                        let userId = user[kUserId] as! String
                        let name = user["name"] as! String
                        let token = user[kOAuthToken]! as! String
                        
                        defaults.setValue(userId , forKey: kUserId)
                        defaults.setValue(name , forKey: "name")
                        defaults.setValue(token , forKey: kOAuthToken)
                        let appDell = UIApplication.shared.delegate as! AppDelegate
                        appDell.navigateToTabController()
                        FKCoreDBHelper.coreDBHelper.updateCurrentUser()

                    }
                } else if(responseCode == 307 ) {
                    let dashboardVC = self.storyboard?.instantiateViewController(withIdentifier: "FKOtpViewController") as! FKOtpViewController
                    dashboardVC.enteredEmail = self.emailFieldView.text.trimWhiteSpace
                    self.navigationController!.pushViewController(dashboardVC, animated: true)
                } else {
                    
                    if let data = result![kResponseMsg]  {
                        MessageView.showMessage(message: data as! String, time: 5.0, verticalAlignment: .bottom)
                    }
                }
            }
        }
    }
    
    @IBAction func fbBtnAction(_ sender: UIButton) {
  
  
        
        FacebookManager.facebookManager.getFacebookInfoWithCompletionHandler(self) { (result, str, error) in

            if result != nil{
                var params = Dictionary<String,Any>()

                var user  = Dictionary<String,Any>()
                user["name"] =  result!["name"]
                user["fb_uid"] = result!["id"]
                user["name"]    =  result!["name"]

                params ["user"] = user
                params["device"] = AppDelegate.Device()

                self.socialApiCall(params: params)
            }
        }
        
       
    }
    
    @IBAction func googleBtnAction(_ sender: UIButton) {
       
        FKGoogleSignIn.sharedInstance.getGoogleInfo(controller: self) { (user, error) in

            if user != nil {

                var params = Dictionary<String,Any>()
                var userDict  = Dictionary<String,Any>()
                userDict["name"] =  user?.profile.name!
                userDict["google_uid"] = user?.userID!
                params ["user"] = userDict
                params["device"] = AppDelegate.Device()

                self.socialApiCall(params: params)
            }
        }
    }
    
    func socialApiCall(params:Dictionary<String,Any>){
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        ServiceHelper.request(params: params, method: MethodType.post, apiName: "socials/social_login") { (result, error, statusCode) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if (error == nil) {
                if statusCode == 201 || statusCode == 204 {
                    if let data = result!["user"]  {
                        defaults.set(true, forKey: "socialLogin")
                        
                        let user = data as! Dictionary<String, Any>
                        let userId = user[kUserId] as! String
                        let name = user["name"] as! String
                        let token = user[kOAuthToken]! as! String
                        
                        defaults.setValue(userId , forKey: kUserId)
                        defaults.setValue(name , forKey: "name")
                        defaults.setValue(token , forKey: kOAuthToken)
                        let appDell = UIApplication.shared.delegate as! AppDelegate
                        appDell.navigateToTabController()
                    
                      FKCoreDBHelper.coreDBHelper.updateCurrentUser()
                        
                      
                        
                    }
                }else{
                    if let data = result![kResponseMsg]  {
                        MessageView.showMessage(message: data as! String, time: 5.0, verticalAlignment: .bottom)
                    }
                }
            }
        }
    }
    
    @IBAction func loginBtn(_ sender: Any) {
        self.view.endEditing(true)
        
        
        
        if allFieldVerified() {
            callApiToLogin()
        }
    }
    
    private func showWalkThrough() {
        
        //        let walkThroughVC = mainStoryboard.instantiateViewController(withIdentifier: "WalkThroughVC") as! WalkThroughVC
        //        self.present(walkThroughVC, animated: false, completion: nil)
        //
        //        walkThroughVC.didTriggerWalkThroughAction = {
        //            () -> Void in
        //            Debug.log("walkThroughVC dismissed")
        //
        //        }
    }
    
    func allFieldVerified()-> Bool {
        
        var isVerified = false
        if emailFieldView.text.trimWhiteSpace.isEmpty {
            emailFieldView.setFKErrorMessage(message: validEmailPassword)
        } else {
            
            if emailFieldView.text.trimWhiteSpace.isMobileNumber {
                if passwordFieldView.text.isEmpty{
                    passwordFieldView.setFKErrorMessage(message: blankPassword)
                    
                } else if passwordFieldView.text.trimWhiteSpace.length > 5{
                    isVerified = true
                } else {
                    passwordFieldView.setFKErrorMessage(message: minCurrentPassword)
                }
            } else {
                if !emailFieldView.text.trimWhiteSpace.isEmail {
                    emailFieldView.setFKErrorMessage(message: validEmailPassword)
                    
                } else {
                    if passwordFieldView.text.isEmpty{
                        passwordFieldView.setFKErrorMessage(message: blankPassword)
                        
                    } else if passwordFieldView.text.trimWhiteSpace.length > 5 {
                        isVerified = true
                    } else {
                        passwordFieldView.setFKErrorMessage(message: minCurrentPassword)
                    }
                }
            }
        }
        return isVerified
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.returnKeyType == UIReturnKeyType.next {
            let txtField = self.view.viewWithTag(textField.tag + 1) as! UITextField
            txtField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    //MARK: FKTextFieldView Overrides
    @objc func fkTextFieldShouldReturn(textField: FKTextFieldView) -> Bool
    {
        if textField.FKReturnKeyType() == .next {
            let nextFieldView = self.view.viewWithTag(textField.tag + 1) as! FKTextFieldView
            nextFieldView.becomeFKFirstResponder()
        } else {
            textField.resignFKFirstResponder()
        }
        return true
    }
    
    @objc  func textField(textField: FKTextFieldView, fkShouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        return true
    }
    
    // MARK:- Memory Management function
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

