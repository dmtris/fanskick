//
//  FKForgotPasswordVC.swift
//  FansKick
//
//  Created by FansKick-Raj on 13/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKForgotPasswordVC: UIViewController, FKTextFieldDelegate {
    
    @IBOutlet weak var emailTextFieldView: FKTextFieldView!
    @IBOutlet weak var submitButton: UIButton!
    
    
    //MARK:- UIViewController life cycle >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Email field initialization
        emailTextFieldView.setFKKeyboardType(keyboardType: .emailAddress)
        emailTextFieldView.setFKReturnKeyType(returnKeyType: .default)
        emailTextFieldView.delegate = self
        emailTextFieldView.setFKAutocapitalizationType(autocapitalizationType: .none)
        emailTextFieldView.setFKAutocorrectionType(autocorrectionType: .no)
    }
    override func viewWillAppear(_ animated: Bool) {
        //let value = UIInterfaceOrientation.portrait.rawValue
       // UIDevice.current.setValue(value, forKey: "orientation")
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        emailTextFieldView.leftPadingFontIcon(iconName: "a")
    }
    
    
    // MARK: - IBActions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    
    @IBAction func onBackButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSubmitButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        
        if validateAndCallApi() {
            var userDict  = [String : Any]()
            if emailTextFieldView.text.trimWhiteSpace.isMobileNumber {
                
//                let number = "\(Int(self.emailTextFieldView.text.trimWhiteSpace)!)"
//                
//                var mobileNumber = ""
//                if number.length > 10{
//                    mobileNumber = "+"+number
//                    
//                }else{
//                    mobileNumber = "+60"+number
//                }
                
                userDict["mobile"] = emailTextFieldView.text.mobileNumberWithCountryCode
                
//                userDict["mobile"] = "+60"+emailTextFieldView.text.trimWhiteSpace
            }else{
                userDict[pEmail] = emailTextFieldView.text.trimWhiteSpace
            }
            let dataDict  = [puser : userDict]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            ServiceHelper.request(params: dataDict, method: .post, apiName: "passwords/forget_password", completionBlock: { (result, error, responseCode) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if (error == nil) {
                    if responseCode == 200 {
                        if let data = result![kResponseMsg]  {
                            MessageView.showMessage(message: data as! String, time: 5.0, verticalAlignment: .bottom)
                            self.perform(#selector(self.onBackButtonAction(_:)), with: nil, afterDelay: 0.1)
                        }
                    }else{
                        if let data = result![kResponseMsg]  {
                            MessageView.showMessage(message: data as! String, time: 5.0, verticalAlignment: .bottom)
                        }
                    }
                }
            })
        }
    }
    
    //MARK: FKTextFieldView Overrides
    
    @objc  func fkTextFieldDidBeginEditing(textField: FKTextFieldView){
        
    }
    @objc func fkTextFieldShouldReturn(textField: FKTextFieldView) -> Bool
    {
        if textField.FKReturnKeyType() == .next{
            let nextFieldView = self.view.viewWithTag(textField.tag + 1) as! FKTextFieldView
            nextFieldView.becomeFKFirstResponder()
        }else{
            textField.resignFKFirstResponder()
        }
        
        return true
    }
    @objc func fkTextFieldDidEndEditing(textField:FKTextFieldView)
    {
        
    }
    @objc  func textField(textField: FKTextFieldView, fkShouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        return true
    }
    
    func validateAndCallApi() -> Bool {
        
        var isVerified = false
        
        if emailTextFieldView.text.trimWhiteSpace.isEmpty {
            emailTextFieldView.setFKErrorMessage(message: "Please enter Email/Mobile number")
        }else {
            
            if emailTextFieldView.text.trimWhiteSpace.isMobileNumber {
                    isVerified = true
            } else {
                if !emailTextFieldView.text.trimWhiteSpace.isEmail {
                    emailTextFieldView.setFKErrorMessage(message: "Please enter valid Email")
                }else {
                    isVerified = true
                }
            }
        }
        return isVerified
    }
    
    // MARK:- --->UIResponder function
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK:- Memory Management function
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
