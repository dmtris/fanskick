//
//  FKOtpViewController.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 10/13/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKOtpViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var textfield1: UITextField!
    @IBOutlet weak var textfield2: UITextField!
    @IBOutlet weak var textfield3: UITextField!
    @IBOutlet weak var textfield4: UITextField!
    
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var otpTextfield: UITextField!
    var enteredEmail = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleText.text = "Please type verification code sent to: \(enteredEmail)"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resendBtnAction() {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let userDict = ["mobile" : enteredEmail.mobileNumberWithCountryCode] as [String : Any]
        let dataDict  = [puser : userDict]
        ServiceHelper.request(params: dataDict, method: .post, apiName: "users/resend_otp", completionBlock: { (result, error, responseCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if (error == nil) {
                if responseCode == 200 {
                    if let data = result![kResponseMsg]  {
                        MessageView.showMessage(message: data as! String, time: 5.0, verticalAlignment: .bottom)
                    }
                } else {
                    if let data = result![kResponseMsg]  {
                        MessageView.showMessage(message: data as! String, time: 5.0, verticalAlignment: .bottom)
                    }
                }
            }
        })
    }
    
    @IBAction func submitBtn(_ sender: Any) {
        self.view.endEditing(true)
        
        if textfield1.text!.trimWhiteSpace.length != 0 && textfield2.text!.trimWhiteSpace.length != 0 && textfield3.text!.trimWhiteSpace.length != 0 && textfield4.text!.trimWhiteSpace.length != 0 {
            
            let otpText = textfield1.text!.trimWhiteSpace+textfield2.text!.trimWhiteSpace+textfield3.text!.trimWhiteSpace+textfield4.text!.trimWhiteSpace
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let userDict = ["mobile" : enteredEmail.mobileNumberWithCountryCode] as [String : Any]
            let code = ["otp" : otpText] as [String : Any]
            
            let dataDict  = [puser : userDict,"activation_code": code]
            ServiceHelper.request(params: dataDict, method: .post, apiName: "users/otp_verification", completionBlock: { (result, error, responseCode) in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                if (error == nil) {
                    if responseCode == 200 {
                        
                        if let data = result!["user"]  {
                            let user = data as! Dictionary<String, Any>
                            let userId = user[kUserId] as! String
                            let name = user["name"] as! String
                            let token = user[kOAuthToken]! as! String
                            
                            defaults.setValue(userId , forKey: kUserId)
                            defaults.setValue(name , forKey: "name")
                            defaults.setValue(token , forKey: kOAuthToken)
                            let appDell = UIApplication.shared.delegate as! AppDelegate
                            appDell.navigateToTabController()
                            FKCoreDBHelper.coreDBHelper.updateCurrentUser()

                        }
                    }else{
                        if let data = result![kResponseMsg]  {
                            MessageView.showMessage(message: data as! String, time: 5.0, verticalAlignment: .bottom)
                        }
                    }
                }
            })
        } else {
            MessageView.showMessage(message: "Please enter code", time: 5.0, verticalAlignment: .bottom)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var updatedString:NSString = textField.text! as NSString
        updatedString = updatedString.replacingCharacters(in: range, with: string) as NSString
        
        if updatedString.length == 0 {
            return true
        } else if updatedString.length < 2 {
            self.perform(#selector(callSelectorToUpdateNextTextfieldAsResponder), with: nil, afterDelay: 0.1)
        } else {
            return false
        }
        return true
    }
    
    @objc func callSelectorToUpdateNextTextfieldAsResponder() {
        
        if textfield1.isFirstResponder {
            textfield2.becomeFirstResponder()
        } else if textfield2.isFirstResponder {
            textfield3.becomeFirstResponder()
        } else if textfield3.isFirstResponder {
            textfield4.becomeFirstResponder()
        } else if textfield4.isFirstResponder {
            textfield4.resignFirstResponder()
        }
    }
}
