//
//  FKRegisterViewController.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 10/13/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKRegisterViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,FKTextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var picker = UIPickerView()
    var datePicker = UIDatePicker()
    
    
    let placeholderTextArr = ["Name",
                              "Phone number",
                              "Email(optional)",
                              "DOB (optional)",
                              "State",
                              "Address (optional)",
                              "Password",
                              "Confirm password"]
    
    let fontIconArray = ["a",// name
        "M",// phone
        "t", // email
        "9", // dob
        "O", //state
        "y", //addres
        "6", // password
        "6"]
    
    var indexPath:IndexPath?
    var stateArr = [String]()
    var stateArray = [FKListInfo]()
    
    let fkUser = FKUser()
    var selectedDate = Date()
    var selectedStateId = String()
    var errorTag = 50
    
    // @IBOutlet weak var pickerMainView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        callApiToGetStates()
        self.initialise()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
    }
    
    private func initialise() {
        self.picker.delegate = self
        self.picker.dataSource = self
        datePicker.maximumDate = Date()
        datePicker.datePickerMode = .date
    }
    
    private func pickerToolBar() -> UIToolbar {
        let toolBar = UIToolbar.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 44))
        let doneItem =   UIBarButtonItem.init(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(FKRegisterViewController.doneBtnAction))
        let cancelItem =   UIBarButtonItem.init(title: "Cancel", style: UIBarButtonItemStyle.done, target: self, action: #selector(FKRegisterViewController.cancelBtnAction))
        let space =  UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        toolBar.setItems([cancelItem, space, doneItem], animated: true)
        toolBar.barTintColor = UIColor.white
        toolBar.tintColor = UIColor.black
        
        return toolBar
    }
    
    private func numberPadToolBar() -> UIToolbar {
        let toolbar = UIToolbar.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 44))
        let doneItem =   UIBarButtonItem.init(title: "Next", style: UIBarButtonItemStyle.done, target: self, action: #selector(FKRegisterViewController.nextBtnAction))
        let space =  UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        toolbar.setItems([space, doneItem], animated: true)
        toolbar.barTintColor = UIColor.white
        toolbar.tintColor = UIColor.black
        
        return toolbar
    }
    
    func callApiToGetStates() {
        
        let params = Dictionary<String,Any>()
        ServiceHelper.request(params: params, method: .get, apiName: "users/state_list", hudType: loadingIndicatorType.withoutLoader) { (result, error, responseCode) in
            
            if (error == nil) {
                if responseCode == 200 {
                    let results = result as! Dictionary<String, AnyObject>
                    if let data = results["state_list"] {
                        self.stateArray.append(contentsOf: FKListInfo.list(data: data as! Array<Dictionary<String, AnyObject>>))
                        for state in self.stateArray{
                            self.stateArr.append(state.name)
                        }
                    } else {
                        if let data = result![kResponseMsg] {
                            MessageView.showMessage(message: data as! String, time: 5.0, verticalAlignment: .bottom)
                        }
                    }
                } else {
                }
            }
        }
    }
    
    @IBAction func termAndCondition(_ sender: Any) {
        
        let staticVC = self.storyboard?.instantiateViewController(withIdentifier: "staticContentVC")
        self.navigationController?.pushViewController(staticVC!, animated: true)
    }
    
    func getStateByName(name:String)-> FKListInfo {
        var stateInfo:FKListInfo?
        for state in self.stateArray {
            if name == state.name{
                stateInfo = state
                break
            }
        }
        if stateInfo != nil {
            return stateInfo!
        } else {
            stateInfo?.name = ""
            stateInfo?.id = ""
            
            return stateInfo!
        }
    }
    
    @objc func doneBtnAction() {
        
        if indexPath?.row == 3 {
            // date
            
            fkUser.selectedDob = datePicker.date
            fkUser.dob = (fkUser.selectedDob!.dateString(kdateFormatMMDDYY))
            let cell = tableView.cellForRow(at: IndexPath.init(row: 3, section: 0)) as! FKInputTableViewCell
            cell.textField.setFKText(fkText:fkUser.dob)
        } else {
            // state
            fkUser.state =  stateArr[self.picker.selectedRow(inComponent: 0)]
            let cell = tableView.cellForRow(at: IndexPath.init(row: 4, section: 0)) as! FKInputTableViewCell
            cell.textField.setFKText(fkText: fkUser.state)
        }
        
        self.view.endEditing(true)
    }
    @objc func cancelBtnAction(){
        self.view.endEditing(true)
    }
    
    
    @IBAction func backBtn(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.placeholderTextArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:FKInputTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FKInputTableViewCell", for: indexPath) as! FKInputTableViewCell
        cell.textField.setFKPlaceHolder(placeHolder: self.placeholderTextArr[indexPath.row])
        
        cell.textField.leftPadingFontIcon(iconName: fontIconArray[indexPath.row])
        cell.textField.delegate = self
        cell.textField.setFKReturnKeyType(returnKeyType: .next)
        cell.textField.indexPath = indexPath
        cell.textField.setFKSecureTextEntry(secureTextEntry: false)
        cell.textField.setFKKeyboardType(keyboardType: .asciiCapable)
        cell.textField.maxLength = 0
        cell.textField.setFKAutocapitalizationType(autocapitalizationType: .none)
        
        switch indexPath.row {
        case 0:
            cell.textField.text = fkUser.name
            cell.textField.maxLength = 60
            cell.textField.setFKAutocapitalizationType(autocapitalizationType: UITextAutocapitalizationType.words)
            break
        case 1:
            cell.textField.text = fkUser.mobileNo
            cell.textField.setFKKeyboardType(keyboardType: .numberPad)
            cell.textField.maxLength = 13
            break
        case 2:
            cell.textField.text = fkUser.email
            cell.textField.setFKKeyboardType(keyboardType: .emailAddress)
            cell.textField.maxLength = 60
            
            break
        case 3:
            cell.textField.text = fkUser.dob
            cell.textField.rightPadding(width: 30, image: #imageLiteral(resourceName: "arrowDown_icon"))
            break
        case 4:
            cell.textField.text = fkUser.state
            cell.textField.rightPadding(width: 30, image: #imageLiteral(resourceName: "arrowDown_icon"))
            break
        case 5:
            cell.textField.text = fkUser.address
            break
        case 6:
            cell.textField.text = fkUser.password
            cell.textField.setFKSecureTextEntry(secureTextEntry: true)
            cell.textField.maxLength = 16
            break
        case 7:
            cell.textField.text = fkUser.confrmPassword
            cell.textField.setFKSecureTextEntry(secureTextEntry: true)
            cell.textField.setFKReturnKeyType(returnKeyType: .default)
            cell.textField.maxLength = 16
            break
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 70
    }
    
    @objc func nextBtnAction() {
        
        let indexPath = IndexPath.init(row: 2, section: 0)
        if !(tableView.indexPathsForVisibleRows?.contains(indexPath))! {
            tableView.scrollToRow(at: indexPath, at: .none, animated: false)
        }
        let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
        cell.textField.becomeFKFirstResponder()
    }
    
    //MARK: FKTextFieldView Overrides
    
    @objc  func fkTextFieldDidBeginEditing(textField: FKTextFieldView) {
        
        self.indexPath = textField.indexPath
        if textField.indexPath?.row == 3 {
            textField.setInputAccessoryView(inputAccessoryView:self.pickerToolBar())
            textField.setInputView(inputView: datePicker)
            
        }else if textField.indexPath?.row == 4 {
            textField.setInputAccessoryView(inputAccessoryView:self.pickerToolBar())
            textField.setInputView(inputView: picker)
            
        }else if textField.indexPath?.row == 1{
            textField.setInputAccessoryView(inputAccessoryView:self.numberPadToolBar())
            
        }
    }
    @objc func fkTextFieldShouldReturn(textField: FKTextFieldView) -> Bool {
        
        if textField.FKReturnKeyType() == .next {
            let indexPath = IndexPath.init(row: (textField.indexPath?.row)! + 1, section: 0)
            if !(tableView.indexPathsForVisibleRows?.contains(indexPath))!{
                tableView.scrollToRow(at: indexPath, at: .none, animated: false)
            }
            let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
            cell.textField.becomeFKFirstResponder()
        } else {
            textField.resignFKFirstResponder()
        }
        return true
    }
    
    @objc func fkTextFieldDidEndEditing(textField:FKTextFieldView)
    {
        switch indexPath!.row {
        case 0:
            fkUser.name = textField.text
            break
        case 1:
            fkUser.mobileNo = textField.text
            break
        case 2:
            fkUser.email = textField.text
            break
        case 5:
            fkUser.address = textField.text
            break
        case 6:
            fkUser.password = textField.text
            break
        case 7:
            fkUser.confrmPassword = textField.text
            break
        default:
            break
        }
    }
    
    @objc  func textField(textField: FKTextFieldView, fkShouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    
    // MARK:- Submit Button
    @IBAction func submitBtn(_ sender: Any) {
        self.view.endEditing(true)
        
        if isAllFieldVerified(){
            callApiToRegister()
        }
    }
    
    // MARK:- Api to register
    private func callApiToRegister() {
        
        var dateStr = ""
        if !fkUser.dob.isEmpty {
            dateStr = (fkUser.selectedDob?.UTCStringFromDate()!)!
        }
        
        let state = getStateByName(name: fkUser.state)
        let userDict = [pName       : fkUser.name,
                        pEmail     : fkUser.email,
                        pPassword  : fkUser.password,
                        pmobile    : fkUser.mobileNo.mobileNumberWithCountryCode,
                        pdob       : dateStr,
                        pstate     : state.id ,
                        paddress   : fkUser.address] as [String : Any]
        
        let params  = [puser : userDict,pdevice : AppDelegate.Device()]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: .post, apiName: "users/sign_up") { (result, error, responseCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if (error == nil) {
                if responseCode == 201 {
                    defaults.set(false, forKey: "socialLogin")
                    
                    let dashboardVC = self.storyboard?.instantiateViewController(withIdentifier: "FKOtpViewController") as! FKOtpViewController
                    dashboardVC.enteredEmail = self.fkUser.mobileNo
                    self.navigationController!.pushViewController(dashboardVC, animated: true)
                } else if(responseCode == 307) {
                    defaults.set(false, forKey: "socialLogin")
                    
                    let dashboardVC = self.storyboard?.instantiateViewController(withIdentifier: "FKOtpViewController") as! FKOtpViewController
                    dashboardVC.enteredEmail = self.fkUser.mobileNo
                    self.navigationController!.pushViewController(dashboardVC, animated: true)
                } else {
                    if let data = result![kResponseMsg] {
                        MessageView.showMessage(message: data as! String, time: 5.0, verticalAlignment: .bottom)
                    }
                }
            }
        }
    }
    
    // DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return stateArr.count
    }
    
    // Delegate
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return stateArr[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func isAllFieldVerified() -> Bool {
        var isVerified = false
        
        if fkUser.name.isEmpty {
            let indexPath = IndexPath.init(row: 0, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: false)
            let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
            cell.textField.setFKErrorMessage(message: BLANK_NAME)
        } else if fkUser.mobileNo.isEmpty {
            let indexPath = IndexPath.init(row: 1, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: false)
            let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
            cell.textField.setFKErrorMessage(message: kBlank_Phone)
        } else if (!fkUser.mobileNo.trimWhiteSpace.isMobileNumber) {
            
            let indexPath = IndexPath.init(row: 1, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: false)
            let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
            cell.textField.setFKErrorMessage(message: kValidPhone)
        } else if (!fkUser.email.isEmpty && !fkUser.email.isEmail) {
            
            let indexPath = IndexPath.init(row: 2, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: false)
            let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
            cell.textField.setFKErrorMessage(message: kValidEmail)
        } else if fkUser.state.trimWhiteSpace.isEmpty {
            
            let indexPath = IndexPath.init(row: 4, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: false)
            let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
            cell.textField.setFKErrorMessage(message: kBlankState)
        } else if fkUser.password.isEmpty {
            
            let indexPath = IndexPath.init(row: 6, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: false)
            let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
            cell.textField.setFKErrorMessage(message: blankPassword)
        } else if fkUser.password.trimWhiteSpace.length < 6 {
            
            let indexPath = IndexPath.init(row: 6, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: false)
            let cell = tableView.cellForRow(at: indexPath) as! FKInputTableViewCell
            cell.textField.setFKErrorMessage(message: kMinPassword)
        } else if fkUser.password.trimWhiteSpace != fkUser.confrmPassword.trimWhiteSpace {
            
            MessageView.showMessage(message: mismatchNewAndRePassword, time: 5, verticalAlignment: .top)
        } else {
            isVerified = true
        }
        return isVerified
    }
}
