//
//  FKPreviousResultVc.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 10/31/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKPreviousResultVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var team1Name:UILabel?
    @IBOutlet weak var team2Name:UILabel?
    @IBOutlet weak var team1Logo:UIImageView?
    @IBOutlet weak var team2Logo:UIImageView?
    @IBOutlet weak var stadiumName:UILabel?
    
    @IBOutlet weak var scoreLbl:UILabel?

    @IBOutlet weak var tblView:UITableView?

    var previousResult: FKPreviousResultInfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title =  "\(self.previousResult?.homeTeam ?? "") Vs \(self.previousResult?.awayTeam ?? "") (\(previousResult?.kickOffTime.dateFromUTC()?.dateString(DATE_FORMAT) ?? ""))"   //"Previous Result"
        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]

			tblView?.rowHeight = UITableViewAutomaticDimension
			tblView?.estimatedRowHeight = 50
        tblView?.tableFooterView = UIView.init()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
        
        self.initialiseContent()
    }
    
   private func initialiseContent(){
        
        self.team1Name?.text = self.previousResult?.homeTeam
        self.team2Name?.text = self.previousResult?.awayTeam
        self.team1Logo?.normalLoad((self.previousResult?.homeTeamLogo)!)
        self.team2Logo?.normalLoad((self.previousResult?.awayTeamLogo)!)
        
        self.stadiumName?.text = self.previousResult?.stadiumName

		self.scoreLbl?.text = "\(self.previousResult?.homeTeamScoreString ?? "0") : \(self.previousResult?.awayTeamScoreString ?? "0")"

    }
    // use to navigate back screen
    @objc func backBtnAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
			
//			if section == 0 {
//        return 4
//			}else {
			
				return (self.previousResult?.events.count)!
		//	}
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
			
//			if indexPath.section == 0 {
//
//        let cell = tableView.dequeueReusableCell(withIdentifier: "FKPreviousResultStateCell", for: indexPath) as! FKPreviousResultStateCell
//        if indexPath.row == 0 {
//
//            cell.team1Lbl?.text = self.previousResult?.homeTeamScore?.extraScore
//            cell.nameLbl?.text = "Extra score"
//            cell.team2Lbl?.text = self.previousResult?.awayTeamScore?.extraScore
//
//        }else if indexPath.row == 1{
//            cell.team1Lbl?.text = self.previousResult?.homeTeamScore?.halfScore
//            cell.nameLbl?.text = "Half score"
//            cell.team2Lbl?.text = self.previousResult?.awayTeamScore?.halfScore
//
//        }else if indexPath.row == 2{
//            cell.team1Lbl?.text = self.previousResult?.homeTeamScore?.ninetyScore
//            cell.nameLbl?.text = "Ninety score"
//            cell.team2Lbl?.text = self.previousResult?.awayTeamScore?.ninetyScore
//
//        }else if indexPath.row == 3{
//            cell.team1Lbl?.text = self.previousResult?.homeTeamScore?.penalityScore
//            cell.nameLbl?.text = "Extra score"
//            cell.team2Lbl?.text = self.previousResult?.awayTeamScore?.penalityScore
//        }
//
//        return cell
//			}else {
				let cell = tableView.dequeueReusableCell(withIdentifier: "FKPreviousResultStateInfoCell", for: indexPath) as! FKPreviousResultStateCell

				cell.nameLbl?.text = (self.previousResult?.events[indexPath.row])! 
				return cell 
		//	}
    }
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
	{
		return UITableViewAutomaticDimension
	}
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
}
