//
//  FKStaticWebPageVC.swift
//  FansKick
//
//  Created by Sunil Verma on 11/2/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit
import WebKit

enum STATIC_PAGE_TYPE {
    case ABOUT
    case TERMSANDCONDITION
    case HELP
}

class FKStaticWebPageVC: UIViewController, WKNavigationDelegate, WKUIDelegate {

    var pageType : STATIC_PAGE_TYPE?
    var webView = WKWebView()
    
    @IBOutlet weak var mainView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let backBarItem = UIBarButtonItem.init(image: UIImage.init(named: "back_arrow"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backBtnAction))
        
        self.navigationItem.leftBarButtonItems = [backBarItem,AppUtility.fanskickLogo()]
        loadWebPage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
        self.webView.allowsLinkPreview = true
        self.webView.navigationDelegate = self
        self.webView.uiDelegate = self
        webView.allowsBackForwardNavigationGestures = true
        self.webView.configuration.dataDetectorTypes = .link
        self.webView.configuration.dataDetectorTypes = .phoneNumber
        self.webView.configuration.dataDetectorTypes = .address
        self.view.addSubview(self.webView)
    }
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Swift.Void)
    {
        if webView != self.webView {
            decisionHandler(.allow)
            return
        }
        
        let app = UIApplication.shared
        if let url = navigationAction.request.url {
            // Handle target="_blank"
            if navigationAction.targetFrame == nil {
                if app.canOpenURL(url) {
                    app.openURL(url)
                    decisionHandler(.cancel)
                    return
                }
            }
            
            // Handle phone and email links
            if url.scheme == "tel" || url.scheme == "mailto" {
                if app.canOpenURL(url) {
                    app.openURL(url)
                    decisionHandler(.cancel)
                    return
                }
            }
            decisionHandler(.allow)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.webView.frame = mainView.frame

    }
    func loadWebPage() {
        
        if self.pageType == STATIC_PAGE_TYPE.ABOUT {
            self.navigationItem.title = "ABOUT US"
        } else if self.pageType == STATIC_PAGE_TYPE.HELP {
            self.navigationItem.title = "HELP"
            }
        
        self.staticContentApiCall()
    }
    
    func staticContentApiCall(){
        
        var params = Dictionary<String, Any>()
        
        var staticDict = Dictionary<String, Any>()
        
        if self.pageType == STATIC_PAGE_TYPE.ABOUT {
            staticDict["title"] = "About Us"
            
        } else if self.pageType == STATIC_PAGE_TYPE.HELP {
            staticDict["title"] = "Help"
        }
         // "About Us"// “Help”

        params["static_page"] = staticDict
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: .post, apiName: "news/static_pages") { (result, error , responseCode) in
        MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil{
                
                let results = result as! Dictionary<String, AnyObject>
                
                if let data = results["static_page"] {
                    self.webView.loadHTMLString(data["body"] as! String, baseURL: nil)
                }
            }
        }
        
    }
    
    // use to navigate back screen
    @IBAction func backBtnAction(){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
