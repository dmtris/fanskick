//
//  FKStaticContentVC.swift
//  FansKick
//
//  Created by FansKick-Sunil on 31/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit
import WebKit
class FKStaticContentVC: UIViewController {
    
    var webView = WKWebView()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadWebPage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.webView.frame =  CGRect.init(x: 0, y: 64, width: self.view.frame.width, height: (self.view.frame.height - CGFloat(64.0)))
        self.view.addSubview(self.webView)
    }
    
    func loadWebPage(){
        self.staticContentApiCall()
    }
    
    func staticContentApiCall(){
        
        var params = Dictionary<String, Any>()
        var staticDict = Dictionary<String, Any>()
        staticDict["title"] = "Terms & Conditions"
        params["static_page"] = staticDict
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ServiceHelper.request(params: params, method: .post, apiName: "news/static_pages") { (result, error , responseCode) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil{
                let results = result as! Dictionary<String, AnyObject>
                if let data = results["static_page"] {
                    self.webView.loadHTMLString(data["body"] as! String, baseURL: nil)
                }
            }
        }
        
    }
    // use to navigate back screen
    @IBAction func backBtnAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
