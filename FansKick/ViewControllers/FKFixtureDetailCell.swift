//
//  FKFixtureDetailCell.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 10/20/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKFixtureDetailCell: UITableViewCell {

    @IBOutlet weak var titleLbl:UILabel?
    @IBOutlet weak var contentTextView:UITextView?
    @IBOutlet weak var dateLbl:UILabel?
    @IBOutlet weak var imgView: UIImageView?
    @IBOutlet weak var playBtn: UIButton?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
