//
//  FKPreviewController.swift
//  FansKick
//
//  Created by Sunil Verma on 22/11/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit
import QuickLook
class FKPreviewController: QLPreviewController {

    override func viewDidLoad() {
        super.viewDidLoad()
     
    }
    
    func show(controller: UIViewController) {
    self.reloadData()
    if let navController = controller.navigationController {
    navController.pushViewController(self, animated: false)
    }
    else {
    controller.show(self, sender: nil)
    }
    }
    override func viewDidLayoutSubviews() {
        
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.backgroundColor = UIColor.clear
        self.navigationItem.title = "Preview"
        if !(self.navigationItem.rightBarButtonItems?.isEmpty)!{
            self.navigationItem.rightBarButtonItems?[0] = UIBarButtonItem()
        }

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
