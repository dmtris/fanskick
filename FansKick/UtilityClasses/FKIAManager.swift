//
//  FKIAManager.swift
//  FansKick
//
//  Created by Sunil Verma on 11/13/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit
import IndoorAtlas

let INDOOR_API_KEY = "263a823a-6a50-41da-a769-7938e5cd74f1"
let INDOOR_SEC_KEY = "ymwfFuKHr/WkKfEJwOZiga9K5Js2YmECnCRJmJQMwFbBO2F+Cnq4HblfVxZ1cohjTkjdSZ1fvvz2QILxzQtgD8LmJLxOoaATdXmvhrXYIMwRqe9bvOU4C/bGAfCaTw=="

class FKIAManager: NSObject, IALocationManagerDelegate {
    
    var manager: IALocationManager?
    
   static let shared = FKIAManager()
    
    // one time calling
    func authenticateAndRequestLocation(){
        self.manager = IALocationManager.sharedInstance()
        
        self.manager?.delegate = self
        
        // Set IndoorAtlas ApiKey and secret
        
        self.manager?.setApiKey(INDOOR_API_KEY, andSecret: INDOOR_SEC_KEY)
        
        // Request location updates
        self.manager?.startUpdatingLocation()
        
    }
    
    /**
     * Tells the delegate that new location data is available.
     *
     * Implementation of this method is optional but recommended.
     *
     * @param manager The location manager object that generated the update event.
     * @param locations An array of <IALocation> objects containing the location data. This array always contains at least one object representing the current location.
     * If updates were deferred or if multiple locations arrived before they could be delivered, the array may contain additional entries.
     * The objects in the array are organized in the order in which they occured. Threfore, the most recent location update is at the end of the array.
     */
    func indoorLocationManager(_ manager: IALocationManager, didUpdateLocations locations: [Any]) {
        
    }
    
    /**
     * Tells the delegate that the user entered the specified region.
     * @param manager The location manager object that generated the event.
     * @param region The region related to event.
     */
    
    func indoorLocationManager(_ manager: IALocationManager, didEnter region: IARegion) {
        
    }
    
    /**
     * Tells the delegate that the user left the specified region.
     * @param manager The location manager object that generated the event.
     * @param region The region related to event.
     */
    
    func indoorLocationManager(_ manager: IALocationManager, didExitRegion region: IARegion) {
        
    }
    
    /**
     * Tells that <IALocationManager> status changed. This is used to signal network connection issues.
     * @param manager The location manager object that generated the event.
     * @param status The status at the time of the event.
     */
    
    func indoorLocationManager(_ manager: IALocationManager, statusChanged status: IAStatus) {
        
    }
    
    
    /**
     * Tells that calibration quality changed.
     * @param manager The location manager object that generated the event.
     * @param quality The calibration quality at the time of the event.
     */
    
    func indoorLocationManager(_ manager: IALocationManager, calibrationQualityChanged quality: ia_calibration) {
        
    }
    
    
    /**
     * Tells that extra information dictionary was received. This dictionary contains
     * identifier for debugging positioning.
     * @param manager The location manager object that generated the event.
     * @param extraInfo NSDictionary containing extra information about positioning.
     */
    
    func indoorLocationManager(_ manager: IALocationManager, didReceiveExtraInfo extraInfo: [AnyHashable : Any]) {
        
    }
    
    /**
     * Tells the delegate that updated heading information is available.
     * @param manager The location manager object that generated the event.
     * @param newHeading New heading data.
     */
    func indoorLocationManager(_ manager: IALocationManager, didUpdate newHeading: IAHeading) {
        
    }
    
    /**
     * Tells the delegate that updated attitude (orientation) information is available.
     * @param manager The location manager object that generated the event.
     * @param newAttitude New attitude data.
     */
    func indoorLocationManager(_ manager: IALocationManager, didUpdate newAttitude: IAAttitude) {
        
    }
    
}
