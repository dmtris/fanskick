//
//  FKImagePreview.swift
//  FansKick
//
//  Created by Sunil Verma on 22/11/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

import AXPhotoViewer

class FKImagePreview: NSObject, AXPhotosViewControllerDelegate {
    
    private var urlStr = ""

    static let shared = FKImagePreview()
    
    func previewImage(url: String, controller:UIViewController){
        
        urlStr = url
        
        let photos = [
					AXPhoto(attributedTitle: NSAttributedString(string: ""),
                  attributedDescription: NSAttributedString(string: ""),
                  attributedCredit: NSAttributedString(string: ""),
                  url: URL(string: url))
        ]

			let dataSource = AXPhotosDataSource(photos: photos)
        let photosViewController = AXPhotosViewController(dataSource: dataSource)
        controller.present(photosViewController, animated: true)
    }
	
}
