//
//  FKPlayerHelper.swift
//  PlayerDemo
//
//  Created by Sunil Verma on 11/8/17.
//  Copyright © 2017 Sunil Verma. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

let PLAYER_FAIL_NOTIFICATION = "PLAYER_FAIL_NOTIFICATION"

class FKPlayerHelper: NSObject, AVPlayerViewControllerDelegate  {
    
    private var parentController:UIViewController?
    private var playerViewController = AVPlayerViewController()
    private var  player: AVPlayer?
    private var vidoeThumbnail = [String:Any]()

    static let shared = FKPlayerHelper()
    
    func removeThumbnail() {
        
        self.vidoeThumbnail.removeAll()
        
    }
    
    func  playOnRootWindow(url:String){
        let appDell =  UIApplication.shared.delegate as! AppDelegate
        self.play(url: url, controller: (appDell.window?.rootViewController)!)
    }
    
    
    func play(url:String, controller:UIViewController){
        parentController = controller
        let videoURL = URL(string: url)
        if videoURL == nil{
            MessageView.showMessage(message: "Media type not suppoted", time: 4.0, verticalAlignment: .bottom)
            return

        }
        let appDell =  UIApplication.shared.delegate as! AppDelegate
        appDell.window?.isUserInteractionEnabled = false
        let playerItem = AVPlayerItem.init(url: videoURL!)
        let player = AVPlayer.init(playerItem: playerItem)
        playerViewController.player = player
        playerViewController.delegate = self
        playerViewController.showsPlaybackControls = true
        parentController?.modalPresentationCapturesStatusBarAppearance = true
        parentController?.present(playerViewController, animated: true) {
            appDell.window?.isUserInteractionEnabled = true

            self.playerViewController.player!.play()
        }
    }
    
    
    
    func play(url:String, view:UIView){
        
        let videoURL = URL(string: url)
        if videoURL == nil{
            MessageView.showMessage(message: "Media type not suppoted", time: 4.0, verticalAlignment: .bottom)
            return
        }
        let playerItem = AVPlayerItem.init(url: videoURL!)
        let player = AVPlayer.init(playerItem: playerItem)
        playerViewController.view.frame = view.bounds
        playerViewController.player = player
        playerViewController.delegate = self
        view.addSubview(playerViewController.view)
        playerViewController.player?.play()
        
    }
    
    func embedPlayer(url:String, view:UIView){
        let videoURL = URL(string: url)
        if videoURL == nil{
            MessageView.showMessage(message: "Media type not suppoted", time: 4.0, verticalAlignment: .bottom)
            return
        }
        let playerItem = AVPlayerItem.init(url: videoURL!)
        //  playerItem.preferredPeakBitRate = 100.0
        player = AVPlayer.init(playerItem: playerItem)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = view.bounds
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        view.layer.addSublayer(playerLayer)
        player?.play()
    
      //  playerItem.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions.old , context: nil)
        
    }

//    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
//        print("vvvvvvvvvvvvv")
//        if keyPath == "status"{
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: PLAYER_FAIL_NOTIFICATION), object: nil)
//        }
//    }
//    
    public var currentTime: CMTime {
        return (playerViewController.player?.currentTime())!
    }
    //currentTime
    func stop(){
        
        self.playerViewController.player?.pause()
        player?.pause()
    }
    
    //MARK:  AVPlayerController overrides
    
    func playerViewControllerDidStopPictureInPicture(_ playerViewController: AVPlayerViewController) {
        
    UIApplication.shared.isStatusBarHidden = false
    }
    
    class  func captureImageFrom(url: String, completion:@escaping (_ image: UIImage)->()){
        
        if let image =  FKPlayerHelper.shared.vidoeThumbnail[url]{
            
            completion(image as! UIImage)
            
            return
        }
        
        let actualUrl  = URL.init(string: url)
        if actualUrl == nil {
            completion(UIImage.init(named: "placeholder")!)
            return
        }
        DispatchQueue.global(qos: .background).async {
            let asset = AVURLAsset(url: actualUrl!)
            let duration = asset.duration
            let durationTime = CMTimeGetSeconds(duration)
            let generator = AVAssetImageGenerator(asset: asset)
            generator.appliesPreferredTrackTransform = true
            let timestamp = CMTime(seconds: durationTime/2, preferredTimescale: 60)
            do {
                let imageRef = try generator.copyCGImage(at: timestamp, actualTime: nil)
                let img =   UIImage(cgImage: imageRef)
                DispatchQueue.main.async {
                    FKPlayerHelper.shared.vidoeThumbnail[url] = img
                    completion(img)
                }
            }
            catch let error as NSError
            {
                print("Image generation failed with error \(error)")
                DispatchQueue.main.async {
                    completion(UIImage.init(named: "placeholder")!)
                }
            }
        }
    }
    
}

