//
//  FKQRCodeManager.swift
//  FansKick
//
//  Created by Sunil Verma on 11/01/2018.
//  Copyright © 2018 FansKick Dev. All rights reserved.
//

import UIKit

class FKQRCodeManager: NSObject {

   class func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
}
