//
//  FKLayerLabel.swift
//  Demo
//
//  Created by Sunil Verma on 06/12/2017.
//  Copyright © 2017 Sunil Verma. All rights reserved.
//

import UIKit

class FKLayerLabel: UILabel {

    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        let path = UIBezierPath.init()
        path.move(to: CGPoint.init(x: 5.0, y: 0))
        path.addLine(to: CGPoint.init(x: self.frame.width, y: 0))
        path.addLine(to: CGPoint.init(x: self.frame.width - 5.0, y: self.frame.height))
        path.addLine(to: CGPoint.init(x: 0, y: self.frame.height))
        let shapeLayer           =  CAShapeLayer()
        shapeLayer.path          = path.cgPath
        self.layer.mask = shapeLayer
    }
    

}
