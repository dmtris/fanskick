//
//  FKShareHelper.swift
//  FansKick
//
//  Created by Sunil Verma on 23/11/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKShareHelper: NSObject {

    static let share = FKShareHelper()
    
    func openActivity(options:Array<Any>, controller:UIViewController){
        
        let activityViewController = UIActivityViewController(activityItems:options, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = controller.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [] //[ UIActivityType.airDrop, UIActivityType.postToFacebook ]
      //  activityViewController.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.postToFacebook]
        
        // present the view controller
        controller.present(activityViewController, animated: true, completion: nil)
    }
}
