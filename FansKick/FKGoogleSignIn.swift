//
//  FKGoogleSignIn.swift
//  FansKick
//
//  Created by FansKick-Sunil on 30/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

// needed to install  pod 'Google/SignIn'


import UIKit
import Google
import GoogleSignIn

typealias GoogleCompletionBlock = (GIDGoogleUser?, Error?) -> Void



class FKGoogleSignIn: NSObject, GIDSignInDelegate, GIDSignInUIDelegate {
    var parentControlelr:UIViewController?
    var completionBlock: GoogleCompletionBlock?
    
    /**
     * use to create the shared instance of the class
     */
    static let sharedInstance : FKGoogleSignIn = {
        let instance = FKGoogleSignIn()
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
       // GIDSignIn.sharedInstance().delegate = self
        
        return instance
    }()
    
    /**
     * use to get the google profile information
     *
     * @param controller controller object where facebook auth controller needed to be present
     * @param onCompletion  call back block
     */
    func getGoogleInfo(controller:UIViewController, onCompletion:  @escaping GoogleCompletionBlock) -> Void {
        
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        if !appDel.isReachable{
            
            AlertController.alert(title: "Connection Error!", message: NO_INTERNET_CONNECTION, buttons: ["Ok"], tapBlock: { (action, index) in
                
            })
            return
        }
        
       self.parentControlelr = controller
        self.completionBlock = onCompletion
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().shouldFetchBasicProfile = true
        
        GIDSignIn.sharedInstance().signIn()
        
        
    }
    
    /**
     * use to signout from google
     */
    func signOut(){
        GIDSignIn.sharedInstance().signOut()
    }
    //MARK: GIDSignInDelegate
    
    
    /**
     * use to notify when google signIn successfull
     *
     * @param signIn return GIDSignIn object
     * @param user return GIDGoogleUser object
     * @param error return Error object
     */
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
       
        self.parentControlelr?.dismiss(animated: true, completion: {
            })
        self.completionBlock!(user,error)
    }
    
    /**
     * use to notify when google signIn failed
     *
     * @param signIn return GIDSignIn object
     * @param user return GIDGoogleUser object
     * @param error return Error object
     */
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        self.parentControlelr?.dismiss(animated: true, completion: {
            
        })
        self.completionBlock!(user,error)

    }
    
    //MARK: GIDSignInUIDelgate
    
    /**
     * use to notify when google signIn controller presented
     *
     * @param signIn return GIDSignIn object
     * @param viewController presenting controller object
     */
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.parentControlelr?.present(viewController, animated: true, completion: {
            
        })
    }
    
    /**
     * use to notify when google signIn controller dismissed
     *
     * @param signIn return GIDSignIn object
     * @param viewController presenting controller object
     */
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        
        self.parentControlelr?.dismiss(animated: true, completion: {
            
        })
    }
    
   
}
