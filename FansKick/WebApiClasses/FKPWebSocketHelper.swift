//
//  FKPWebSocketHelper.swift
//  FanskickPlayer
//
//  Created by Sunil Verma on 27/11/2017.
//  Copyright © 2017 Sunil Verma. All rights reserved.
//

// Needed to install pod for web socket=== >>>>   pod 'Starscream'


import UIKit
import Starscream

//  local url
//let WEB_SOCKET_URL =  "ws://192.77.166.106:8080/"

// Staging url
//let WEB_SOCKET_URL =  "ws://52.77.166.60:8080/"

// PRODUCTION URL
let WEB_SOCKET_URL =  "ws://52.221.153.72:8080/"


let MESSAGE_RECEIVE_NOTIFICATION = "MESSAGE_RECEIVE_NOTIFICATION"
let MESSAGE_CONNECT_NOTIFICATION = "MESSAGE_CONNECT_NOTIFICATION"

class FKPWebSocketHelper: NSObject, WebSocketDelegate {
    
    var socket : WebSocket?
    
    /**
     * use to create the shared instance of the class
     *
     */
    
    class var sharedInstance: FKPWebSocketHelper {
        struct Static {
            static let instance = FKPWebSocketHelper()
        }
        return Static.instance
    }
    
    override private init() {
        super.init()
        self.initialiseSocket()
    }
    
    /**
     * use initialise the resource, This method is one time initalisation throughout the life cycle
     *
     */
    private func  initialiseSocket(){
        
          let url = "\(WEB_SOCKET_URL)?id=\(UserDefaults.standard.value(forKey: kUserId) ?? "")&type=user"
			
			print(" >>>>>>  \(url)")
           let req = URLRequest.init(url: URL(string: url)!)
     //   let req = URLRequest.init(url: URL(string: WEB_SOCKET_URL)!)
        
        socket =   WebSocket(request: req)
        socket?.delegate = self
        
        //socket?.connect()
        
        
        socket?.onConnect = {
            print("websocket is connected")
					NotificationCenter.default.post(name: NSNotification.Name(rawValue: MESSAGE_CONNECT_NOTIFICATION), object: nil, userInfo: nil)

        }
        //websocketDidDisconnect
        socket?.onDisconnect = { (error: Error?) in
            print("websocket is disconnected: \(error?.localizedDescription ?? "")")
        }
        //websocketDidReceiveMessage
        socket?.onText = { (text: String) in
            print("got some text: \(text)")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: MESSAGE_RECEIVE_NOTIFICATION), object: text, userInfo: nil)

        }
        //websocketDidReceiveData
        socket?.onData = { (data: Data) in
            print("got some data: \(data.count)")
        }
        //you could do onPong as well.
        //   socket?.connect()
        
    }
    
    /**
     * use to connect the socket if it is already initialised
     *
     * @warning socket must be initialise before connection
     */
    func connect(){
        socket?.connect()
    }
    
    /**
     * use use to disconnect the socket if it is already connected
     *
     */
    func disconnect(){
        socket?.disconnect()
    }
    
    /**
     * use to check socket connected or not
     *
     * @return TRUE if it is connected else false
     */
    public var isConnected: Bool {
        return (socket?.isConnected)!
    }

    //MARK: Web socket overrides
    
    
    /**
     * use to notify is socket get connected
     *
     * @param socket return the connected socket object
     */
    func websocketDidConnect(socket: WebSocketClient) {
        print("websocketDidConnect")
        
      //  MessageView.showMessage(message: "websocketDidConnect", time: 5.0, verticalAlignment: .top)
    }
    
    
    /**
     * use to notify if socket get disconnected
     *
     * @param socket return the disconnected socket object
     * @param error return the error message
     */
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        print("websocketDidDisconnect", error ?? "")
       // MessageView.showMessage(message: "websocketDidDisconnect", time: 5.0, verticalAlignment: .top)

    }
    
    /**
     * use to notify if socket receive the text message
     *
     * @param socket return the connected socket
     * @param text message that will received
     */
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        //print("websocketDidReceiveMessage  ===  ",text)
       // MessageView.showMessage(message: "websocketDidReceiveMessage", time: 5.0, verticalAlignment: .top)

    }
    
    /**
     * use to notify if socket receive the Data packet
     *
     * @param socket return the connected socket
     * @param Data object that will received
     */
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
       // MessageView.showMessage(message: "websocketDidReceiveData", time: 5.0, verticalAlignment: .top)

     //   print("websocketDidReceiveData  ===  ",data)
    }
    
    
    /**
     * use to send the message through the socket
     *
     * @param data Dictionary object that needed to be send over socket
     */
    func sendData(data:Dictionary<String, Any>){
        
        print("Socket connected   ",socket?.isConnected ?? "xxxxx")
        print("Data send =====>>>>>>>  ",data)
        if !(socket?.isConnected)!{
            socket?.connect()
            return
        }
        socket?.write(string: data.toJsonString())
        
    }
}

