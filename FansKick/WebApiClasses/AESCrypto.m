	//
	//  AESCrypto.m
	//  Fanskick
	//
    //  Created by Sunil Verma on 27/11/2017.
    //  Copyright © 2017 Sunil Verma. All rights reserved.

#import "AESCrypto.h"
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonHMAC.h>
#import <CommonCrypto/CommonHMAC.h>


@implementation NSString (AES256)

-(NSData*) hexToBytes {
	NSMutableData* data = [NSMutableData data];
	int idx;
	for (idx = 0; idx+2 <= self.length; idx+=2) {
		NSRange range = NSMakeRange(idx, 2);
		NSString* hexStr = [self substringWithRange:range];
		NSScanner* scanner = [NSScanner scannerWithString:hexStr];
		unsigned int intValue;
		[scanner scanHexInt:&intValue];
		[data appendBytes:&intValue length:1];
	}
	return data;
}

@end

@implementation NSData (AES256)

	-(NSString *)dataToHex {
	/* Returns hexadecimal string of NSData. Empty string if data is empty.   */
	
	const unsigned char *dataBuffer = (const unsigned char *)[self bytes];
	
	if (!dataBuffer)
		return [NSString string];
	
	NSUInteger          dataLength  = [self length];
	NSMutableString     *hexString  = [NSMutableString stringWithCapacity:(dataLength * 2)];
	
	for (int i = 0; i < dataLength; ++i)
		[hexString appendString:[NSString stringWithFormat:@"%02lx", (unsigned long)dataBuffer[i]]];
	
	return [NSString stringWithString:hexString];
}

@end


@implementation AESCrypto


#pragma AES encryption

+ (NSString *) AES128EncryptString:(NSString*)plaintext withKey:(NSString*)key {
	
	char keyPtr[kCCKeySizeAES128+1]; // room for terminator (unused)
	bzero(keyPtr, sizeof(keyPtr)); // fill with zeroes (for padding)
	
		// fetch key data
	[key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
	
	NSData *data = [plaintext dataUsingEncoding:NSUTF8StringEncoding];
	
	NSUInteger dataLength = [data length];
	
	size_t bufferSize = dataLength + kCCBlockSizeAES128;
	void *buffer = malloc(bufferSize);
	
	size_t numBytesEncrypted = 0;
	CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding,
																				keyPtr, kCCKeySizeAES128,
																				NULL /* initialization vector (optional) */,
																				[data bytes], dataLength, /* input */
																				buffer, bufferSize, /* output */
																				&numBytesEncrypted);
	if (cryptStatus == kCCSuccess) {
			//the returned NSData takes ownership of the buffer and will free it on deallocation
		NSData *encryptedData = [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
		
		return [encryptedData base64EncodedStringWithOptions:kNilOptions] ;
	}
	
	free(buffer); //free the buffer;
    
	return nil;
}

+ (NSString *) AES128DecryptString:(NSString *)ciphertext withKey:(NSString*)key {
	
    if(!ciphertext){
        return @"";
    }
	NSData *data = [[NSData alloc] initWithBase64EncodedString:ciphertext options:kNilOptions];
	
		// 'key' should be 32 bytes for AES256, will be null-padded otherwise
	char keyPtr[kCCKeySizeAES128+1]; // room for terminator (unused)
	bzero(keyPtr, sizeof(keyPtr)); // fill with zeroes (for padding)
	
		// fetch key data
	[key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
	
	NSUInteger dataLength = [data length];
	
	
	size_t bufferSize = dataLength + kCCBlockSizeAES128;
	void *buffer = malloc(bufferSize);
	
	size_t numBytesDecrypted = 0;
	CCCryptorStatus cryptStatus = CCCrypt(kCCDecrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding,
																				keyPtr, kCCKeySizeAES128,
																				NULL /* initialization vector (optional) */,
																				[data bytes], dataLength, /* input */
																				buffer, bufferSize, /* output */
																				&numBytesDecrypted);
	
	if (cryptStatus == kCCSuccess) {
			//the returned NSData takes ownership of the buffer and will free it on deallocation
		NSData *decryptedData = [NSData dataWithBytesNoCopy:buffer length:numBytesDecrypted];
		return [[NSString alloc] initWithData:decryptedData encoding:NSUTF8StringEncoding];
	}
	
	free(buffer); //free the buffer;
	return nil;
}

#pragma AES256

+ (NSString *) AES256EncryptString:(NSString*)plaintext withKey:(NSString*)key {
	
	
	NSLog(@"Message:=  %@   key=: %@",plaintext, key);
	
	NSData *data = [plaintext dataUsingEncoding:NSUTF8StringEncoding];
	
		// 'key' should be 32 bytes for AES256, will be null-padded otherwise
	char keyPtr[kCCKeySizeAES256 + 1]; // room for terminator (unused)
	bzero(keyPtr, sizeof(keyPtr)); // fill with zeroes (for padding)
	
		// fetch key data
	[key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
	
	NSUInteger dataLength = [data length];
	
		//See the doc: For block ciphers, the output size will always be less than or
		//equal to the input size plus the size of one block.
		//That's why we need to add the size of one block here
	size_t bufferSize           = dataLength + kCCBlockSizeAES128;
	void* buffer                = malloc(bufferSize);
	
	size_t numBytesEncrypted    = 0;
	NSString *IV = @"1234567890987654";// [AESCrypto randomPINS:16];
	
	NSData *ivd  = [IV dataUsingEncoding:NSUTF8StringEncoding];
	
	CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding,keyPtr, kCCKeySizeAES256,	[ivd bytes] /* initialization vector (optional) */,	[data bytes], dataLength, /* input */ buffer, bufferSize, /* output */ &numBytesEncrypted);
	
	if (cryptStatus == kCCSuccess) {
		NSData *encryptedData = [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
		NSLog(@" >>>>>>><<<<<  %@",encryptedData);
		
		NSString *encryptedString = [NSString stringWithFormat:@"%@:%@",[ivd dataToHex],[encryptedData dataToHex]];
			//the returned NSData takes ownership of the buffer and will free it on deallocation
		
		NSLog(@" >>>>  %@",encryptedString);
		return encryptedString;
		}
	free(buffer); //free the buffer;
	return nil;
}


+ (NSString*) AES256DecryptString:(NSString *)ciphertext withKey:(NSString*)key {
	
	NSArray *array =	[ciphertext componentsSeparatedByString:@":"];
	NSString *text = [array lastObject];
	NSData *data = [text hexToBytes];
	
		// 'key' should be 32 bytes for AES256, will be null-padded otherwise
	char keyPtr[kCCKeySizeAES256 + 1]; // room for terminator (unused)
	bzero(keyPtr, sizeof(keyPtr)); // fill with zeroes (for padding)
	
		// fetch key data
	[key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
	NSUInteger dataLength = [data length];
	
		//See the doc: For block ciphers, the output size will always be less than or
		//equal to the input size plus the size of one block.
		//That's why we need to add the size of one block here
	size_t bufferSize           = dataLength + kCCBlockSizeAES128;
	void* buffer                = malloc(bufferSize);
	NSString *IV = [array firstObject];
	
	NSData *ivd  = [IV hexToBytes];
	
	size_t numBytesDecrypted    = 0;
	CCCryptorStatus cryptStatus = CCCrypt(kCCDecrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding,
																				keyPtr, kCCKeySizeAES256,
																				[ivd bytes] /* initialization vector (optional) */,
																				[data bytes], dataLength, /* input */
																				buffer, bufferSize, /* output */
																				&numBytesDecrypted);
	
	if (cryptStatus == kCCSuccess) {
		NSData *decryptedStr = [NSData dataWithBytesNoCopy:buffer length:numBytesDecrypted];
			//the returned NSString takes ownership of the buffer and will free it on deallocation
		return [[NSString alloc] initWithData:decryptedStr
																 encoding:NSUTF8StringEncoding];
		}
	
	free(buffer); //free the buffer;
	return nil;
}





#pragma mark SHA

+ (NSString *)SHA256:(NSString *)key {
	
		// Allocate memory for out put
	NSMutableData *shaOut = [NSMutableData dataWithLength:CC_SHA256_DIGEST_LENGTH];
	
	NSData *dataIn  = [key dataUsingEncoding:NSUTF8StringEncoding];
	
		// SHA conversion
	CC_SHA256(dataIn.bytes, (CC_LONG)dataIn.length, shaOut.mutableBytes);
	
	NSString *hash= [shaOut description];
	
		// Remove <, > and spacing
	hash = [hash stringByReplacingOccurrencesOfString:@" " withString:@""];
	hash = [hash stringByReplacingOccurrencesOfString:@"<" withString:@""];
	hash = [hash stringByReplacingOccurrencesOfString:@">" withString:@""];
	
		// Verified at:  http://passwordsgenerator.net/sha256-hash-generator/
	
	return hash;
}


#pragma MD5 hassing

+ (NSString *)MD5:(NSString *)key {
	const char* str = [key UTF8String];
	unsigned char result[CC_MD5_DIGEST_LENGTH];
	CC_MD5(str, (CC_LONG)strlen(str), result);
	
	NSMutableString *ret = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH*2];
	for(int i = 0; i<CC_MD5_DIGEST_LENGTH; i++) {
		[ret appendFormat:@"%02x",result[i]];
	}
	return ret;
	
		// verified at:  http://www.md5.cz/
}


	// use to generate random number of length

+(NSString *)randomPINS:(NSInteger)length
{
	NSMutableString *returnString = [NSMutableString stringWithCapacity:length];
	
	NSString *numbers = @"0123456789";
	
		// First number cannot be 0
	[returnString appendFormat:@"%C", [numbers characterAtIndex:(arc4random() % ([numbers length]-1))+1]];
	
	for (int i = 1; i < length; i++) {
		[returnString appendFormat:@"%C", [numbers characterAtIndex:arc4random() % [numbers length]]];
	}
	
	return returnString;
}

@end

/* Call encryption decryption decrption method like belo
 
 NSString *ec =  [AESCrypto encryptString:@"sunil2" withKey:@"1234567890"];
 
 NSString *dc =  [AESCrypto decryptString:ec withKey:@"1234567890Encryption "];
 
 NSLog(@"   %@       %@",ec,dc);
 
 For MD5
 NSLog(@"......  %@",[AESCrypto MD5:@"Hello"]);
 
 for SHA 256
 
 NSLog(@"  %@    %@ ",ec ,[AESCrypto SHA256:@"Sunil"]);
 
 */
