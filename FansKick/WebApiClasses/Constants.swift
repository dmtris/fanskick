//
//  Constants.swift
//  TravellerApp
//
//  Created by Amritpal Singh on 05/10/16.
//  Copyright © 2016 Amritpal Singh. All rights reserved.
//

import Foundation
import UIKit
import CoreData

public func delay(_ delay:Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}

/*Declare Global Variable for AppDelegate*/
extension UIViewController {
    
    var appDelegate:AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
}

/*Check Device Type*/
struct DeviceType {
    
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X          =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    static let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
}

enum UIUserInterfaceIdiom : Int {
    
    case Unspecified
    case Phone
    case Pad
}

struct ScreenSize {
    
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

let kdateFormatMMDDYY = "MMM dd, yyyy"
let pNotification         = "notification"
let pNotifications        = "notifications"
let pContent              = "content"
let pCreatedAt            = "created_at"

//let kBaseUrl                = "http://192.168.0.140:3000/"
let kBaseUrl                = "http://ec2-52-77-166-60.ap-southeast-1.compute.amazonaws.com/"

let kSignUp                 = "users/sign_up"
let kLogin                  = "users/login"
let kVerifyOTP              = "users/otp_verification"
let kGetStates              = "users/state_list"
let kforgotPw               = "users/forget_password"

let kRating                 = "/rateToProfessional"
let kGetNotifiSettings      = "/getUserPhoneSetting"
let kSetNotifiSettings      = "/setUserPhoneSetting"
let kResendOTP              = "/resendOTP"
let kAddProject             = "/addProject"
let kCategoryListApi        = "/getCategories"
let kStaticContent          = "/staticContent"
let kViewProfile            = "/userProfile"
let kLogout                 = "/logout"
let kLogoutSocket                 = "/logout"
let kCustomerProfile        = "/userProfile"
let kProjectList            = "/getMyProjects"
let kCompletedProjects      = "/getCompletedProjects"
let kScheduleList           = "/getAssignedProjects"
let kProjectDetail          = "/getProjectDetails"
let kReachCust              = "/bidProject"
let kAssignProject          = "/assignProject"
let kMarkComplete           = "/markAsComplete"
let kmapLocation            = "/getProjectsMapView"
let kProjectsContacts       = "/getProjectContacts"
let kGetNotifications       = "/getNotifications"

let kGetContacts            = "/getContacts"
let kUpdatePro            =     "/updateProfile"

let kSuccess = "Success"
let kFailure = "Failure"
let kOAuthToken = "access_token" 
let KisRemember = "isRemember"

//MARK:- Alert messages
let kOTPStatus  = "OTP Verified Successfully"
let kFacebookStatus  = "Facebook Login Successfull"
let kFillAllFields = "Please fill all fields"
let kInvalidEmail  = "Invalid email format"
let kAcceptTerms  = "Please Accept Terms and Conditions"
let kSuccessLogin  = "Congratulations! You have registered successfully. Please Login to Continue."
let kLoginFailure = "Unable to login. Please try again later"
let kRequestFailure = "Request failure, try again."

let kResponseMsg = "responseMessage"
let kResponseCode = "responseCode"
let kMessage = "responseMessage"
let kResponseStatus = "responseCode"
let kUserId = "id"
let kUserRole = "role"
let kSuccessCode = "200"
let kDeviceToken = "deviceToken"
let kPushNotification = "pushNotification"


//login parameters
let kEmail = "emailID"
let kPassword = "password"
let kLoginEmail = "emailID"
let kOldPassword = "oldPassword"


//signUp parameters
let kName = "name"
let kPhone = "phone"
let kCity = "city"
let kState = "state"
let kLat = "latitude"
let kLng = "longitude"
let kLong = "long"
let kZipCode = "zipcode"
let kFirstName = "first_name"
let kSocialID = "SocialID"
let kProfileImage = "profileImage"
let kIsProfessional = "is_professional_user"

//chat history parameter
let kSenderId       = "senderId"
let kReceiverID     = "receiverID"
let kPageNumber     = "pageNumber"

//contact list parameter
let kContactList   = "contactList"

/****************** Validation Alerts ****************************/
let kBlank = ""
let kBlankEmail = "*Please enter email id."
let kValidEmail = "*Please enter valid email ID."
let kBlankPassword = "Please enter password."
let kValidName = "*Please enter valid name."
let kValidPhone = "*Please enter valid phone number."
let kMinPassword = "*Password must have atleast 6 characters."
let kMinZipCode = "*Please enter 6 digit zipcode."
let kConfirmPassword = "*Passwords do no match."
let kMatchPassword = "*Password and confirm password must be same."
let kBlankOtp = "*Please enter OTP."
let kBlankState = "Please select state."

let kBlankConfirmPassword = "*Please enter confirm password."
let kBlank_ZipCode         = "*Please enter zipcode."
let BLANK_EVENTNAME = "Please enter event name."
let VALID_EVENTNAME = "Event name must be of atleast 2 characters."
let BLANK_STARTDATE = "Please enter start date & time."
let BLANK_ENDTIME = "Please enter end time."
let VALID_DESCRIPTION = "Description must be of atleast 2 characters."
let kBlank_Phone = "*Please enter phone number."
let AGREE_TERMS  = "Please agree to terms and conditions."

let BLANK = ""
let BLANK_NAME = "Please enter name."
let PROFILE_UPDATE = "Profile updated successfully."
let BLANK_OTP = "Please enter OTP."
let VALID_OTP = "Please enter valid OTP."
let EVENT_CREATE = "Event created successfully."
let PASSWORD_CHANGE = "Password changed successfully."


let VALID_NAME = "Please enter valid name."
let MINI_NAME = "Name must be of atleast 2 characters."
let BLANK_FIRSTNAME = "Please enter first name."
let MINI_FIRSTNAME = "Name must be of atleast 2 characters."
let BLANK_LASTNAME = "Please enter last name."
let MINI_LASTNAME = "Last name must be of atleast 2 characters."
let BLANK_EMAIL = "Please enter email address."
let MAX_EMAIL = "Email ID has maximum limit of 80 characters."
let BLANK_PASSWORD = "Please enter password."
let MAX_PASSWORD = "password has maximum limit of 16 characters."
let MIN_PASSWORD = "Password must be of atleast 8 characters."

let MIN_OLDPASSWORD = "Old password must be of atleast 8 characters."

let EDIT_DETAIL = "Post updated successfully."

let BLANK_CONFIRMPASSWORD = "Please enter confirm password."
let MIN_CONFIRMPASSWORD = "Confirm password must be of atleast 8 characters."
let MAX_CONFIRMPASSWORD = "Confirm password has maximum limit of 16 characters."
let VALID_FIRSTNAME = "Please enter valid first name."
let VALID_LASTNAME = "Please enter valid last name."
let VALID_EMAIL = "Please enter valid email address."
let BLANK_MOBILENUMBER = "Please enter mobile number."
let MAX_MOBILENUMBER = "Mobile number has maximum limit of atleast 15 characters."
let MINI_MOBILENUMBER = "Mobile number must be of atleast 7  characters."
let MATCH_PASSWORD = "Password and confirm password must be same."
let BLANK_DOB = "Please select date of birth."
let SUCCES = "Success."
let SELECT_IMAGE_SOURCE = "Please select image source."
let CAMERA_NOT_AVAILABELE = "Camera is not available."
let SUCCESS_SIGNUP = "Successfully signed up."

let BLANK_OLD_PASS = "Please enter old password."
let BLANK_NEW_PASS = "Please enter new password."
let MIN_NEW_PASSWORD = " New Password must be of atleast 8 characters."
let BLANK_CONFIRM_PASS = "Please enter confirm password."
let MIN_CONFIRM_PASS = "Confirm password must be of atleast 8 characters."
let BLANK_POSTCODE = "Please enter post code."
let MIN_POSTCODE = "Post code must be of 6 digits."
let BLANK_PINCODE = "Please enter pincode."
let MIN_PINCODE = "Pincode must be of 6 digits."

let BLANK_ADDRESS = "Please enter address."
let MAX_ADDRESS = "Address has maximum limit of atleast 120 characters"

let BLANK_COUNTRY = "Please enter country."
let VALID_COUNTRY = "Please enter valid country name."
let BLANK_SERVICE = "Please enter type of body massage."
let VALID_BODY_MASSAGE = "Please enter valid body massage type."
let BLANK_DATE_BOOK = "Please choose the date of booking."
let BLANK_TITLE = "Please enter title."
let BLANK_DESCRIPTION = "Please enter description."

let BLANK_TEAM = "Please enter team."
let BLANK_ZIPCODE = "Please enter zipcode."
let MIN_TITLE = "Please enter atleast two character of title."
let MAX_TITLE = "Title has maximum limit of atleast 30 characters"
let BLANK_EMAILORMOBILE = "Please enter email address/mobile number."

/*Check Platform*/
struct Platform {
    static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
            isSim = true
        #endif
        return isSim
    }()
}

/*Declare fblogin Manager*/



/*Check for String is Valid Email*/
extension String {
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{1,10}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    var isAlphanumeric: Bool {
        return range(of: "^[a-zA-Z0-9]+$", options: .regularExpression) != nil
    }
    
    func isImageUrl() -> Bool {
        // Add here your image formats.
        let imageFormats = ["jpg", "jpeg", "png", "gif"]
        
        if let ext = self.getExtension() {
            return imageFormats.contains(ext)
        }
        
        return false
    }
    
    public func getExtension() -> String? {
        let ext = (self as NSString).pathExtension
        
        if ext.isEmpty {
            return nil
        }
        
        return ext
    }
}

extension UIImage {
    
    func resizeImage(image: UIImage) -> UIImage {
        
        var actualHeight: Float = Float(image.size.height)
        var actualWidth: Float = Float(image.size.width)
        let maxHeight: Float = 200.0
        let maxWidth: Float = 300.0
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        let compressionQuality: Float = 0.5
        //50 percent compression
        
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        
        let rect = CGRect(x:0.0, y:0.0, width:CGFloat(actualWidth), height:CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = UIImageJPEGRepresentation(img!,CGFloat(compressionQuality))
        UIGraphicsEndImageContext()
        return UIImage(data: imageData!)!
    }
}



