//
//  ApiConstants.swift
//  FansKick
//
//  Created by FansKick-Raj on 13/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

//API Names
let kAPINameLogin           = "v2/customers/auth"
let kAPINameForgotPassword  = "v2/customers/register"
let kAPINameSignup          = "/users/sign_up"

func kAPINameViewDocument(_ document_id: String) -> String {
    return "v1/customers/documents/\(document_id)/download"
}


//Parameters Names

let pImage                                  = "image"
let pTitle                                  = "title"
let pPassword                               = "password"
let paddress                                = "address"
let pstate                                  = "state_id"
let pSubTitle                               = "subtTitle"
let pIcon                                   = "icon"
let pFormattedAddress                       = "formattedAddress"
let pUserId                                 = "userId"
let pEmail                                  = "email"
let pId                                     = "id"
let pToken                                  = "token"
let pUser                                   = "user"
let pContact                                = "contact"
let pName                                   = "name"
let pcode                                   = "code"

let pError                                  = "error"
let pCreatedBy                              = "createdBy"
let pFirstName                              = "firstName"
let pLastName                               = "lastName"
let pData                                   = "data"
let pDeviceToken                            = "device_token"
let pDevice_Type                             = "device_type"
let pDeviceType                             = "deviceType"
let pdevice_id                              = "device_id"
let pdevice                                 = "device"
let pmobile                                 = "mobile"
let pdob                                 = "dob"
let puser                                 = "user"
let pPassword_confirmation                                 = "password_confirmation"


//Other constants
let kDummyDeviceToken                       = "60de1f8d628b3f265b028ab3a69223af2dfc0b56b2671244bb6910b68764e612"
let kDeviceType                             = "deviceType"


class ApiConstants: NSObject {

}
