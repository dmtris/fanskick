//
//  FKSenzPayHelper.swift
//  FansKick
//
//  Created by Sunil Verma on 03/01/2018.
//  Copyright © 2018 FansKick Dev. All rights reserved.
//

import UIKit


let SENZ_PAY_URL = "https://senzpay.asia/api/"


let SENZPAY_USER_ID      = "uid"
let SENZPAY_MESSAGE      = "msg"
let SENZPAY_BALANCE      = "Bal"
let SENZPAY_STATUS       = "status"
let SENZPAY_FNAME        = "first_name"
let SENZPAY_LNAMEE       = "last_name"
let SENZPAY_EMAIL        = "email"
let SENZPAY_PASSWORD     = "password"
let SENZPAY_PHONE        = "phone"
let SENZPAY_DEPOSIT_ID   = "deposit_id"
let SENZPAY_TOPUP_METHOD = "topup_method"


let SENZPAY_EMAIL_DEFAULT        = "SENZPAY_EMAIL_DEFAULT"
let SENZPAY_PASSWORD_DEFAULT     = "SENZPAY_PASSWORD_DEFAULT"


let SENZPAY_TOPUP_METHOD_INFO  =  "topup_method_info"
let SENZPAY_SKIP_SMS_ALERT     =  "skipSMSTopupAlert"
let SENZPAY_TOPUP_DATE         =  "topup_dt"

class FKSenzPayHelper: NSObject {
    class func senzpayRequest(params: [String: Any],
                       method: MethodType,
                       apiName: String,
                       completionBlock: ((AnyObject?, Error?, Int)->())?) {
        
        //>>>>>>>>>>> create request
        let url = requestURL(method, apiName: apiName, parameterDict: params)
        
        var request = URLRequest(url: url)
        request.httpMethod = methodName(method)
        request.timeoutInterval = timeoutInterval
        
        let bodyData = body(method, parameterDict: params)
        request.httpBody = bodyData
        
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
//        if UserDefaults.standard.value(forKey: kOAuthToken) != nil{
//            request.addValue(UserDefaults.standard.value(forKey: kOAuthToken) as! String, forHTTPHeaderField: "AUTHTOKEN")
//        }
        
        
        Debug.log("\n\n Request URL  >>>>>>\(url)")
        Debug.log("\n\n Request Header >>>>>> \n\(request.allHTTPHeaderFields.debugDescription)")
        Debug.log("\n\n Request Method  >>>>>>\(request.httpMethod ?? "")")

        //Debug.log("Content-Length >>> \(String (jsonData.count))")
        Debug.log("\n\n Request Parameters >>>>>>\n\(params.toJsonString())")
        //logInfo("\n\n Request Body  >>>>>>\(request.HTTPBody)")
        
        print("request start time with api name:>>>>> ",apiName," ",Date())
        
        request.performSenzpay() { (responseObject: AnyObject?, error: Error?, httpResponse: HTTPURLResponse?) in
            
            print("request response time with api name:>>>>> ",apiName," ",Date())
            
            Debug.log("RESPONSE:  \(String(describing: responseObject))")
            
            guard let block = completionBlock else {
                return
            }
            
            DispatchQueue.main.async(execute: {
                
                guard let httpResponse = httpResponse else {
                    block(responseObject, error, 9999)
                    return
                }
                
                if httpResponse.statusCode  == 200{
                    
                    if responseObject != nil {
                        block(responseObject, error, httpResponse.statusCode )
                        
                    } else {
                        block(responseObject, error, httpResponse.statusCode)
                    }
                }else{
                    MessageView.showMessage(message: "Unable to process your request, please try after some time", time: 4.0, verticalAlignment: .bottom)
                    
                    block(responseObject, error, httpResponse.statusCode)
                }
            })
        }
    }
    
    //MARK:- Private Functions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    class fileprivate func methodName(_ method: MethodType)-> String {
        
        switch method {
        case .get: return "GET"
        case .post: return "POST"
        case .delete: return "DELETE"
        case .put: return "PUT"
        case .patch: return "PATCH"
            
        }
    }
    
    class fileprivate func body(_ method: MethodType, parameterDict: [String: Any]) -> Data {
        
        // Create json with your parameters
        switch method {
        case .post: fallthrough
        case .patch: fallthrough
        case .delete: fallthrough
        case .put: return parameterDict.formData()
        case .get: fallthrough
            
        default: return Data()
        }
    }
    
    class fileprivate func requestURL(_ method: MethodType, apiName: String, parameterDict: [String: Any]) -> URL {
        
        let urlString = SENZ_PAY_URL + apiName
        switch method {
        case .get:
            return getURL(apiName, parameterDict: parameterDict)
            
        case .post: fallthrough
        case .put: fallthrough
        case .patch: fallthrough
        case .delete: fallthrough
        default: return URL(string: urlString)!
        }
    }
    
    class fileprivate func getURL(_ apiName: String, parameterDict: [String: Any]) -> URL {
        
        var urlString = SENZ_PAY_URL + apiName
        var isFirst = true
        
        for key in parameterDict.keys {
            
            let object = parameterDict[key]
            
            if object is NSArray {
                
                let array = object as! NSArray
                for eachObject in array {
                    var appendedStr = "&"
                    if (isFirst == true) {
                        appendedStr = "?"
                    }
                    urlString += appendedStr + (key) + "=" + (eachObject as! String)
                    isFirst = false
                }
                
            } else {
                var appendedStr = "&"
                if (isFirst == true) {
                    appendedStr = "?"
                }
                let parameterStr = parameterDict[key] as! String
                urlString += appendedStr + (key) + "=" + parameterStr
            }
            
            isFirst = false
        }
        
        let strUrl = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        //let strUrl = urlString.addingPercentEscapes(using: String.Encoding.utf8)
        
        return URL(string:strUrl!)!
    }
    

}

extension URLRequest  {
    
    
    func performSenzpay(completionBlock: @escaping (AnyObject?, Error?, HTTPURLResponse?) -> Void) -> Void {
        
        //hud_type = hudType
        if (APPDELEGATE.isReachable == false) {
            AlertController.alert(title: "Connection Error!", message: NO_INTERNET_CONNECTION)
            let err  =  NSError.init(domain: "com.senzpay", code: 100, userInfo: nil) as Error
            completionBlock(nil, err, nil)
            return
        }
        
        
        let config = URLSessionConfiguration.default // Session Configuration
        let session = URLSession(configuration: config) // Load configuration into Session
        //var session = URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
        
        let task = session.dataTask(with: self, completionHandler: {
            (data, response, error) in
            
            
            if let error = error {
                Debug.log("\n\n error  >>>>>>\n\(error)")
                completionBlock(nil, error, nil)
            } else {
                
                let httpResponse = response as! HTTPURLResponse
                let responseCode = httpResponse.statusCode
                
                //let responseHeaderDict = httpResponse.allHeaderFields
                //Debug.log("\n\n Response Header >>>>>> \n\(responseHeaderDict.debugDescription)")
                Debug.log("Response Code : \(responseCode))")
                
                if let responseString = NSString.init(data: data!, encoding: String.Encoding.utf8.rawValue) {
                    Debug.log("Response String : \n \(responseString)")
                }
                
                do {
                    let result = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                    //Debug.log("\n\n result  >>>>>>\n\(result)")
                    completionBlock(result as AnyObject?, nil, httpResponse)
                } catch {
                    
                    Debug.log("\n\n error in JSONSerialization")
                    Debug.log("\n\n error  >>>>>>\n\(error)")
                    
                        AlertController.alert(title: "", message: "Something went wrong. Please try after some time.")
                        completionBlock(nil, error, httpResponse)
                }
            }
        })
        
        task.resume()
    }
}



/* Format the request
 
 var params = Dictionary<String,Any>()
 params[SENZPAY_EMAIL] = "nitin.agnihotri@mobiloittegroup.com"
 params[SENZPAY_PASSWORD] = "qqqqqqqq1"
 
 params[SENZPAY_PHONE] = "60146318751"
 params[SENZPAY_FNAME] = "Nitin"
 params[SENZPAY_LNAMEE] = "Agnihotri"
 
 
 //user_login.php
 //"user_signup.php"
 FKSenzPayHelper.senzpayRequest(params: params, method: .post, apiName: "user_signup.php") { (result, error, responseCode) in
 print(result ?? "")
 
 print(responseCode)
 
 let responseData = result as! Dictionary<String,AnyObject>
 
 let status        = responseData.validatedValue(SENZPAY_STATUS, expected:"" as AnyObject ) as! String
 
 if status == "false" {
 // false case error messsage
 MessageView.showMessage(message: responseData["msg"] as! String, time: 5.0, verticalAlignment: .bottom)
 }else {
 // handle true response
 defaults.set(responseData.validatedValue(SENZPAY_USER_ID, expected:"" as AnyObject ) as! String, forKey: SENZPAY_USER_ID)
 }
 
 
 }
 
 return
 */


