//
//  ServerRequest.swift
//  DemoMVC
//
//  Created by Anuradha Sharma on 03/02/17.
//  Copyright © 2017 Anuradha Sharma. All rights reserved.
//

import UIKit

class ServerRequest: NSObject
{
    static let sharedInstance = ServerRequest()
    
    func connectToServerWithRequest(url : String, returnSelector : Selector, returnDelegate : AnyObject,parameters : [String : Any], requestType : String)
    {
        var postData = Data()
        do {
            postData = try JSONSerialization.data(withJSONObject: parameters, options: [])
        } catch let error {
            print(error.localizedDescription)
        }
        print("url is: ",url)
        print(parameters)
        
        let urlString = NSURL(string: url)!
        let request = NSMutableURLRequest(url: urlString as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 60.0)
        request.httpMethod = requestType
        if requestType == "POST" {
            request.httpBody = postData as Data
        }	
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/fanskick.com; version=1", forHTTPHeaderField: "Accept")

//        let authStr = basicAuthUserName + ":" + basicAuthPassword
//        let authData = authStr.data(using: .ascii)
//        let authValue = "Basic " + (authData?.base64EncodedString(options: .lineLength64Characters))!
//        request.addValue(authValue, forHTTPHeaderField: "Authorization")

        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            DispatchQueue.main.async {
                if (error != nil) {
                    print(error!)
                    let responseDict = ["status" : "0", kMessage : "Server not responding please try again later."]
                    returnDelegate.performSelector(onMainThread: returnSelector, with: responseDict, waitUntilDone: true)
                } else {
                    let httpResponse = response as? HTTPURLResponse
                    if httpResponse?.statusCode == 200
                    {
                        do {
                            let responseDict = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]
                            if let JSON = responseDict
                            {
                                print(JSON)
                                returnDelegate.performSelector(onMainThread: returnSelector, with: JSON, waitUntilDone: true)
                            }
                            else
                            {
                                let responseDict = ["status" : "0" , kMessage : kRequestFailure]
                                returnDelegate.performSelector(onMainThread: returnSelector, with: responseDict, waitUntilDone: true)
                            }
                        } catch
                        {
                            let responseDict = ["status" : "0", kMessage : kRequestFailure]
                            returnDelegate.performSelector(onMainThread: returnSelector, with: responseDict, waitUntilDone: true)
                            print(error.localizedDescription)
                        }
                    }
                    else
                    {
                        print("httpResponse?.statusCode:- ",httpResponse?.statusCode ?? "no code")
                        let responseDict = ["status" : "0", kMessage : kRequestFailure]
                        returnDelegate.performSelector(onMainThread: returnSelector, with: responseDict, waitUntilDone: true)
                    }
                }
            }
        })
        dataTask.resume()
    }
}
