//
//  AESCrypto.h
//  Fanskick
//
//  Created by Sunil Verma on 27/11/2017.
//  Copyright © 2017 Sunil Verma. All rights reserved.

#import <Foundation/Foundation.h>

static NSString *cryptoKey = @"H1!2@3#7&Y";

static NSString *crypto256Key = @"9D7A7A5F8A49DBD4761D1DB999CC9343";

static NSString *crypto128Key = @"9D7A7A5F8A49DBD4761D1DB999CC9343";


@interface AESCrypto : NSObject

/**
 * use to encrypt the string by using key
 *
 * @param plaintext Source string which needed to encrypt
 * @param key Source  string by which string going to encrypt
 * @return encrypted cipher text
 */
+ (NSString *) AES128EncryptString:(NSString*)plaintext withKey:(NSString*)key;

/**
 * use to decrypt the string by using key
 *
 * @param ciphertext Source string which needed to decrypt
 * @param key Source  string by which string going to decrypt
 * @return encrypted plain text
 */
+ (NSString *) AES128DecryptString:(NSString *)ciphertext withKey:(NSString*)key;

/**
 * use for sha256 hashing the text
 *
 * @param key Source string which need apply hashing 256
 * @return hashed text
 */
+ (NSString *)SHA256:(NSString *)key;

/**
 * use for MD5 hashing
 *
 * @param key Source string which need apply MD5
 * @return hashed text
 */
+ (NSString *)MD5:(NSString *)key;


/**
 * use to encrypt the string by using key
 *
 * @param plaintext Source string which needed to encrypt
 * @param key Source  string by which string going to encrypt
 * @return encrypted 256 cipher text
 */
+ (NSString *) AES256EncryptString:(NSString*)plaintext withKey:(NSString*)key ;

/**
 * use to decrypt the string by using key
 *
 * @param ciphertext Source string which needed to decrypt
 * @param key Source  string by which string going to decrypt
 * @return encrypted plain text
 */
+ (NSString*) AES256DecryptString:(NSString *)ciphertext withKey:(NSString*)key;

/**
 * use to generate random number with particular length
 *
 * @param length length of pin
 * @return plain text
 */
+(NSString *)randomPINS:(NSInteger)length;

@end
