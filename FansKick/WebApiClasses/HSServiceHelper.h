//
//  ServiceHelper.h
//  Helposity
//
//  Created by Sunil Verma on 02/02/17.
//  Copyright © 2017 Mobiloitte. All rights reserved.
//

#import <Foundation/Foundation.h>


static float SCROLLUPREFRESHHEIGHT = 40.0;

typedef struct {
    int  pageIndex;
    int totalPage;
} PAGE;



typedef void(^RequestComplitopnBlock)(id result, NSError  *error);

@interface HSServiceHelper : NSObject

+(HSServiceHelper *)sharedServiceHelper;

	// use to cancel get request session
-(void)cancelGetRequestSession;

	// use to cancel post request session
-(void)cancelPostRequestSession;

	// use to cancel put request
-(void)cancelPutRequestSession;


/**
 * use to make multipart post request api call
 *
 * @param parameterDict request params dictionary which need to be send in multipart request
 * @param apiName  receive the end point of the URI
 * @param block  call back block
 */
-(void)multipartApiCallWithParameter:(NSMutableDictionary *)parameterDict methodtype:(NSString *)methodtype apiName:(NSString *)apiName  WithComptionBlock:(RequestComplitopnBlock)block;

@end
