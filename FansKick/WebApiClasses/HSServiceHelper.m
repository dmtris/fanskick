//
//  ServiceHelper.h
//  Helposity
//
//  Created by Sunil Verma on 02/02/17.
//  Copyright © 2017 Mobiloitte. All rights reserved.
//
#import "HSServiceHelper.h"
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>




@interface NSDictionary (NullChecker)

-(id)objectForKeyNotNull:(id)object expectedObj:(id)obj;
-(id)objectForKeyNotNull:(id)key;

+(id)dictionaryWithContentsOfJSONURLData:(NSData *)JSONData;
-(NSData*)toJSON;
-(NSString*) JSONString;
@end


@implementation NSDictionary (NullChecker)


+ (id)dictionaryWithContentsOfJSONURLData:(NSData *)JSONData
{
    __autoreleasing NSError* error = nil;
    if(JSONData == nil) {
        return [NSDictionary dictionary];
        
    }
    id result = [NSJSONSerialization JSONObjectWithData:JSONData options:kNilOptions error:&error];
    if (error != nil) return nil;
    return result ;
}

- (NSData*)toJSON
{
    __autoreleasing NSError* error = nil;
    id result = [NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:&error];
    
    
    if (error != nil) return nil;
    return result;
}


-(NSString*) JSONString {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:0
                                                         error:&error];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}


- (id)objectForKeyNotNull:(id)key expectedObj:(id)obj {
    
    id object = [self objectForKey:key];
    
    if (object == nil) {
        return obj;
    }
    if (object == [NSNull null])
        return obj;
    
    if ([object isKindOfClass:[NSNumber class]]) {
        CFNumberType numberType = CFNumberGetType((CFNumberRef)object);
        if (numberType == kCFNumberFloatType || numberType == kCFNumberDoubleType || numberType == kCFNumberFloat32Type || numberType == kCFNumberFloat64Type) {
            return [NSString stringWithFormat:@"%f",[object floatValue]];
            
        }else{
            return [NSString stringWithFormat:@"%ld",(long)[object integerValue]];
            
        }
    }
    return object;
}

- (id)objectForKeyNotNull:(id)key {
    id object = [self objectForKey:key];
    if([object isKindOfClass:[NSString class]]){
        if ([object isEqualToString:@"<null>"]||[object isEqualToString:@"(null)"]) {
            return @"";
        }
    }
    
    if (object == nil) {
        return @"";
    }
    if (object == [NSNull null])
        return @"";
    return object;
}

@end

#warning needed to check url at the time of ipa release

NSString *const NO_INTERNATE_CONNECTION = @"The internet connection appears to be offline.";

// Local url
//static NSString *BASE_URL = @"http://172.16.16.229:8080/";

//Staging url
//static NSString *BASE_URL = @"http://ec2-52-77-166-60.ap-southeast-1.compute.amazonaws.com/";

// PRODUCTION URL
static NSString *BASE_URL = @"https://admin.fanskick.com/";
@interface HSServiceHelper()<NSURLSessionDelegate, NSURLSessionTaskDelegate>
{
    NSUInteger responseCode;
    NSURLSession *getRequestSession;
    NSURLSession *postRequestSession;
    NSURLSession *putRequestSession;
    
}

@property (nonatomic, strong) NSMutableData *downLoadedData;

@end

static HSServiceHelper *serviceHelper = nil;

@implementation HSServiceHelper

+ (id)sharedServiceHelper
{
    
    static dispatch_once_t onceToken;
    
    dispatch_once (&onceToken, ^{
        serviceHelper = [[HSServiceHelper alloc] init];
    });
    
    return serviceHelper;
}

- (void)cancelGetRequestSession
{
    [getRequestSession invalidateAndCancel];
}

- (void)cancelPostRequestSession
{
    [postRequestSession invalidateAndCancel];
}

- (void)cancelPutRequestSession
{
    [putRequestSession invalidateAndCancel];
}

#pragma mark multipartApiCallWithParameter
- (void)multipartApiCallWithParameter:(NSMutableDictionary *)parameterDict methodtype:(NSString *)methodtype apiName:(NSString *)apiName  WithComptionBlock:(RequestComplitopnBlock)block
{
    NSMutableString *urlString = [NSMutableString stringWithString:BASE_URL];
    
    [urlString appendFormat:@"%@",apiName];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    [request setHTTPMethod:methodtype];
    
    [request setValue:@"application/fanskick.com; version=1" forHTTPHeaderField:@"Accept"];
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"access_token"] != nil){
        [request setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"access_token"] forHTTPHeaderField:@"AUTHTOKEN"];
    }
    
    NSLog(@"URL   %@    %@",urlString, parameterDict);
    
    NSString *boundary = @"14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    [parameterDict enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        
        if ([obj isKindOfClass:[NSData class]]) {
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition:form-data; name=\"%@\"; filename=\"%@\"\r\n",key,@"file.jpeg"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:obj];
        }else
        {
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",key] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@",obj] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
    }];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    
    RequestComplitopnBlock completionBlock = [block copy];
        
    
   
//       if(![APPDELEGATE isReachable])
//           {
//
//               // [RequestTimeOutView showWithMessage:NO_INTERNATE_CONNECTION forTime:3.0];
//           block(nil,  [NSError errorWithDomain:@"com.helpsity" code:100 userInfo:nil]);
//
//           return;
//           }
    
    // Setup the session
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPAdditionalHeaders = @{
                                                   @"Content-Type"  : [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary]
                                                   };
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
    
    NSURLSessionDataTask *uploadTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            // success response
            
            //NSHTTPURLResponse *res = (NSHTTPURLResponse *)response;
            
            //NSString *responseStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            id result = [NSDictionary dictionaryWithContentsOfJSONURLData:data];
            
            if ([[result objectForKeyNotNull:@"responseCode"] integerValue] == 501){
                dispatch_async(dispatch_get_main_queue(), ^{
                    completionBlock(result, [NSError errorWithDomain:@"com.fanskick" code:100 userInfo:nil]);
                    
                    
                });
            }else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    completionBlock(result, error);
                    
                });
            }
            
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                completionBlock(nil, error);
                
            });
        }
    }];
    [uploadTask resume];
}


// use to get mime Type
- (NSString *)mimeTypeByGuessingFromData:(NSData *)data {
    
    char bytes[12] = {0};
    [data getBytes:&bytes length:12];
    
    const char bmp[2] = {'B', 'M'};
    const char gif[3] = {'G', 'I', 'F'};
    //    const char swf[3] = {'F', 'W', 'S'};
    //    const char swc[3] = {'C', 'W', 'S'};
    const char jpg[3] = {0xff, 0xd8, 0xff};
    const char psd[4] = {'8', 'B', 'P', 'S'};
    const char iff[4] = {'F', 'O', 'R', 'M'};
    const char webp[4] = {'R', 'I', 'F', 'F'};
    const char ico[4] = {0x00, 0x00, 0x01, 0x00};
    const char tif_ii[4] = {'I','I', 0x2A, 0x00};
    const char tif_mm[4] = {'M','M', 0x00, 0x2A};
    const char png[8] = {0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a};
    const char jp2[12] = {0x00, 0x00, 0x00, 0x0c, 0x6a, 0x50, 0x20, 0x20, 0x0d, 0x0a, 0x87, 0x0a};
    
    
    if (!memcmp(bytes, bmp, 2)) {
        return @"image/x-ms-bmp";
    } else if (!memcmp(bytes, gif, 3)) {
        return @"image/gif";
    } else if (!memcmp(bytes, jpg, 3)) {
        return @"image/jpeg";
    } else if (!memcmp(bytes, psd, 4)) {
        return @"image/psd";
    } else if (!memcmp(bytes, iff, 4)) {
        return @"image/iff";
    } else if (!memcmp(bytes, webp, 4)) {
        return @"image/webp";
    } else if (!memcmp(bytes, ico, 4)) {
        return @"image/vnd.microsoft.icon";
    } else if (!memcmp(bytes, tif_ii, 4) || !memcmp(bytes, tif_mm, 4)) {
        return @"image/tiff";
    } else if (!memcmp(bytes, png, 8)) {
        return @"image/png";
    } else if (!memcmp(bytes, jp2, 12)) {
        return @"image/jp2";
    }
    
    return @"application/octet-stream"; // default type
    
}

#pragma mark NSURLSessionDelegate
- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(nullable NSError *)error
{
    NSLog(@"");
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
 completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * __nullable credential))completionHandler
{
    NSLog(@"");
    
    completionHandler(NSURLSessionAuthChallengeUseCredential, [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]);
}

- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session
{
    NSLog(@"");
}

#pragma mark NSURLSessionTaskDelegate

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
willPerformHTTPRedirection:(NSHTTPURLResponse *)response
        newRequest:(NSURLRequest *)request
 completionHandler:(void (^)(NSURLRequest * __nullable))completionHandler
{
    NSLog(@"");
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
 completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * __nullable credential))completionHandler
{
    NSLog(@"");
    completionHandler(NSURLSessionAuthChallengeUseCredential, [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]);
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
 needNewBodyStream:(void (^)(NSInputStream * __nullable bodyStream))completionHandler
{
    NSLog(@"");
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
   didSendBodyData:(int64_t)bytesSent
    totalBytesSent:(int64_t)totalBytesSent
totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend
{
    NSLog(@"");
    
}
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
didCompleteWithError:(nullable NSError *)error
{
    NSLog(@"");
}
@end
