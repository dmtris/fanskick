//
//  ButtonExtension.swift
//  FansKick
//
//  Created by FansKick-Raj on 11/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

extension UIButton {

    func underLine(state: UIControlState = .normal) {
        
        if let title = self.title(for: state) {
            
            let color = self.titleColor(for: state)

            let attrs = [
                NSAttributedStringKey.foregroundColor.rawValue : color ?? UIColor.blue,
                NSAttributedStringKey.underlineStyle : 1] as [AnyHashable : Any]
            
            let buttonTitleStr = NSMutableAttributedString(string: title, attributes: (attrs as! [NSAttributedStringKey : Any]))
            self.setAttributedTitle(buttonTitleStr, for: state)
            
        }
    }
    
    func normalLoad(_ string:String) {
        
        if let url = URL(string: string) {
            //self.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder")!, options: .refreshCached)
            self.sd_setImage(with: url, for: .normal, completed: nil)
        } else {
            self.setImage(UIImage(named: "placeholder")!, for: .normal)
        }
    }
}

