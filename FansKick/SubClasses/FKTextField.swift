//
//  FKTextField.swift
//  FansKick
//
//  Created by FansKick-Sunil on 31/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//


import UIKit

class FKTextField: UITextField {
    var indexPath:IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.leftPadding(width: 5, image: nil)
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    func leftPaddingTitile(width:CGFloat, title:String?)
    {
        let lbl = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: width, height: self.frame.height))
       
        lbl.backgroundColor = UIColor.clear
        if title != nil {
        lbl.text = title
        }
        lbl.textColor = self.textColor
        self.leftView = lbl
        self.leftViewMode = UITextFieldViewMode.always
    }
    

    func leftPadding(width:CGFloat, image:UIImage?)
    {
        let btn = UIButton.init(type: UIButtonType.custom)
        btn.frame = CGRect.init(x: 0, y: 0, width: width, height: self.frame.height)
        btn.backgroundColor = UIColor.clear
        btn.isUserInteractionEnabled = false
        if image != nil {
            btn.setImage(image, for: UIControlState.normal)
        }
        self.leftView = btn
        self.leftViewMode = UITextFieldViewMode.always
    }
    
    func rightPadding(width:CGFloat, image:UIImage?){
        
        let btn = UIButton.init(type: UIButtonType.custom)
        btn.frame = CGRect.init(x: 0, y: 0, width: width, height: self.frame.height)
        btn.backgroundColor = UIColor.clear
        btn.isUserInteractionEnabled = false
        if image != nil {
            btn.setImage(image, for: UIControlState.normal)
        }
        self.rightView = btn
        self.rightViewMode = UITextFieldViewMode.always
        
    }
    
}
