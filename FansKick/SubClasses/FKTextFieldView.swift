//
//  FKTextFieldView.swift
//  FansKick
//
//  Created by FansKick-Sunil on 31/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit
import CoreGraphics


@objc  protocol FKTextFieldDelegate: NSObjectProtocol{
    
    @objc optional  func fkTextFieldDidBeginEditing(textField: FKTextFieldView)
    @objc optional func fkTextFieldShouldReturn(textField: FKTextFieldView) -> Bool
    @objc optional func fkTextFieldDidEndEditing(textField:FKTextFieldView)
    @objc optional  func textField(textField: FKTextFieldView, fkShouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool // return NO to not change text
    
}


class FKTextFieldView: UIView,UITextFieldDelegate {
    
    // orivate property
    private var txtField:FKTextField?
    private var titleLbl:UILabel?
    private var errorLbl:UILabel?
    private var iconBtn:UIButton?
    private var bgView:UIView?



    private var isInitialised:Bool? = false
    private var font:UIFont? = UIFont.init(name: "Gilroy-Light", size: 15)
    private var textAlignment:NSTextAlignment? = .left
    private var returnKeyType : UIReturnKeyType? = .default
    private var autocapitalizationType:UITextAutocapitalizationType? = .sentences // default is UITextAutocapitalizationTypeSentences
    private var autocorrectionType: UITextAutocorrectionType? = .default // default is UITextAutocorrectionTypeDefault
    private var spellCheckingType:UITextSpellCheckingType? = .default// default is UITextSpellCheckingTypeDefault;
    private var secureTextEntry:Bool? = false
    private var keyboardType: UIKeyboardType? = .default
    private var showPasswordEnable:Bool? = false
    private var isHighlighted:Bool? = false
    private var fontIcon:String? = ""

    
    // Public property
    var indexPath:IndexPath?
    weak var delegate: FKTextFieldDelegate? // default is nil. weak reference
    
    @IBInspectable var backgroumdColor: UIColor? {
        get { return self.backgroundColor }
        set { self.backgroundColor = newValue }
    }
    @IBInspectable var textColor: UIColor = UIColor.white{
        didSet{
            self.txtField?.textColor = textColor
        }
    }
    
    @IBInspectable var  maxLength : Int = 0

    @IBInspectable var placeholderColor: UIColor = UIColor.white
    @IBInspectable var errorTextColor: UIColor = UIColor.red
    @IBInspectable var text: String = ""
    @IBInspectable var placeHolder: String = ""
    @IBInspectable var errorMessage: String = ""

    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = self.backgroumdColor;
        self.isHighlighted = false;
        self.isInitialised = false;
        
    }
    
    override func layoutSubviews() {
        if !isInitialised! {
            self.isInitialised = true
            self.initializeField()
        }
    }
    
    func attributtedPlaceholder() -> NSAttributedString{
        
        let placeholder = NSMutableAttributedString.init(string: self.placeHolder)
        placeholder.addAttribute(NSAttributedStringKey.foregroundColor, value: self.placeholderColor.withAlphaComponent(1.0), range: NSRange.init(location: 0, length:self.placeHolder.count))
        
        return placeholder;
    }
    func initializeField() {
        
        let leftTextPadding = 30.0 as CGFloat
        
        self.bgView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.frame.width, height: 49))
        
        self.bgView?.backgroundColor = UIColor.RGB(r: 48, g: 92, b: 44, alpha: 0.3)
        
        self.addSubview(self.bgView!)
        self.iconBtn = UIButton.init(type: .custom)
        self.iconBtn?.frame = CGRect.init(x: 5, y: 20, width: 30, height: 30)
        self.iconBtn?.backgroundColor = UIColor.clear
        
        self.iconBtn?.titleLabel?.font = UIFont.init(name: fanskickFontIcons, size: 22);
        
        self.iconBtn?.setTitle(fontIcon, for: .normal)
        self.addSubview(self.iconBtn!)
        self.txtField  = FKTextField.init(frame: CGRect.init(x: leftTextPadding, y: 20, width: self.frame.width - leftTextPadding , height: 30))
        self.txtField?.borderStyle = .none
        self.txtField?.backgroundColor = UIColor.init(white: 0.0, alpha: 0.0)
        self.txtField?.attributedPlaceholder = self.attributtedPlaceholder()
            
        self.txtField?.delegate = self
        self.txtField?.font = self.font
        self.txtField?.textColor = self.textColor
        self.txtField?.returnKeyType = self.returnKeyType!
        self.txtField?.autocorrectionType = self.autocorrectionType!
        self.txtField?.isSecureTextEntry = self.secureTextEntry!
        self.txtField?.spellCheckingType = self.spellCheckingType!
        self.txtField?.font = self.font
        self.txtField?.textAlignment = self.textAlignment!
        self.txtField?.autocapitalizationType = self.autocapitalizationType!
        self.txtField?.keyboardType = self.keyboardType!
        self.txtField?.text = self.text;

        
        if (self.showPasswordEnable)! {
            self.setShowPasswordOption()
        }
        self.leftPadding()
        self.addSubview(txtField!)
        
        // title lable initialization
        
        self.titleLbl = UILabel.init(frame: CGRect.init(x: leftTextPadding, y: 0, width: (txtField?.frame.width)!, height: 20))
        self.titleLbl?.backgroundColor = UIColor.clear
        self.titleLbl?.textColor = self.placeholderColor
        self.titleLbl?.font = self.font
        self.addSubview(self.titleLbl!)
        
        
        self.errorLbl = UILabel.init(frame: CGRect.init(x: leftTextPadding, y: 50, width: (txtField?.frame.width)!, height: 20))
        self.errorLbl?.backgroundColor = UIColor.clear
        self.errorLbl?.textColor = self.placeholderColor
        self.errorLbl?.font = self.font
        self.addSubview(self.errorLbl!)
        
        // use to change background color
        self.backgroumdColor = UIColor.clear
    }
    

    func setFKPlaceHolder(placeHolder:String){
        self.placeHolder = placeHolder
        
        if !self.text.isEmpty{
            self.setAttributtedTitle(isError: false)
        }else{
            self.txtField?.attributedPlaceholder = self.attributtedPlaceholder()
        }
    }
    
    func setFKPlaceHolderMessage(placeHolder:String){
        self.placeHolder = placeHolder
        self.setAttributtedTitle(isError: false)
    }
    
    func setFKErrorMessage(message:String){
        self.errorMessage = message
        self.setAttributtedTitle(isError: true)
    }
    func setFKFont(font:UIFont){
        self.txtField?.font = font
        self.font = font
    }
    func FKFont()-> UIFont{
        return (self.txtField?.font)!
    }
    
    
    func setFKTextColor(textColor:UIColor){
        self.txtField?.textColor = textColor
    }
    
    func setFKTextAlignment(textAlignment: NSTextAlignment){
        self.txtField?.textAlignment = textAlignment
        self.textAlignment = textAlignment
    }
    
    func FKTextAlignment() -> NSTextAlignment{
        return self.textAlignment!
    }
    
    func setFKText(fkText:String){
        self.txtField?.text = fkText
        self.text = fkText;
    }
    
    func setFKReturnKeyType(returnKeyType:UIReturnKeyType){
        self.txtField?.returnKeyType = returnKeyType
        self.returnKeyType = returnKeyType;
    }
    
    func FKReturnKeyType() -> UIReturnKeyType{
        return self.returnKeyType!;
    }
    
    func setFKAutocapitalizationType(autocapitalizationType:UITextAutocapitalizationType){
        self.txtField?.autocapitalizationType = autocapitalizationType
        self.autocapitalizationType = autocapitalizationType
    }
    func setFKAutocorrectionType(autocorrectionType:UITextAutocorrectionType){
        self.txtField?.autocorrectionType = autocorrectionType
        self.autocorrectionType = autocorrectionType
    }
    func setFKSpellCheckingType(spellCheckingType:UITextSpellCheckingType){
        self.txtField?.spellCheckingType  = spellCheckingType
        self.spellCheckingType = spellCheckingType;
    }
    func setFKSecureTextEntry(secureTextEntry:Bool){
        self.txtField?.isSecureTextEntry =  secureTextEntry
        self.secureTextEntry = secureTextEntry;
    }
    func FKSecureTextEntry() -> Bool{
        return self.secureTextEntry!;
    }
    
    func setFKKeyboardType(keyboardType:UIKeyboardType){
        self.txtField?.keyboardType = keyboardType
        self.keyboardType = keyboardType;
    }
    
    func FKKeyboardType() -> UIKeyboardType{
        return  self.keyboardType!;
    }
    
    func setAttributtedTitle(isError:Bool){
        var str = ""
        self.errorLbl?.text = ""
        if !(self.placeHolder.isEmpty) {
            str.append(self.placeHolder)
        }
        if isError {
            if !(self.errorMessage.isEmpty){
                str = ""
                str.append(self.errorMessage)
                let  attribute = NSMutableAttributedString.init(string: str)
                attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: self.errorTextColor, range: NSRange.init(location: 0, length:str.count))
                self.errorLbl?.attributedText  = attribute
                self.isHighlighted = true
                self.setNeedsDisplay()
            }
        }else {
            
           // if !self.text.isEmpty{
            
            let  attribute = NSMutableAttributedString.init(string: str)
            self.titleLbl?.attributedText  = attribute
           // }
        }
    }
    
    func leftPadingFontIcon(iconName:String){
        
        if self.iconBtn != nil{
            self.fontIcon = iconName
            self.iconBtn?.setTitle(iconName, for: .normal)
        }
    }
    
    private func leftPadding(){
        let  paddingLbl = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: 10, height: (self.txtField?.frame.height)!))
        paddingLbl.backgroundColor = UIColor.clear
        self.txtField?.leftView = paddingLbl
        self.txtField?.leftViewMode = .always
    }
    func leftPadding(width:CGFloat, image:UIImage?){
        let btn = UIButton.init(type: UIButtonType.custom)
        btn.frame = CGRect.init(x: 0, y: 0, width: width, height: self.frame.height)
        btn.backgroundColor = UIColor.clear
        btn.isUserInteractionEnabled = false
        if image != nil {
            btn.setImage(image, for: UIControlState.normal)
        }
        self.txtField?.leftView = btn
        self.txtField?.leftViewMode = UITextFieldViewMode.always
    }
    
    func rightPadding(width:CGFloat, image:UIImage?){
        let btn = UIButton.init(type: UIButtonType.custom)
        btn.frame = CGRect.init(x: 0, y: 0, width: width, height: self.frame.height)
        btn.backgroundColor = UIColor.clear
        btn.isUserInteractionEnabled = false
        if image != nil {
            btn.setImage(image, for: UIControlState.normal)
        }
        self.txtField?.rightView = btn
        self.txtField?.rightViewMode = UITextFieldViewMode.always
    }
    
    func setShowPasswordOption(){
        let btn = UIButton.init(type: .custom)
        btn.setImage(UIImage.init(named: ""), for: .normal)
        btn.frame = CGRect.init(x: 0, y: 0, width: 40, height: 30)
        btn.addTarget(self, action: #selector(FKTextFieldView.showPasswordAction), for: .touchUpInside)
        self.txtField?.rightView = btn
        self.txtField?.rightViewMode = .whileEditing
    }
    @objc func showPasswordAction(){
        self.showPasswordEnable = !self.showPasswordEnable!
        self.txtField?.isSecureTextEntry = self.showPasswordEnable!
    }
    func enableShowPassOption(enable:Bool){
        self.showPasswordEnable = enable
    }
    
    func resignFKFirstResponder(){
        self.txtField?.resignFirstResponder()
    }
    func becomeFKFirstResponder(){
        self.txtField?.becomeFirstResponder();
    }
    
    func setInputView(inputView:UIView){
        self.txtField?.inputView = inputView
    }
    
    func setInputAccessoryView(inputAccessoryView:UIView){
        self.txtField?.inputAccessoryView = inputAccessoryView
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.isHighlighted = true
//        self.setFKPlaceHolderMessage(placeHolder: self.placeHolder)
        if let delegate = self.delegate {
            if(delegate.responds(to: #selector(FKTextFieldDelegate.fkTextFieldDidBeginEditing(textField:)))){
                delegate.fkTextFieldDidBeginEditing!(textField: self)
            }
        }
        self.setNeedsDisplay()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.setNeedsDisplay()
        self.isHighlighted = false
        if !(textField.text?.isEmpty)! {
            self.titleLbl?.text = self.placeHolder
        }else{
            self.titleLbl?.text = ""
        }
        self.txtField?.attributedPlaceholder = self.attributtedPlaceholder()
        self.text = textField.text!
        if let delegate = self.delegate {
            if(delegate.responds(to: #selector(FKTextFieldDelegate.fkTextFieldDidEndEditing(textField:)))){
                delegate.fkTextFieldDidEndEditing!(textField: self)
            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.setNeedsDisplay()
        if let delegate = self.delegate {
            if(delegate.responds(to: #selector(FKTextFieldDelegate.fkTextFieldShouldReturn(textField:)))){
                return delegate.fkTextFieldShouldReturn!(textField: self)
            }
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        if !newString.isEmpty{
            self.setFKPlaceHolderMessage(placeHolder: self.placeHolder)
        }else{
            self.titleLbl?.text = ""
        }
        if !string.isEmpty{
            if self.maxLength > 0{
                if  newString.length > maxLength{
                    return false
                }
            }
        }
        
        if let delegate = self.delegate {
            if(delegate.responds(to: #selector(FKTextFieldDelegate.textField(textField:fkShouldChangeCharactersIn:replacementString:)))){
                return delegate.textField!(textField: self, fkShouldChangeCharactersIn: range, replacementString: string)
            }
        }
        return true
    }
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
//        let textRect = self.txtField?.bounds
        let  contextRef = UIGraphicsGetCurrentContext()
        // set to same colour as text
        if (self.isHighlighted)! {
            contextRef!.setStrokeColor(self.errorTextColor.cgColor)
        }else{
            contextRef!.setStrokeColor(self.placeholderColor.cgColor)
        }
        contextRef?.move(to: CGPoint.init(x: 0, y: 50))
        contextRef?.addLine(to: CGPoint.init(x: self.frame.width , y: 50))
        
        contextRef?.closePath()
        contextRef?.drawPath(using: .stroke)
    }
}
