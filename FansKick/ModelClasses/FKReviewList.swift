//
//  FKReviewList.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 11/16/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKReviewList: NSObject {
    
    var userId = ""
    var desc = ""
    var rating = ""
    var userName = ""
    var dateCreated = ""
    
    class func reviewList(data:Array<Dictionary<String, AnyObject>>) -> Array<FKReviewList>{
        var list = Array<FKReviewList>()
        for dict in data {
            list.append(FKReviewList.reviews(data: dict))
        }
        return list;
    }
    
    class func reviews(data:Dictionary<String,AnyObject>) -> FKReviewList {
        
        let item = FKReviewList()
        item.userId  =  data.validatedValue("user_id", expected: "" as AnyObject) as! String
        item.desc  =  data.validatedValue("description", expected: "" as AnyObject) as! String
        item.rating  =  data.validatedValue("rate", expected: "" as AnyObject) as! String
        item.userName  =  data.validatedValue("user", expected: "" as AnyObject) as! String
        item.dateCreated  =  data.validatedValue("updated_at", expected: "" as AnyObject) as! String
        return item
        
    }
}
