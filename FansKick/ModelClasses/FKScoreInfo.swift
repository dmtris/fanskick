//
//  FKScoreInfo.swift
//  FansKick
//
//  Created by Sunil Verma on 17/11/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKScoreInfo: NSObject {

    var extraScore = "0"
    var halfScore = "0"
    var ninetyScore = "0"
    var penalityScore = "0"
    var score = "0"
    
    class func scoreInfo(data:Dictionary<String,AnyObject>) -> FKScoreInfo {
        
        let item = FKScoreInfo()
        item.extraScore = data.validatedValue("extra_score", expected: "0" as AnyObject) as! String
        item.halfScore = data.validatedValue("half_score", expected: "0" as AnyObject) as! String
        item.ninetyScore = data.validatedValue("ninety_score", expected: "0" as AnyObject) as! String
        item.penalityScore = data.validatedValue("penalty_score", expected: "0" as AnyObject) as! String
        item.score = data.validatedValue("score", expected: "0" as AnyObject) as! String
       
        return item
    }
    
    
}
