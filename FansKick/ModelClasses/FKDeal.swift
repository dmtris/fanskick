//
//  FKDeal.swift
//  FansKick
//
//  Created by FansKick-Sunil on 20/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKDeal: NSObject {
    var id = ""
    var dealId = ""
    var dealName = ""
    var imageUrl = ""
    var qrImage = ""
    var dealDetails = ""
    var availability : Int = 0
    var temsAndServices = ""
    var startDate = ""
    var endDate = ""
    var redeemDate = ""

    var isRedeemed = false
    var stores = [FKStoreInfo]()
    
    func remainingTime() -> Int {
        
        return (endDate.dateFromUTC()? .minutesFrom(startDate.dateFromUTC()!))!
    }
    
    class func dealList(data:Array<Dictionary<String, AnyObject>>) -> Array<FKDeal> {
        
        var list = Array<FKDeal>()
        
        for dict in data {
            list.append(FKDeal.deatlItem(data: dict))
        }
        return list;
    }
    
    class func deatlItem(data:Dictionary<String,AnyObject>) -> FKDeal {
        
        let item = FKDeal()
        
        item.id = data["id"] as! String
        
        item.dealName = data.validatedValue("name", expected: "" as AnyObject) as! String
        item.imageUrl = data.validatedValue("image", expected: "" as AnyObject) as! String
        item.qrImage = data.validatedValue("deal_qr", expected: "" as AnyObject) as! String
        item.temsAndServices = data.validatedValue("tc", expected: "" as AnyObject) as! String
        item.availability = data.validatedValue("availability", expected: "" as AnyObject).integerValue
        item.startDate = data.validatedValue("valid_from", expected: "" as AnyObject) as! String
        item.endDate = data.validatedValue("valid_to", expected: "" as AnyObject) as! String
        
        if let stores = data["stores"] {
            item.stores =  FKStoreInfo.storesList(data: stores as! Array<Dictionary<String, AnyObject>>)
        }
        
        //redeem_date
        if data["redeem_date"] != nil{
            item.redeemDate = data.validatedValue("redeem_date", expected: "" as AnyObject) as! String
            item.isRedeemed = true
        }
        return item
    }
}
