//
//  FKProductImageInfo.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 11/16/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKProductImageInfo: NSObject {
    
    var id = ""
    var imagable_type = ""
    var imagable_Id = ""
    var image = ""

    class func productImageList(data:Array<Dictionary<String, AnyObject>>) -> Array<FKProductImageInfo> {
        
        var list = Array<FKProductImageInfo>()
        for dict in data {
            list.append(FKProductImageInfo.productImages(data: dict))
        }
        return list;
    }
    
    class func productImages(data:Dictionary<String,AnyObject>) -> FKProductImageInfo {
        
        let story   = FKProductImageInfo()
        story.id =  data.validatedValue("_id", expected: "" as AnyObject) as! String
        story.imagable_Id =   data.validatedValue("imagable_id", expected: "" as AnyObject) as! String
        story.imagable_type =   data.validatedValue("imagable_type", expected: "" as AnyObject) as! String
        story.image =   data.validatedValue("image", expected: "" as AnyObject) as! String
        
        return story
    }
}
