//
//  FKPCommetInfo.swift
//  FanskickPlayer
//
//  Created by Sunil Verma on 02/12/2017.
//  Copyright © 2017 Sunil Verma. All rights reserved.
//

import UIKit

class FKCommetInfo: NSObject {

    var id = ""
    var message = ""
    var messageDate = ""
    var senderName = ""
    var senderImage = ""
    var roomID = ""


    class func messageWith(params:Dictionary<String, Any>) -> FKCommetInfo {

        let message = FKCommetInfo()
        message.message = params.validatedValue("body", expected: "" as AnyObject) as! String
        message.messageDate = params.validatedValue("created_at", expected: "" as AnyObject) as! String
        message.senderName = params.validatedValue("user_name", expected: "" as AnyObject) as! String
        message.senderImage = params.validatedValue("user_image", expected: "" as AnyObject) as! String
        message.roomID = params.validatedValue("room_id", expected: "" as AnyObject) as! String

        return message

    }
    
    func updateMessage(params:Dictionary<String, Any>)  {
        self.id = params.validatedValue("id", expected: "" as AnyObject) as! String
        self.message = params.validatedValue("", expected: "" as AnyObject) as! String
        self.messageDate = params.validatedValue("", expected: "" as AnyObject) as! String
        self.senderName = params.validatedValue("", expected: "" as AnyObject) as! String
        self.senderImage = params.validatedValue("", expected: "" as AnyObject) as! String
    }
}
