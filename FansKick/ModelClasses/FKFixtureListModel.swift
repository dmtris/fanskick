//
//  FKFixtureListModel.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 10/25/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKFixtureGroup: NSObject {
    
    var groupDate:String?
    var groupArray = [FKFixtureListModel]()
    var isExpanded = false
    
    // use to grouped the fixture data
    class func getGroupedFixture(array:Array<FKFixtureListModel>) -> Array<FKFixtureGroup>{
        
        var groups = [FKFixtureGroup]()
        var set = Set<String>()
        for item in array {
            set.update(with: (item.start_time.dateFromUTC()?.dateString("yyyy-MM-dd"))!)
            
            //            set.update(with: (item.start_time.dateFromUTC()?.onlyDateString("yyyy-MM-dd"))!)
        }
        
        for startDate in set {
            let group = FKFixtureGroup()
            print("Start date ====   \(startDate)")
            group.groupDate = startDate;
           // let data = array.filter{ ($0.start_time.dateFromUTC()?.UTCStringFromDate()?.contains(startDate))!}
             let data = array.filter{ ($0.start_time.dateFromUTC()?.dateString("yyyy-MM-dd").contains(startDate))!}
            // use to sort the data
            
            print(">>>>>>>>>>>>>>>>>>>\(data.count)")
            
            group.groupArray = data.sorted(by: { (fixture1, fixture2) -> Bool in
                return fixture1.start_time < fixture2.start_time
            })
            groups.append(group)
        }
        
        
//        var groupArray = [FKFixtureGroup]()
//
//        for item  in groups {
//            if item.groupArray.count > 0{
//                groupArray.append(item)
//            }
//        }

        return groups
    }
}

class FKFixtureListModel: NSObject {
    
    var id = ""
    var title = ""
    var stadium = ""
    var club = ""
    var apponent = ""
    var start_time = ""
    var league = ""
    var apponentImage = ""
    var clubImage = ""
    
    class func fixtureAndGroup(data:Array<Dictionary<String, AnyObject>>) -> (Array<FKFixtureListModel>, Array<FKFixtureGroup>) {
        
        var list = Array<FKFixtureListModel>()
        for dict in data {
            list.append(FKFixtureListModel.fixtures(data: dict))
        }
        let groups =  FKFixtureGroup.getGroupedFixture(array: list)
        return  (list, groups);
    }
    
    class func fixtureGroup(data:Array<Dictionary<String, AnyObject>>) -> (Array<FKFixtureGroup>) {
        
        var list = Array<FKFixtureListModel>()
        for dict in data {
            list.append(FKFixtureListModel.fixtures(data: dict))
        }
        let groups =  FKFixtureGroup.getGroupedFixture(array: list)
        return  groups;
    }
    
    class func fixturesList(data:Array<Dictionary<String, AnyObject>>) -> Array<FKFixtureListModel> {
        var list = Array<FKFixtureListModel>()
        for dict in data {
            list.append(FKFixtureListModel.fixtures(data: dict))
        }
        return list
    }
    
    class func fixtures(data:Dictionary<String,AnyObject>) -> FKFixtureListModel {
        
        let item = FKFixtureListModel()
        item.id         = data["id"] as! String
        item.title      = data.validatedValue("title", expected: kBlank as AnyObject) as! String
        item.stadium    = data.validatedValue("stadium", expected: kBlank as AnyObject) as! String
        item.club       = data.validatedValue("home_team", expected: kBlank as AnyObject) as! String
        item.apponent   = data.validatedValue("away_team", expected: kBlank as AnyObject) as! String
        item.start_time = data.validatedValue("kickoff_time", expected: kBlank as AnyObject) as! String
        item.league     = data.validatedValue("league", expected: kBlank as AnyObject) as! String
				item.clubImage     = data.validatedValue("small_home_team_logo", expected: kBlank as AnyObject) as! String // home_team_logo
			   item.apponentImage     = data.validatedValue("small_away_team_logo", expected: kBlank as AnyObject) as! String  // away_team_logo
        return item
    }
}
