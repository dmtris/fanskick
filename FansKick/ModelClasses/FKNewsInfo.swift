//
//  FKNewsInfo.swift
//  FansKick
//
//  Created by FansKick-Sunil on 27/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKNewsInfo: NSObject {
    var id = ""
    var titleText = ""
    var contentText = ""
    var publishDate = ""
    var imageUrl = ""
		var imageUrlLorge = ""

    
    class func list(data:Array<Dictionary<String, AnyObject>>) -> Array<FKNewsInfo> {
        
        var list = Array<FKNewsInfo>()
        for dict in data {
            list.append(FKNewsInfo.newsItem(data: dict))
        }
        return list;
    }
    
    class func newsItem(data:Dictionary<String,AnyObject>) -> FKNewsInfo {
        
        let item = FKNewsInfo()
        item.id = data["_id"] as! String
        item.titleText      =  data.validatedValue("title", expected: "" as AnyObject) as! String
        item.contentText    =  data.validatedValue("body", expected: "" as AnyObject) as! String
        item.publishDate    =  data.validatedValue("updated_at", expected: "" as AnyObject) as! String
				item.imageUrlLorge       =  data.validatedValue("image", expected: "" as AnyObject) as! String
			
			let image = data["small_image"]
			if image is Dictionary<String, Any>{
				let img = image as! Dictionary<String, Any>
				if let file = img["file"] {
					let fil = file as! Dictionary<String, Any>
					let thumb = fil["thumb"]
					if thumb is Dictionary<String, AnyObject>{
						let thumbUrl = thumb as! Dictionary<String, Any>
						item.imageUrl = thumbUrl.validatedValue("url", expected: kBlank as AnyObject) as! String
					}
				}
			}else{
				item.imageUrl       =  data.validatedValue("image", expected: "" as AnyObject) as! String

			}
			
		
			
        return item
    }
}
