//
//  FKSpecialOfferInfo.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 11/20/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKSpecialOfferInfo: NSObject {
    
    var id = ""
    var discountPrice = ""
    var name = ""
    var sponsorImage = ""
    
    var offerImage: FKProductImageInfo?
    var productImageArr = [FKProductImageInfo]()
    
    class func offersList(data:Array<Dictionary<String, AnyObject>>) -> Array<FKSpecialOfferInfo>{
        var list = Array<FKSpecialOfferInfo>()
        for dict in data {
            list.append(FKSpecialOfferInfo.offers(data: dict))
        }
        return list;
    }
    
    class func offers(data:Dictionary<String,AnyObject>) -> FKSpecialOfferInfo {
        
        let item = FKSpecialOfferInfo()
        item.id             =  data.validatedValue("id", expected: "" as AnyObject) as! String
        item.discountPrice  =  data.validatedValue("discounted_price", expected: "" as AnyObject) as! String
        item.name           =  data.validatedValue("name", expected: "" as AnyObject) as! String
        
        if let images = data["offer_images"] {
            item.productImageArr =  FKProductImageInfo.productImageList(data: images as! Array<Dictionary<String, AnyObject>>)
            
            if item.productImageArr.count > 0 {
                let image = item.productImageArr.first
                item.sponsorImage = (image?.image)!
            } else {
                item.sponsorImage = ""
            }
        }
        return item
    }
}
