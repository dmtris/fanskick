//
//  FKClubInfo.swift
//  FansKick
//
//  Created by Sunil Verma on 16/11/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKClubInfo: NSObject {

    var id = ""
    var name = ""
    var clubImg = ""
    var goal = ""
    
    class func clubList(data:Array<Dictionary<String, AnyObject>>) -> Array<FKClubInfo>{
        var list = Array<FKClubInfo>()
        for dict in data {
            list.append(FKClubInfo.club(data: dict))
        }
        return list;
    }
    
    class func club(data:Dictionary<String,AnyObject>) -> FKClubInfo {
        
        let item = FKClubInfo()
        item.id  =  data.validatedValue("_id", expected: "" as AnyObject) as! String
        item.name  =  data.validatedValue("name", expected: "" as AnyObject) as! String
        item.clubImg  =  data.validatedValue("small_club_logo", expected: "" as AnyObject) as! String  //club_logo
        item.goal     =  data.validatedValue("goals", expected: "" as AnyObject) as! String
        
        
        
        return item
    }
}
