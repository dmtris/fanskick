//
//  FKProductInfo.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 11/16/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKProductInfo: NSObject {
    
    var id = ""
    var name = ""
    var detail = ""
    var sku = ""
    var product_material = ""
    var unitPrice = "0"
    var unitStock = ""
    var quantity = ""
    var shippingPrice = "0"
    var categoryId = ""
    var productImage = ""
    var discountedPrice = "0"
    var discount = "0"
    
    var availedSize = [String]()
    var availedColor = [String]()

    var productImageArr = [FKProductImageInfo]()
   
    class func productsList(data:Array<Dictionary<String, AnyObject>>) -> Array<FKProductInfo>{
        var list = Array<FKProductInfo>()
        for dict in data {
            list.append(FKProductInfo.products(data: dict))
        }
        return list;
    }
    
    class func products(data:Dictionary<String,AnyObject>) -> FKProductInfo {
        let item = FKProductInfo()
        print(data)
        item.id  =  data.validatedValue("id", expected: "" as AnyObject) as! String
        item.name  =  data.validatedValue("name", expected: "" as AnyObject) as! String
				let untiPrice = Float(data.validatedValue("unit_price", expected: "0" as AnyObject) as! String)
				item.unitPrice  =  String(format: "%.2f", untiPrice!)
      //  item.unitPrice  =  data.validatedValue("unit_price", expected: "" as AnyObject) as! String
        item.unitStock  =  data.validatedValue("unit_stock", expected: "" as AnyObject) as! String
        item.product_material = data.validatedValue("product_material", expected: "" as AnyObject) as! String
        item.detail = data.validatedValue("description", expected: "" as AnyObject) as! String
        item.discount = data.validatedValue("discount", expected: "0" as AnyObject) as! String
        item.categoryId = data.validatedValue("product_category_id", expected: "" as AnyObject) as! String
        print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",data["discounted_amount"] ?? "no discount")
        print(data.validatedValue("discounted_amount", expected: "0" as AnyObject) as! String)
        
        item.discountedPrice = data.validatedValue("discounted_amount", expected: "0" as AnyObject) as! String
        
        if let size = data["availed_size"] as? [String] {
            item.availedSize = size
        }
        if let images = data["images"] {
            item.productImageArr =  FKProductImageInfo.productImageList(data: images as! Array<Dictionary<String, AnyObject>>)
            
            if item.productImageArr.count > 0 {
                let image = item.productImageArr.first
                item.productImage = (image?.image)!
            } else {
                item.productImage = ""
            }
        }
        return item
    }
    
    // MARK:- Get My order's product
    class func orderProducts(data:Array<Dictionary<String, AnyObject>>) -> Array<FKProductInfo>{
        var list = Array<FKProductInfo>()
        for dict in data {
            list.append(FKProductInfo.allProducts(data: dict))
        }
        return list;
    }
    
    class func allProducts(data:Dictionary<String,AnyObject>) -> FKProductInfo {
        let item = FKProductInfo()
        item.id  =  data.validatedValue("id", expected: "" as AnyObject) as! String
				let untiPrice = Float(data.validatedValue("unit_price", expected: "0" as AnyObject) as! String)
				item.unitPrice  =  String(format: "%.2f", untiPrice!)
       // item.unitPrice  =  data.validatedValue("unit_price", expected: "" as AnyObject) as! String
        item.unitStock  =  data.validatedValue("unit_stock", expected: "" as AnyObject) as! String
        item.product_material = data.validatedValue("product_material", expected: "" as AnyObject) as! String
        item.detail = data.validatedValue("description", expected: "" as AnyObject) as! String
        item.categoryId = data.validatedValue("product_category_id", expected: "" as AnyObject) as! String
        item.quantity = data.validatedValue("quantity", expected: "" as AnyObject) as! String
        item.discount = data.validatedValue("discount", expected: "0" as AnyObject) as! String
        item.shippingPrice = data.validatedValue("shipping_amount", expected: "0" as AnyObject) as! String

        if let _ = data["product_image"] {
            item.productImage = data.validatedValue("product_image", expected: "" as AnyObject) as! String
        } else {
            item.productImage = data.validatedValue("image", expected: "" as AnyObject) as! String
        }
        
        if let _ = data["product_name"] {
            item.name  =  data.validatedValue("product_name", expected: "" as AnyObject) as! String
        } else {
            item.name  =  data.validatedValue("name", expected: "" as AnyObject) as! String
        }
        
        return item
    }

    
    // MARK:- Get My Favorite list
    class func favoriteList(data:Array<Dictionary<String, AnyObject>>) -> Array<FKProductInfo>{
        var list = Array<FKProductInfo>()
        for dict in data {
            list.append(FKProductInfo.favorites(data: dict))
        }
        return list;
    }
    
    class func favorites(data:Dictionary<String,AnyObject>) -> FKProductInfo {
        let item = FKProductInfo()
        item.id  =  data.validatedValue("id", expected: "" as AnyObject) as! String

				let untiPrice = Float(data.validatedValue("unit_price", expected: "" as AnyObject) as! String)
				item.unitPrice  =  String(format: "%.2f", untiPrice!)   //data.validatedValue("unit_price", expected: "" as AnyObject) as! String
        item.unitStock  =  data.validatedValue("unit_stock", expected: "" as AnyObject) as! String
        item.product_material = data.validatedValue("product_material", expected: "" as AnyObject) as! String
        item.detail = data.validatedValue("description", expected: "" as AnyObject) as! String
        item.categoryId = data.validatedValue("product_category_id", expected: "" as AnyObject) as! String
        item.quantity = data.validatedValue("quantity", expected: "" as AnyObject) as! String
        item.name  =  data.validatedValue("name", expected: "" as AnyObject) as! String

        if let size = data["availed_size"] as? [String] {
            item.availedSize = size
        }
        
//        if let size = data["availed_size"] {
//            item.availedSize = size as! Array<String>
//        }
        
        if let images = data["images"] {
            item.productImageArr =  FKProductImageInfo.productImageList(data: images as! Array<Dictionary<String, AnyObject>>)
            
            if item.productImageArr.count > 0 {
                let image = item.productImageArr.first
                item.productImage = (image?.image)!
            } else {
                item.productImage = ""
            }
        }
        return item
    }
}
