//
//  FKNotificationSetting.swift
//  FansKick
//
//  Created by Sunil Verma on 29/11/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKNotificationSetting: NSObject {

    var goal = false
    var halfTimeScore = false
    var kickOff = false
    var matchScore = false
    var playerOnLineReminder = false
    var redCard = false
    var matchReminder = false


   class  func userSetting(params:Dictionary<String, Any>)-> FKNotificationSetting{
    
    let  setting = FKNotificationSetting()
    setting.goal = params.validatedValue("goal", expected: false as AnyObject) as! Bool
    setting.halfTimeScore = params.validatedValue("half_time_score", expected: false as AnyObject) as! Bool
    setting.kickOff = params.validatedValue("kick_off", expected: false as AnyObject) as! Bool
    setting.matchScore = params.validatedValue("match_score", expected: false as AnyObject) as! Bool
    setting.playerOnLineReminder = params.validatedValue("player_online_reminder", expected: false as AnyObject) as! Bool
    setting.redCard = params.validatedValue("red_card", expected: false as AnyObject) as! Bool
    setting.matchReminder = params.validatedValue("match_reminder", expected: false as AnyObject) as! Bool

    return setting
    }
    func updateUserSetting(params:Dictionary<String, Any>){
        self.goal = params.validatedValue("goal", expected: false as AnyObject) as! Bool
        self.halfTimeScore = params.validatedValue("half_time_score", expected: false as AnyObject) as! Bool
        self.kickOff = params.validatedValue("kick_off", expected: false as AnyObject) as! Bool
        self.matchScore = params.validatedValue("match_score", expected: false as AnyObject) as! Bool
        self.playerOnLineReminder = params.validatedValue("player_online_reminder", expected: false as AnyObject) as! Bool
        self.redCard = params.validatedValue("red_card", expected: false as AnyObject) as! Bool
        self.matchReminder = params.validatedValue("match_reminder", expected: false as AnyObject) as! Bool

    }

}
