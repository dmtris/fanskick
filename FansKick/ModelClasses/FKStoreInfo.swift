//
//  FKStoreInfo.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 12/2/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit
import CoreLocation
class FKStoreInfo: NSObject {
    
    var id = ""
    var address = ""
    var contact_person = ""
    var name = ""
    var email = ""
    var phone = ""
    var url = ""
    var cordinate2D:CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid
    
    class func storesList(data:Array<Dictionary<String, AnyObject>>) -> Array<FKStoreInfo>{
        var list = Array<FKStoreInfo>()
        for dict in data {
            list.append(FKStoreInfo.stores(data: dict))
        }
        return list;
    }
    
    class func stores(data:Dictionary<String,AnyObject>) -> FKStoreInfo {
        
        let item = FKStoreInfo()
        item.id             =  data.validatedValue("id", expected: "" as AnyObject) as! String
        item.address        =  data.validatedValue("address", expected: "" as AnyObject) as! String
        item.contact_person =  data.validatedValue("contact_person", expected: "" as AnyObject) as! String
        item.name           =  data.validatedValue("name", expected: "" as AnyObject) as! String
        item.email          =  data.validatedValue("email", expected: "" as AnyObject) as! String
        item.url            =  data.validatedValue("url", expected: "" as AnyObject) as! String
        
        let cordinatesArr = data["coordinates"]
        
        if cordinatesArr is Array<Double> {
            item.cordinate2D.longitude   = (cordinatesArr![0] as AnyObject).doubleValue
            item.cordinate2D.latitude  = (cordinatesArr![1] as AnyObject).doubleValue
        }
        return item
    }
}
