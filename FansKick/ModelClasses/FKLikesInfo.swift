//
//  FKLikesInfo.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 11/17/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKLikesInfo: NSObject {
    
    var id = ""
    var name = ""
    
    class func likeList(data:Array<Dictionary<String, AnyObject>>) -> Array<FKLikesInfo>{
        var list = Array<FKLikesInfo>()
        for dict in data {
            list.append(FKLikesInfo.like(data: dict))
        }
        return list;
    }
    
    class func like(data:Dictionary<String,AnyObject>) -> FKLikesInfo {
        
        let item = FKLikesInfo()
        item.id         =  data.validatedValue("user_id", expected: "" as AnyObject) as! String
        item.name       =  data.validatedValue("user", expected: "" as AnyObject) as! String
        
        return item
    }
}
