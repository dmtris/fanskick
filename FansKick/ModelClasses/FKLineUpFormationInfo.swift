//
//  FKLineUpFormationInfo.swift
//  FansKick
//
//  Created by Sunil Verma on 17/11/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKLineUpGroupInfo: NSObject {

    var groupName = ""
    var playes = [FKPlayerInfo]()

    class func group(data:Array<FKPlayerInfo>) -> Array<FKLineUpGroupInfo> {

        var groups = [FKLineUpGroupInfo]()
        var set = Set<String>()
        for item in data {
            set.update(with: item.role)
        }

        for name in set {
            let group = FKLineUpGroupInfo()
            group.groupName = name
            let items = data.filter{ $0.role.contains(name)}
            
            // use to sort the data
            group.playes = items.sorted(by: { (player1, player2) -> Bool in
                return player1.name < player2.name
            })
            groups.append(group)
        }
        return groups
    }
}

class FKLineUpFormationInfo: NSObject {

    var homeFormation = ""
    var awayFormation = ""
    
    var homePlayer = [FKPlayerInfo]()
    var awayPlayer = [FKPlayerInfo]()
    
    class func lineupInfo(data:Dictionary<String,AnyObject>) -> FKLineUpFormationInfo {
        
        let item = FKLineUpFormationInfo()

        let   lineUp =   data["lineup_formation"]
        
        if  lineUp is Dictionary<String, AnyObject>{
           let  linupInfo = lineUp as! Dictionary<String, AnyObject>
            item.homeFormation = linupInfo.validatedValue("home_formation", expected: "" as AnyObject) as! String
            item.awayFormation = linupInfo.validatedValue("away_formation", expected: "" as AnyObject) as! String
        }
      
        let homeSqud =   data["home_squad"]
        
        if  homeSqud is Array < Dictionary<String, AnyObject>>{
            let  homeSqudInfo = homeSqud as! Array < Dictionary<String, AnyObject>>
            let list =  FKPlayerInfo.playerList(data: homeSqudInfo )
            item.homePlayer = list
        }
        let   awaySqud =   data["away_squad"]
        
        if  awaySqud is Array < Dictionary<String, AnyObject>>{
            let  awaySqudInfo = awaySqud as! Array < Dictionary<String, AnyObject>>
            let list =  FKPlayerInfo.playerList(data: awaySqudInfo )
            item.awayPlayer = list
        }
        
        return item
    }

}
