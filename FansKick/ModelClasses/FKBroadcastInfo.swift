//
//  FKBroadcastInfo.swift
//  FansKick
//
//  Created by Sunil Verma on 10/01/2018.
//  Copyright © 2018 FansKick Dev. All rights reserved.
//

import UIKit

class FKBroadcastInfo: NSObject {

    var id = ""
    var broadcastTitle = ""
    var broadcastDesc = ""
		var updateTime = ""
    var media = ""

   
    // For hilights
    class func broadcastList(data:Array<Dictionary<String, AnyObject>>) -> Array<FKBroadcastInfo> {
        
        var list = Array<FKBroadcastInfo>()
        
        for dict in data {
            list.append(FKBroadcastInfo.broadcastItem(data: dict))
        }
        return list;
        
    }
    
    class func broadcastItem(data:Dictionary<String,AnyObject>) -> FKBroadcastInfo {
        
        let item  = FKBroadcastInfo()
			
			if(data["id"] is Dictionary<String,Any>) {
				let videoId = data["id"] as! Dictionary<String,Any>
				item.id = videoId.validatedValue("videoId", expected: "" as AnyObject) as! String
			}
			if(data["snippet"] is Dictionary<String,Any>) {
				let snippet = data["snippet"] as! Dictionary<String,Any>

				item.broadcastTitle = snippet.validatedValue("title", expected: "" as AnyObject) as! String
				let thumbnail = snippet["thumbnails"] as! Dictionary<String,Any>
				let high = thumbnail["high"] as! Dictionary<String,Any>
				item.media = high.validatedValue("url", expected: "" as AnyObject) as! String
			}
			
			
//			item.id           = data.validatedValue("id", expected: "" as AnyObject) as! String
//        item.broadcastTitle           = data.validatedValue("title", expected: "" as AnyObject) as! String
//        item.media           = data.validatedValue("media", expected: "" as AnyObject) as! String
//        item.broadcastDesc           = data.validatedValue("description", expected: "" as AnyObject) as! String
//        item.updateTime  = data.validatedValue("updated_at", expected: "" as AnyObject) as! String
        return item
    }
    
    // for live
    
    class func livebroadcastList(data:Array<Dictionary<String, AnyObject>>) -> Array<FKBroadcastInfo> {
        
        var list = Array<FKBroadcastInfo>()
        
        for dict in data {
            list.append(FKBroadcastInfo.livebroadcastItem(data: dict))
        }
        return list;
        
    }
    
    class func livebroadcastItem(data:Dictionary<String,AnyObject>) -> FKBroadcastInfo {
        
        let item  = FKBroadcastInfo()
       // item.id           = data.validatedValue("id", expected: "" as AnyObject) as! String
        item.broadcastTitle           = data.validatedValue("name", expected: "" as AnyObject) as! String
//        item.media           = data.validatedValue("media", expected: "" as AnyObject) as! String
//        item.broadcastDesc           = data.validatedValue("description", expected: "" as AnyObject) as! String
//        item.updateTime  = data.validatedValue("updated_at", expected: "" as AnyObject) as! String
        return item
    }
}


