//
//  FKListInfo.swift
//  FansKick
//
//  Created by FansKick-Sunil on 26/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKListInfo: NSObject {

    var id = ""
    var name = ""
    var detail = ""
    var clubImg = ""
    var clubState = ""
    var categoryId = ""
    var price = ""
    
    var productImageArr = [String]()

    class func list(data:Array<Dictionary<String, AnyObject>>) -> Array<FKListInfo>{
        var list = Array<FKListInfo>()
        for dict in data {
            list.append(FKListInfo.item(data: dict))
        }
        return list;
    }
    
    class func categoriesList(data:Array<Dictionary<String, AnyObject>>) -> Array<FKListInfo>{
        var list = Array<FKListInfo>()
        for dict in data {
            list.append(FKListInfo.categories(data: dict))
        }
        return list;
    }

    class func item(data:Dictionary<String,AnyObject>) -> FKListInfo {
        let item = FKListInfo()
        item.id = data["id"] as! String
        item.name  =  data.validatedValue("name", expected: "" as AnyObject) as! String
        item.clubImg  =  data.validatedValue("small_logo", expected: "" as AnyObject) as! String //logo
        item.clubState  =  data.validatedValue("state", expected: "" as AnyObject) as! String
        item.categoryId  =  data.validatedValue("club_id", expected: "" as AnyObject) as! String
            
        return item
    }
    
    class func categories(data:Dictionary<String,AnyObject>) -> FKListInfo {
        let item = FKListInfo()
        item.name  =  data.validatedValue("name", expected: "" as AnyObject) as! String
        item.categoryId  =  data.validatedValue("id", expected: "" as AnyObject) as! String
        return item
    }
    
}
