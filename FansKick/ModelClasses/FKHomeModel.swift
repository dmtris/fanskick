//
//  FKHomeModel.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 10/24/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKHomeModel: NSObject {
	
	var id = ""
	var title = ""
	var imageUrl = ""
	var imageUrlLorge = ""
	var details = ""
	var newsDate = ""
	var readMore = ""
	var isExpanded = false
	
	
	class func homeList(data:Array<Dictionary<String, AnyObject>>) -> Array<FKHomeModel> {
		
		var list = Array<FKHomeModel>()
		
		for dict in data {
			list.append(FKHomeModel.homeNews(data: dict))
		}
		return list;
	}
	
	class func homeNews(data:Dictionary<String,AnyObject>) -> FKHomeModel {
		
		let item = FKHomeModel()
		item.id         = data.validatedValue("id", expected: kBlank as AnyObject) as! String
		item.title      = data.validatedValue("title", expected: kBlank as AnyObject) as! String
		
		let image = data["image"]
		if image is Dictionary<String, Any>{
			let img = image as! Dictionary<String, Any>
			if let file = img["file"] {
				let fil = file as! Dictionary<String, Any>
				item.imageUrlLorge   = fil.validatedValue("url", expected: kBlank as AnyObject) as! String
				let thumb = fil["thumb2"]
				if thumb is Dictionary<String, AnyObject>{
					let thumbUrl = thumb as! Dictionary<String, Any>
					item.imageUrl = thumbUrl.validatedValue("url", expected: kBlank as AnyObject) as! String
				}
			}
		}
		
		item.details    = data.validatedValue("body", expected: kBlank as AnyObject) as! String
		item.newsDate   = data.validatedValue("created_at", expected: kBlank as AnyObject) as! String
		item.readMore    = "false"
		
		let textHeight = item.details.heightOfString(withConstrainedWidth: UIScreen.main.bounds.size.width-20, font: UIFont.systemFont(ofSize: 15.0))
		
		if textHeight > 55.0 {
			item.readMore    = "true"
		}
		return item
	}
	
}
