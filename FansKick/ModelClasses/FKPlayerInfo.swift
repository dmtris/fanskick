//
//  FKPlayerInfo.swift
//  FansKick
//
//  Created by Sunil Verma on 11/13/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKPlayerInfo: NSObject {

    var id = ""
    var name = ""
    var dob = ""
    var birthPlace = ""
    var debut = ""
    var height = ""
    var jerseyNumber = ""
    var joinDate = ""
    var knowName = ""
    var nationality = ""
    var position = ""
    var positionSide = ""
    var role = ""
    var bio = ""
    var rating = ""
    var subPosition = ""
    var weight = ""
    var price = "0"
    var age  = ""
    var profileImage = ""
    var clubName = ""
    var clubImage = ""
    var isOnline = false
    
    // stats
    var goal = "0"
    var appearances = "0"
    var assists = "0"
    var pass = "0"
    var redCard = "0"
    var saved = "0"
    var shots = "0"
    var subsTitute = "0"
    var subsTitute_on = "0"
    var trackles = "0"
    var yellowCared = "0"
    var statsImage = ""
    
    
    
    
    
    var roomID = ""
    var groupID = ""
    var username = ""


    
    var storyArray = [FKStoryInfo] ()
    var videoArray = [FKStoryInfo] ()
   // var states: FKPlayerStatsInfo?
    
    
    class func playerList(data:Array<Dictionary<String, AnyObject>>) -> Array<FKPlayerInfo> {
        
        var list = Array<FKPlayerInfo>()
        
        for dict in data {
            list.append(FKPlayerInfo.playerItem(data: dict))
        }
        return list;
        
    }
    
    class func playerItem(data:Dictionary<String,AnyObject>) -> FKPlayerInfo {
        
        let player  = FKPlayerInfo()
        
        player.id           = data.validatedValue("id", expected: "" as AnyObject) as! String
        player.name         = data.validatedValue("name", expected: "" as AnyObject) as! String
        player.dob          = data.validatedValue("birth_date", expected: "" as AnyObject) as! String
        player.birthPlace   = data.validatedValue("birth_place", expected: "" as AnyObject) as! String
        player.debut        = data.validatedValue("debut", expected: "" as AnyObject) as! String
        player.height       = data.validatedValue("height", expected: "" as AnyObject) as! String
        player.jerseyNumber = data.validatedValue("jersey_number", expected: "" as AnyObject) as! String
        player.joinDate     = data.validatedValue("join_date", expected: "" as AnyObject) as! String
        player.knowName     = data.validatedValue("known_name", expected: "" as AnyObject) as! String
        player.nationality  = data.validatedValue("nationality", expected: "" as AnyObject) as! String
        
        player.position     = data.validatedValue("position", expected: "" as AnyObject) as! String
        player.role     = data.validatedValue("role", expected: "" as AnyObject) as! String

        player.positionSide = data.validatedValue("position_side", expected: "" as AnyObject) as! String

        player.bio          = data.validatedValue("profile_description", expected: "" as AnyObject) as! String
        player.rating       = data.validatedValue("rating", expected: "" as AnyObject) as! String
        player.subPosition  = data.validatedValue("sub_position", expected: "" as AnyObject) as! String
        player.weight       = data.validatedValue("weight", expected: "" as AnyObject) as! String
        player.profileImage = data.validatedValue("small_image", expected: "" as AnyObject) as! String //image
        player.clubName     = data.validatedValue("club_name", expected: "" as AnyObject) as! String
        player.clubImage    = data.validatedValue("small_club_logo", expected: "" as AnyObject) as! String //club_logo
        player.price        = data.validatedValue("price", expected: "0" as AnyObject) as! String
        
        player.goal        = data.validatedValue("goals", expected: "0" as AnyObject) as! String
        player.appearances        = data.validatedValue("appearances", expected: "0" as AnyObject) as! String
        player.assists        = data.validatedValue("assists", expected: "0" as AnyObject) as! String
        player.pass        = data.validatedValue("passes", expected: "0" as AnyObject) as! String
        player.redCard        = data.validatedValue("red_card", expected: "0" as AnyObject) as! String
        player.saved        = data.validatedValue("saved", expected: "0" as AnyObject) as! String
        player.shots        = data.validatedValue("shots", expected: "0" as AnyObject) as! String
        player.subsTitute_on        = data.validatedValue("substitute_on", expected: "0" as AnyObject) as! String
        player.trackles        = data.validatedValue("tackles", expected: "0" as AnyObject) as! String
        player.yellowCared        = data.validatedValue("yellow_card", expected: "0" as AnyObject) as! String
        player.isOnline        = data.validatedValue("is_live", expected: 0 as NSNumber) as! Bool
        player.username        = data.validatedValue("username", expected: "" as AnyObject) as! String
        player.statsImage      = data.validatedValue("stats_image_url", expected: "" as AnyObject) as! String
        
        //is_live

        if let group = data["player_group"]{
            let groupID = group as! Dictionary<String, AnyObject>
            player.groupID =  groupID["id"] as! String
        }
        if let room = data["player_room"]{
            let roomID = room as! Dictionary<String, AnyObject>
            player.roomID =  roomID["id"] as! String
        }
        return player
    }
    
    
    func updatePlayerInfo(data:Dictionary<String,AnyObject>){
        
        self.id =      data.validatedValue("id", expected: "" as AnyObject) as! String
        self.name =      data.validatedValue("name", expected: "" as AnyObject) as! String
        self.dob =      data.validatedValue("birth_date", expected: "" as AnyObject) as! String
        self.birthPlace =      data.validatedValue("birth_place", expected: "" as AnyObject) as! String
        self.debut =      data.validatedValue("debut", expected: "" as AnyObject) as! String
        self.height =      data.validatedValue("height", expected: "" as AnyObject) as! String
        self.jerseyNumber =      data.validatedValue("jersey_number", expected: "" as AnyObject) as! String
        self.joinDate =      data.validatedValue("join_date", expected: "" as AnyObject) as! String
        self.knowName =      data.validatedValue("known_name", expected: "" as AnyObject) as! String
        self.nationality =      data.validatedValue("nationality", expected: "" as AnyObject) as! String
        self.position =      data.validatedValue("position", expected: "" as AnyObject) as! String
        self.positionSide =      data.validatedValue("position_side", expected: "" as AnyObject) as! String
        self.bio =      data.validatedValue("profile_description", expected: "" as AnyObject) as! String
        self.rating =      data.validatedValue("rating", expected: "" as AnyObject) as! String
        self.subPosition =      data.validatedValue("sub_position", expected: "" as AnyObject) as! String
        self.weight =      data.validatedValue("weight", expected: "" as AnyObject) as! String
        self.profileImage = data.validatedValue("small_image", expected: "" as AnyObject) as! String //image
        self.clubName = data.validatedValue("club_name", expected: "" as AnyObject) as! String
        self.clubImage = data.validatedValue("small_club_logo", expected: "" as AnyObject) as! String //club_logo
        self.goal        = data.validatedValue("goals", expected: "0" as AnyObject) as! String
        self.appearances        = data.validatedValue("appearances", expected: "0" as AnyObject) as! String
        self.assists        = data.validatedValue("assists", expected: "0" as AnyObject) as! String
        self.pass        = data.validatedValue("passes", expected: "0" as AnyObject) as! String
        self.redCard        = data.validatedValue("red_card", expected: "0" as AnyObject) as! String
        self.saved        = data.validatedValue("saved", expected: "0" as AnyObject) as! String
        self.shots        = data.validatedValue("shots", expected: "0" as AnyObject) as! String
        self.subsTitute_on        = data.validatedValue("substitute_on", expected: "0" as AnyObject) as! String
        self.trackles        = data.validatedValue("tackles", expected: "0" as AnyObject) as! String
        self.yellowCared        = data.validatedValue("yellow_card", expected: "0" as AnyObject) as! String
        self.isOnline        = data.validatedValue("is_live", expected: 0 as NSNumber) as! Bool
        self.role     =     data.validatedValue("role", expected: "" as AnyObject) as! String
        self.username        = data.validatedValue("username", expected: "" as AnyObject) as! String
        self.price        = data.validatedValue("price", expected: "0" as AnyObject) as! String


        if let story = data["player_stories"]{
            self.updateStory(data: story as! Array<Dictionary<String, AnyObject>>)
        }
        if let videos = data["player_video"]{
            self.updateVideo(data: videos as! Array<Dictionary<String, AnyObject>>)
        }
        
       
    }
    
    func updateStory(data:Array<Dictionary<String, AnyObject>>){
      self.storyArray =  FKStoryInfo.storyList(data: data)
    }

    func updateVideo(data:Array<Dictionary<String, AnyObject>>){
        self.videoArray =  FKStoryInfo.storyList(data: data)
    }
    
    func getDetailArray() -> Array<FKListInfo>  {
        
        var profileArray = [FKListInfo]()
        
        if !self.nationality.isEmpty{
            let nationality = FKListInfo()
            nationality.name = "National Team"
            nationality.detail = self.nationality
        profileArray.append(nationality)
        }
        if !self.birthPlace.isEmpty{
            let birth = FKListInfo()
            birth.name = "Country of Birth"
            birth.detail = self.birthPlace
            profileArray.append(birth)

        }
        if !self.dob.isEmpty{
            let dob = FKListInfo()
            dob.name = "Date of Birth"
            dob.detail = (self.dob.dateFromUTC()?.dateString(DATE_FORMAT))!
            profileArray.append(dob)

        }
        if !self.debut.isEmpty{
            let debut = FKListInfo()
            debut.name = "PL Debut"
            debut.detail = (self.debut.dateFromUTC()?.dateString(DATE_FORMAT))!
            profileArray.append(debut)
        }
        if !self.height.isEmpty{
            let height = FKListInfo()
            height.name = "Height"
            height.detail = "\(self.height) cm"
            profileArray.append(height)
        }
        if !self.weight.isEmpty{
            let weight = FKListInfo()
            weight.name = "Weight"
            weight.detail = "\(self.weight) kg"
            profileArray.append(weight)
        }
        
        if !self.jerseyNumber.isEmpty{
            let jersey = FKListInfo()
            jersey.name = "Jersey number"
            jersey.detail = self.jerseyNumber
            profileArray.append(jersey)
        }
        
        if !self.position.isEmpty{
            let position = FKListInfo()
            position.name = "Appearances"
            position.detail = self.position
            profileArray.append(position)
        }
        
        if !self.positionSide.isEmpty{
            let subPosition = FKListInfo()
            subPosition.name = "Appearances as sub"
            subPosition.detail = self.subPosition
            profileArray.append(subPosition)
            
        }
        
        return profileArray
        
//        placeHolderArray = ["National Team","Country of Birth", "Age","Date of Birth", "PL Debut","Height, Weight","Appearances","Goals","Appearances as sub"]

        
    }

    func getStatsInfo() -> Array<String>{
        var array = [String]()
        
        if !self.pass.isEmpty{
            array.append("\(self.pass) PASS")
        }
        
        if !self.trackles.isEmpty{
            array.append("\(self.trackles) TACKLE")
        }
        
        if !self.saved.isEmpty{
            array.append("\(self.saved) SAVE")
        }
        
        if !self.appearances.isEmpty{
            array.append("\(self.appearances) APPEAR")
        }
        if !self.assists.isEmpty{
            array.append("\(self.assists) ASSITS")
        }
        
        if !self.goal.isEmpty{
            array.append("\(self.goal) GOAL")
        }
        
        if !self.shots.isEmpty{
            array.append("\(self.shots) SHOT")
        }
        
        if !self.subsTitute_on.isEmpty{
            array.append("\(self.subsTitute_on) SUBSTITUTE")
        }
        
        return array
    }
}
