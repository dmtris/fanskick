//
//  FKUser.swift
//  FansKick
//
//  Created by FansKick Dev on 12/10/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKUser: NSObject {
    
    var userId = ""
    var name = ""
    var email = ""
    var password = ""
    var confrmPassword = ""
    var countryCode = ""
    var mobileNo = ""
    var city = ""
    var state = ""
    var stateId = ""
    var address = ""
    var gender = ""
    var dob = ""
    var firstName = ""
    var lastName = ""
    var errorMsg = ""
    var oldPassword = ""
    var imgUrl = ""
    var postalCode = ""
    var selectedDob:Date?

    
    class func user(_ infoDict: Dictionary<String, AnyObject>?) -> FKUser {
        
        let info = FKUser()
        if let userDict = infoDict {
            
            info.name   = "\(userDict.validatedValue("name", expected: "" as AnyObject))"
            info.email  = "\(userDict.validatedValue("email", expected: "" as AnyObject))"
            info.dob    = "\(userDict.validatedValue("dob", expected: "" as AnyObject))"
            info.state  = "\(userDict.validatedValue("state", expected: "" as AnyObject))"
            info.stateId  = "\(userDict.validatedValue("state_id", expected: "" as AnyObject))"
            info.address  = "\(userDict.validatedValue("address", expected: "" as AnyObject))"
            info.imgUrl  = "\(userDict.validatedValue("image", expected: "" as AnyObject))"
            
        }
        return info
    }
}
//
//extension FKUser {
//
//    func authenticate(_ hudType: loadingIndicatorType, completionBlock: @escaping (AnyObject?, Error?, Int) -> Void) ->Void {
//
//        let paramDict = [
//            "mobile" : self.email,
//            "password" : self.password,
//            "channel_id" : AppUtility.deviceUDID()
//        ]
//
//        ServiceHelper.request(params: paramDict, method: .post, apiName: kAPINameLogin) { (result, error, httpCode) in
//
//            if let error = error {
//                AlertController.alert(title: error.localizedDescription)
//            }
//
//            completionBlock(result, error, httpCode)
//        }
//    }
//}

