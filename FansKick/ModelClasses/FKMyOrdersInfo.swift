//
//  FKMyOrdersInfo.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 11/22/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKMyOrdersInfo: NSObject {
    
    var id = ""
    var price = "0"
    var status = ""
    var productImage = ""
    var orderDate = ""
    var discountAmount = "0"
    var amountAfterDiscount = "0"
    
    
    var product: FKProductInfo?
    var productsArray = [FKProductInfo]()
    var productImageArr = [FKProductImageInfo]()
    
    class func orderList(data:Array<Dictionary<String, AnyObject>>) -> Array<FKMyOrdersInfo>{
        var list = Array<FKMyOrdersInfo>()
        for dict in data {
            list.append(FKMyOrdersInfo.orders(data: dict))
        }
        return list;
    }
    
    class func orders(data:Dictionary<String,AnyObject>) -> FKMyOrdersInfo {
        
        let item = FKMyOrdersInfo()
        item.id  =  data.validatedValue("id", expected: "" as AnyObject) as! String
        item.price  =  data.validatedValue("total_amount", expected: "0" as AnyObject) as! String
        item.status  =  data.validatedValue("status", expected: "" as AnyObject) as! String
        item.orderDate = data.validatedValue("created_at", expected: "" as AnyObject) as! String
        item.discountAmount = data.validatedValue("discount_amount", expected: "0" as AnyObject) as! String
        item.amountAfterDiscount = data.validatedValue("total_amount_after_discount", expected: "0" as AnyObject) as! String

			if item.price.length > 0{
				item.price = String(format: "%.2f", item.price)
			}
        switch Int(item.status)! {
        case 0:
            item.status = "Order Placed"
        case 1:
            item.status = "Packed"
        case 2:
            item.status = "Dispatched"
        case 3:
            item.status = "Delivered"
        case 4:
            item.status = "Cancelled"

        default:
            break
        }
        
        if let products = data["order_details"] {
            item.productsArray =  FKProductInfo.orderProducts(data: products as! Array<Dictionary<String, AnyObject>>)
            
            if item.productImageArr.count > 0 {
                let image = item.productImageArr.first
                item.productImage = (image?.image)!
            } else {
                item.productImage = ""
            }
        }
        
        return item
    }
    
    func updateOrder(data:Dictionary<String,AnyObject>){
        
        self.id  =  data.validatedValue("id", expected: "" as AnyObject) as! String
        self.price  =  data.validatedValue("total_amount", expected: "0" as AnyObject) as! String
        self.status  =  data.validatedValue("status", expected: "" as AnyObject) as! String
        self.orderDate = data.validatedValue("created_at", expected: "" as AnyObject) as! String
			
			if self.price.length > 0{
				self.price = String(format: "%.2f", self.price)
			}
			
        switch Int(self.status)! {
        case 0:
            self.status = "Order Placed"
        case 1:
            self.status = "Packed"
        case 2:
            self.status = "Dispatched"
        case 3:
            self.status = "Delivered"
        case 4:
            self.status = "Canceled"
        default:
            break
        }
        
        if let products = data["order_details"] {
            self.productsArray =  FKProductInfo.orderProducts(data: products as! Array<Dictionary<String, AnyObject>>)
            
            if self.productImageArr.count > 0 {
                let image = self.productImageArr.first
                self.productImage = (image?.image)!
            } else {
                self.productImage = ""
            }
        }
    }
}
