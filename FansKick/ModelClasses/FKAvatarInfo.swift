//
//  FKAvatarInfo.swift
//  FansKick
//
//  Created by Fanskick-Nitin on 11/30/17.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKAvatarInfo: NSObject {
    
    var id = ""
    var dateCreated = ""
    var frontImgUrl = ""
    var backImgUrl = ""
    var name = ""
    var clubId = ""
    var sponsorId = ""
    var jerseyNo = ""

    var selectedDob:Date?
    
    class func avatarImage(_ infoDict: Dictionary<String, AnyObject>?) -> FKAvatarInfo {
        
        let info = FKAvatarInfo()
        if let userDict = infoDict {
            
            info.id             = "\(userDict.validatedValue("id", expected: "" as AnyObject))"
            info.dateCreated    = "\(userDict.validatedValue("created_at", expected: "" as AnyObject))"
            info.frontImgUrl    = "\(userDict.validatedValue("front_image", expected: "" as AnyObject))"
            info.backImgUrl     = "\(userDict.validatedValue("back_image", expected: "" as AnyObject))"
            info.name           = "\(userDict.validatedValue("name", expected: "" as AnyObject))"
            info.clubId         = "\(userDict.validatedValue("club_id", expected: "" as AnyObject))"
            info.sponsorId      = "\(userDict.validatedValue("sponsor_id", expected: "" as AnyObject))"
            info.jerseyNo       = "\(userDict.validatedValue("jersey_no", expected: "" as AnyObject))"
            
        }
        return info
    }
}
