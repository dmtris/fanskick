//
//  FKStoryInfo.swift
//  FansKick
//
//  Created by Sunil Verma on 15/11/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKStoryInfo: NSObject {

    var id = ""
    var content = ""
    var postedDate = ""
    var imageUrl = ""
  //  var videoUrl = ""

    class func storyList(data:Array<Dictionary<String, AnyObject>>) -> Array<FKStoryInfo> {
        
        var list = Array<FKStoryInfo>()
        for dict in data {
            list.append(FKStoryInfo.storyItem(data: dict))
        }
        return list;
    }
    
    class func storyItem(data:Dictionary<String,AnyObject>) -> FKStoryInfo {
        
        let story   = FKStoryInfo()
        story.id =      data.validatedValue("id", expected: "" as AnyObject) as! String
        story.imageUrl = data.validatedValue("story", expected: "" as AnyObject) as! String
        story.content = data.validatedValue("description", expected: "" as AnyObject) as! String
        story.postedDate = data.validatedValue("created_at", expected: "" as AnyObject) as! String
        
        if story.imageUrl.isEmpty{
            story.imageUrl = data.validatedValue("video", expected: "" as AnyObject) as! String

        }
        
       // story.videoUrl = data.validatedValue("video", expected: "" as AnyObject) as! String

        return story
    }
}
