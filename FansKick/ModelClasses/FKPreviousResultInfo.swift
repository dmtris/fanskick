//
//  FKPreviousResultInfo.swift
//  FansKick
//
//  Created by Sunil Verma on 17/11/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKPreviousResultInfo: NSObject {

    var id = ""
    var kickOffTime = ""
    var isDraw = false
    var stadiumName = ""
    var leagueName = ""
    var homeTeam = ""
    var awayTeam = ""
    var homeTeamLogo = ""
    var awayTeamLogo = ""
    var homeTeamScore:FKScoreInfo?
    var awayTeamScore:FKScoreInfo?

	var homeTeamScoreString = "0"
	var awayTeamScoreString = "0"
	var events = Array<String>()

    class func previousResultList(data:Array<Dictionary<String, AnyObject>>) -> Array<FKPreviousResultInfo>{
        var list = Array<FKPreviousResultInfo>()
        for dict in data {
            list.append(FKPreviousResultInfo.result(data: dict))
        }
        return list;
    }
    
    class func result(data:Dictionary<String,AnyObject>) -> FKPreviousResultInfo {
        
        let item = FKPreviousResultInfo()
        item.id  =              data.validatedValue("_id", expected: "" as AnyObject) as! String
        item.kickOffTime  =     data.validatedValue("kickoff_time", expected: "" as AnyObject) as! String
			
			if let draw =  data["is_draw"] {
				item.isDraw  =        draw as! Bool  //  data["is_draw"] as! Bool

			}
        item.stadiumName     =  data.validatedValue("stadium", expected: "" as AnyObject) as! String
        item.leagueName     =   data.validatedValue("league", expected: "" as AnyObject) as! String
        item.homeTeam     =     data.validatedValue("home_team", expected: "" as AnyObject) as! String
        item.awayTeam     =     data.validatedValue("away_team", expected: "" as AnyObject) as! String
        item.homeTeamLogo   =   data.validatedValue("small_home_team_logo", expected: "" as AnyObject) as! String  // home_team_logo
				item.awayTeamLogo     = data.validatedValue("small_away_team_logo", expected: "" as AnyObject) as! String  // away_team_logo

			
        let   homeScore =   data["home_score"]
        if  homeScore is Dictionary<String, AnyObject>{
              item.homeTeamScore = FKScoreInfo.scoreInfo(data: homeScore as! Dictionary<String, AnyObject>)
				}else{
					item.homeTeamScoreString = data.validatedValue("home_score", expected: "0" as AnyObject) as! String
			}
        
        let   awayScore =   data["away_score"]
        if  awayScore is Dictionary<String, AnyObject>{
            item.awayTeamScore = FKScoreInfo.scoreInfo(data: awayScore as! Dictionary<String, AnyObject>)
				}else{
					item.awayTeamScoreString = data.validatedValue("away_score", expected: "0" as AnyObject) as! String
			}
			
			let   event =   data["match_events"]
			if  event is Array<Array<String>>{
				
				let eventInfo = event as! Array<Array<String>>
				
				for info in eventInfo {
					item.events.append(info.first!)
				
				}
			}

        
        return item
    }
}
