//
//  FKFixtureStatsInfo.swift
//  FansKick
//
//  Created by Sunil Verma on 17/11/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKFixtureStatsInfo: NSObject {

    var totalMatchPlayed = ""
    var homeWinCount = ""
    var awayWinCount = ""
    var drawCount = ""
    var homeWinCountAtHome = ""
    var awayWinCountAtHome = ""
    var homeWinCountAtAway = ""
    var awayWinCountAtAway = ""
    var fixtureImage = ""
    
    var previousResult = [FKPreviousResultInfo]()
   
    class func fixtureState(data:Dictionary<String,AnyObject>) -> FKFixtureStatsInfo {
        
        let item = FKFixtureStatsInfo()
        item.totalMatchPlayed = data.validatedValue("total_macthe_count", expected: "" as AnyObject) as! String
        item.homeWinCount = data.validatedValue("home_win_count", expected: "" as AnyObject) as! String
        item.awayWinCount = data.validatedValue("away_win_count", expected: "" as AnyObject) as! String
        item.drawCount = data.validatedValue("draw_count", expected: "" as AnyObject) as! String
        item.homeWinCountAtHome = data.validatedValue("home_wins_count_as_home", expected: "" as AnyObject) as! String
        item.awayWinCountAtHome = data.validatedValue("away_wins_count_as_home", expected: "" as AnyObject) as! String
        item.homeWinCountAtAway = data.validatedValue("home_wins_count_as_away", expected: "" as AnyObject) as! String
        item.awayWinCountAtAway = data.validatedValue("away_wins_count_as_away", expected: "" as AnyObject) as! String
        item.fixtureImage = data.validatedValue("fixture_stats_image", expected: "" as AnyObject) as! String
        
        if let result = data["previous_match_details"]{
            item.previousResult =  FKPreviousResultInfo.previousResultList(data: result as! Array<Dictionary<String, AnyObject>>)
        }
        
        return item
    }
}
