//
//  FKCartInfo.swift
//  FansKick
//
//  Created by Sunil Verma on 16/11/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKCartInfo: NSObject {
	
	var id = ""
	var quantity = ""
	var productId = ""
	var price = "0"
	var size = ""
	var totalPrice = "0"
	var shippingPrice = "0"
	var sponsorImage = ""
	
	var product: FKProductInfo?
	var productImageArr = [FKProductImageInfo]()
	
	class func cartList(data:Array<Dictionary<String, AnyObject>>) -> Array<FKCartInfo>{
		var list = Array<FKCartInfo>()
		for dict in data {
			list.append(FKCartInfo.carts(data: dict))
		}
		return list;
	}
	
	class func carts(data:Dictionary<String,AnyObject>) -> FKCartInfo {
		
		let item = FKCartInfo()
		item.id  =  data.validatedValue("_id", expected: "" as AnyObject) as! String
		item.quantity  =  data.validatedValue("quantity", expected: "" as AnyObject) as! String
		item.productId  =  data.validatedValue("product_id", expected: "" as AnyObject) as! String
		let price = Float(data.validatedValue("price", expected: "0" as AnyObject) as! String)
		item.price  =  String(format: "%.2f", price!)
		
		//  item.price  =  data.validatedValue("price", expected: "" as AnyObject) as! String
		
		item.size  =  data.validatedValue("size", expected: "" as AnyObject) as! String
		item.totalPrice  =  data.validatedValue("total_price", expected: "0" as AnyObject) as! String
		item.shippingPrice  =  data.validatedValue("shipping_price", expected: "0" as AnyObject) as! String
		
		
		if item.totalPrice.length > 0{
			item.totalPrice = String(format: "%.2f", item.totalPrice)
		}
		
		if item.shippingPrice.length > 0{
			item.shippingPrice = String(format: "%.2f", item.shippingPrice)
		}
		
		if let product = data["product"] {
			item.product = FKProductInfo.products(data: product as! Dictionary<String,AnyObject> )
		}
		if let images = data["images"] {
			item.productImageArr =  FKProductImageInfo.productImageList(data: images as! Array<Dictionary<String, AnyObject>>)
			
			if item.productImageArr.count > 0 {
				let image = item.productImageArr.first
				item.sponsorImage = (image?.image)!
			} else {
				item.sponsorImage = ""
			}
		}
		return item
	}
}
