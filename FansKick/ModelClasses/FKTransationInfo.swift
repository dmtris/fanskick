//
//  FKTransationInfo.swift
//  FansKick
//
//  Created by Sunil Verma on 04/01/2018.
//  Copyright © 2018 FansKick Dev. All rights reserved.
//

import UIKit

class FKTransationInfo: NSObject {

    var transactionId = ""
    var type = ""
    var amount = ""
    var transactionFee = ""
    var totalAmount = ""
    var status = ""
    var sender = ""
    var receiver = ""
    var transactionTime = ""
    var userComment = ""
    var adminComment = ""
    var currency = ""
    
    var statusText: String  {
        get {
            if status == "1" {
                return "PENDING"
            }else if status == "2" {
            return "SUCCESSFUL"
                
            }else {
               return "REJECTED"
            }
        }
    }
    class func transactionList(data:Array<Dictionary<String, AnyObject>>) -> Array<FKTransationInfo> {
        
        var list = Array<FKTransationInfo>()
        
        for dict in data {
            list.append(FKTransationInfo.transactionItem(data: dict))
        }
        return list;
    }
    
    class func transactionItem(data:Dictionary<String,AnyObject>) -> FKTransationInfo {
        
        let item = FKTransationInfo()
        
        item.transactionId = data["id"] as! String
        item.type = data.validatedValue("type", expected: "" as AnyObject) as! String
        item.amount = data.validatedValue("amount", expected: "" as AnyObject) as! String
        item.transactionFee = data.validatedValue("fee", expected: "" as AnyObject) as! String
        item.totalAmount = data.validatedValue("sum", expected: "" as AnyObject) as! String
        item.status = data.validatedValue("status", expected: "" as AnyObject) as! String
        item.sender = data.validatedValue("sender", expected: "" as AnyObject) as! String
        item.receiver = data.validatedValue("receiver", expected: "" as AnyObject) as! String
        item.transactionTime = data.validatedValue("time", expected: "" as AnyObject) as! String
        item.userComment = data.validatedValue("user_comment", expected: "" as AnyObject) as! String
        item.adminComment = data.validatedValue("admin_comment", expected: "" as AnyObject) as! String
        item.currency = data.validatedValue("currency", expected: "" as AnyObject) as! String

        return item
    }
}
