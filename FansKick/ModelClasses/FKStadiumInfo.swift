//
//  FKIndoorInfo.swift
//  FansKick
//
//  Created by Sunil Verma on 21/11/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKStadiumGroupInfo: NSObject {
    var stadiumName = ""
    var stadiumAddress = ""
    var locationID = ""
    var floors = [FKStadiumInfo]()
    
}

class FKStadiumInfo: NSObject {

    var id = ""
    var stadiumName = ""
    var stadiumAddress = ""
    var floorName = ""
    var floorId = ""
    
    class func list(data:Array<Dictionary<String, AnyObject>>) -> Array<FKStadiumGroupInfo> {
        
        var list = Array<FKStadiumGroupInfo>()
        for dict in data {
            let group = FKStadiumGroupInfo()
            group.stadiumName = dict.validatedValue("name", expected: "" as AnyObject) as! String
            group.stadiumAddress = dict.validatedValue("address", expected: "" as AnyObject) as! String
            let floors = dict["floors"]
            if floors is Array<Dictionary<String, Any>>{
                let floorsArray = floors as! Array<Dictionary<String, Any>>
                for floor in floorsArray {
                    let item = FKStadiumInfo.item(data: floor as Dictionary<String, AnyObject>)
                    item.stadiumName = group.stadiumName
                    item.stadiumAddress = group.stadiumAddress
                    group.floors.append(item)
                }
            }
            
            if group.floors.count > 0{
                list.append(group)
            }
        }
        return list;
    }
    
    class func item(data:Dictionary<String,AnyObject>) -> FKStadiumInfo {
        let item = FKStadiumInfo()
        item.id = data["id"] as! String
        item.floorName = data.validatedValue("name", expected: "" as AnyObject) as! String
        item.floorId    =  data.validatedValue("floor_id", expected: "" as AnyObject) as! String
        return item
        
    }
}
