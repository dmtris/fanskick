//
//  FKNotificationInfo.swift
//  FansKick
//
//  Created by Sunil Verma on 29/11/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKNotificationInfo: NSObject {

    var id = ""
    var notificationID = ""
    var notificationType = ""
    var title = ""
    var content = ""
    var notificationDate = ""
    
    
    class func list(data:Array<Dictionary<String, AnyObject>>) -> Array<FKNotificationInfo> {
        
        var list = Array<FKNotificationInfo>()
        for dict in data {
            list.append(FKNotificationInfo.item(data: dict))
        }
        return list;
    }
    
    class func item(data:Dictionary<String,AnyObject>) -> FKNotificationInfo {
        
        let item = FKNotificationInfo()
        item.id = data["id"] as! String
        item.notificationID      =  data.validatedValue("notifiable_id", expected: "" as AnyObject) as! String
        item.title    =  data.validatedValue("title", expected: "" as AnyObject) as! String
        item.notificationType    =  data.validatedValue("notifiable_type", expected: "" as AnyObject) as! String
        item.content       =  data.validatedValue("content", expected: "" as AnyObject) as! String
        item.notificationDate = data.validatedValue("created_at", expected: "" as AnyObject) as! String
        return item
    }

}
