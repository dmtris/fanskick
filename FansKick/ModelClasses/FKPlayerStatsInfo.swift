//
//  FKPlayerStatsInfo.swift
//  FansKick
//
//  Created by Sunil Verma on 16/11/2017.
//  Copyright © 2017 FansKick Dev. All rights reserved.
//

import UIKit

class FKPlayerStatsInfo: NSObject {

    var id = ""
    var appearances = ""
    var assists = ""
    var goal = ""
    var pass = ""
    var redCard = ""
    var saved = ""
    var shots = ""
    var subsTitute = ""

    var subsTitute_on = ""
    var trackles = ""
    var yellowCared = ""
    
    
    class func statsWithAray(dataArray:Array<Dictionary<String,AnyObject>>) -> FKPlayerStatsInfo {
        
        let item = FKPlayerStatsInfo()

        var appearances = 0
        var assists = 0
        var goal = 0
        var pass = 0
        var redCard = 0
        var saved = 0
        var shots = 0
        var subsTitute_on = 0
        var trackles = 0
        var yellowCared = 0

        for data in dataArray{
            
            appearances = appearances + Int(truncating: (data["appearances"] as AnyObject) as! NSNumber)
            assists = assists + Int(truncating: (data["assists"] as AnyObject) as! NSNumber)
            goal = goal + Int(truncating: (data["goals"] as AnyObject) as! NSNumber)
            pass = pass + Int(truncating: (data["pass"] as AnyObject) as! NSNumber)
            redCard = redCard + Int(truncating: (data["red_card"] as AnyObject) as! NSNumber)
            saved = saved + Int(truncating: (data["saved"] as AnyObject) as! NSNumber)
            shots = shots + Int(truncating: (data["shots"] as AnyObject) as! NSNumber)
        //    subsTitute = subsTitute + Int(truncating: data.validatedValue("substitude", expected: "0" as AnyObject) as! NSNumber)
            subsTitute_on = subsTitute_on + Int(truncating: (data["substitute_on"] as AnyObject) as! NSNumber)
            trackles = trackles + Int(truncating: (data["tackles"] as AnyObject) as! NSNumber)
            yellowCared = yellowCared + Int(truncating: (data["yellow_card"] as AnyObject) as! NSNumber)


        }
        
        item.appearances  =  "\(appearances)"
        item.assists  =  "\(assists)" //data.validatedValue("assists", expected: "" as AnyObject) as! String
        item.goal  = "\(goal)" //data.validatedValue("goals", expected: "" as AnyObject) as! String
        item.pass  =  "\(pass)" //data.validatedValue("pass", expected: "" as AnyObject) as! String
        item.redCard  = "\(redCard)" // data.validatedValue("red_card", expected: "" as AnyObject) as! String
        item.saved  =  "\(saved)" //data.validatedValue("saved", expected: "" as AnyObject) as! String
        item.shots  =   "\(shots)" //data.validatedValue("shots", expected: "" as AnyObject) as! String
     //   item.subsTitute  = "\(subsTitute)" // data.validatedValue("substitude", expected: "" as AnyObject) as! String
        item.subsTitute_on  =   "\(subsTitute_on)" //data.validatedValue("substitute_on", expected: "" as AnyObject) as! String
        item.trackles  =   "\(trackles)" //data.validatedValue("tackles", expected: "" as AnyObject) as! String
        item.yellowCared  =  "\(yellowCared)" //data.validatedValue("yellow_card", expected: "" as AnyObject) as! String
        
        return item
    }

    
    class func stats(data:Dictionary<String,AnyObject>) -> FKPlayerStatsInfo {
        
        let item = FKPlayerStatsInfo()
       
        item.id  =  data.validatedValue("id", expected: "" as AnyObject) as! String
        item.appearances  =  data.validatedValue("appearances", expected: "" as AnyObject) as! String
        item.assists  =  data.validatedValue("assists", expected: "" as AnyObject) as! String
        item.goal  =  data.validatedValue("goals", expected: "" as AnyObject) as! String
        item.pass  =  data.validatedValue("pass", expected: "" as AnyObject) as! String
        item.redCard  =  data.validatedValue("red_card", expected: "" as AnyObject) as! String
        item.saved  =  data.validatedValue("saved", expected: "" as AnyObject) as! String
        item.shots  =  data.validatedValue("shots", expected: "" as AnyObject) as! String
    //    item.subsTitute  =  data.validatedValue("substitude", expected: "" as AnyObject) as! String
        item.subsTitute_on  =  data.validatedValue("substitute_on", expected: "" as AnyObject) as! String
        item.trackles  =  data.validatedValue("tackles", expected: "" as AnyObject) as! String
        item.yellowCared  =  data.validatedValue("yellow_card", expected: "" as AnyObject) as! String

        
        return item
    }
    
    func getStatsInfo() -> Array<String>{
        var array = [String]()
        
        if !self.pass.isEmpty{
            array.append("\(self.pass) PASS")
        }
        
        if !self.trackles.isEmpty{
            array.append("\(self.trackles) TACKLE")
        }
        
        if !self.saved.isEmpty{
            array.append("\(self.saved) SAVE")
        }
        
        if !self.appearances.isEmpty{
            array.append("\(self.appearances) APPEAR")
        }
        if !self.assists.isEmpty{
            array.append("\(self.assists) ASSITS")
        }
        
        if !self.goal.isEmpty{
            array.append("\(self.goal) GOAL")
        }
        
        if !self.shots.isEmpty{
            array.append("\(self.shots) SHOT")
        }
        
        if !self.subsTitute.isEmpty{
            array.append("\(self.subsTitute) SUBSTITUTE")
        }
        if !self.subsTitute_on.isEmpty{
            array.append("\(self.subsTitute_on) SUBSTITUTE")
        }
        
        return array
    }
}
